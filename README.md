# Reference Implementations for Database Design patterns

This project contains implementations for several database design patterns with varying levels of abstraction.

## General Information
For every pattern, a SQL-Script for the creation of all necessary tables is supplied.
The used database is Postgre-SQL.
Used Frameworks are Spring Boot in combination with Hibernate.

## DB-Setup
The standard setup assumes a Postgre database, with a user named postgres. The password is postgres and the name of the database is referenceimpl. The port that is listened on is 5432.
The database setup can be changed in src/main/resources/application.properties.

## Structure
### Java Code
The java implementation can be found under src/main/java/at.fhv.rupp.ba.dbimpl.
In this package, the subpackages are the names of the patterns that were implemented for the thesis:
classification, recursive and status.
Inside the packages named after the patterns, there is further structuring into the different level of the patterns, e.g. levelone, leveltwo and so on.
These packages contain controller (the REST controller), dtos (the DTOs, which includes the mappers in its own subpackage), entities (the hibernate entities), exceptions(custom exceptions for the REST-controller) and repos(JPA-Repositories, offer CRUD-Services).
### SQL-scripts
The sql scripts for creating the databases can be found under src/main/resources/sql. 
### ER-diagrams
The ER-diagrams for the various patterns can be found under src/main/resources/erdiagrams. 

## Naming Conventions
  - HibernateEntities: `<Shorthand for pattern><level as written number><Name of Table>`, e.g. CLThreeEntity
  - DTO: `<Shorthand for pattern><level as written number><Name of Table>DTO`, e.g. CLThreeEntityDTO
  - mapper: `<Shorthand for pattern><level as written number><Name of Table>Mapper`, e.g. CLThreeEntityMapper
  - Repo: `<Shorthand for pattern><level as written number><Name of Table>Repo`, e.g. CLThreeEntityRepo
  - Exceptions: named after when they are thrown, for a not found exception, for example: `<Shorthand for pattern><level as written number><Name of Table>NotFoundException` (e.g. CLThreeEntityNotFoundException)
  - SQL-scrips: `<name of pattern>level<level of pattern>.sql`, e.g. classificationlevelthree.sql
  - ER-diagrams: `<name of pattern>level<level of pattern>.png`, e.g. classificationlevelthree.png

## Goals
### Main Goals
  - Creation of SQL-scripts for each level for each pattern
  - Translating diagrams into UML-notation
  - Implementation of selected database design patterns (classification, recursive, status)
  - Implementing complex relations (e.g. entity knows its types) and modifying the repository-classes accordingly
  - Show base implementation complexity (how hard is the implementation for the different levels of the patterns?)
  - Show difficulty of changes in data model (e.g. add new Type): simple insert per sql, changes in classes, how much needs to be changed, etc. 
  - Documentation of findings and problems/interesting things found along the way in bachelor thesis
### Side goals
  - Test different ways if mapping from entity to DTO (e.g. self-written, Mapstruct, other frameworks)
  - Test ways to fill database with generated data (e.g. FakerDB)
  - Documentation of findings regarding these points in bachelor thesis
  
## Usage
The current pattern allows the manipulation of the database over a Rest-Controller.
Since a graphical user interface would be overkill for this project, using an app like Postman is advised.

### REST-paths
This a list of all the paths that can be accessed via the REST-controller when the application is running.
The base path is http://localhost:8080//ba, followed by the information for pattern/level/what you want to do.
#### PUT and POST
Both PUT and POST Mappings rely on proper objects (e.g. in json format) being supplied.
This means that fields that contain another "object" (e.g. a type for an entity, where type has its own database table and object) have to contain a proper object, including id!
This would be an example object:
`{
         "id": 3554,
         "dateTime": "2039-04-26T01:53:20",
         "statusFrom": "2039-03-04",
         "statusThru": null,
         "from": "2038-07-12",
         "thru": null,
         "type": {
             "id": 3517,
             "name": "terry.biz"
         },
         "entityOne": null,
         "entityTwo": null,
         "entityThree": {
             "id": 3545,
             "attr": "biz"
         }
     }`
The first line, the id, should of course not be there when using POST (since you are creating a new object which does not have an id yet).
It is also not necessary when using PUT (since you pull the entity from the database by id anyway and changes to the id would not be propagated internally).
#### PathVariables
Path variables are the variables in {}.
They are to be replaced by what you need/want, e.g. instead of {id} you write 42 (with 42 being the id of the entity you want, for example)
The used variables are:
  - {id} -> an id, format is Integer (e.g. 42)
  - {name} -> name of an entity, format is String (e.g. Shipped)
  - {date} -> usually startDate and endDate, format is LocalDate (e.g. 2020-12-12)
  - {time} -> usually startTime and endTime, format is LocalDateTime (e.g. 2020-12-12T01:30:00.000-01:00/2021-12-12T01:30:00.000-01:00)

#### GET-Mappings
##### Core
  - http://localhost:8080//ba/core/RESET (resets all database-tables)
  - http://localhost:8080//ba/core/RESTART (restarts the application)
  - http://localhost:8080//ba/core/STOP (exits the application with code 0)
##### Classification
###### Level 1
  - http://localhost:8080//ba/classification/L1/entities (get all Entities)
  - http://localhost:8080//ba/classification/L1/entities/mapstruct (get all Entities, mapped with mapstruct)
  - http://localhost:8080//ba/classification/L1/entities/{id} (get one Entity by id)
  - http://localhost:8080//ba/classification/L1/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/classification/L1/RESET (resets database-tables for this pattern)
###### Level 2
  - http://localhost:8080//ba/classification/L2/entities (get all Entities)
  - http://localhost:8080//ba/classification/L2/mapstructEntities (get all Entities, mapped with mapstruct)
  - http://localhost:8080//ba/classification/L2/entitiesWTypes (gets all entities joined with their types)
  - http://localhost:8080//ba/classification/L2/entitiesWTypesNoView (gets all entities joined with their types without help of database view)
  - http://localhost:8080//ba/classification/L2/entitiesWTypesById/{id} (gets entities with joined types by entity id)
  - http://localhost:8080//ba/classification/L2/entityWTypesByTypeOneName/{name} (gets all entities with joined types by name of type one)
  - http://localhost:8080//ba/classification/L2/entity/{id} (gets Entity by Id)
  - http://localhost:8080//ba/classification/L2/typeOne/{id} (gets Type One by id)
  - http://localhost:8080//ba/classification/L2/typeTwo/{id} (gets Type Two by id)
  - http://localhost:8080//ba/classification/L2/typeThree/{id} (gets Type Three by id)
  - http://localhost:8080//ba/classification/L2/classificationTypeThree/{id} (gets classification-entity by id)
  - http://localhost:8080//ba/classification/L2/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/classification/L2/RESET (resets database-tables for this pattern)
###### Level 3
  - http://localhost:8080//ba/classification/L3/entities (get all Entities)
  - http://localhost:8080//ba/classification/L3/entity/{id} (gets Entity by Id)
  - http://localhost:8080//ba/classification/L3/category/{id} (gets Category by Id)
  - http://localhost:8080//ba/classification/L3/categoryType/{id} (gets Category Type by Id)
  - http://localhost:8080//ba/classification/L3/classification/{id} (gets Category Classification by Id)
  - http://localhost:8080//ba/classification/L3/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/classification/L3/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/classification/L3/entitiesWTypes (gets all entities joined with their types)
  - http://localhost:8080//ba/classification/L3/entitiesWTypes/entityId/{id} (gets all entities joined with their types by entity id)
  - http://localhost:8080//ba/classification/L3/entitiesWTypes/catName/{name} (gets all entities joined with their types by category name)
  - http://localhost:8080//ba/classification/L3/entitiesWTypes/{start}/{end} (gets all entities joined with their types with from date between start and end (both are dates))
  - http://localhost:8080//ba/classification/L3/entitiesWTypes/catTopParents (gets all entities joined with their types where category has no parent (meaning they are at the top of the hierarchy))
###### Level 3 With Rollups
  - http://localhost:8080//ba/classification/L3Rollups/entities (get all entities)
  - http://localhost:8080//ba/classification/L3Rollups/entities/{id} (gets Entity by Id)
  - http://localhost:8080//ba/classification/L3Rollups/dataProvider/{id} (gets Data Provider by Id)
  - http://localhost:8080//ba/classification/L3Rollups/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/classification/L3Rollups/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes (gets all entities joined with their types)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/catHierarchy/{id} (gets all entities joined with their types in hierarchy by category id) TODO check if works
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/catName/{name} (gets all entities joined with their types by category name)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/catTypeName/{name} (gets all entities joined with their types by category type name)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/dpName/{name} (gets all entities joined with their types by data provider name)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/schemeName/{name} (gets all entities joined with their types by category scheme name)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/rollupTypeName/{name} (gets all entities joined with their types by rollup type name)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/typeRollupTypeName/{name} (gets all entities joined with their types by type rollup type name)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/classificationFrom/{startDate}/{endDate} (gets all entities joined with their types where classification from date is between start date and end date)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/catRollupFrom/{startDate}/{endDate} (gets all entities joined with their types where category rollup from date is between start date and end date)
  - http://localhost:8080//ba/classification/L3Rollups/entitiesWTypes/catTypeRollupFrom/{startDate}/{endDate} (gets all entities joined with their types where category type rollup from date is between start date and end date)
##### Recursive
##### Level 1
  - http://localhost:8080//ba/recursion/L1/entities/one/{id} (gets entity one by id)
  - http://localhost:8080//ba/recursion/L1/entities/two/{id} (gets entity two by id)
  - http://localhost:8080//ba/recursion/L1/entities/three/{id} (gets entity three by id)
  - http://localhost:8080//ba/recursion/L1/entities/one/all (gets all entity ones)
  - http://localhost:8080//ba/recursion/L1/entities/two/all (gets all entity twos)
  - http://localhost:8080//ba/recursion/L1/entities/three/all (gets all entity threes)
  - http://localhost:8080//ba/recursion/L1/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/recursion/L1/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/recursion/L1/entities/allHierarchy (gets all entities joined by hierarchical info)  
  - http://localhost:8080//ba/recursion/L1/entities/allHierarchy/oneName/{name} (gets all entities joined by hierarchical info by name of entity one)  
  - http://localhost:8080//ba/recursion/L1/entities/allHierarchy/twoName/{name} (gets all entities joined by hierarchical info by name of entity two)  
  - http://localhost:8080//ba/recursion/L1/entities/allHierarchy/three/{name} (gets all entities joined by hierarchical info by name of entity three)  
##### Level 2
  - http://localhost:8080//ba/recursion/L2/entity/{id} (gets entity by id)
  - http://localhost:8080//ba/recursion/L2/entities/all (gets all entities)
  - http://localhost:8080//ba/recursion/L2/type/{id} (gets type by id)
  - http://localhost:8080//ba/recursion/L2/types/all (gets all types)
  - http://localhost:8080//ba/recursion/L2/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/recursion/L2/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/recursion/L2/entitiesWTypes (gets all entities joined with their entity types)  
  - http://localhost:8080//ba/recursion/L2/entitiesWTypes/typeName/{name} (gets all entities joined with their entity types by type name)  
  - http://localhost:8080//ba/recursion/L2/entitiesWTypes/entityId/{id} (gets all entities joined with their entity types by entity id)  
##### Level 2 Expanded
  - http://localhost:8080//ba/recursion/L2Exp/entity/{id} (gets entity by id)
  - http://localhost:8080//ba/recursion/L2Exp/entities/all (gets all entities)
  - http://localhost:8080//ba/recursion/L2Exp/type/{id} (gets type by id)
  - http://localhost:8080//ba/recursion/L2Exp/types/all (gets all types)
  - http://localhost:8080//ba/recursion/L2Exp/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/recursion/L2Exp/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/recursion/L2Exp/entitiesWAssociations (gets all entities joined with their associations and types)
  - http://localhost:8080//ba/recursion/L2Exp/entitiesWAssociations/entityId/{id} (gets all entities joined with their associations and types by entity id)
  - http://localhost:8080//ba/recursion/L2Exp/entitiesWAssociations/assocOneFrom/{id} (gets all entities joined with their associations and types by association one from entity id)
  - http://localhost:8080//ba/recursion/L2Exp/entitiesWAssociations/assocOneTo/{id} (gets all entities joined with their associations and types by association one to entity id)
  - http://localhost:8080//ba/recursion/L2Exp/entitiesWAssociations/assocTwoFrom/{id} (gets all entities joined with their associations and types by association two from entity id)
  - http://localhost:8080//ba/recursion/L2Exp/entitiesWAssociations/assocTwoTo/{id} (gets all entities joined with their associations and types by association two to entity id)
  - http://localhost:8080//ba/recursion/L2Exp/entitiesWAssociations/entityInfoOnly (gets all entities joined with their associations and types, only displays info relevant for entity)
##### Level 3
  - http://localhost:8080//ba/recursion/L3/entity/{id} (gets entity by id)
  - http://localhost:8080//ba/recursion/L3/entities/all (gets all entities)
  - http://localhost:8080//ba/recursion/L3/type/{id} (gets type by id)
  - http://localhost:8080//ba/recursion/L3/types/all (gets all types)
  - http://localhost:8080//ba/recursion/L3/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/recursion/L3/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/recursion/L3/entitiesWAssoc (gets all entities joined with their associations and types)
  - http://localhost:8080//ba/recursion/L3/entitiesWAssoc/entityId/{id} (gets all entities joined with their associations and types by entity id)
  - http://localhost:8080//ba/recursion/L3/entitiesWAssoc/thru (gets all entities joined with their associations and types where thru date is not null)
  - http://localhost:8080//ba/recursion/L3/entitiesWAssoc/notThru (gets all entities joined with their associations and types where thru date is null)
  - http://localhost:8080//ba/recursion/L3/entitiesWAssoc/typeName/{name} (gets all entities joined with their associations and types by type name)
  - http://localhost:8080//ba/recursion/L3/entitiesWAssoc/{startDate}/{endDate} (gets all entities joined with their associations and types where the associations from date is between start date and end date)
##### Level 3 with Rules
  - http://localhost:8080//ba/recursion/L3WRules/entity/{id} (gets entity by id)
  - http://localhost:8080//ba/recursion/L3WRules/entities/all (gets all entities)
  - http://localhost:8080//ba/recursion/L3WRules/type/{id} (gets type by id)
  - http://localhost:8080//ba/recursion/L3WRules/types/all (gets all types)
  - http://localhost:8080//ba/recursion/L3WRules/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/recursion/L3WRules/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/recursion/L3WRules/entitiesWAssoc (gets all entities joined with their associations and types)
  - http://localhost:8080//ba/recursion/L3WRules/entitiesWAssoc/entityId/{id} (gets all entities joined with their associations and types by entity id)
  - http://localhost:8080//ba/recursion/L3WRules/entitiesWAssoc/thru (gets all entities joined with their associations and types where thru date is not null)
  - http://localhost:8080//ba/recursion/L3WRules/entitiesWAssoc/notThru (gets all entities joined with their associations and types where thru date is null)
  - http://localhost:8080//ba/recursion/L3WRules/entitiesWAssoc/typeName/{name} (gets all entities joined with their associations and types by type name)
  - http://localhost:8080//ba/recursion/L3WRules/entitiesWAssoc/{startDate}/{endDate} (gets all entities joined with their associations and types where the associations from date is between start date and end date)
  - http://localhost:8080//ba/recursion/L3WRules/entitiesWAssoc/ruleName/{name} (gets all entities joined with their associations and types by association rule name)
#### Status
##### Level 1
  - http://localhost:8080//ba/status/L1/entities (get all Entities)
  - http://localhost:8080//ba/status/L1/entities/{id} (gets Entity by Id)
  - http://localhost:8080//ba/status/L1/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/status/L1/RESET (resets database-tables for this pattern)
##### Level 2
  - http://localhost:8080//ba/status/L2/entities (get all Entities)
  - http://localhost:8080//ba/status/L2/entities/{id} (gets Entity by Id)
  - http://localhost:8080//ba/status/L2/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/status/L2/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/status/L2/statusTypes (get all Status Types)
  - http://localhost:8080//ba/status/L2/statusTypes/{id} (gets Status Types by Id)
  - http://localhost:8080//ba/status/L2/entitiesWStatus (gets entities joined with status)
  - http://localhost:8080//ba/status/L2/entitiesWStatus/entityId/{id} (gets entities joined with status by entity id)
  - http://localhost:8080//ba/status/L2/entitiesWStatus/statusTypeName/{name} (gets entities joined with status by status type name)
  - http://localhost:8080//ba/status/L2/entitiesWStatus/{startTime}/{endTime} (gets entities joined with status where status datetime between start time and end time)
##### Level 3
  - http://localhost:8080//ba/status/L3/entities (get all Entities)
  - http://localhost:8080//ba/status/L3/entities/{id} (gets Entity by Id)
  - http://localhost:8080//ba/status/L3/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/status/L3/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/status/L3/entityStatus (get all Entity Status)
  - http://localhost:8080//ba/status/L3/entityStatus/{id} (gets Entity Status by Id)
  - http://localhost:8080//ba/status/L3/entityWStatus (gets entities joined with status)
  - http://localhost:8080//ba/status/L3/entityWStatus/entityId/{id} (gets entities joined with status by entity id)
  - http://localhost:8080//ba/status/L3/entityWStatus/entityNotThru (gets entities joined with status where entity thru date is null)
  - http://localhost:8080//ba/status/L3/entityWStatus/entityStatusNotThru (gets entities joined with status where entity status thru date is null)
  - http://localhost:8080//ba/status/L3/entityWStatus/statusType/{id} (gets entities joined with status by status type id)
  - http://localhost:8080//ba/status/L3/entityWStatus/statusTypeName/{name} (gets entities joined with status by status type name)
  - http://localhost:8080//ba/status/L3/entityWStatus/entityFromDate/{startDate}/{endDate} (gets entities joined with status where entity from date is between start date and end date)
  - http://localhost:8080//ba/status/L3/entityWStatus/entityStatusFromDate/{startDate}/{endDate} (gets entities joined with status where entity status from date is between start date and end date)
  - http://localhost:8080//ba/status/L3/entityWStatus/statusDatetime/{startTime}/{endTime} (gets entities joined with status where status datetime is between start time and end time)
##### Level 4
  - http://localhost:8080//ba/status/L4/statusApplications (get all Status Applications)                
  - http://localhost:8080//ba/status/L4/statusApplications/{id} (get all Status Applications by Status Application id)
  - http://localhost:8080//ba/status/L4/datagen (triggers generation of test-data for this pattern)
  - http://localhost:8080//ba/status/L4/RESET (resets database-tables for this pattern)
  - http://localhost:8080//ba/status/L4/entitiesWStatus (gets entities joined with status)
  - http://localhost:8080//ba/status/L4/entitiesWStatus/entityOne (gets entities joined with status for all status where entity one is not null)
  - http://localhost:8080//ba/status/L4/entitiesWStatus/entityTwo (gets entities joined with status for all status where entity two is not null)
  - http://localhost:8080//ba/status/L4/entitiesWStatus/entityThree (gets entities joined with status for all status where entity three is not null)
  - http://localhost:8080//ba/status/L4/entitiesWStatus/noEntityOne (gets entities joined with status for all status where entity one is null)
  - http://localhost:8080//ba/status/L4/entitiesWStatus/noEntityTwo (gets entities joined with status for all status where entity two is null)
  - http://localhost:8080//ba/status/L4/entitiesWStatus/noEntityThree (gets entities joined with status for all status where entity three is null)
  - http://localhost:8080//ba/status/L4/entitiesWStatus/datetime/{startTime}/{endTime} (gets entities joined with status where status datetime is between start time and end time)
                                                                                                                                                    
#### POST-Mappings
##### Classification
###### Level 1
  - http://localhost:8080//ba/classification/L1/entities (creates a new Entity from the body of the request and saves it in db)
###### Level 2
  - http://localhost:8080//ba/classification/L2/entities/subtypeOne (creates a new Subtype One of Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/classification/L2/entities/subtypeTwo (creates a new Subtype Two of Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/classification/L2/entities/subtypeThree (creates a new Subtype Three of Entity from the body of the request and saves it in db)
###### Level 3
  - http://localhost:8080//ba/classification/L3/entities (creates a new Entity from the body of the request and saves it in db)
###### Level 3 With Rollups
  - http://localhost:8080//ba/classification/L3Rollups/entities (creates a new Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/classification/L3Rollups/dataProvider (creates a new Data Provider from the body of the request and saves it in db)
##### Recursive
##### Level 1
  - http://localhost:8080//ba/recursion/L1/entities/one (creates a new Entity One from the body of the request and saves it in db)
  - http://localhost:8080//ba/recursion/L1/entities/two (creates a new Entity Two from the body of the request and saves it in db)
  - http://localhost:8080//ba/recursion/L1/entities/three (creates a new Entity Three from the body of the request and saves it in db)
###### Level 2
  - http://localhost:8080//ba/recursion/L2/entities (creates a new Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/recursion/L2/types (creates a new Type from the body of the request and saves it in db)
##### Level 2 Expanded
  - http://localhost:8080//ba/recursion/L2Exp/entities (creates a new Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/recursion/L2Exp/types (creates a new Type from the body of the request and saves it in db)
##### Level 3
  - http://localhost:8080//ba/recursion/L3/entities (creates a new Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/recursion/L3/types (creates a new Type from the body of the request and saves it in db)
##### Level 3 with Rules
  - http://localhost:8080//ba/recursion/L3WRules/entities (creates a new Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/recursion/L3WRules/types (creates a new Type from the body of the request and saves it in db)
#### Status
##### Level 1
  - http://localhost:8080//ba/status/L1/entities (creates a new Entity from the body of the request and saves it in db)
##### Level 2
  - http://localhost:8080//ba/status/L2/entities (creates a new Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/status/L2/statusTypes (creates a new Status Type from the body of the request and saves it in db)
##### Level 3
  - http://localhost:8080//ba/status/L3/entities (creates a new Entity from the body of the request and saves it in db)
  - http://localhost:8080//ba/status/L3/entityStatus (creates a new Entity Status from the body of the request and saves it in db
##### Level 4
  - http://localhost:8080//ba/status/L4/statusApplications (creates a new Status Application from the body of the request and saves it in db)
  
#### PUT-Mappings
##### Classification
###### Level 1
  - http://localhost:8080//ba/classification/L1/entities/{id} (updates entity with id with information in request-body)
###### Level 2
  - http://localhost:8080//ba/classification/L2/entities/subtypeOne/{id} (updates Entity of Subtype One with id with information in request-body)
  - http://localhost:8080//ba/classification/L2/entities/subtypeTwo/{id} (updates Entity of Subtype Two with id with information in request-body)
  - http://localhost:8080//ba/classification/L2/entities/subtypeThree/{id} (updates Entity of Subtype Three with id with information in request-body)
###### Level 3
  - http://localhost:8080//ba/classification/L3/entities/{id} (updates Entity with id with information in request-body)
###### Level 3 With Rollups
  - http://localhost:8080//ba/classification/L3Rollups/entities/{id} (updates Entity with id with information in request-body)
##### Recursive
##### Level 1
  - http://localhost:8080//ba/recursion/L1/entities/two/{id} (updates entity two with id with information in request-body)
###### Level 2
  - http://localhost:8080//ba/recursion/L2/entities/{id} (updates entity with id with information in request-body)
  - http://localhost:8080//ba/recursion/L2/types/{id} (updates type with id with information in request-body)
##### Level 2 Expanded
  - http://localhost:8080//ba/recursion/L2Exp/entities/{id} (updates entity with id with information in request-body)
  - http://localhost:8080//ba/recursion/L2Exp/types/{id} (updates type with id with information in request-body)
##### Level 3
  - http://localhost:8080//ba/recursion/L3/entities/{id} (updates entity with id with information in request-body)
  - http://localhost:8080//ba/recursion/L3/types/{id} (updates type with id with information in request-body)
##### Level 3 with Rules
  - http://localhost:8080//ba/recursion/L3WRules/entities/{id} (updates entity with id with information in request-body)
  - http://localhost:8080//ba/recursion/L3WRules/types/{id} (updates type with id with information in request-body)
#### Status
##### Level 1
  - http://localhost:8080//ba/status/L1/entities/{id} (updates entity with id with information in request-body)
##### Level 2
  - http://localhost:8080//ba/status/L2/entities/{id} (updates entity with id with information in request-body)
  - http://localhost:8080//ba/status/L2/statusTypes/{id} (updates status type with id with information in request-body)
##### Level 3
  - http://localhost:8080//ba/status/L3/entities/{id} (updates entity with id with information in request-body)
  - http://localhost:8080//ba/status/L3/entityStatus/{id} (updates entity status with id with information in request-body)
##### Level 4
  - http://localhost:8080//ba/status/L4/statusApplications/{id} (updates status application with id with information in request-body)
  
#### DELETE-Mappings
##### Classification
###### Level 1
  - http://localhost:8080//ba/classification/L1/entities/{id} (deletes entity with id from database)
###### Level 2
  - http://localhost:8080//ba/classification/L2/entities/{id} (deletes entity with id (as well as entries with said entity in classification table) from database)
###### Level 3
  - http://localhost:8080//ba/classification/L3/entities/{id} (deletes entity with id (as well as entries with said entity in classification table) from database)
###### Level 3 With Rollups
  - http://localhost:8080//ba/classification/L3Rollups/entities/{id} (deletes entity with id (as well as entries with said entity in classification table) from database)
##### Recursive
##### Level 1
  - http://localhost:8080//ba/recursion/L1/entities/two/{id} (deletes entity two with id from database, sets fields that reference entity two in entity three to null)
###### Level 2
  - http://localhost:8080//ba/recursion/L2/entities/{id} (deletes entity with id from database, entities where it is parent have that field set to null)
  - http://localhost:8080//ba/recursion/L2/types/{id} (deletes type with id from database, entities where it is parent have that field set to null)
##### Level 2 Expanded
  - http://localhost:8080//ba/recursion/L2Exp/entities/{id} (deletes entity with id from database, entities where it is associated to or from have that field set to null, associations (one and two) that contain entity are deleted as well)
  - http://localhost:8080//ba/recursion/L2Exp/types/{id} (deletes type with id from database, entities where it is type have that field set to null, types where it is parent have field set to null as well)
##### Level 3
  - http://localhost:8080//ba/recursion/L3/entities/{id} (deletes entity with id from database, associations that contain the entity are deleted as well)
  - http://localhost:8080//ba/recursion/L3/types/{id} (deletes type with id from database, entities where it is type have that field set to null, types where it is parent have field set to null as well)
##### Level 3 with Rules
  - http://localhost:8080//ba/recursion/L3WRules/entities/{id} (deletes entity with id from database, associations that contain the entity are deleted as well)
  - http://localhost:8080//ba/recursion/L3WRules/types/{id} (deletes type with id from database, entities where it is type have that field set to null, types where it is parent have field set to null as well)
#### Status
##### Level 1
  - http://localhost:8080//ba/status/L1/entities/{id} (deletes entity with id from database)
##### Level 2
  - http://localhost:8080//ba/status/L2/entities/{id} (deletes entity with id from database)
  - http://localhost:8080//ba/status/L2/statusTypes/{id} (deletes status type with id from database)
##### Level 3
  - http://localhost:8080//ba/status/L3/entities/{id} (deletes entity with id from database)
  - http://localhost:8080//ba/status/L3/entityStatus/{id} (deletes entity status with id from database)
##### Level 4
  - http://localhost:8080//ba/status/L4/statusApplications/{id} (deletes status application with id from database)
  
## Current Status
Note: will only be marked as finished after being added to git.
### Classification
#### Level One
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos (Not applicable -> no relationships)
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Two
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Three
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [ ] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Three with Rollups/schemes
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 

### Recursive
#### Level One
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Two
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Two Expanded
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Three
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Three with Rules
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 

### Status
#### Level One
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos (Not applicable -> no relationships)
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Two (Current Status)
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Three
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 
#### Level Four
  - [x] SQL-script
  - [x] ER-diagram
  - [x] Base Impl
  - [x] More complex repos
  - [ ] Changes
  - [x] Different Mappings
  - [x] Generate Data 