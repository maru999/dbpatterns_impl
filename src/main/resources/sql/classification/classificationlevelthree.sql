DROP VIEW IF EXISTS clthree_entity_types;
DROP TABLE IF EXISTS clthree_entity_category_classification;
DROP TABLE IF EXISTS clthree_entity_category;
DROP TABLE IF EXISTS clthree_entity_category_type;
DROP TABLE IF EXISTS clthree_entity;
-- classification level three


--level 0

CREATE TABLE clthree_entity
(
    clthree_entity_id   serial PRIMARY KEY,
    clthree_entity_name varchar(100) NOT NULL
);

-- level 1

CREATE TABLE clthree_entity_category_type
(
    clthree_entity_category_type_id     serial PRIMARY KEY,
    clthree_entity_category_type_name   varchar(100) NOT NULL,
    clthree_parent_entity_category_type int,
    CONSTRAINT FK_clthreeEntityCategoryTypeParent FOREIGN KEY (clthree_parent_entity_category_type) REFERENCES clthree_entity_category_type (clthree_entity_category_type_id)
);

-- level 2

CREATE TABLE clthree_entity_category
(
    clthree_entity_category_id              serial PRIMARY KEY,
    clthree_entity_category_name            varchar(100) NOT NULL,
    clthree_entity_category_from            date         NOT NULL,
    clthree_entity_category_thru            date,
    clthree_entity_category_parent_category int,
    clthree_entity_category_category_type   int,
    CONSTRAINT FK_clthreeEntityCategoryParentCategory FOREIGN KEY (clthree_entity_category_parent_category) REFERENCES clthree_entity_category (clthree_entity_category_id),
    CONSTRAINT FK_clthreeEntityCategoryCategoryType FOREIGN KEY (clthree_entity_category_category_type) REFERENCES clthree_entity_category_type (clthree_entity_category_type_id)
);

CREATE TABLE clthree_entity_category_classification
(
    clthree_entity_category_classification_id              serial PRIMARY KEY,
    clthree_entity_category_classification_from            date NOT NULL,
    clthree_entity_category_classification_thru            date,
    clthree_entity_category_classification_entity          int,
    clthree_entity_category_classification_entity_category int,
    CONSTRAINT FK_clthreeEntityCategoryClassificationEntity FOREIGN KEY (clthree_entity_category_classification_entity) REFERENCES clthree_entity (clthree_entity_id),
    CONSTRAINT FK_clthreeEntityCategoryClassificationEntityCategory FOREIGN KEY (clthree_entity_category_classification_entity_category) REFERENCES clthree_entity_category (clthree_entity_category_id)
);

CREATE VIEW clthree_entity_types AS
SELECT *
FROM clthree_entity,
     clthree_entity_category_type,
     clthree_entity_category,
     clthree_entity_category_classification
WHERE clthree_entity_category_category_type = clthree_entity_category_type_id
  AND clthree_entity_category_classification_entity = clthree_entity_id
  AND clthree_entity_category_classification_entity_category = clthree_entity_category_id;