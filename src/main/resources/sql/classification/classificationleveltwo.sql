DROP VIEW IF EXISTS entTypes;
DROP TABLE IF EXISTS cltwo_entity_subtype_three;
DROP TABLE IF EXISTS cltwo_entity_subtype_two;
DROP TABLE IF EXISTS cltwo_entity_subtype_one;
DROP TABLE IF EXISTS cltwo_entity_type_three_class;
DROP TABLE IF EXISTS cltwo_entity;
DROP TABLE IF EXISTS cltwo_entity_type_two;
DROP TABLE IF EXISTS cltwo_entity_type_three;
DROP TABLE IF EXISTS cltwo_entity_type_one;


-- classification_ltwo

-- Level 0

CREATE TABLE cltwo_entity_type_one
(
    cltwo_entity_type_one_id   serial PRIMARY KEY,
    cltwo_entity_type_one_name varchar(100) NOT NULL
);

CREATE TABLE cltwo_entity_type_three
(
    cltwo_entity_type_three_id   serial PRIMARY KEY,
    cltwo_entity_type_three_name varchar(100) NOT NULL
);

--Level 1

CREATE TABLE cltwo_entity_type_two
(
    cltwo_entity_type_two_id     serial PRIMARY KEY,
    cltwo_entity_type_two_name   varchar(100) NOT NULL,
    cltwo_entity_type_two_parent int,
    CONSTRAINT FK_EntityTypeTwoParent FOREIGN KEY (cltwo_entity_type_two_parent) REFERENCES cltwo_entity_type_two (cltwo_entity_type_two_id)
);

--Level 2
CREATE TABLE cltwo_entity
(
    cltwo_entity_id          serial PRIMARY KEY,
    cltwo_entity_name        varchar(100) NOT NULL,
    cltwo_entity_type_one_fk int,
    cltwo_entity_type_two_fk int,
    CONSTRAINT FK_EntityTypeOne FOREIGN KEY (cltwo_entity_type_one_fk)
    REFERENCES cltwo_entity_type_one (cltwo_entity_type_one_id),
    CONSTRAINT FK_EntityTypeTwo FOREIGN KEY (cltwo_entity_type_two_fk)
    REFERENCES cltwo_entity_type_two (cltwo_entity_type_two_id)
);

CREATE TABLE cltwo_entity_subtype_one
(
    cltwo_entity_subtype_one_id   serial PRIMARY KEY,
    cltwo_entity_subtype_one_desc varchar(100),
    CONSTRAINT FK_clTwoEntitySubOnePK FOREIGN KEY (cltwo_entity_subtype_one_id) REFERENCES cltwo_entity (cltwo_entity_id)
);

CREATE TABLE cltwo_entity_subtype_two
(
    cltwo_entity_subtype_two_id   serial PRIMARY KEY,
    cltwo_entity_subtype_two_attr varchar(100),
    CONSTRAINT FK_clTwoEntitySubTwoPK FOREIGN KEY (cltwo_entity_subtype_two_id) REFERENCES cltwo_entity (cltwo_entity_id)
);

CREATE TABLE cltwo_entity_subtype_three
(
    cltwo_entity_subtype_three_id   serial PRIMARY KEY,
    cltwo_entity_subtype_three_info varchar(100),
    CONSTRAINT FK_clTwoEntitySubThreePK FOREIGN KEY (cltwo_entity_subtype_three_id) REFERENCES cltwo_entity (cltwo_entity_id)
);

CREATE TABLE cltwo_entity_type_three_class
(
    cltwo_entity_type_three_class_id      serial PRIMARY KEY,
    cltwo_entity_type_three_class_entid   int,
    cltwo_entity_type_three_class_threeid int,
    CONSTRAINT FK_EntityClassification FOREIGN KEY (cltwo_entity_type_three_class_entid) REFERENCES cltwo_entity (cltwo_entity_id),
    CONSTRAINT FK_TypeClassification FOREIGN KEY (cltwo_entity_type_three_class_threeid) REFERENCES cltwo_entity_type_three (cltwo_entity_type_three_id)
);

CREATE VIEW entTypes AS
SELECT cltwo_entity_id,
       cltwo_entity_name,
       cltwo_entity_type_one_name,
       cltwo_entity_type_two_name,
       cltwo_entity_type_three_name
FROM cltwo_entity,
     cltwo_entity_type_three_class,
     cltwo_entity_type_three,
     cltwo_entity_type_one,
     cltwo_entity_type_two
WHERE cltwo_entity_id = cltwo_entity_type_three_class_entid
  AND cltwo_entity_type_three_id = cltwo_entity_type_three_class_threeid
  AND cltwo_entity_type_two_id = cltwo_entity_type_two_fk
  AND cltwo_entity_type_one_id = cltwo_entity_type_one_fk;