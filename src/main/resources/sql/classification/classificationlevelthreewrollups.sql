DROP VIEW IF EXISTS clthreewr_ent_types_view;
DROP TABLE IF EXISTS clthreewr_entity_category_type_rollup;
DROP TABLE IF EXISTS clthreewr_entity_category_rollup;
DROP TABLE IF EXISTS clthreewr_entity_category_classification;
DROP TABLE IF EXISTS clthreewr_entity_category;
DROP TABLE IF EXISTS clthreewr_entity_category_type;
DROP TABLE IF EXISTS clthreewr_entity_category_type_scheme;
DROP TABLE IF EXISTS clthreewr_entity_category_type_rollup_type;
DROP TABLE IF EXISTS clthreewr_entity_category_rollup_type;
DROP TABLE IF EXISTS clthreewr_data_provider;
DROP TABLE IF EXISTS clthreewr_party_role;
DROP TABLE IF EXISTS clthreewr_entity;

-- level 0

CREATE TABLE clthreewr_entity
(
    clthreewr_entity_id   serial PRIMARY KEY,
    clthreewr_entity_name varchar(100) NOT NULL
);

CREATE TABLE clthreewr_party_role
(
    clthreewr_party_role_id serial PRIMARY KEY,
    role_name               varchar(100)
);


-- level 1
CREATE TABLE clthreewr_data_provider
(
    data_provider_id int PRIMARY KEY,
    dp_name          varchar(100),
    CONSTRAINT FK_partyRoleDataProvider FOREIGN KEY (data_provider_id) REFERENCES clthreewr_party_role (clthreewr_party_role_id)
);

CREATE TABLE clthreewr_entity_category_rollup_type
(
    clthreewr_entity_category_rollup_type_id        serial PRIMARY KEY,
    clthreewr_entity_category_rollup_type_name      varchar(100),
    clthreewr_entity_category_rollup_type_parent_id int,
    CONSTRAINT FK_clthreewrParentEntityCategoryRollupType FOREIGN KEY (clthreewr_entity_category_rollup_type_parent_id) REFERENCES clthreewr_entity_category_rollup_type (clthreewr_entity_category_rollup_type_id)
);

CREATE TABLE clthreewr_entity_category_type_rollup_type
(
    clthreewr_entity_category_type_rollup_type_id    serial PRIMARY KEY,
    clthreewr_entity_category_type_rollup_type_name  varchar(100),
    clthreewr_entity_category_trollup_type_parent_id int,
    CONSTRAINT FK_clthreewrParentEntityCategoryTypeRollupType FOREIGN KEY (clthreewr_entity_category_trollup_type_parent_id) REFERENCES clthreewr_entity_category_type_rollup_type (clthreewr_entity_category_type_rollup_type_id)
);

CREATE TABLE clthreewr_entity_category_type_scheme
(
    clthreewr_entity_category_type_scheme_id    serial PRIMARY KEY,
    clthreewr_entity_category_type_scheme_name  varchar(100) NOT NULL,
    clthreewr_entity_category_type_scheme_dp_id int,
    CONSTRAINT FK_clthreewrEntityCategoryTypeSchemeDataProvider FOREIGN KEY (clthreewr_entity_category_type_scheme_dp_id) REFERENCES clthreewr_data_provider (data_provider_id)
);

CREATE TABLE clthreewr_entity_category_type
(
    clthreewr_entity_category_type_id                             serial PRIMARY KEY,
    clthreewr_entity_category_type_name                           varchar(100) NOT NULL,
    clthreewr_entity_category_type_entity_category_type_scheme_id int,
    CONSTRAINT FK_clthreewrEntityCategoryTypeEntityCategoryTypeScheme FOREIGN KEY (clthreewr_entity_category_type_entity_category_type_scheme_id) REFERENCES clthreewr_entity_category_type_scheme (clthreewr_entity_category_type_scheme_id)
);

CREATE TABLE clthreewr_entity_category
(
    clthreewr_entity_category_id                      serial PRIMARY KEY,
    clthreewr_entity_category_name                    varchar(100) NOT NULL,
    clthreewr_entity_category_entity_category_type_id int,
    CONSTRAINT FK_clthreewrEntityCategoryEntityCategoryType FOREIGN KEY (clthreewr_entity_category_entity_category_type_id) REFERENCES clthreewr_entity_category_type (clthreewr_entity_category_type_id)
);

-- level 2

CREATE TABLE clthreewr_entity_category_classification
(
    clthreewr_entity_category_classification_id                 serial PRIMARY KEY,
    clthreewr_entity_category_classification_from               date NOT NULL,
    clthreewr_entity_category_classification_thru               date,
    clthreewr_entity_category_classification_entity_id          int,
    clthreewr_entity_category_classification_entity_category_id int,
    CONSTRAINT FK_clthreewrEntityCategoryClassificationEntity FOREIGN KEY (clthreewr_entity_category_classification_entity_id) REFERENCES clthreewr_entity (clthreewr_entity_id),
    CONSTRAINT FK_clthreewrEntityCategoryClassificationEntityCategory FOREIGN KEY (clthreewr_entity_category_classification_entity_category_id) REFERENCES clthreewr_entity_category (clthreewr_entity_category_id)
);

-- level 3

CREATE TABLE clthreewr_entity_category_rollup
(
    clthreewr_entity_category_rollup_id                             serial PRIMARY KEY,
    clthreewr_entity_category_rollup_from                           date NOT NULL,
    clthreewr_entity_category_rollup_thru                           date,
    clthreewr_entity_category_rollup_parent_entity_category_id      int,
    clthreewr_entity_category_rollup_child_entity_category_id       int,
    clthreewr_entity_category_rollup_entity_category_rollup_type_id int,
    CONSTRAINT FK_clthreewrEntityCategoryRollupParentEntityCategory FOREIGN KEY (clthreewr_entity_category_rollup_parent_entity_category_id) REFERENCES clthreewr_entity_category (clthreewr_entity_category_id),
    CONSTRAINT FK_clthreewrEntityCategoryRollupChildEntityCategory FOREIGN KEY (clthreewr_entity_category_rollup_child_entity_category_id) REFERENCES clthreewr_entity_category (clthreewr_entity_category_id),
    CONSTRAINT FK_clthreewrEntityCategoryRollupEntityCategoryRollupType FOREIGN KEY (clthreewr_entity_category_rollup_entity_category_rollup_type_id) REFERENCES clthreewr_entity_category_rollup_type (clthreewr_entity_category_rollup_type_id)
);

CREATE TABLE clthreewr_entity_category_type_rollup
(
    clthreewr_entity_category_type_rollup_id                        serial PRIMARY KEY,
    clthreewr_entity_category_type_rollup_from                      date NOT NULL,
    clthreewr_entity_category_type_rollup_thru                      date,
    clthreewr_entity_category_type_rollup_parent_entity_category_id int,
    clthreewr_entity_category_type_rollup_child_entity_category_id  int,
    clthreewr_entity_category_typer_entity_category_type_rtype_id   int,
    CONSTRAINT FK_clthreewrEntityCategoryTypeRollupParentEntityCategory FOREIGN KEY (clthreewr_entity_category_type_rollup_parent_entity_category_id) REFERENCES clthreewr_entity_category_type (clthreewr_entity_category_type_id),
    CONSTRAINT FK_clthreewrEntityCategoryTypeRollupChildEntityCategory FOREIGN KEY (clthreewr_entity_category_type_rollup_child_entity_category_id) REFERENCES clthreewr_entity_category_type (clthreewr_entity_category_type_id),
    CONSTRAINT FK_clthreewrEntityCategoryTypeREntityCategoryTypeRType FOREIGN KEY (clthreewr_entity_category_typer_entity_category_type_rtype_id) REFERENCES clthreewr_entity_category_type_rollup_type (clthreewr_entity_category_type_rollup_type_id)
);

CREATE VIEW clthreewr_ent_types_view AS
SELECT *
FROM clthreewr_data_provider,
     clthreewr_entity_category_type_scheme,
     clthreewr_entity_category_type,
     clthreewr_entity,
     clthreewr_entity_category_rollup_type,
     clthreewr_entity_category_type_rollup_type,
     clthreewr_entity_category,
     clthreewr_entity_category_classification,
     clthreewr_entity_category_rollup,
     clthreewr_entity_category_type_rollup
WHERE clthreewr_entity_category_classification_entity_category_id = clthreewr_entity_category_id
  AND clthreewr_entity_category_classification_entity_id = clthreewr_entity_id
  AND clthreewr_entity_category_entity_category_type_id = clthreewr_entity_category_type_id
  AND clthreewr_entity_category_type_entity_category_type_scheme_id = clthreewr_entity_category_type_scheme_id
  AND data_provider_id = clthreewr_entity_category_type_scheme_dp_id
  AND ((clthreewr_entity_category_rollup_child_entity_category_id = clthreewr_entity_category_id) OR
       (clthreewr_entity_category_rollup_parent_entity_category_id = clthreewr_entity_category_id))
  AND ((clthreewr_entity_category_type_rollup_parent_entity_category_id = clthreewr_entity_category_type_id) OR
       (clthreewr_entity_category_type_rollup_child_entity_category_id = clthreewr_entity_category_type_id))
  AND clthreewr_entity_category_rollup_entity_category_rollup_type_id = clthreewr_entity_category_rollup_type_id
  AND clthreewr_entity_category_typer_entity_category_type_rtype_id = clthreewr_entity_category_type_rollup_type_id;


