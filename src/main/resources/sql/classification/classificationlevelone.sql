DROP TABLE IF EXISTS clone_entity;

CREATE TABLE clone_entity
(
    clone_entity_id         serial PRIMARY KEY,
    clone_entity_name       varchar(100) NOT NULL,
    clone_entity_type_one   varchar(50)  NOT NULL,
    clone_entity_type_two   varchar(50),
    clone_entity_type_three varchar(100) NOT NULL
);

