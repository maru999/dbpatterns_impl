DROP VIEW IF EXISTS rltwo_entities_with_type;
DROP TABLE IF EXISTS rltwo_entity;
DROP TABLE IF EXISTS rltwo_entity_type;

-- level 1

CREATE TABLE rltwo_entity_type
(
    rltwo_entity_type_id     serial PRIMARY KEY,
    rltwo_entity_type_name   varchar(100),
    rltwo_entity_type_parent int,
    CONSTRAINT FK_rlTwoEntityTypeParent FOREIGN KEY (rltwo_entity_type_parent) REFERENCES rltwo_entity_type (rltwo_entity_type_id)
);

-- level 2

CREATE TABLE rltwo_entity
(
    rltwo_entity_id      serial PRIMARY KEY,
    rltwo_entity_parent  int,
    rltwo_entity_type_fk int,
    CONSTRAINT FK_rlTwoEntityParent FOREIGN KEY (rltwo_entity_parent) REFERENCES rltwo_entity (rltwo_entity_id),
    CONSTRAINT FK_rlTwoEntityType FOREIGN KEY (rltwo_entity_type_fk) REFERENCES rltwo_entity_type (rltwo_entity_type_id)

);

CREATE VIEW rltwo_entities_with_type AS
SELECT rltwo_entity_type_name, rltwo_entity_id
FROM rltwo_entity_type,
     rltwo_entity
WHERE rltwo_entity_type_id = rltwo_entity_type_fk;