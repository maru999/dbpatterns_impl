DROP VIEW IF EXISTS rltwoxp_associated_entities;
DROP TABLE IF EXISTS rltwoxp_entity_assoc_two;
DROP TABLE IF EXISTS rltwoxp_entity_assoc_one;
DROP TABLE IF EXISTS rltwoxp_entity;
DROP TABLE IF EXISTS rltwoxp_entity_assoc_type;
DROP TABLE IF EXISTS rltwoxp_entity_type;

CREATE TABLE rltwoxp_entity_type
(
    rltwoxp_entity_type_id     serial PRIMARY KEY,
    rltwoxp_entity_type_name   varchar(100) NOT NULL,
    rltwoxp_entity_type_parent int,
    CONSTRAINT FK_rlTwoExpTypeParent FOREIGN KEY (rltwoxp_entity_type_parent) REFERENCES rltwoxp_entity_type (rltwoxp_entity_type_id)
);

CREATE TABLE rltwoxp_entity_assoc_type
(
    rltwoxp_entity_assoc_type_id   serial PRIMARY KEY,
    rltwoxp_entity_assoc_type_name varchar(100) NOT NULL
);


CREATE TABLE rltwoxp_entity
(
    rltwoxp_entity_id                  serial PRIMARY KEY,
    rltwoxp_entity_assoc_to_entity_one int,
    rltwoxp_entity_assoc_to_entity_two int,
    rltwoxp_entity_type_fk             int,
    CONSTRAINT rlTwoExpAssocToEntity FOREIGN KEY (rltwoxp_entity_assoc_to_entity_one) REFERENCES rltwoxp_entity (rltwoxp_entity_id),
    CONSTRAINT rlTwoExpAssocFromEntity FOREIGN KEY (rltwoxp_entity_assoc_to_entity_two) REFERENCES rltwoxp_entity (rltwoxp_entity_id),
    CONSTRAINT rlTwoExpEntityType FOREIGN KEY (rltwoxp_entity_type_fk) REFERENCES rltwoxp_entity_type (rltwoxp_entity_type_id)
);

CREATE TABLE rltwoxp_entity_assoc_one
(
    rltwoxp_entity_assoc_one_id          serial PRIMARY KEY,
    rltwoxp_entity_assoc_one_from_date   date NOT NULL,
    rltwoxp_entity_assoc_one_thru_date   date,
    rltwoxp_entity_assoc_one_from_entity int,
    rltwoxp_entity_assoc_one_to_entity   int,
    CONSTRAINT FK_rlTwoExpAssocOneFromEnt FOREIGN KEY (rltwoxp_entity_assoc_one_from_entity) REFERENCES rltwoxp_entity (rltwoxp_entity_id),
    CONSTRAINT FK_rlTwoExpAssocOneToEnt FOREIGN KEY (rltwoxp_entity_assoc_one_to_entity) REFERENCES rltwoxp_entity (rltwoxp_entity_id)
);

CREATE TABLE rltwoxp_entity_assoc_two
(
    rltwoxp_entity_assoc_two_id          serial PRIMARY KEY,
    rltwoxp_entity_assoc_two_from_date   date NOT NULL,
    rltwoxp_entity_assoc_two_thru_date   date,
    rltwoxp_entity_assoc_two_from_entity int,
    rltwoxp_entity_assoc_two_to_entity   int,
    rltwoxp_entity_assoc_two_type        int,
    CONSTRAINT FK_rlTwoExpAssocTwoFromEnt FOREIGN KEY (rltwoxp_entity_assoc_two_from_entity) REFERENCES rltwoxp_entity (rltwoxp_entity_id),
    CONSTRAINT FK_rlTwoExpAssocTwoToEnt FOREIGN KEY (rltwoxp_entity_assoc_two_to_entity) REFERENCES rltwoxp_entity (rltwoxp_entity_id),
    CONSTRAINT FK_rlTwoExpAssocTwoType FOREIGN KEY (rltwoxp_entity_assoc_two_type) REFERENCES rltwoxp_entity_assoc_type (rltwoxp_entity_assoc_type_id)
);

CREATE VIEW rltwoxp_associated_entities AS
SELECT *
FROM rltwoxp_entity_type,
     rltwoxp_entity_assoc_type,
     rltwoxp_entity,
     rltwoxp_entity_assoc_one,
     rltwoxp_entity_assoc_two
WHERE rltwoxp_entity_type_id = rltwoxp_entity_type_fk
  AND (rltwoxp_entity_assoc_one_to_entity = rltwoxp_entity_id
  OR rltwoxp_entity_assoc_one_from_entity = rltwoxp_entity_id
  OR rltwoxp_entity_assoc_two_to_entity = rltwoxp_entity_id
  OR rltwoxp_entity_assoc_two_from_entity = rltwoxp_entity_id);
