DROP VIEW IF EXISTS rlone_all_entities;
DROP TABLE IF EXISTS rlone_entity_three;
DROP TABLE IF EXISTS rlone_entity_two;
DROP TABLE IF EXISTS rlone_entity_one;

-- level 0

CREATE TABLE rlone_entity_one
(
    rlone_entity_one_id   serial PRIMARY KEY,
    rlone_entity_one_name varchar(100) NOT NULL
);


--level 1

CREATE TABLE rlone_entity_two
(
    rlone_entity_two_id     serial PRIMARY KEY,
    rlone_entity_two_name   varchar(100) NOT NULL,
    rlone_entity_two_parent int,
    CONSTRAINT FK_rlOneEntityTwoParent FOREIGN KEY (rlone_entity_two_parent) REFERENCES rlone_entity_one (rlone_entity_one_id)
);

CREATE TABLE rlone_entity_three
(
    rlone_entity_three_id     serial PRIMARY KEY,
    rlone_entity_three_name   varchar(100) NOT NULL,
    rlone_entity_three_parent int,
    CONSTRAINT FK_rlOneEntityThreeParent FOREIGN KEY (rlone_entity_three_parent) REFERENCES rlone_entity_two (rlone_entity_two_id)
);

CREATE VIEW rlone_all_entities AS
SELECT rlone_entity_one_name,
       rlone_entity_two_name,
       rlone_entity_three_name
FROM rlone_entity_one,
     rlone_entity_two,
     rlone_entity_three
WHERE rlone_entity_one_id = rlone_entity_two_parent
  AND rlone_entity_two_id = rlone_entity_three_parent;