DROP VIEW IF EXISTS rlthree_entity_with_associations;
DROP TABLE IF EXISTS rlthree_entity_assoc;
DROP TABLE IF EXISTS rlthree_entity;
DROP TABLE IF EXISTS rlthree_entity_assoc_type;
DROP TABLE IF EXISTS rlthree_entity_type;

-- level 1

CREATE TABLE rlthree_entity_type
(
    rlthree_entity_type_id     serial PRIMARY KEY,
    rlthree_entity_type_name   varchar(100) NOT NULL,
    rlthree_entity_type_parent int,
    CONSTRAINT FK_rlThreeEntityTypeParent FOREIGN KEY (rlthree_entity_type_parent) REFERENCES rlthree_entity_type (rlthree_entity_type_id)
);

CREATE TABLE rlthree_entity_assoc_type
(
    rlthree_entity_assoc_type_id     serial PRIMARY KEY,
    rlthree_entity_assoc_type_name   varchar(100) NOT NULL,
    rlthree_entity_assoc_type_parent int,
    CONSTRAINT FK_rlThreeAssocTypeParent FOREIGN KEY (rlthree_entity_assoc_type_parent) REFERENCES rlthree_entity_assoc_type (rlthree_entity_assoc_type_id)
);

CREATE TABLE rlthree_entity
(
    rlthree_entity_id      serial PRIMARY KEY,
    rlthree_entity_type_fk int,
    CONSTRAINT FK_rlThreeEntityType FOREIGN KEY (rlthree_entity_type_fk) REFERENCES rlthree_entity_type (rlthree_entity_type_id)
);

CREATE TABLE rlthree_entity_assoc
(
    rlthree_entity_assoc_id        serial PRIMARY KEY,
    rlthree_entity_assoc_from_date date NOT NULL,
    rlthree_entity_assoc_thru_date date,
    rlthree_entity_assoc_type_fk   int,
    rlthree_entity_assoc_from_ent  int,
    rlthree_entity_assoc_to_ent    int,
    CONSTRAINT FK_rlthreeEntityAssocType FOREIGN KEY (rlthree_entity_assoc_type_fk) REFERENCES rlthree_entity_assoc_type (rlthree_entity_assoc_type_id),
    CONSTRAINT FK_rlThreeAssocEntFrom FOREIGN KEY (rlthree_entity_assoc_from_ent) REFERENCES rlthree_entity (rlthree_entity_id),
    CONSTRAINT FK_rlThreeAssocEntTo FOREIGN KEY (rlthree_entity_assoc_to_ent) REFERENCES rlthree_entity (rlthree_entity_id)
);

CREATE VIEW rlthree_entity_with_associations
AS
SELECT *
FROM rlthree_entity,
     rlthree_entity_type,
     rlthree_entity_assoc_type,
     rlthree_entity_assoc
WHERE (rlthree_entity_id = rlthree_entity_assoc_to_ent OR rlthree_entity_id = rlthree_entity_assoc_from_ent)
  AND rlthree_entity_assoc_type_fk = rlthree_entity_assoc_type_id
  AND rlthree_entity_type_id = rlthree_entity_type_fk;
