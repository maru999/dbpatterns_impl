DROP VIEW IF EXISTS rlthreewr_entity_associations;
DROP TABLE IF EXISTS rlthreewr_entity_assoc;
DROP TABLE IF EXISTS rlthreewr_entity;
DROP TABLE IF EXISTS rlthreewr_entity_assoc_type;
DROP TABLE IF EXISTS rlthreewr_entity_type;
DROP TABLE IF EXISTS rlthreewr_entity_assoc_rule;


-- level 0

CREATE TABLE rlthreewr_entity_assoc_rule
(
    rlthreewr_entity_assoc_rule_id   serial PRIMARY KEY,
    rlthreewr_entity_assoc_rule_name varchar(100) NOT NULL
);

-- level 1

CREATE TABLE rlthreewr_entity_type
(
    rlthreewr_entity_type_id     serial PRIMARY KEY,
    rlthreewr_entity_type_name   varchar(100) NOT NULL,
    rlthreewr_entity_type_parent int,
    CONSTRAINT FK_rlthreewrEntityTypeParent FOREIGN KEY (rlthreewr_entity_type_parent) REFERENCES rlthreewr_entity_type (rlthreewr_entity_type_id)
);

CREATE TABLE rlthreewr_entity_assoc_type
(
    rlthreewr_entity_assoc_type_id     serial PRIMARY KEY,
    rlthreewr_entity_assoc_type_name   varchar(100) NOT NULL,
    rlthreewr_entity_assoc_type_parent int,
    CONSTRAINT FK_rlthreewrAssocTypeParent FOREIGN KEY (rlthreewr_entity_assoc_type_parent) REFERENCES rlthreewr_entity_assoc_type (rlthreewr_entity_assoc_type_id)
);

CREATE TABLE rlthreewr_entity
(
    rlthreewr_entity_id      serial PRIMARY KEY,
    rlthreewr_entity_type_fk int,
    CONSTRAINT FK_rlthreewrEntityType FOREIGN KEY (rlthreewr_entity_type_fk) REFERENCES rlthreewr_entity_type (rlthreewr_entity_type_id)
);

CREATE TABLE rlthreewr_entity_assoc
(
    rlthreewr_entity_assoc_id        serial PRIMARY KEY,
    rlthreewr_entity_assoc_from_date date NOT NULL,
    rlthreewr_entity_assoc_thru_date date,
    rlthreewr_entity_assoc_type_fk   int,
    rlthreewr_entity_assoc_from_ent  int,
    rlthreewr_entity_assoc_to_ent    int,
    rlthreewr_entity_assoc_rule_fk   int,
    CONSTRAINT FK_rlthreewrEntityAssocType FOREIGN KEY (rlthreewr_entity_assoc_type_fk) REFERENCES rlthreewr_entity_assoc_type (rlthreewr_entity_assoc_type_id),
    CONSTRAINT FK_rlthreewrAssocEntFrom FOREIGN KEY (rlthreewr_entity_assoc_from_ent) REFERENCES rlthreewr_entity (rlthreewr_entity_id),
    CONSTRAINT FK_rlthreewrAssocEntTo FOREIGN KEY (rlthreewr_entity_assoc_to_ent) REFERENCES rlthreewr_entity (rlthreewr_entity_id),
    CONSTRAINT FK_rlthreewrAssocRule FOREIGN KEY (rlthreewr_entity_assoc_rule_fk) REFERENCES rlthreewr_entity_assoc_rule (rlthreewr_entity_assoc_rule_id)
);

CREATE VIEW rlthreewr_entity_associations
AS
SELECT *
FROM rlthreewr_entity,
     rlthreewr_entity_type,
     rlthreewr_entity_assoc_type,
     rlthreewr_entity_assoc,
     rlthreewr_entity_assoc_rule
WHERE (rlthreewr_entity_id = rlthreewr_entity_assoc_to_ent OR rlthreewr_entity_id = rlthreewr_entity_assoc_from_ent)
  AND rlthreewr_entity_assoc_type_fk = rlthreewr_entity_assoc_type_id
  AND rlthreewr_entity_type_id = rlthreewr_entity_type_fk
  AND rlthreewr_entity_assoc_rule_id = rlthreewr_entity_assoc_rule_fk;
