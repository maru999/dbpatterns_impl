DROP VIEW IF EXISTS slfour_status_view;
DROP TABLE IF EXISTS slfour_status_appli;
DROP TABLE IF EXISTS slfour_entity_three;
DROP TABLE IF EXISTS slfour_entity_two;
DROP TABLE IF EXISTS slfour_entity_one;
DROP TABLE IF EXISTS slfour_status_type;

CREATE TABLE slfour_status_type
(
    slfour_status_type_id   serial PRIMARY KEY,
    slfour_status_type_name varchar(50) NOT NULL
);

CREATE TABLE slfour_entity_one
(
    slfour_entity_one_id   serial PRIMARY KEY,
    slfour_entity_one_desc varchar(100)
);

CREATE TABLE slfour_entity_two
(
    slfour_entity_two_id   serial PRIMARY KEY,
    slfour_entity_two_name varchar(50)
);

CREATE TABLE slfour_entity_three
(
    slfour_entity_three_id   serial PRIMARY KEY,
    slfour_entity_three_attr varchar(50)
);

CREATE TABLE slfour_status_appli
(
    slfour_status_appli_id           serial PRIMARY KEY,
    slfour_status_appli_datetime     timestamp,
    slfour_status_appli_status_from  date,
    slfour_status_appli_status_thru  date,
    slfour_status_appli_from         date NOT NULL,
    slfour_status_appli_thru         date,
    slfour_status_appli_type         int,
    slfour_status_appli_entity_one   int,
    slfour_status_appli_entity_two   int,
    slfour_status_appli_entity_three int,
    CONSTRAINT FK_slFourStatusAppliType FOREIGN KEY (slfour_status_appli_type) REFERENCES slfour_status_type (slfour_status_type_id),
    CONSTRAINT FK_slFourStatusAppliEntityOne FOREIGN KEY (slfour_status_appli_entity_one) REFERENCES slfour_entity_one (slfour_entity_one_id),
    CONSTRAINT FK_slFourStatusAppliEntityTwo FOREIGN KEY (slfour_status_appli_entity_two) REFERENCES slfour_entity_two (slfour_entity_two_id),
    CONSTRAINT FK_slFourStatusAppliEntityThree FOREIGN KEY (slfour_status_appli_entity_three) REFERENCES slfour_entity_three (slfour_entity_three_id),
    CONSTRAINT check_OnlyOneEntity CHECK ( num_nonnulls(slfour_status_appli_entity_one, slfour_status_appli_entity_two,
                                                        slfour_status_appli_entity_three) = 1 )
);

CREATE VIEW slfour_status_view AS
SELECT *
FROM slfour_status_type,
     slfour_entity_one,
     slfour_entity_two,
     slfour_entity_three,
     slfour_status_appli
WHERE slfour_status_appli_type = slfour_status_type_id
  AND ((slfour_status_appli_entity_one = slfour_entity_one_id AND slfour_status_appli_entity_two IS NULL AND
        slfour_status_appli_entity_three IS NULL)
    OR (slfour_status_appli_entity_two = slfour_entity_two_id AND slfour_status_appli_entity_one IS NULL AND
        slfour_status_appli_entity_three IS NULL)
    OR (slfour_status_appli_entity_three = slfour_entity_three_id AND slfour_status_appli_entity_one IS NULL AND
        slfour_status_appli_entity_two IS NULL));