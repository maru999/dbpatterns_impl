DROP VIEW IF EXISTS slthree_status_view;
DROP TABLE IF EXISTS slthree_entity_status;
DROP TABLE IF EXISTS slthree_entity;
DROP TABLE IF EXISTS slthree_status_type;

CREATE TABLE slthree_status_type
(
    slthree_status_type_id   serial PRIMARY KEY,
    slthree_status_type_name varchar(50) NOT NULL
);

CREATE TABLE slthree_entity
(
    slthree_entity_id serial PRIMARY KEY
);

CREATE TABLE slthree_entity_status
(
    slthree_entity_status_id               serial PRIMARY KEY,
    slthree_entity_status_date_time        timestamp,
    slthree_entity_status_status_from_date date NOT NULL,
    slthree_entity_status_status_thru_date date,
    slthree_entity_status_from_date        date NOT NULL,
    slthree_entity_status_thru_date        date,
    slthree_entity_status_type_fk          int,
    slthree_entity_status_entity_fk        int,
    CONSTRAINT FK_slThreeEntityStatusType FOREIGN KEY (slthree_entity_status_type_fk) REFERENCES slthree_status_type (slthree_status_type_id),
    CONSTRAINT FK_slThreeEntity FOREIGN KEY (slthree_entity_status_entity_fk) REFERENCES slthree_entity (slthree_entity_id)
);

CREATE VIEW slthree_status_view AS
SELECT *
FROM slthree_status_type,
     slthree_entity,
     slthree_entity_status
WHERE slthree_entity_id = slthree_entity_status_entity_fk
  AND slthree_entity_status_type_fk = slthree_status_type_id;