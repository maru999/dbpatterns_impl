DROP VIEW IF EXISTS sltwo_entity_status;
DROP TABLE IF EXISTS sltwo_entity;
DROP TABLE IF EXISTS sltwo_entity_status_type;
DROP TABLE IF EXISTS sltwo_status_type;

CREATE TABLE sltwo_status_type
(
    sltwo_status_type_id   serial PRIMARY KEY,
    sltwo_status_type_name varchar(100) NOT NULL
);

CREATE TABLE sltwo_entity_status_type
(
    sltwo_entity_status_type_id        int PRIMARY KEY,
    sltwo_entity_status_type_some_attr varchar(100),
    CONSTRAINT FK_slTwoEntityStatusTypeStatusType FOREIGN KEY (sltwo_entity_status_type_id) REFERENCES sltwo_status_type (sltwo_status_type_id)
);

CREATE TABLE sltwo_entity
(
    sltwo_entity_id              serial PRIMARY KEY,
    sltwo_entity_status_datetime timestamp,
    sltwo_entity_status_type_fk  int,
    CONSTRAINT FK_slTwoEntityToStatusType FOREIGN KEY (sltwo_entity_status_type_fk) REFERENCES sltwo_entity_status_type (sltwo_entity_status_type_id)
);

CREATE VIEW sltwo_entity_status AS
SELECT *
FROM sltwo_status_type,
     sltwo_entity_status_type,
     sltwo_entity
WHERE sltwo_entity_status_type_fk = sltwo_entity_status_type_id;