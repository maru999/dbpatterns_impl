DROP TABLE IF EXISTS slone_entity;

CREATE TABLE slone_entity
(
    slone_entity_id              serial PRIMARY KEY,
    slone_entity_event_one_dt    timestamp,
    slone_entity_event_two_dt    timestamp,
    slone_entity_event_three_dt  timestamp,
    slone_entity_event_indicator varchar(100),
    slone_entity_event_from_date date NOT NULL,
    slone_entity_event_thru_date date
);