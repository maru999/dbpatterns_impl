package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeThreeDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeThree;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLTwoEntityTypeOneMapstructMapper.class, CLTwoEntityTypeTwoMapstructMapper.class})
public interface CLTwoEntitySubtypeThreeMapstructMapper {

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeThreeInfo", target = "info")
    CLTwoEntitySubtypeThreeDTO toDTO(CLTwoEntitySubtypeThree entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLTwoEntitySubtypeThree toEntity(CLTwoEntitySubtypeThreeDTO dto);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeThreeInfo", target = "info")
    void updateEntityFromDTO(CLTwoEntitySubtypeThree entity, @MappingTarget CLTwoEntitySubtypeThreeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLTwoEntitySubtypeThreeDTO dto, @MappingTarget CLTwoEntitySubtypeThree entity);

    List<CLTwoEntitySubtypeThreeDTO> toDTOs(List<CLTwoEntitySubtypeThree> entities);

    List<CLTwoEntitySubtypeThree> toEntities(List<CLTwoEntitySubtypeThreeDTO> dtos);
}
