package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeOneDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeOne;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLTwoEntityTypeOneMapstructMapper.class, CLTwoEntityTypeTwoMapstructMapper.class})
public interface CLTwoEntitySubtypeOneMapstructMapper {

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeOneDesc", target = "desc")
    CLTwoEntitySubtypeOneDTO toDTO(CLTwoEntitySubtypeOne entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLTwoEntitySubtypeOne toEntity(CLTwoEntitySubtypeOneDTO dto);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeOneDesc", target = "desc")
    void updateEntityFromDTO(CLTwoEntitySubtypeOne entity, @MappingTarget CLTwoEntitySubtypeOneDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLTwoEntitySubtypeOneDTO dto, @MappingTarget CLTwoEntitySubtypeOne entity);

    List<CLTwoEntitySubtypeOneDTO> toDTOs(List<CLTwoEntitySubtypeOne> entities);

    List<CLTwoEntitySubtypeOne> toEntities(List<CLTwoEntitySubtypeOneDTO> dtos);
}
