package at.fhv.rupp.ba.dbimpl.core;

import com.github.javafaker.Faker;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.zone.ZoneRulesException;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public abstract class DataGenJavaFaker {
    public static final LocalDate START_DATE = LocalDate.now();
    public static final LocalDate END_DATE = LocalDate.of(2040, 12, 30);
    public static final Locale LOCALE = new Locale("en", "GB");
    public static final Faker FAKER = new Faker(LOCALE);
    public static final int MIDDLE_CHAR_LIMIT = 99;
    public static final int LOW_CHAR_LIMIT = 49;
    public static final LocalDateTime START_DATETIME = LocalDateTime.now();
    public static final LocalDateTime END_DATETIME = LocalDateTime.of(2040, 12, 30, 12, 12, 12);
    public static final int TINY_GEN_AMOUNT = 10;
    public static final int LOW_GEN_AMOUNT = 30;
    public static final int MIDDLE_GEN_AMOUNT = 50;
    public static final int HIGH_GEN_AMOUNT = 100;

    public abstract void fillDB();

    public int getRandomIntByBound(int bound) {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(bound);
    }

    public String trimStringIfTooLong(String stringToCheck, int bound) {
        return stringToCheck.length() < bound ? stringToCheck : stringToCheck.substring(0, bound - 1);
    }

    public LocalDate getDateBetween(LocalDate startInclusive, LocalDate endExclusive) {
        long startEpochDay = startInclusive.toEpochDay();
        long endEpochDay = endExclusive.toEpochDay();
        long randomDay = ThreadLocalRandom
                .current()
                .nextLong(startEpochDay, endEpochDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    public LocalDateTime getDateTimeBetween(LocalDateTime startInclusive, LocalDateTime endExclusive) {
        long startEpochSec = startInclusive.toEpochSecond(ZoneOffset.UTC);
        long endEpochSec = endExclusive.toEpochSecond(ZoneOffset.UTC);
        long randomSec = ThreadLocalRandom
                .current()
                .nextLong(startEpochSec, endEpochSec);
        return LocalDateTime.ofEpochSecond(randomSec, 0, ZoneOffset.UTC);
    }
}
