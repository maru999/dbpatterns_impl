package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos;

public class RLThreeWRulesEntityTypeDTO {
    private Integer id;
    private String name;
    private RLThreeWRulesEntityTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLThreeWRulesEntityTypeDTO getParent() {
        return parent;
    }

    public void setParent(RLThreeWRulesEntityTypeDTO parent) {
        this.parent = parent;
    }
}
