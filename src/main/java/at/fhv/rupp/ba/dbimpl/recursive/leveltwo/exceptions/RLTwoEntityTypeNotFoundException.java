package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.exceptions;

public class RLTwoEntityTypeNotFoundException extends Exception {
    public RLTwoEntityTypeNotFoundException(String message) {
        super(message);
    }
}
