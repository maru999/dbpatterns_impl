package at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos;

public class CLThreeEntityCategoryTypeDTO {

    private Integer id;
    private String name;
    private CLThreeEntityCategoryTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLThreeEntityCategoryTypeDTO getParent() {
        return parent;
    }

    public void setParent(CLThreeEntityCategoryTypeDTO parent) {
        this.parent = parent;
    }
}
