package at.fhv.rupp.ba.dbimpl.recursive.levelthree.exceptions;

public class RLThreeEntityNotFoundException extends Exception {
    public RLThreeEntityNotFoundException(String message) {
        super(message);
    }
}
