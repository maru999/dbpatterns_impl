package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityTypeOneDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeOne;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntityTypeOneMapper {

    private CLTwoEntityTypeOneMapper() {
        // hidden constructor
    }

    public static CLTwoEntityTypeOneDTO toDTO(CLTwoEntityTypeOne entity) {
        CLTwoEntityTypeOneDTO dto = new CLTwoEntityTypeOneDTO();
        dto.setId(entity.getClTwoEntityTypeOneId());
        dto.setName(entity.getClTwoEntityTypeOneName());
        return dto;
    }

    public static CLTwoEntityTypeOne toEntity(CLTwoEntityTypeOneDTO dto) {
        CLTwoEntityTypeOne entity = new CLTwoEntityTypeOne();
        if (dto.getId() != null) {
            entity.setClTwoEntityTypeOneId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setClTwoEntityTypeOneName(dto.getName());
        }
        return entity;
    }

    public static List<CLTwoEntityTypeOneDTO> toDTOs(List<CLTwoEntityTypeOne> entities) {
        LinkedList<CLTwoEntityTypeOneDTO> result = new LinkedList<>();
        for (CLTwoEntityTypeOne entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLTwoEntityTypeOne> toEntities(List<CLTwoEntityTypeOneDTO> dtos) {
        LinkedList<CLTwoEntityTypeOne> result = new LinkedList<>();
        for (CLTwoEntityTypeOneDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
