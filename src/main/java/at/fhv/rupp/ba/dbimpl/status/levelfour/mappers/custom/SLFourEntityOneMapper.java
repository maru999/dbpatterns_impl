package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityOneDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityOne;

import java.util.LinkedList;
import java.util.List;

public class SLFourEntityOneMapper {

    private SLFourEntityOneMapper() {
        //hidden constructor
    }

    public static SLFourEntityOneDTO toDTO(SLFourEntityOne entity) {
        SLFourEntityOneDTO dto = new SLFourEntityOneDTO();
        dto.setId(entity.getSlFourEntityOneId());
        dto.setDesc(entity.getSlFourEntityOneDesc());
        return dto;
    }

    public static SLFourEntityOne toEntity(SLFourEntityOneDTO dto) {
        SLFourEntityOne entity = new SLFourEntityOne();
        if (dto.getId() != null) {
            entity.setSlFourEntityOneId(dto.getId());
        }
        if (dto.getDesc() != null) {
            entity.setSlFourEntityOneDesc(dto.getDesc());
        }
        return entity;
    }

    public static List<SLFourEntityOneDTO> toDTOs(List<SLFourEntityOne> entities) {
        LinkedList<SLFourEntityOneDTO> result = new LinkedList<>();
        for (SLFourEntityOne entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLFourEntityOne> toEntities(List<SLFourEntityOneDTO> dtos) {
        LinkedList<SLFourEntityOne> result = new LinkedList<>();
        for (SLFourEntityOneDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
