package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityTypeOneDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeOne;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLTwoEntityTypeOneMapstructMapper {

    @Mapping(source = "clTwoEntityTypeOneId", target = "id")
    @Mapping(source = "clTwoEntityTypeOneName", target = "name")
    CLTwoEntityTypeOneDTO toDTO(CLTwoEntityTypeOne entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLTwoEntityTypeOne toEntity(CLTwoEntityTypeOneDTO dto);

    @Mapping(source = "clTwoEntityTypeOneId", target = "id")
    @Mapping(source = "clTwoEntityTypeOneName", target = "name")
    void updateEntityFromDTO(CLTwoEntityTypeOne entity, @MappingTarget CLTwoEntityTypeOneDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLTwoEntityTypeOneDTO dto, @MappingTarget CLTwoEntityTypeOne entity);

    List<CLTwoEntityTypeOneDTO> toDTOs(List<CLTwoEntityTypeOne> entities);

    List<CLTwoEntityTypeOne> toEntities(List<CLTwoEntityTypeOneDTO> dtos);
}
