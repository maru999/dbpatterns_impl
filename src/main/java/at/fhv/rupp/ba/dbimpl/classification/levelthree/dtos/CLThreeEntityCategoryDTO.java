package at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos;

import java.time.LocalDate;

public class CLThreeEntityCategoryDTO {

    private Integer id;
    private String name;
    private LocalDate from;
    private LocalDate thru;
    private CLThreeEntityCategoryDTO parent;
    private CLThreeEntityCategoryTypeDTO categoryType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getThru() {
        return thru;
    }

    public void setThru(LocalDate thru) {
        this.thru = thru;
    }

    public CLThreeEntityCategoryDTO getParent() {
        return parent;
    }

    public void setParent(CLThreeEntityCategoryDTO parent) {
        this.parent = parent;
    }

    public CLThreeEntityCategoryTypeDTO getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CLThreeEntityCategoryTypeDTO categoryType) {
        this.categoryType = categoryType;
    }
}
