package at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityWAssociationsIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface RLThreeEntityRepo extends JpaRepository<RLThreeEntity, Integer> {

    @Query(value = "SELECT * FROM rlthree_entity_with_associations", nativeQuery = true)
    List<RLThreeEntityWAssociationsIProjection> getEntitiesWithAssociations();

    @Query(value = "SELECT * FROM rlthree_entity_with_associations WHERE rlthree_entity_id = :id", nativeQuery = true)
    List<RLThreeEntityWAssociationsIProjection> getEntitiesWithAssociationsByEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rlthree_entity_with_associations WHERE rlthree_entity_type_name = :name", nativeQuery = true)
    List<RLThreeEntityWAssociationsIProjection> getEntitiesWithAssociationsByTypeName(@Param("name") String typeName);

    @Query(value = "SELECT * FROM rlthree_entity_with_associations WHERE rlthree_entity_assoc_thru_date IS NULL", nativeQuery = true)
    List<RLThreeEntityWAssociationsIProjection> getEntitiesWithAssociationsStillInUse();

    @Query(value = "SELECT * FROM rlthree_entity_with_associations WHERE rlthree_entity_assoc_thru_date IS NOT NULL", nativeQuery = true)
    List<RLThreeEntityWAssociationsIProjection> getEntitiesWithAssociationsNoLongerInUse();

    @Query(value = "SELECT * FROM rlthree_entity_with_associations WHERE rlthree_entity_assoc_from_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<RLThreeEntityWAssociationsIProjection> getEntitiesWithAssociationsWhereFromDateBetween(@Param("startDate") LocalDate start, @Param("endDate") LocalDate end);

    @Query(value = "SELECT * FROM rlthree_entity WHERE rlthree_entity_type_fk = :id", nativeQuery = true)
    List<RLThreeEntity> getEntitiesByTypeId(@Param("id") Integer typeId);
}
