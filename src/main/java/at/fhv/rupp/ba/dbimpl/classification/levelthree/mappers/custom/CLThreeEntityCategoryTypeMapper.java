package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryType;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityCategoryTypeDTO;

import java.util.LinkedList;
import java.util.List;

public class CLThreeEntityCategoryTypeMapper {

    private CLThreeEntityCategoryTypeMapper() {
        //hidden constructor
    }

    public static CLThreeEntityCategoryTypeDTO toDTO(CLThreeEntityCategoryType entity) {
        CLThreeEntityCategoryTypeDTO dto = new CLThreeEntityCategoryTypeDTO();
        dto.setId(entity.getClthreeEntityCategoryTypeId());
        dto.setName(entity.getClthreeEntityCategoryTypeName());
        if (entity.getClThreeEntityCategoryTypeParent() != null) {
            dto.setParent(toDTO(entity.getClThreeEntityCategoryTypeParent()));
        }
        return dto;
    }

    public static CLThreeEntityCategoryType toEntity(CLThreeEntityCategoryTypeDTO dto) {
        CLThreeEntityCategoryType entity = new CLThreeEntityCategoryType();
        if (dto.getId() != null) {
            entity.setClthreeEntityCategoryTypeId(dto.getId());
        }
        entity.setClthreeEntityCategoryTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setClThreeEntityCategoryTypeParent(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<CLThreeEntityCategoryTypeDTO> toDTOs(List<CLThreeEntityCategoryType> entities) {
        LinkedList<CLThreeEntityCategoryTypeDTO> result = new LinkedList<>();
        for (CLThreeEntityCategoryType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeEntityCategoryType> toEntities(List<CLThreeEntityCategoryTypeDTO> dtos) {
        LinkedList<CLThreeEntityCategoryType> result = new LinkedList<>();
        for (CLThreeEntityCategoryTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
