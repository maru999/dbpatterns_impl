package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rltwo_entity_type")
@EntityListeners(AuditingEntityListener.class)
public class RLTwoEntityType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rltwo_entity_type_id")
    private int rlTwoEntityTypeId;

    @Column(name = "rltwo_entity_type_name")
    private String rlTwoEntityTypeName;

    @ManyToOne
    @JoinColumn(name = "rltwo_entity_type_parent")
    private RLTwoEntityType rlTwoEntityTypeParent;

    public int getRlTwoEntityTypeId() {
        return rlTwoEntityTypeId;
    }

    public void setRlTwoEntityTypeId(int rlTwoEntityTypeId) {
        this.rlTwoEntityTypeId = rlTwoEntityTypeId;
    }

    public String getRlTwoEntityTypeName() {
        return rlTwoEntityTypeName;
    }

    public void setRlTwoEntityTypeName(String rlTwoEntityTypeName) {
        this.rlTwoEntityTypeName = rlTwoEntityTypeName;
    }

    public RLTwoEntityType getRlTwoEntityTypeParent() {
        return rlTwoEntityTypeParent;
    }

    public void setRlTwoEntityTypeParent(RLTwoEntityType rlTwoEntityTypeParent) {
        this.rlTwoEntityTypeParent = rlTwoEntityTypeParent;
    }
}
