package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLThreeWRollupsEntityCategoryTypeRepo extends JpaRepository<CLThreeWRollupsEntityCategoryType, Integer> {
}
