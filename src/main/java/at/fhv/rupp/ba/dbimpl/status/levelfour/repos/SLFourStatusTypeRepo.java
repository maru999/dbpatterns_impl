package at.fhv.rupp.ba.dbimpl.status.levelfour.repos;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityStatusViewIProjection;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourStatusType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface SLFourStatusTypeRepo extends JpaRepository<SLFourStatusType, Integer> {

    @Query(value = "SELECT * FROM slfour_status_view", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatus();

    @Query(value = "SELECT * FROM slfour_status_view WHERE slfour_status_appli_entity_one IS NOT NULL", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusEntityOne();

    @Query(value = "SELECT * FROM slfour_status_view WHERE slfour_status_appli_entity_two IS NOT NULL", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusEntityTwo();

    @Query(value = "SELECT * FROM slfour_status_view WHERE slfour_status_appli_entity_three IS NOT NULL", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusEntityThree();

    @Query(value = "SELECT * FROM slfour_status_view WHERE slfour_status_appli_entity_one IS NULL", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusNoEntityOne();

    @Query(value = "SELECT * FROM slfour_status_view WHERE slfour_status_appli_entity_two IS NULL", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusNoEntityTwo();

    @Query(value = "SELECT * FROM slfour_status_view WHERE slfour_status_appli_entity_three IS NULL", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusNoEntityThree();

    @Query(value = "SELECT * FROM slfour_status_view WHERE slfour_status_appli_datetime BETWEEN :startTime AND :endTime", nativeQuery = true)
    List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusWhereStatusDateTimeBetween(@Param("startTime")LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);


}
