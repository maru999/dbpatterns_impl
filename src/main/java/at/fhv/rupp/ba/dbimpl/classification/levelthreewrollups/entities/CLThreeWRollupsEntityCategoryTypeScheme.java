package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_entity_category_type_scheme")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategoryTypeScheme {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_type_scheme_id")
    private int clthreewrEntityCategoryTypeSchemeId;

    @Column(name = "clthreewr_entity_category_type_scheme_name")
    private String clthreewrEntityCategoryTypeSchemeName;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_type_scheme_dp_id")
    private CLThreeWRollupsDataProvider schemeDataProvider;

    public int getClthreewrEntityCategoryTypeSchemeId() {
        return clthreewrEntityCategoryTypeSchemeId;
    }

    public void setClthreewrEntityCategoryTypeSchemeId(int clthreewrEntityCategoryTypeSchemeId) {
        this.clthreewrEntityCategoryTypeSchemeId = clthreewrEntityCategoryTypeSchemeId;
    }

    public String getClthreewrEntityCategoryTypeSchemeName() {
        return clthreewrEntityCategoryTypeSchemeName;
    }

    public void setClthreewrEntityCategoryTypeSchemeName(String clthreewrEntityCategoryTypeSchemeName) {
        this.clthreewrEntityCategoryTypeSchemeName = clthreewrEntityCategoryTypeSchemeName;
    }

    public CLThreeWRollupsDataProvider getSchemeDataProvider() {
        return schemeDataProvider;
    }

    public void setSchemeDataProvider(CLThreeWRollupsDataProvider schemeDataProvider) {
        this.schemeDataProvider = schemeDataProvider;
    }
}
