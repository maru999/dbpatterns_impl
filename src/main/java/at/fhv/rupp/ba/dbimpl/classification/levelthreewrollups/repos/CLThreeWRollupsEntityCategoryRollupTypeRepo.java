package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryRollupType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLThreeWRollupsEntityCategoryRollupTypeRepo extends JpaRepository<CLThreeWRollupsEntityCategoryRollupType, Integer> {
}
