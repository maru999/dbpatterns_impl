package at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos;

public class SLTwoEntityStatusTypeDTO {
    private Integer id;
    private String name;
    private String someAttr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSomeAttr() {
        return someAttr;
    }

    public void setSomeAttr(String someAttr) {
        this.someAttr = someAttr;
    }
}
