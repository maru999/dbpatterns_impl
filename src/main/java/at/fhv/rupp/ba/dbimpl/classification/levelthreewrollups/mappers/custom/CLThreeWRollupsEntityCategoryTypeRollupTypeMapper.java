package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeRollupTypeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryTypeRollupType;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryTypeRollupTypeMapper {

    private CLThreeWRollupsEntityCategoryTypeRollupTypeMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryTypeRollupTypeDTO toDTO(CLThreeWRollupsEntityCategoryTypeRollupType entity) {
        CLThreeWRollupsEntityCategoryTypeRollupTypeDTO dto = new CLThreeWRollupsEntityCategoryTypeRollupTypeDTO();
        dto.setId(entity.getClthreewrEntityCategoryTypeRollupTypeId());
        dto.setName(entity.getClthreewrEntityCategoryTypeRollupTypeName());
        if (entity.getParentTypeRollupType() != null) {
            dto.setParent(toDTO(entity.getParentTypeRollupType()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategoryTypeRollupType toEntity(CLThreeWRollupsEntityCategoryTypeRollupTypeDTO dto) {
        CLThreeWRollupsEntityCategoryTypeRollupType entity = new CLThreeWRollupsEntityCategoryTypeRollupType();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryTypeRollupTypeId(dto.getId());
        }
        entity.setClthreewrEntityCategoryTypeRollupTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setParentTypeRollupType(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryTypeRollupTypeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryTypeRollupType> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryTypeRollupTypeDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryTypeRollupType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategoryTypeRollupType> toEntities(List<CLThreeWRollupsEntityCategoryTypeRollupTypeDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategoryTypeRollupType> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryTypeRollupTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
