package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityDTO;

import java.time.LocalDate;

public class RLThreeWRulesEntityAssocDTO {
    private Integer id;
    private LocalDate fromDate;
    private LocalDate thruDate;
    private RLThreeWRulesEntityAssocTypeDTO type;
    private RLThreeWRulesEntityDTO fromEntity;
    private RLThreeWRulesEntityDTO toEntity;
    private RLThreeWRulesEntityAssocRuleDTO rule;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getThruDate() {
        return thruDate;
    }

    public void setThruDate(LocalDate thruDate) {
        this.thruDate = thruDate;
    }

    public RLThreeWRulesEntityAssocTypeDTO getType() {
        return type;
    }

    public void setType(RLThreeWRulesEntityAssocTypeDTO type) {
        this.type = type;
    }

    public RLThreeWRulesEntityDTO getFromEntity() {
        return fromEntity;
    }

    public void setFromEntity(RLThreeWRulesEntityDTO fromEntity) {
        this.fromEntity = fromEntity;
    }

    public RLThreeWRulesEntityDTO getToEntity() {
        return toEntity;
    }

    public void setToEntity(RLThreeWRulesEntityDTO toEntity) {
        this.toEntity = toEntity;
    }

    public RLThreeWRulesEntityAssocRuleDTO getRule() {
        return rule;
    }

    public void setRule(RLThreeWRulesEntityAssocRuleDTO rule) {
        this.rule = rule;
    }
}
