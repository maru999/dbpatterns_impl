package at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos;

public interface RLOneAllEntitiesIProjection {
    String getrlone_entity_one_name();

    String getrlone_entity_two_name();

    String getrlone_entity_three_name();
}
