package at.fhv.rupp.ba.dbimpl.status.levelfour.exceptions;

public class SLFourStatusApplicationNotFoundException extends Exception {
    public SLFourStatusApplicationNotFoundException(String message) {
        super(message);
    }
}
