package at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos;

public class CLTwoEntityEntityTypeThreeClassificationDTO {

    private Integer id;
    private CLTwoEntityDTO entity;
    private CLTwoEntityTypeThreeDTO typeThree;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CLTwoEntityDTO getEntity() {
        return entity;
    }

    public void setEntity(CLTwoEntityDTO entity) {
        this.entity = entity;
    }

    public CLTwoEntityTypeThreeDTO getTypeThree() {
        return typeThree;
    }

    public void setTypeThree(CLTwoEntityTypeThreeDTO typeThree) {
        this.typeThree = typeThree;
    }
}
