package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLThreeEntityMapstructMapper {

    @Mapping(source = "clthreeEntityId", target = "id")
    @Mapping(source = "clthreeEntityName", target = "name")
    CLThreeEntityDTO toDTO(CLThreeEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeEntity toEntity(CLThreeEntityDTO dto);

    @Mapping(source = "clthreeEntityId", target = "id")
    @Mapping(source = "clthreeEntityName", target = "name")
    void updateEntityFromDTO(CLThreeEntity entity, @MappingTarget CLThreeEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeEntityDTO dto, @MappingTarget CLThreeEntity entity);

    List<CLThreeEntityDTO> toDTOs(List<CLThreeEntity> entities);

    List<CLThreeEntity> toEntities(List<CLThreeEntityDTO> dtos);
}
