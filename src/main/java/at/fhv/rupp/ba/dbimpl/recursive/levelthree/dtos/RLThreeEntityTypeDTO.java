package at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos;

public class RLThreeEntityTypeDTO {
    private Integer id;
    private String name;
    private RLThreeEntityTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLThreeEntityTypeDTO getParent() {
        return parent;
    }

    public void setParent(RLThreeEntityTypeDTO parent) {
        this.parent = parent;
    }
}
