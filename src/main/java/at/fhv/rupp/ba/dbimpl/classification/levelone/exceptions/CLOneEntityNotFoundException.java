package at.fhv.rupp.ba.dbimpl.classification.levelone.exceptions;

public class CLOneEntityNotFoundException extends Exception {
    public CLOneEntityNotFoundException(String message) {
        super(message);
    }
}
