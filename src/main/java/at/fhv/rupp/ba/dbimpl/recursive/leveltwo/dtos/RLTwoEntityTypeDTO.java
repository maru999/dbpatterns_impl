package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos;

public class RLTwoEntityTypeDTO {
    private Integer id;
    private String name;
    private RLTwoEntityTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLTwoEntityTypeDTO getParent() {
        return parent;
    }

    public void setParent(RLTwoEntityTypeDTO parent) {
        this.parent = parent;
    }
}
