package at.fhv.rupp.ba.dbimpl.status.levelfour.exceptions;

public class SLFourXORConstraintViolationException extends Exception{
    public SLFourXORConstraintViolationException(String message) {
        super(message);
    }
}
