package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryClassification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CLThreeWRollupsEntityCategoryClassificationRepo extends JpaRepository<CLThreeWRollupsEntityCategoryClassification, Integer> {

    @Query(value = "SELECT * FROM clthreewr_entity_category_classification WHERE clthreewr_entity_category_classification_entity_id = :id", nativeQuery = true)
    List<CLThreeWRollupsEntityCategoryClassification> getClassificationsByEntityId(@Param("id") Integer id);
}
