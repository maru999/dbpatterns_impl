package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.datagen.RLThreeWRulesJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntitiesWAssociationsIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntity;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssoc;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityType;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.exceptions.RLThreeWRulesEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.exceptions.RLThreeWRulesEntityTypeNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.custom.RLThreeWRulesEntityMapper;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.custom.RLThreeWRulesEntityTypeMapper;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.repos.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/recursion/L3WRules")
public class RLThreeWRulesController {
    private final RLThreeWRulesEntityTypeRepo entityTypeRepo;
    private final RLThreeWRulesEntityRepo entityRepo;
    private final RLThreeWRulesEntityAssocRepo assocRepo;
    private final RLThreeWRulesEntityAssocTypeRepo assocTypeRepo;
    private final RLThreeWRulesEntityAssocRuleRepo assocRuleRepo;
    private final DataGenJavaFaker faker;

    public RLThreeWRulesController(RLThreeWRulesEntityTypeRepo entityTypeRepo, RLThreeWRulesEntityRepo entityRepo, RLThreeWRulesEntityAssocRepo assocRepo, RLThreeWRulesEntityAssocTypeRepo assocTypeRepo, RLThreeWRulesEntityAssocRuleRepo assocRuleRepo) {
        this.entityTypeRepo = entityTypeRepo;
        this.entityRepo = entityRepo;
        this.assocRepo = assocRepo;
        this.assocTypeRepo = assocTypeRepo;
        this.assocRuleRepo = assocRuleRepo;
        this.faker = new RLThreeWRulesJavaFaker(entityRepo, entityTypeRepo, assocRepo, assocTypeRepo, assocRuleRepo);
    }

    @GetMapping("/entity/{id}")
    public ResponseEntity<RLThreeWRulesEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws RLThreeWRulesEntityNotFoundException {
        RLThreeWRulesEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLThreeWRulesEntityNotFoundException(getExceptionString("RLThreeWRulesEntity", entityId)));
        return ResponseEntity.ok().body(RLThreeWRulesEntityMapper.toDTO(entity));
    }

    @GetMapping("/entities/all")
    public List<RLThreeWRulesEntityDTO> getAllEntities() {
        return RLThreeWRulesEntityMapper.toDTOs(entityRepo.findAll());
    }

    @PostMapping("/entities")
    public RLThreeWRulesEntityDTO createEntity(@Valid @RequestBody RLThreeWRulesEntityDTO entityDTO) {
        return RLThreeWRulesEntityMapper.toDTO(entityRepo.save(RLThreeWRulesEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<RLThreeWRulesEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLThreeWRulesEntityDTO entityDetails) throws RLThreeWRulesEntityNotFoundException {
        RLThreeWRulesEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLThreeWRulesEntityNotFoundException(getExceptionString("RLThreeWRulesEntity", entityId)));
        RLThreeWRulesEntityDTO entityDTO = RLThreeWRulesEntityMapper.toDTO(entity);
        if (entityDetails.getType() != null) {
            entityDTO.setType(entityDetails.getType());
        }
        final RLThreeWRulesEntityDTO updatedEntity = RLThreeWRulesEntityMapper.toDTO(entityRepo.save(RLThreeWRulesEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws RLThreeWRulesEntityNotFoundException {
        RLThreeWRulesEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLThreeWRulesEntityNotFoundException(getExceptionString("RLThreeWRulesEntity", entityId)));
        List<RLThreeWRulesEntityAssoc> assocs = assocRepo.getAssociationsByEntityFromId(entityId);
        for (RLThreeWRulesEntityAssoc assoc : assocs) {
            assocRepo.delete(assoc);
        }
        assocs = assocRepo.getAssociationsByEntityToId(entityId);
        for (RLThreeWRulesEntityAssoc assoc : assocs) {
            assocRepo.delete(assoc);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/type/{id}")
    public ResponseEntity<RLThreeWRulesEntityTypeDTO> getEntityTypeById(@PathVariable(value = "id") Integer entityId) throws RLThreeWRulesEntityTypeNotFoundException {
        RLThreeWRulesEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLThreeWRulesEntityTypeNotFoundException(getExceptionString("RLThreeWRulesEntityType", entityId)));
        return ResponseEntity.ok().body(RLThreeWRulesEntityTypeMapper.toDTO(entity));
    }

    @GetMapping("/types/all")
    public List<RLThreeWRulesEntityTypeDTO> getAllEntityTypes() {
        return RLThreeWRulesEntityTypeMapper.toDTOs(entityTypeRepo.findAll());
    }

    @PostMapping("/types")
    public RLThreeWRulesEntityTypeDTO createEntityType(@Valid @RequestBody RLThreeWRulesEntityTypeDTO entityDTO) {
        return RLThreeWRulesEntityTypeMapper.toDTO(entityTypeRepo.save(RLThreeWRulesEntityTypeMapper.toEntity(entityDTO)));
    }

    @PutMapping("/types/{id}")
    public ResponseEntity<RLThreeWRulesEntityTypeDTO> updateEntityType(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLThreeWRulesEntityTypeDTO entityDetails) throws RLThreeWRulesEntityTypeNotFoundException {
        RLThreeWRulesEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLThreeWRulesEntityTypeNotFoundException(getExceptionString("RLThreeWRulesEntityType", entityId)));
        RLThreeWRulesEntityTypeDTO entityDTO = RLThreeWRulesEntityTypeMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        if (entityDetails.getParent() != null) {
            entityDTO.setParent(entityDetails.getParent());
        }
        final RLThreeWRulesEntityTypeDTO updatedEntity = RLThreeWRulesEntityTypeMapper.toDTO(entityTypeRepo.save(RLThreeWRulesEntityTypeMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/types/{id}")
    public Map<String, Boolean> deleteEntityType(@PathVariable(value = "id") Integer entityId) throws RLThreeWRulesEntityTypeNotFoundException {
        RLThreeWRulesEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLThreeWRulesEntityTypeNotFoundException(getExceptionString("RLThreeWRulesEntityType", entityId)));
        List<RLThreeWRulesEntityType> childTypes = entityTypeRepo.getTypesWhereTypeIsParent(entityId);
        for (RLThreeWRulesEntityType childType : childTypes) {
            childType.setRlThreeWRulesEntityTypeParent(null);
            entityTypeRepo.save(childType);
        }
        List<RLThreeWRulesEntity> refEntities = entityRepo.getEntitiesByTypeId(entityId);
        for (RLThreeWRulesEntity refEntity : refEntities) {
            refEntity.setRlThreeWRulesEntityType(null);
            entityRepo.save(refEntity);
        }
        entityTypeRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/recursive/recursivelevelthreewithrules.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entitiesWAssoc")
    public List<RLThreeWRulesEntitiesWAssociationsIProjection> getAllEntitiesWassociations() {
        return entityRepo.getAllEntitiesWAssociations();
    }

    @GetMapping("/entitiesWAssoc/entityId/{id}")
    public List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWassociationsByEntityId(@PathVariable("id") Integer entityId) {
        return entityRepo.getEntitiesWAssociationsByEntityId(entityId);
    }

    @GetMapping("/entitiesWAssoc/thru")
    public List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWassociationsThatAreThru() {
        return entityRepo.getEntitiesWithAssociationsNoLongerInUse();
    }

    @GetMapping("/entitiesWAssoc/notThru")
    public List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWassociationsThatAreNotThru() {
        return entityRepo.getEntitiesWithAssociationsStillInUse();
    }

    @GetMapping("/entitiesWAssoc/typeName/{name}")
    public List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWassociationsByTypeName(@PathVariable("name") String typeName) {
        return entityRepo.getEntitiesWithAssociationsByTypeName(typeName);
    }

    @GetMapping("/entitiesWAssoc/{startDate}/{endDate}")
    public List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWassociationsWithFromBetween(@PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start, @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end) {
        return entityRepo.getEntitiesWithAssociationsWhereFromDateBetween(start, end);
    }

    @GetMapping("/entitiesWAssoc/ruleName/{name}")
    public List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWassociationsByRuleName(@PathVariable("name") String ruleName) {
        return entityRepo.getEntitiesWAssociationsByRuleName(ruleName);
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }
}
