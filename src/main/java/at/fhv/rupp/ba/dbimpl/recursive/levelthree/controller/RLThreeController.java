package at.fhv.rupp.ba.dbimpl.recursive.levelthree.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.datagen.RLThreeJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityWAssociationsIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntity;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssoc;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityType;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.exceptions.RLThreeEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.exceptions.RLThreeEntityTypeNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.custom.RLThreeEntityMapper;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.custom.RLThreeEntityTypeMapper;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityAssocRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityAssocTypeRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityTypeRepo;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/recursion/L3")
public class RLThreeController {
    private final RLThreeEntityTypeRepo entityTypeRepo;
    private final RLThreeEntityRepo entityRepo;
    private final RLThreeEntityAssocRepo assocRepo;
    private final RLThreeEntityAssocTypeRepo assocTypeRepo;
    private final DataGenJavaFaker faker;

    public RLThreeController(RLThreeEntityTypeRepo entityTypeRepo, RLThreeEntityRepo entityRepo, RLThreeEntityAssocRepo assocRepo, RLThreeEntityAssocTypeRepo assocTypeRepo) {
        this.entityTypeRepo = entityTypeRepo;
        this.entityRepo = entityRepo;
        this.assocRepo = assocRepo;
        this.assocTypeRepo = assocTypeRepo;
        this.faker = new RLThreeJavaFaker(entityRepo, entityTypeRepo, assocRepo, assocTypeRepo);
    }

    @GetMapping("/entity/{id}")
    public ResponseEntity<RLThreeEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws RLThreeEntityNotFoundException {
        RLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLThreeEntityNotFoundException(getExceptionString("RLThreeEntity", entityId)));
        return ResponseEntity.ok().body(RLThreeEntityMapper.toDTO(entity));
    }

    @GetMapping("/entities/all")
    public List<RLThreeEntityDTO> getAllEntities() {
        return RLThreeEntityMapper.toDTOs(entityRepo.findAll());
    }

    @PostMapping("/entities")
    public RLThreeEntityDTO createEntity(@Valid @RequestBody RLThreeEntityDTO entityDTO) {
        return RLThreeEntityMapper.toDTO(entityRepo.save(RLThreeEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<RLThreeEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLThreeEntityDTO entityDetails) throws RLThreeEntityNotFoundException {
        RLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLThreeEntityNotFoundException(getExceptionString("RLThreeEntity", entityId)));
        RLThreeEntityDTO entityDTO = RLThreeEntityMapper.toDTO(entity);
        if (entityDetails.getType() != null) {
            entityDTO.setType(entityDetails.getType());
        }
        final RLThreeEntityDTO updatedEntity = RLThreeEntityMapper.toDTO(entityRepo.save(RLThreeEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws RLThreeEntityNotFoundException {
        RLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLThreeEntityNotFoundException(getExceptionString("RLThreeEntity", entityId)));
        List<RLThreeEntityAssoc> assocs = assocRepo.getAssociationsByFromEntityId(entityId);
        for (RLThreeEntityAssoc assoc : assocs) {
            assocRepo.delete(assoc);
        }
        assocs = assocRepo.getAssociationsByToEntityId(entityId);
        for (RLThreeEntityAssoc assoc : assocs) {
            assocRepo.delete(assoc);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/type/{id}")
    public ResponseEntity<RLThreeEntityTypeDTO> getEntityTypeById(@PathVariable(value = "id") Integer entityId) throws RLThreeEntityTypeNotFoundException {
        RLThreeEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLThreeEntityTypeNotFoundException(getExceptionString("RLThreeEntityType", entityId)));
        return ResponseEntity.ok().body(RLThreeEntityTypeMapper.toDTO(entity));
    }

    @GetMapping("/types/all")
    public List<RLThreeEntityTypeDTO> getAllEntityTypes() {
        return RLThreeEntityTypeMapper.toDTOs(entityTypeRepo.findAll());
    }

    @PostMapping("/types")
    public RLThreeEntityTypeDTO createEntityType(@Valid @RequestBody RLThreeEntityTypeDTO entityDTO) {
        return RLThreeEntityTypeMapper.toDTO(entityTypeRepo.save(RLThreeEntityTypeMapper.toEntity(entityDTO)));
    }

    @PutMapping("/types/{id}")
    public ResponseEntity<RLThreeEntityTypeDTO> updateEntityType(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLThreeEntityTypeDTO entityDetails) throws RLThreeEntityTypeNotFoundException {
        RLThreeEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLThreeEntityTypeNotFoundException(getExceptionString("RLThreeEntityType", entityId)));
        RLThreeEntityTypeDTO entityDTO = RLThreeEntityTypeMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        if (entityDetails.getParent() != null) {
            entityDTO.setParent(entityDetails.getParent());
        }
        final RLThreeEntityTypeDTO updatedEntity = RLThreeEntityTypeMapper.toDTO(entityTypeRepo.save(RLThreeEntityTypeMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/types/{id}")
    public Map<String, Boolean> deleteEntityType(@PathVariable(value = "id") Integer entityId) throws RLThreeEntityTypeNotFoundException {
        RLThreeEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLThreeEntityTypeNotFoundException(getExceptionString("RLThreeEntityType", entityId)));
        List<RLThreeEntityType> childTypes = entityTypeRepo.getTypesWhereTypeIsParent(entityId);
        for (RLThreeEntityType childType : childTypes) {
            childType.setRlThreeEntityTypeParent(null);
            entityTypeRepo.save(childType);
        }
        List<RLThreeEntity> refEntities = entityRepo.getEntitiesByTypeId(entityId);
        for (RLThreeEntity rlThreeEntity : refEntities) {
            rlThreeEntity.setRlThreeEntityType(null);
            entityRepo.save(rlThreeEntity);
        }
        entityTypeRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/recursive/recursivelevelthree.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entitiesWAssoc")
    public List<RLThreeEntityWAssociationsIProjection> getAllEntitiesWassociations() {
        return entityRepo.getEntitiesWithAssociations();
    }

    @GetMapping("/entitiesWAssoc/entityId/{id}")
    public List<RLThreeEntityWAssociationsIProjection> getEntitiesWassociationsByEntityId(@PathVariable("id") Integer entityId) {
        return entityRepo.getEntitiesWithAssociationsByEntityId(entityId);
    }

    @GetMapping("/entitiesWAssoc/thru")
    public List<RLThreeEntityWAssociationsIProjection> getEntitiesWassociationsThatAreThru() {
        return entityRepo.getEntitiesWithAssociationsNoLongerInUse();
    }

    @GetMapping("/entitiesWAssoc/notThru")
    public List<RLThreeEntityWAssociationsIProjection> getEntitiesWassociationsThatAreNotThru() {
        return entityRepo.getEntitiesWithAssociationsStillInUse();
    }

    @GetMapping("/entitiesWAssoc/typeName/{name}")
    public List<RLThreeEntityWAssociationsIProjection> getEntitiesWassociationsByTypeName(@PathVariable("name") String typeName) {
        return entityRepo.getEntitiesWithAssociationsByTypeName(typeName);
    }

    @GetMapping("/entitiesWAssoc/{startDate}/{endDate}")
    public List<RLThreeEntityWAssociationsIProjection> getEntitiesWassociationsWithFromBetween(@PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start, @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end) {
        return entityRepo.getEntitiesWithAssociationsWhereFromDateBetween(start, end);
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }
}
