package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeRollupDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryTypeRollup;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLThreeWRollupsEntityCategoryTypeMapstructMapper.class, CLThreeWRollupsEntityCategoryTypeRollupTypeMapstructMapper.class})
public interface CLThreeWRollupsEntityCategoryTypeRollupMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryTypeRollupId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupFromDate", target = "from")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupThruDate", target = "thru")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupType", target = "type")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupParentCategory", target = "parent")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupChildCategory", target = "child")
    CLThreeWRollupsEntityCategoryTypeRollupDTO toDTO(CLThreeWRollupsEntityCategoryTypeRollup entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategoryTypeRollup toEntity(CLThreeWRollupsEntityCategoryTypeRollupDTO dto);

    @Mapping(source = "clthreewrEntityCategoryTypeRollupId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupFromDate", target = "from")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupThruDate", target = "thru")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupType", target = "type")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupParentCategory", target = "parent")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupChildCategory", target = "child")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategoryTypeRollup entity, @MappingTarget CLThreeWRollupsEntityCategoryTypeRollupDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryTypeRollupDTO dto, @MappingTarget CLThreeWRollupsEntityCategoryTypeRollup entity);

    List<CLThreeWRollupsEntityCategoryTypeRollupDTO> toDTOs(List<CLThreeWRollupsEntityCategoryTypeRollup> entities);

    List<CLThreeWRollupsEntityCategoryTypeRollup> toEntities(List<CLThreeWRollupsEntityCategoryTypeRollupDTO> dtos);
}
