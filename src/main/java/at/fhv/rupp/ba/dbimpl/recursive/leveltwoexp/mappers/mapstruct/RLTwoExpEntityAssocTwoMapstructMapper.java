package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityAssocTwoDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocTwo;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {RLTwoExpEntityMapstructMapper.class, RLTwoExpEntityAssocTypeMapstructMapper.class})
public interface RLTwoExpEntityAssocTwoMapstructMapper {
    @Mapping(source = "rlTwoExpEntityAssocTwoId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocTwoFromDate", target = "fromDate")
    @Mapping(source = "rlTwoExpEntityAssocTwoThruDate", target = "thruDate")
    @Mapping(source = "rlTwoExpEntityAssocTwoFromEntity", target = "fromEntity")
    @Mapping(source = "rlTwoExpEntityAssocTwoToEntity", target = "toEntity")
    @Mapping(source = "rlTwoExpEntityAssocTwoType", target = "type")
    RLTwoExpEntityAssocTwoDTO toDTO(RLTwoExpEntityAssocTwo entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLTwoExpEntityAssocTwo toEntity(RLTwoExpEntityAssocTwoDTO dto);

    @Mapping(source = "rlTwoExpEntityAssocTwoId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocTwoFromDate", target = "fromDate")
    @Mapping(source = "rlTwoExpEntityAssocTwoThruDate", target = "thruDate")
    @Mapping(source = "rlTwoExpEntityAssocTwoFromEntity", target = "fromEntity")
    @Mapping(source = "rlTwoExpEntityAssocTwoToEntity", target = "toEntity")
    @Mapping(source = "rlTwoExpEntityAssocTwoType", target = "type")
    void updateEntityFromDTO(RLTwoExpEntityAssocTwo entity, @MappingTarget RLTwoExpEntityAssocTwoDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLTwoExpEntityAssocTwoDTO dto, @MappingTarget RLTwoExpEntityAssocTwo entity);

    List<RLTwoExpEntityAssocTwoDTO> toDTOs(List<RLTwoExpEntityAssocTwo> entities);

    List<RLTwoExpEntityAssocTwo> toEntities(List<RLTwoExpEntityAssocTwoDTO> dtos);
}
