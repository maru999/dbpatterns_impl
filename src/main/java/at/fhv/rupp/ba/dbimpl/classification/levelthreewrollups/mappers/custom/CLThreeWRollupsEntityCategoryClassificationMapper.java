package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryClassificationDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryClassification;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryClassificationMapper {

    private CLThreeWRollupsEntityCategoryClassificationMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryClassificationDTO toDTO(CLThreeWRollupsEntityCategoryClassification entity) {
        CLThreeWRollupsEntityCategoryClassificationDTO dto = new CLThreeWRollupsEntityCategoryClassificationDTO();
        dto.setId(entity.getClthreewrEntityCategoryClassificationId());
        dto.setFrom(entity.getClthreewrEntityCategoryClassificationFromDate());
        if (entity.getClthreewrEntityCategoryClassificationThruDate() != null) {
            dto.setThru(entity.getClthreewrEntityCategoryClassificationThruDate());
        }
        if (entity.getClthreewrEntityCategoryClassifciationEntity() != null) {
            dto.setEntity(CLThreeWRollupsEntityMapper.toDTO(entity.getClthreewrEntityCategoryClassifciationEntity()));
        }
        if (entity.getClthreewrEntityCategoryClassificationEntityCategory() != null) {
            dto.setCategory(CLThreeWRollupsEntityCategoryMapper.toDTO(entity.getClthreewrEntityCategoryClassificationEntityCategory()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategoryClassification toEntity(CLThreeWRollupsEntityCategoryClassificationDTO dto) {
        CLThreeWRollupsEntityCategoryClassification entity = new CLThreeWRollupsEntityCategoryClassification();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryClassificationId(dto.getId());
        }
        entity.setClthreewrEntityCategoryClassificationFromDate(dto.getFrom());
        if (dto.getThru() != null) {
            entity.setClthreewrEntityCategoryClassificationThruDate(dto.getThru());
        }
        if (dto.getEntity() != null) {
            entity.setClthreewrEntityCategoryClassifciationEntity(CLThreeWRollupsEntityMapper.toEntity(dto.getEntity()));
        }
        if (dto.getCategory() != null) {
            entity.setClthreewrEntityCategoryClassificationEntityCategory(CLThreeWRollupsEntityCategoryMapper.toEntity(dto.getCategory()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryClassificationDTO> toDTOs(List<CLThreeWRollupsEntityCategoryClassification> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryClassificationDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryClassification entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategoryClassification> toEntities(List<CLThreeWRollupsEntityCategoryClassificationDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategoryClassification> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryClassificationDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
