package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "rltwoxp_entity_assoc_two")
@EntityListeners(AuditingEntityListener.class)
public class RLTwoExpEntityAssocTwo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rltwoxp_entity_assoc_two_id")
    private int rlTwoExpEntityAssocTwoId;

    @Column(name = "rltwoxp_entity_assoc_two_from_date", nullable = false)
    private LocalDate rlTwoExpEntityAssocTwoFromDate;

    @Column(name = "rltwoxp_entity_assoc_two_thru_date")
    private LocalDate rlTwoExpEntityAssocTwoThruDate;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_assoc_two_from_entity")
    private RLTwoExpEntity rlTwoExpEntityAssocTwoFromEntity;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_assoc_two_to_entity")
    private RLTwoExpEntity rlTwoExpEntityAssocTwoToEntity;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_assoc_two_type")
    private RLTwoExpEntityAssocType rlTwoExpEntityAssocTwoType;

    public int getRlTwoExpEntityAssocTwoId() {
        return rlTwoExpEntityAssocTwoId;
    }

    public void setRlTwoExpEntityAssocTwoId(int rlTwoExpEntityAssocTwoId) {
        this.rlTwoExpEntityAssocTwoId = rlTwoExpEntityAssocTwoId;
    }

    public LocalDate getRlTwoExpEntityAssocTwoFromDate() {
        return rlTwoExpEntityAssocTwoFromDate;
    }

    public void setRlTwoExpEntityAssocTwoFromDate(LocalDate rlTwoExpEntityAssocTwoFromDate) {
        this.rlTwoExpEntityAssocTwoFromDate = rlTwoExpEntityAssocTwoFromDate;
    }

    public LocalDate getRlTwoExpEntityAssocTwoThruDate() {
        return rlTwoExpEntityAssocTwoThruDate;
    }

    public void setRlTwoExpEntityAssocTwoThruDate(LocalDate rlTwoExpEntityAssocTwoThruDate) {
        this.rlTwoExpEntityAssocTwoThruDate = rlTwoExpEntityAssocTwoThruDate;
    }

    public RLTwoExpEntity getRlTwoExpEntityAssocTwoFromEntity() {
        return rlTwoExpEntityAssocTwoFromEntity;
    }

    public void setRlTwoExpEntityAssocTwoFromEntity(RLTwoExpEntity rlTwoExpEntityAssocTwoFromEntity) {
        this.rlTwoExpEntityAssocTwoFromEntity = rlTwoExpEntityAssocTwoFromEntity;
    }

    public RLTwoExpEntity getRlTwoExpEntityAssocTwoToEntity() {
        return rlTwoExpEntityAssocTwoToEntity;
    }

    public void setRlTwoExpEntityAssocTwoToEntity(RLTwoExpEntity rlTwoExpEntityAssocTwoToEntity) {
        this.rlTwoExpEntityAssocTwoToEntity = rlTwoExpEntityAssocTwoToEntity;
    }

    public RLTwoExpEntityAssocType getRlTwoExpEntityAssocTwoType() {
        return rlTwoExpEntityAssocTwoType;
    }

    public void setRlTwoExpEntityAssocTwoType(RLTwoExpEntityAssocType rlTwoExpEntityAssocTwoType) {
        this.rlTwoExpEntityAssocTwoType = rlTwoExpEntityAssocTwoType;
    }
}
