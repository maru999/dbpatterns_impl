package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLThreeEntityTypeMapstructMapper {
    @Mapping(source = "rlThreeEntityTypeId", target = "id")
    @Mapping(source = "rlThreeEntityTypeName", target = "name")
    @Mapping(source = "rlThreeEntityTypeParent", target = "parent")
    RLThreeEntityTypeDTO toDTO(RLThreeEntityType entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeEntityType toEntity(RLThreeEntityTypeDTO dto);

    @Mapping(source = "rlThreeEntityTypeId", target = "id")
    @Mapping(source = "rlThreeEntityTypeName", target = "name")
    @Mapping(source = "rlThreeEntityTypeParent", target = "parent")
    void updateEntityFromDTO(RLThreeEntityType entity, @MappingTarget RLThreeEntityTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeEntityTypeDTO dto, @MappingTarget RLThreeEntityType entity);

    List<RLThreeEntityTypeDTO> toDTOs(List<RLThreeEntityType> entities);

    List<RLThreeEntityType> toEntities(List<RLThreeEntityTypeDTO> dtos);
}
