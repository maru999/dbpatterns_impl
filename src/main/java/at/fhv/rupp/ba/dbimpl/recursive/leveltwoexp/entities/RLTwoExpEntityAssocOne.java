package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "rltwoxp_entity_assoc_one")
@EntityListeners(AuditingEntityListener.class)
public class RLTwoExpEntityAssocOne {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rltwoxp_entity_assoc_one_id")
    private int rlTwoExpEntityAssocOneId;

    @Column(name = "rltwoxp_entity_assoc_one_from_date", nullable = false)
    private LocalDate rlTwoExpEntityAssocOneFromDate;

    @Column(name = "rltwoxp_entity_assoc_one_thru_date")
    private LocalDate rlTwoExpEntityAssocOneThruDate;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_assoc_one_from_entity")
    private RLTwoExpEntity rlTwoExpEntityAssocOneFromEntity;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_assoc_one_to_entity")
    private RLTwoExpEntity rlTwoExpEntityAssocOneToEntity;

    public int getRlTwoExpEntityAssocOneId() {
        return rlTwoExpEntityAssocOneId;
    }

    public void setRlTwoExpEntityAssocOneId(int rlTwoExpEntityAssocOneId) {
        this.rlTwoExpEntityAssocOneId = rlTwoExpEntityAssocOneId;
    }

    public LocalDate getRlTwoExpEntityAssocOneFromDate() {
        return rlTwoExpEntityAssocOneFromDate;
    }

    public void setRlTwoExpEntityAssocOneFromDate(LocalDate rlTwoExpEntityAssocOneFromDate) {
        this.rlTwoExpEntityAssocOneFromDate = rlTwoExpEntityAssocOneFromDate;
    }

    public LocalDate getRlTwoExpEntityAssocOneThruDate() {
        return rlTwoExpEntityAssocOneThruDate;
    }

    public void setRlTwoExpEntityAssocOneThruDate(LocalDate rlTwoExpEntityAssocOneThruDate) {
        this.rlTwoExpEntityAssocOneThruDate = rlTwoExpEntityAssocOneThruDate;
    }

    public RLTwoExpEntity getRlTwoExpEntityAssocOneFromEntity() {
        return rlTwoExpEntityAssocOneFromEntity;
    }

    public void setRlTwoExpEntityAssocOneFromEntity(RLTwoExpEntity rlTwoExpEntityAssocOneFromEntity) {
        this.rlTwoExpEntityAssocOneFromEntity = rlTwoExpEntityAssocOneFromEntity;
    }

    public RLTwoExpEntity getRlTwoExpEntityAssocOneToEntity() {
        return rlTwoExpEntityAssocOneToEntity;
    }

    public void setRlTwoExpEntityAssocOneToEntity(RLTwoExpEntity rlTwoExpEntityAssocOneToEntity) {
        this.rlTwoExpEntityAssocOneToEntity = rlTwoExpEntityAssocOneToEntity;
    }
}
