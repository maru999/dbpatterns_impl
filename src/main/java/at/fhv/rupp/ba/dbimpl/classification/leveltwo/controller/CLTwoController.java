package at.fhv.rupp.ba.dbimpl.classification.leveltwo.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.datagen.CLTwoJavaFaker;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.*;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.*;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.exceptions.*;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom.*;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct.CLTwoEntityMapstructMapper;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.repos.*;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/classification/L2")
public class CLTwoController {

    private final CLTwoEntityRepo entityRepo;
    private final CLTwoEntityTypeOneRepo typeOneRepo;
    private final CLTwoEntityTypeTwoRepo typeTwoRepo;
    private final CLTwoEntityTypeThreeRepo typeThreeRepo;
    private final CLTwoEntityEntityTypeThreeClassificationRepo entityClassificationRepo;
    private final DataGenJavaFaker faker;
    private final CLTwoEntityMapstructMapper mapstructMapper;

    public CLTwoController(CLTwoEntityRepo entityRepo, CLTwoEntityTypeOneRepo typeOneRepo, CLTwoEntityTypeTwoRepo typeTwoRepo, CLTwoEntityTypeThreeRepo typeThreeRepo, CLTwoEntityEntityTypeThreeClassificationRepo entityClassificationRepo, CLTwoEntityMapstructMapper mapstructMapper) {
        this.entityRepo = entityRepo;
        this.typeOneRepo = typeOneRepo;
        this.typeTwoRepo = typeTwoRepo;
        this.typeThreeRepo = typeThreeRepo;
        this.entityClassificationRepo = entityClassificationRepo;
        this.mapstructMapper = mapstructMapper;
        this.faker = new CLTwoJavaFaker(entityRepo, entityClassificationRepo, typeOneRepo, typeTwoRepo, typeThreeRepo);
    }

    @GetMapping("/entities")
    public List<CLTwoEntityDTO> getAllEntities() {
        return CLTwoEntityMapper.toDTOs(entityRepo.findAll());
    }

    @GetMapping("/mapstructEntities")
    public List<CLTwoEntityDTO> getAllEntitiesWMapstruct() {
        return mapstructMapper.toDTOs(entityRepo.findAll());
    }

    @GetMapping("/entitiesWTypes")
    public List<CLTwoEntityNamedTypesIProjection> getAllEntitiesWTypesNamed() {
        return entityRepo.getNamedTypes();
    }

    @GetMapping("/entitiesWTypesNoView")
    public List<CLTwoEntityNamedTypesIProjection> getAllEntitiesWTypesNamedWithoutView() {
        return entityRepo.getNamedTypesWithoutView();
    }

    @GetMapping("/entityWTypesById/{id}")
    public List<CLTwoEntityNamedTypesIProjection> getEntityWTypesById(@PathVariable(value = "id") Integer entityId) {
        return entityRepo.getNamedTypeByEntityId(entityId);
    }

    @GetMapping("/entityWTypesByTypeOneName/{name}")
    public List<CLTwoEntityNamedTypesIProjection> getEntityWTypesByTypeOneName(@PathVariable(value = "name") String typeOneName) {
        return entityRepo.getNamedTypesByTypeOneName(typeOneName);
    }

    @PostMapping("/entities/subtypeOne")
    public CLTwoEntitySubtypeOneDTO createEntitySubTypeOne(@Valid @RequestBody CLTwoEntitySubtypeOneDTO entityDTO) {
        return CLTwoEntitySubtypeOneMapper.toDTO(entityRepo.save(CLTwoEntitySubtypeOneMapper.toEntity(entityDTO)));
    }

    @PostMapping("/entities/subtypeTwo")
    public CLTwoEntitySubtypeTwoDTO createEntitySubTypeTwo(@Valid @RequestBody CLTwoEntitySubtypeTwoDTO entityDTO) {
        return CLTwoEntitySubtypeTwoMapper.toDTO(entityRepo.save(CLTwoEntitySubtypeTwoMapper.toEntity(entityDTO)));
    }

    @PostMapping("/entities/subtypeThree")
    public CLTwoEntitySubtypeThreeDTO createEntitySubTypeThree(@Valid @RequestBody CLTwoEntitySubtypeThreeDTO entityDTO) {
        return CLTwoEntitySubtypeThreeMapper.toDTO(entityRepo.save(CLTwoEntitySubtypeThreeMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/subtypeOne/{id}")
    public ResponseEntity<CLTwoEntityDTO> updateEntitySubTypeOne(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody CLTwoEntitySubtypeOneDTO entityDetails) throws CLTwoEntityNotFoundException {
        CLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLTwoEntityNotFoundException(getExceptionString("CLTwoEntity", entityId)));
        CLTwoEntitySubtypeOneDTO entityDTO = CLTwoEntitySubtypeOneMapper.toDTO((CLTwoEntitySubtypeOne) entity);
        entityDTO.setName(entityDetails.getName());
        entityDTO.setDesc(entityDetails.getDesc());
        entityDTO.setTypeTwo(entityDetails.getTypeTwo());
        entityDTO.setTypeOne(entityDetails.getTypeOne());
        final CLTwoEntityDTO updatedEntity = CLTwoEntityMapper.toDTO(entityRepo.save(CLTwoEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @PutMapping("/entities/subtypeTwo/{id}")
    public ResponseEntity<CLTwoEntityDTO> updateEntitySubTypeTwo(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody CLTwoEntitySubtypeTwoDTO entityDetails) throws CLTwoEntityNotFoundException {
        CLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLTwoEntityNotFoundException(getExceptionString("CLTwoEntity", entityId)));
        CLTwoEntitySubtypeTwoDTO entityDTO = CLTwoEntitySubtypeTwoMapper.toDTO((CLTwoEntitySubtypeTwo) entity);
        entityDTO.setName(entityDetails.getName());
        entityDTO.setAttr(entityDetails.getAttr());
        entityDTO.setTypeTwo(entityDetails.getTypeTwo());
        entityDTO.setTypeOne(entityDetails.getTypeOne());
        final CLTwoEntityDTO updatedEntity = CLTwoEntityMapper.toDTO(entityRepo.save(CLTwoEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @PutMapping("/entities/subtypeThree/{id}")
    public ResponseEntity<CLTwoEntityDTO> updateEntitySubTypeThree(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody CLTwoEntitySubtypeThreeDTO entityDetails) throws CLTwoEntityNotFoundException {
        CLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLTwoEntityNotFoundException(getExceptionString("CLTwoEntity", entityId)));
        CLTwoEntitySubtypeThreeDTO entityDTO = CLTwoEntitySubtypeThreeMapper.toDTO((CLTwoEntitySubtypeThree) entity);
        entityDTO.setName(entityDetails.getName());
        entityDTO.setInfo(entityDetails.getInfo());
        entityDTO.setTypeTwo(entityDetails.getTypeTwo());
        entityDTO.setTypeOne(entityDetails.getTypeOne());
        final CLTwoEntityDTO updatedEntity = CLTwoEntityMapper.toDTO(entityRepo.save(CLTwoEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws CLTwoEntityNotFoundException {
        CLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLTwoEntityNotFoundException(getExceptionString("CLTwoEntity", entityId)));
        List<CLTwoEntityEntityTypeThreeClassification> classifications = entityClassificationRepo.getAllClassificationsByEntityId(entityId);
        for (CLTwoEntityEntityTypeThreeClassification classification : classifications) {
            entityClassificationRepo.delete(classification);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/entity/{id}")
    public ResponseEntity<CLTwoEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws CLTwoEntityNotFoundException {
        CLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLTwoEntityNotFoundException(getExceptionString("CLTwoEntity", entityId)));
        return ResponseEntity.ok().body(CLTwoEntityMapper.toDTO(entity));
    }

    @GetMapping("/typeOne/{id}")
    public ResponseEntity<CLTwoEntityTypeOneDTO> getTypeOneById(@PathVariable(value = "id") Integer typeOneId) throws CLTwoEntityTypeOneNotFoundException {
        CLTwoEntityTypeOne entity = typeOneRepo.findById(typeOneId).orElseThrow(() -> new CLTwoEntityTypeOneNotFoundException(getExceptionString("Type One", typeOneId)));
        return ResponseEntity.ok().body(CLTwoEntityTypeOneMapper.toDTO(entity));
    }

    @GetMapping("/typeTwo/{id}")
    public ResponseEntity<CLTwoEntityTypeTwoDTO> getTypeTwoById(@PathVariable(value = "id") Integer typeTwoId) throws CLTwoEntityTypeTwoNotFoundException {
        CLTwoEntityTypeTwo entity = typeTwoRepo.findById(typeTwoId).orElseThrow(() -> new CLTwoEntityTypeTwoNotFoundException(getExceptionString("TypeTwo", typeTwoId)));
        return ResponseEntity.ok().body(CLTwoEntityTypeTwoMapper.toDTO(entity));
    }

    @GetMapping("/typeThree/{id}")
    public ResponseEntity<CLTwoEntityTypeThreeDTO> getTypeThreeById(@PathVariable(value = "id") Integer typeThreeId) throws CLTwoEntityTypeThreeNotFoundException {
        CLTwoEntityTypeThree entity = typeThreeRepo.findById(typeThreeId).orElseThrow(() -> new CLTwoEntityTypeThreeNotFoundException(getExceptionString("Type Three", typeThreeId)));
        return ResponseEntity.ok().body(CLTwoEntityTypeThreeMapper.toDTO(entity));
    }

    @GetMapping("/classificationTypeThree/{id}")
    public ResponseEntity<CLTwoEntityEntityTypeThreeClassificationDTO> getClassificationById(@PathVariable(value = "id") Integer classificationId) throws CLTwoEntityEntityTypeThreeClassificationNotFoundException {
        CLTwoEntityEntityTypeThreeClassification entity = entityClassificationRepo.findById(classificationId).orElseThrow(() -> new CLTwoEntityEntityTypeThreeClassificationNotFoundException(getExceptionString("Classification", classificationId)));
        return ResponseEntity.ok().body(CLTwoEntityEntityTypeThreeClassificationMapper.toDTO(entity));
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fillDBForCLTwo() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/classification/classificationleveltwo.sql");
        DbimplApplication.restart();
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }
}
