package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLTwoExpEntityTypeMapstructMapper {
    @Mapping(source = "rlTwoExpEntityTypeId", target = "id")
    @Mapping(source = "rlTwoExpEntityTypeName", target = "name")
    @Mapping(source = "rlTwoExpEntityTypeParent", target = "parent")
    RLTwoExpEntityTypeDTO toDTO(RLTwoExpEntityType entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLTwoExpEntityType toEntity(RLTwoExpEntityTypeDTO dto);

    @Mapping(source = "rlTwoExpEntityTypeId", target = "id")
    @Mapping(source = "rlTwoExpEntityTypeName", target = "name")
    @Mapping(source = "rlTwoExpEntityTypeParent", target = "parent")
    void updateEntityFromDTO(RLTwoExpEntityType entity, @MappingTarget RLTwoExpEntityTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLTwoExpEntityTypeDTO dto, @MappingTarget RLTwoExpEntityType entity);

    List<RLTwoExpEntityTypeDTO> toDTOs(List<RLTwoExpEntityType> entities);

    List<RLTwoExpEntityType> toEntities(List<RLTwoExpEntityTypeDTO> dtos);
}
