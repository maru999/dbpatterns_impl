package at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlthree_entity_assoc_type")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeEntityAssocType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthree_entity_assoc_type_id")
    private int rlThreeEntityAssocTypeId;

    @Column(name = "rlthree_entity_assoc_type_name")
    private String rlThreeEntityAssocTypeName;

    @ManyToOne
    @JoinColumn(name = "rlthree_entity_assoc_type_parent")
    private RLThreeEntityAssocType rlThreeEntityAssocTypeParent;

    public int getRlThreeEntityAssocTypeId() {
        return rlThreeEntityAssocTypeId;
    }

    public void setRlThreeEntityAssocTypeId(int rlThreeEntityAssocTypeId) {
        this.rlThreeEntityAssocTypeId = rlThreeEntityAssocTypeId;
    }

    public String getRlThreeEntityAssocTypeName() {
        return rlThreeEntityAssocTypeName;
    }

    public void setRlThreeEntityAssocTypeName(String rlThreeEntityAssocTypeName) {
        this.rlThreeEntityAssocTypeName = rlThreeEntityAssocTypeName;
    }

    public RLThreeEntityAssocType getRlThreeEntityAssocTypeParent() {
        return rlThreeEntityAssocTypeParent;
    }

    public void setRlThreeEntityAssocTypeParent(RLThreeEntityAssocType rlThreeEntityAssocTypeParent) {
        this.rlThreeEntityAssocTypeParent = rlThreeEntityAssocTypeParent;
    }
}
