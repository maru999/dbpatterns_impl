package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntity;

import java.util.LinkedList;
import java.util.List;

public class RLThreeEntityMapper {
    private RLThreeEntityMapper() {
        //hidden constructor
    }

    public static RLThreeEntityDTO toDTO(RLThreeEntity entity) {
        RLThreeEntityDTO dto = new RLThreeEntityDTO();
        dto.setId(entity.getRlThreeEntityId());
        if (entity.getRlThreeEntityType() != null) {
            dto.setType(RLThreeEntityTypeMapper.toDTO(entity.getRlThreeEntityType()));
        }
        return dto;
    }

    public static RLThreeEntity toEntity(RLThreeEntityDTO dto) {
        RLThreeEntity entity = new RLThreeEntity();
        if (dto.getId() != null) {
            entity.setRlThreeEntityId(dto.getId());
        }
        if (dto.getType() != null) {
            entity.setRlThreeEntityType(RLThreeEntityTypeMapper.toEntity(dto.getType()));
        }
        return entity;
    }

    public static List<RLThreeEntityDTO> toDTOs(List<RLThreeEntity> entities) {
        LinkedList<RLThreeEntityDTO> result = new LinkedList<>();
        for (RLThreeEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeEntity> toEntities(List<RLThreeEntityDTO> dtos) {
        LinkedList<RLThreeEntity> result = new LinkedList<>();
        for (RLThreeEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
