package at.fhv.rupp.ba.dbimpl.recursive.levelthree.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntity;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssoc;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssocType;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityType;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityAssocRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityAssocTypeRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos.RLThreeEntityTypeRepo;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class RLThreeJavaFaker extends DataGenJavaFaker {
    private final RLThreeEntityRepo entityRepo;
    private final RLThreeEntityTypeRepo typeRepo;
    private final RLThreeEntityAssocRepo assocRepo;
    private final RLThreeEntityAssocTypeRepo assocTypeRepo;

    public RLThreeJavaFaker(RLThreeEntityRepo entityRepo, RLThreeEntityTypeRepo typeRepo, RLThreeEntityAssocRepo assocRepo, RLThreeEntityAssocTypeRepo assocTypeRepo) {
        this.entityRepo = entityRepo;
        this.typeRepo = typeRepo;
        this.assocRepo = assocRepo;
        this.assocTypeRepo = assocTypeRepo;
    }

    @Override
    public void fillDB() {
        RLThreeEntity entity;
        RLThreeEntityType entityType;
        RLThreeEntityAssoc assoc;
        RLThreeEntityAssocType assocType;
        List<RLThreeEntity> createdEntities = new LinkedList<>();
        List<RLThreeEntityType> createdTypes = new LinkedList<>();
        List<RLThreeEntityAssocType> createdAssocTypes = new LinkedList<>();
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityType = new RLThreeEntityType();
            entityType.setRlThreeEntityTypeName(trimStringIfTooLong(FAKER.ancient().hero(), MIDDLE_CHAR_LIMIT));
            if (!createdTypes.isEmpty()) {
                if (getRandomIntByBound(24) % 2 == 0) {
                    entityType.setRlThreeEntityTypeParent(createdTypes.get(getRandomIntByBound(createdTypes.size())));
                }
            }
            createdTypes.add(entityType);
            typeRepo.save(entityType);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assocType = new RLThreeEntityAssocType();
            assocType.setRlThreeEntityAssocTypeName(trimStringIfTooLong(FAKER.ancient().god(), MIDDLE_CHAR_LIMIT));
            if (!createdAssocTypes.isEmpty()) {
                if (getRandomIntByBound(20) % 2 == 0) {
                    assocType.setRlThreeEntityAssocTypeParent(createdAssocTypes.get(getRandomIntByBound(createdAssocTypes.size())));
                }
            }
            createdAssocTypes.add(assocType);
            assocTypeRepo.save(assocType);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new RLThreeEntity();
            if (!createdTypes.isEmpty()) {
                entity.setRlThreeEntityType(createdTypes.get(getRandomIntByBound(createdTypes.size())));
                createdEntities.add(entity);
                entityRepo.save(entity);
            }
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assoc = new RLThreeEntityAssoc();
            LocalDate from = getDateBetween(START_DATE, END_DATE);
            assoc.setRlThreeEntityAssocFromDate(from);
            if (getRandomIntByBound(33) % 2 == 0) {
                assoc.setRlThreeEntityAssocThruDate(getDateBetween(from, END_DATE));
            }
            if (!createdAssocTypes.isEmpty()) {
                assoc.setRlThreeEntityAssocType(createdAssocTypes.get(getRandomIntByBound(createdAssocTypes.size())));
            }
            if (createdEntities.size() >= 2) {
                RLThreeEntity fromEnt = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                assoc.setTlThreeAssocFromEntity(fromEnt);
                RLThreeEntity toEnt = null;
                while (toEnt == null) {
                    RLThreeEntity temp = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                    if (temp != fromEnt) toEnt = temp;
                }
                assoc.setTlThreeAssocToEntity(toEnt);
            }
            assocRepo.save(assoc);
        }
    }
}
