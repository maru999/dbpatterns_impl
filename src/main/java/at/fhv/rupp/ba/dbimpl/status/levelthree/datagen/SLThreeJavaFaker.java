package at.fhv.rupp.ba.dbimpl.status.levelthree.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntity;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntityStatus;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeStatusType;
import at.fhv.rupp.ba.dbimpl.status.levelthree.repos.SLThreeEntityRepo;
import at.fhv.rupp.ba.dbimpl.status.levelthree.repos.SLThreeEntityStatusRepo;
import at.fhv.rupp.ba.dbimpl.status.levelthree.repos.SLThreeStatusTypeRepo;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class SLThreeJavaFaker extends DataGenJavaFaker {
    private final SLThreeEntityRepo entityRepo;
    private final SLThreeEntityStatusRepo statusRepo;
    private final SLThreeStatusTypeRepo statusTypeRepo;

    public SLThreeJavaFaker(SLThreeEntityRepo entityRepo, SLThreeEntityStatusRepo statusRepo, SLThreeStatusTypeRepo statusTypeRepo) {
        this.entityRepo = entityRepo;
        this.statusRepo = statusRepo;
        this.statusTypeRepo = statusTypeRepo;
    }

    @Override
    public void fillDB() {
        SLThreeEntity entity;
        SLThreeStatusType statusType;
        SLThreeEntityStatus entityStatus;
        List<SLThreeEntity> createdEntities = new LinkedList<>();
        List<SLThreeStatusType> createdStatusTypes = new LinkedList<>();
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            statusType = new SLThreeStatusType();
            statusType.setSlThreeStatusTypeName(trimStringIfTooLong(FAKER.zelda().character(), LOW_CHAR_LIMIT));
            createdStatusTypes.add(statusType);
            statusTypeRepo.save(statusType);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new SLThreeEntity();
            createdEntities.add(entity);
            entityRepo.save(entity);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entityStatus = new SLThreeEntityStatus();
            LocalDate fromDate = getDateBetween(START_DATE, END_DATE);
            entityStatus.setSlThreeEntityStatusFromDate(fromDate);
            if (getRandomIntByBound(10) % 2 == 0) {
                entityStatus.setSlThreeEntityStatusThruDate(getDateBetween(fromDate, END_DATE));
            }
            LocalDate fromStatusDate = getDateBetween(START_DATE, END_DATE);
            entityStatus.setSlThreeEntityStatusStatusFromDate(fromStatusDate);
            if (getRandomIntByBound(20) % 2 == 0) {
                entityStatus.setSlThreeEntityStatusStatusThruDate(getDateBetween(fromStatusDate, END_DATE));
            }
            entityStatus.setSlThreeEntityStatusDateTime(getDateTimeBetween(fromStatusDate.atStartOfDay(), END_DATETIME));
            if (!createdEntities.isEmpty()) {
                entityStatus.setSlThreeEntityStatusEntity(createdEntities.get(getRandomIntByBound(createdEntities.size())));
            }
            if (!createdStatusTypes.isEmpty()) {
                entityStatus.setSlThreeEntityStatusStatusType(createdStatusTypes.get(getRandomIntByBound(createdStatusTypes.size())));
            }
            statusRepo.save(entityStatus);
        }
    }
}
