package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.*;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.repos.*;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class RLThreeWRulesJavaFaker extends DataGenJavaFaker {
    private final RLThreeWRulesEntityRepo entityRepo;
    private final RLThreeWRulesEntityTypeRepo typeRepo;
    private final RLThreeWRulesEntityAssocRepo assocRepo;
    private final RLThreeWRulesEntityAssocTypeRepo assocTypeRepo;
    private final RLThreeWRulesEntityAssocRuleRepo ruleRepo;

    public RLThreeWRulesJavaFaker(RLThreeWRulesEntityRepo entityRepo, RLThreeWRulesEntityTypeRepo typeRepo, RLThreeWRulesEntityAssocRepo assocRepo, RLThreeWRulesEntityAssocTypeRepo assocTypeRepo, RLThreeWRulesEntityAssocRuleRepo ruleRepo) {
        this.entityRepo = entityRepo;
        this.typeRepo = typeRepo;
        this.assocRepo = assocRepo;
        this.assocTypeRepo = assocTypeRepo;
        this.ruleRepo = ruleRepo;
    }

    @Override
    public void fillDB() {
        RLThreeWRulesEntity entity;
        RLThreeWRulesEntityType entityType;
        RLThreeWRulesEntityAssoc assoc;
        RLThreeWRulesEntityAssocType assocType;
        RLThreeWRulesEntityAssocRule assocRule;
        List<RLThreeWRulesEntity> createdEntities = new LinkedList<>();
        List<RLThreeWRulesEntityType> createdTypes = new LinkedList<>();
        List<RLThreeWRulesEntityAssocRule> createdRules = new LinkedList<>();
        List<RLThreeWRulesEntityAssocType> createdAssocTypes = new LinkedList<>();

        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assocRule = new RLThreeWRulesEntityAssocRule();
            assocRule.setRlThreeWRulesEntityAssocRuleName(trimStringIfTooLong(FAKER.ancient().titan(), MIDDLE_CHAR_LIMIT));
            createdRules.add(assocRule);
            ruleRepo.save(assocRule);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityType = new RLThreeWRulesEntityType();
            entityType.setRlThreeWRulesEntityTypeName(trimStringIfTooLong(FAKER.ancient().hero(), MIDDLE_CHAR_LIMIT));
            if (!createdTypes.isEmpty()) {
                if (getRandomIntByBound(24) % 2 == 0) {
                    entityType.setRlThreeWRulesEntityTypeParent(createdTypes.get(getRandomIntByBound(createdTypes.size())));
                }
            }
            createdTypes.add(entityType);
            typeRepo.save(entityType);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assocType = new RLThreeWRulesEntityAssocType();
            assocType.setRlThreeWRulesEntityAssocTypeName(trimStringIfTooLong(FAKER.ancient().god(), MIDDLE_CHAR_LIMIT));
            if (!createdAssocTypes.isEmpty()) {
                if (getRandomIntByBound(20) % 2 == 0) {
                    assocType.setRlThreeWRulesEntityAssocTypeParent(createdAssocTypes.get(getRandomIntByBound(createdAssocTypes.size())));
                }
            }
            createdAssocTypes.add(assocType);
            assocTypeRepo.save(assocType);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new RLThreeWRulesEntity();
            if (!createdTypes.isEmpty()) {
                entity.setRlThreeWRulesEntityType(createdTypes.get(getRandomIntByBound(createdTypes.size())));
                createdEntities.add(entity);
                entityRepo.save(entity);
            }
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assoc = new RLThreeWRulesEntityAssoc();
            LocalDate from = getDateBetween(START_DATE, END_DATE);
            assoc.setRlThreeWRulesEntityAssocFromDate(from);
            if (getRandomIntByBound(33) % 2 == 0) {
                assoc.setRlThreeWRulesEntityAssocThruDate(getDateBetween(from, END_DATE));
            }
            if (!createdAssocTypes.isEmpty()) {
                assoc.setRlThreeWRulesEntityAssocType(createdAssocTypes.get(getRandomIntByBound(createdAssocTypes.size())));
            }
            if (createdEntities.size() >= 2) {
                RLThreeWRulesEntity fromEnt = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                assoc.setRlThreeWRulesAssocFromEntity(fromEnt);
                RLThreeWRulesEntity toEnt = null;
                while (toEnt == null) {
                    RLThreeWRulesEntity temp = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                    if (temp != fromEnt) toEnt = temp;
                }
                assoc.setRlThreeWRulesAssocToEntity(toEnt);
            }
            if (!createdRules.isEmpty()) {
                assoc.setRlThreeWRulesEntityAssocRule(createdRules.get(getRandomIntByBound(createdRules.size())));
            }
            assocRepo.save(assoc);
        }
    }
}
