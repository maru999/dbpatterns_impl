package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityAssocTwoDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocTwo;

import java.util.LinkedList;
import java.util.List;

public class RLTwoExpEntityAssocTwoMapper {
    private RLTwoExpEntityAssocTwoMapper() {
        //hidden constructor
    }

    public static RLTwoExpEntityAssocTwoDTO toDTO(RLTwoExpEntityAssocTwo entity) {
        RLTwoExpEntityAssocTwoDTO dto = new RLTwoExpEntityAssocTwoDTO();
        dto.setId(entity.getRlTwoExpEntityAssocTwoId());
        dto.setFromDate(entity.getRlTwoExpEntityAssocTwoFromDate());
        if (entity.getRlTwoExpEntityAssocTwoThruDate() != null) {
            dto.setThruDate(entity.getRlTwoExpEntityAssocTwoThruDate());
        }
        if (entity.getRlTwoExpEntityAssocTwoFromEntity() != null) {
            dto.setFromEntity(RLTwoExpEntityMapper.toDTO(entity.getRlTwoExpEntityAssocTwoFromEntity()));
        }
        if (entity.getRlTwoExpEntityAssocTwoToEntity() != null) {
            dto.setToEntity(RLTwoExpEntityMapper.toDTO(entity.getRlTwoExpEntityAssocTwoToEntity()));
        }
        if (entity.getRlTwoExpEntityAssocTwoType() != null) {
            dto.setType(RLTwoExpEntityAssocTypeMapper.toDTO(entity.getRlTwoExpEntityAssocTwoType()));
        }
        return dto;
    }

    public static RLTwoExpEntityAssocTwo toEntity(RLTwoExpEntityAssocTwoDTO dto) {
        RLTwoExpEntityAssocTwo entity = new RLTwoExpEntityAssocTwo();
        if (dto.getId() != null) {
            entity.setRlTwoExpEntityAssocTwoId(dto.getId());
        }
        if (dto.getFromDate() != null) {
            entity.setRlTwoExpEntityAssocTwoFromDate(dto.getFromDate());
        }
        if (dto.getThruDate() != null) {
            entity.setRlTwoExpEntityAssocTwoThruDate(dto.getThruDate());
        }
        if (dto.getFromEntity() != null) {
            entity.setRlTwoExpEntityAssocTwoFromEntity(RLTwoExpEntityMapper.toEntity(dto.getFromEntity()));
        }
        if (dto.getToEntity() != null) {
            entity.setRlTwoExpEntityAssocTwoToEntity(RLTwoExpEntityMapper.toEntity(dto.getToEntity()));
        }
        if (dto.getType() != null) {
            entity.setRlTwoExpEntityAssocTwoType(RLTwoExpEntityAssocTypeMapper.toEntity(dto.getType()));
        }
        return entity;
    }

    public static List<RLTwoExpEntityAssocTwoDTO> toDTOs(List<RLTwoExpEntityAssocTwo> entities) {
        LinkedList<RLTwoExpEntityAssocTwoDTO> result = new LinkedList<>();
        for (RLTwoExpEntityAssocTwo entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLTwoExpEntityAssocTwo> toEntities(List<RLTwoExpEntityAssocTwoDTO> dtos) {
        LinkedList<RLTwoExpEntityAssocTwo> result = new LinkedList<>();
        for (RLTwoExpEntityAssocTwoDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
