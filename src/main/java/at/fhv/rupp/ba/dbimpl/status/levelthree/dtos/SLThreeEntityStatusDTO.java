package at.fhv.rupp.ba.dbimpl.status.levelthree.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SLThreeEntityStatusDTO {
    private Integer id;
    private LocalDateTime statusDateTime;
    private LocalDate statusFrom;
    private LocalDate statusThru;
    private LocalDate entityStatusFrom;
    private LocalDate entityStatusThru;
    private SLThreeStatusTypeDTO statusType;
    private SLThreeEntityDTO entity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getStatusDateTime() {
        return statusDateTime;
    }

    public void setStatusDateTime(LocalDateTime statusDateTime) {
        this.statusDateTime = statusDateTime;
    }

    public LocalDate getStatusFrom() {
        return statusFrom;
    }

    public void setStatusFrom(LocalDate statusFrom) {
        this.statusFrom = statusFrom;
    }

    public LocalDate getStatusThru() {
        return statusThru;
    }

    public void setStatusThru(LocalDate statusThru) {
        this.statusThru = statusThru;
    }

    public LocalDate getEntityStatusFrom() {
        return entityStatusFrom;
    }

    public void setEntityStatusFrom(LocalDate entityStatusFrom) {
        this.entityStatusFrom = entityStatusFrom;
    }

    public LocalDate getEntityStatusThru() {
        return entityStatusThru;
    }

    public void setEntityStatusThru(LocalDate entityStatusThru) {
        this.entityStatusThru = entityStatusThru;
    }

    public SLThreeStatusTypeDTO getStatusType() {
        return statusType;
    }

    public void setStatusType(SLThreeStatusTypeDTO statusType) {
        this.statusType = statusType;
    }

    public SLThreeEntityDTO getEntity() {
        return entity;
    }

    public void setEntity(SLThreeEntityDTO entity) {
        this.entity = entity;
    }
}
