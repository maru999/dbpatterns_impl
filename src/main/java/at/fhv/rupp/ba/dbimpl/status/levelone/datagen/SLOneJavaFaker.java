package at.fhv.rupp.ba.dbimpl.status.levelone.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.levelone.entities.SLOneEntity;
import at.fhv.rupp.ba.dbimpl.status.levelone.repos.SLOneEntityRepo;

import java.time.LocalDate;

public class SLOneJavaFaker extends DataGenJavaFaker {
    private final SLOneEntityRepo entityRepo;

    public SLOneJavaFaker(SLOneEntityRepo entityRepo) {
        this.entityRepo = entityRepo;
    }

    @Override
    public void fillDB() {
        SLOneEntity entity;
        for (int i = 0; i < MIDDLE_GEN_AMOUNT; i++) {
            entity = new SLOneEntity();
            LocalDate fromDate = getDateBetween(START_DATE, END_DATE);
            entity.setSlOneEntityEventFromDate(fromDate);
            if(getRandomIntByBound(20) % 2 == 0) {
                entity.setSlOneEntityEventThruDate(getDateBetween(fromDate, END_DATE));
            }
            entity.setSlOneEntityEventIndicator(trimStringIfTooLong(FAKER.backToTheFuture().character(), MIDDLE_CHAR_LIMIT));
            entity.setSlOneEntityEventOneDateTime(getDateTimeBetween(START_DATETIME, END_DATETIME));
            entity.setSlOneEntityEventTwoDateTime(getDateTimeBetween(START_DATETIME, END_DATETIME));
            entity.setSlOneEntityEventThreeDateTime(getDateTimeBetween(START_DATETIME, END_DATETIME));
            entityRepo.save(entity);
        }
    }
}
