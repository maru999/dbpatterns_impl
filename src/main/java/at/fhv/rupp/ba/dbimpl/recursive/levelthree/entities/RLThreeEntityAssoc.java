package at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "rlthree_entity_assoc")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeEntityAssoc {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthree_entity_assoc_id")
    private int rlThreeEntityAssocId;

    @Column(name = "rlthree_entity_assoc_from_date", nullable = false)
    private LocalDate rlThreeEntityAssocFromDate;

    @Column(name = "rlthree_entity_assoc_thru_date")
    private LocalDate rlThreeEntityAssocThruDate;

    @ManyToOne
    @JoinColumn(name = "rlthree_entity_assoc_type_fk")
    private RLThreeEntityAssocType rlThreeEntityAssocType;

    @ManyToOne
    @JoinColumn(name = "rlthree_entity_assoc_from_ent")
    private RLThreeEntity tlThreeAssocFromEntity;

    @ManyToOne
    @JoinColumn(name = "rlthree_entity_assoc_to_ent")
    private RLThreeEntity tlThreeAssocToEntity;

    public int getRlThreeEntityAssocId() {
        return rlThreeEntityAssocId;
    }

    public void setRlThreeEntityAssocId(int rlThreeEntityAssocId) {
        this.rlThreeEntityAssocId = rlThreeEntityAssocId;
    }

    public LocalDate getRlThreeEntityAssocFromDate() {
        return rlThreeEntityAssocFromDate;
    }

    public void setRlThreeEntityAssocFromDate(LocalDate rlThreeEntityAssocFromDate) {
        this.rlThreeEntityAssocFromDate = rlThreeEntityAssocFromDate;
    }

    public LocalDate getRlThreeEntityAssocThruDate() {
        return rlThreeEntityAssocThruDate;
    }

    public void setRlThreeEntityAssocThruDate(LocalDate rlThreeEntityAssocThruDate) {
        this.rlThreeEntityAssocThruDate = rlThreeEntityAssocThruDate;
    }

    public RLThreeEntityAssocType getRlThreeEntityAssocType() {
        return rlThreeEntityAssocType;
    }

    public void setRlThreeEntityAssocType(RLThreeEntityAssocType rlThreeEntityAssocType) {
        this.rlThreeEntityAssocType = rlThreeEntityAssocType;
    }

    public RLThreeEntity getTlThreeAssocFromEntity() {
        return tlThreeAssocFromEntity;
    }

    public void setTlThreeAssocFromEntity(RLThreeEntity tlThreeAssocFromEntity) {
        this.tlThreeAssocFromEntity = tlThreeAssocFromEntity;
    }

    public RLThreeEntity getTlThreeAssocToEntity() {
        return tlThreeAssocToEntity;
    }

    public void setTlThreeAssocToEntity(RLThreeEntity tlThreeAssocToEntity) {
        this.tlThreeAssocToEntity = tlThreeAssocToEntity;
    }
}
