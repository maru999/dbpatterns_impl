package at.fhv.rupp.ba.dbimpl.status.levelone.exceptions;

public class SLOneEntityNotFoundException extends Exception {
    public SLOneEntityNotFoundException(String message) {
        super(message);
    }
}
