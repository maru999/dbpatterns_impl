package at.fhv.rupp.ba.dbimpl.classification.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeTwo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLTwoEntityTypeTwoRepo extends JpaRepository<CLTwoEntityTypeTwo, Integer> {
}
