package at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos;

public abstract class CLTwoEntityDTO {

    private Integer id;
    private String name;
    private CLTwoEntityTypeOneDTO typeOne;
    private CLTwoEntityTypeTwoDTO typeTwo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLTwoEntityTypeOneDTO getTypeOne() {
        return typeOne;
    }

    public void setTypeOne(CLTwoEntityTypeOneDTO typeOne) {
        this.typeOne = typeOne;
    }

    public CLTwoEntityTypeTwoDTO getTypeTwo() {
        return typeTwo;
    }

    public void setTypeTwo(CLTwoEntityTypeTwoDTO typeTwo) {
        this.typeTwo = typeTwo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
