package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.repos;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpAssociatedEntitiesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpFromEntitiesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface RLTwoExpEntityRepo extends JpaRepository<RLTwoExpEntity, Integer> {

    @Query(value = "SELECT * FROM rltwoxp_associated_entities", nativeQuery = true)
    List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesWithAssociations();

    @Query(value = "SELECT * FROM rltwoxp_associated_entities WHERE rltwoxp_entity_id = :id", nativeQuery = true)
    List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesAssociatedToEntityById(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rltwoxp_associated_entities WHERE rltwoxp_entity_assoc_one_from_entity = :id", nativeQuery = true)
    List<RLTwoExpAssociatedEntitiesIProjection> getAllAssociatedEntitiesWithAssocOneFromEntity(@Param("id") Integer fromId);

    @Query(value = "SELECT * FROM rltwoxp_associated_entities WHERE rltwoxp_entity_assoc_one_to_entity = :id", nativeQuery = true)
    List<RLTwoExpAssociatedEntitiesIProjection> getAllAssociatedEntitiesWithAssocOneToEntity(@Param("id") Integer toId);

    @Query(value = "SELECT * FROM rltwoxp_associated_entities WHERE rltwoxp_entity_assoc_two_from_entity = :id", nativeQuery = true)
    List<RLTwoExpAssociatedEntitiesIProjection> getAllAssociatedEntitiesWithAssocTwoFromEntity(@Param("id") Integer fromId);

    @Query(value = "SELECT * FROM rltwoxp_associated_entities WHERE rltwoxp_entity_assoc_two_to_entity = :id", nativeQuery = true)
    List<RLTwoExpAssociatedEntitiesIProjection> getAllAssociatedEntitiesWithAssocTwoToEntity(@Param("id") Integer toId);

    @Query(value = "SELECT * FROM rltwoxp_associated_entities", nativeQuery = true)
    List<RLTwoExpFromEntitiesIProjection> getAssociatedEntitiesEntityInfoOnly();

    @Query(value = "SELECT * FROM rltwoxp_entity WHERE rltwoxp_entity_type_fk = :id", nativeQuery = true)
    List<RLTwoExpEntity> getEntitiesByTypeId(@Param("id") Integer typeId);

    @Query(value = "SELECT * FROM rltwoxp_entity WHERE rltwoxp_entity_assoc_to_entity_one = :id", nativeQuery = true)
    List<RLTwoExpEntity> getEntitiesByAssocOneToEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rltwoxp_entity WHERE rltwoxp_entity_assoc_to_entity_two = :id", nativeQuery = true)
    List<RLTwoExpEntity> getEntitiesByAssocTwoToEntityId(@Param("id") Integer entityId);
}
