package at.fhv.rupp.ba.dbimpl.status.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = SLTwoEntityStatusTypeMapstructMapper.class)
public interface SLTwoEntityMapstructMapper {

    @Mapping(source = "slTwoEntityId", target = "id")
    @Mapping(source = "slTwoEntityStatusDateTime", target = "statusDateTime")
    @Mapping(source = "slTwoEntityStatusType", target = "statusType")
    SLTwoEntityDTO toDTO(SLTwoEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLTwoEntity toEntity(SLTwoEntityDTO dto);

    @Mapping(source = "slTwoEntityId", target = "id")
    @Mapping(source = "slTwoEntityStatusDateTime", target = "statusDateTime")
    @Mapping(source = "slTwoEntityStatusType", target = "statusType")
    void updateEntityFromDTO(SLTwoEntity entity, @MappingTarget SLTwoEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLTwoEntityDTO dto, @MappingTarget SLTwoEntity entity);

    List<SLTwoEntityDTO> toDTOs(List<SLTwoEntity> entities);

    List<SLTwoEntity> toEntities(List<SLTwoEntityDTO> dtos);
}
