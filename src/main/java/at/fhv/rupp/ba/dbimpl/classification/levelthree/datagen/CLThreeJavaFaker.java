package at.fhv.rupp.ba.dbimpl.classification.levelthree.datagen;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntity;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategory;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryClassification;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryType;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityCategoryClassificationRepo;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityCategoryRepo;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityCategoryTypeRepo;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityRepo;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class CLThreeJavaFaker extends DataGenJavaFaker {
    private final CLThreeEntityRepo entityRepo;
    private final CLThreeEntityCategoryRepo categoryRepo;
    private final CLThreeEntityCategoryTypeRepo categoryTypeRepo;
    private final CLThreeEntityCategoryClassificationRepo categoryClassificationRepo;

    public CLThreeJavaFaker(CLThreeEntityRepo entityRepo, CLThreeEntityCategoryRepo categoryRepo, CLThreeEntityCategoryTypeRepo categoryTypeRepo, CLThreeEntityCategoryClassificationRepo categoryClassificationRepo) {
        this.entityRepo = entityRepo;
        this.categoryRepo = categoryRepo;
        this.categoryTypeRepo = categoryTypeRepo;
        this.categoryClassificationRepo = categoryClassificationRepo;
    }

    @Override
    public void fillDB() {
        CLThreeEntity entity;
        List<CLThreeEntity> createdEntities = new LinkedList<>();
        CLThreeEntityCategory category;
        List<CLThreeEntityCategory> createdCategories = new LinkedList<>();
        CLThreeEntityCategoryType catType;
        List<CLThreeEntityCategoryType> createdCatTypes = new LinkedList<>();
        CLThreeEntityCategoryClassification catClass;
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new CLThreeEntity();
            entity.setClthreeEntityName(trimStringIfTooLong(FAKER.company().name(), MIDDLE_CHAR_LIMIT));
            createdEntities.add(entity);
            entityRepo.save(entity);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            catType = new CLThreeEntityCategoryType();
            catType.setClthreeEntityCategoryTypeName(trimStringIfTooLong(FAKER.company().industry(), MIDDLE_CHAR_LIMIT));
            if (!createdCatTypes.isEmpty()) {
                //assign parents randomly
                if (getRandomIntByBound(20) % 2 == 0) {
                    catType.setClThreeEntityCategoryTypeParent(createdCatTypes.get(getRandomIntByBound(createdCatTypes.size())));
                }
            }
            createdCatTypes.add(catType);
            categoryTypeRepo.save(catType);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            category = new CLThreeEntityCategory();
            category.setClthreeEntityCategoryName(trimStringIfTooLong(FAKER.company().profession(), MIDDLE_CHAR_LIMIT));
            LocalDate startCat = getDateBetween(START_DATE, END_DATE);
            category.setClthreeEntityCategoryFromDate(startCat);
            if (getRandomIntByBound(10) % 2 == 0) {
                category.setClthreeEntityCategoryThruDate(getDateBetween(startCat, END_DATE));
            }
            if (!createdCategories.isEmpty()) {
                if (getRandomIntByBound(30) % 2 == 0) {
                    category.setClThreeEntityCategoryParent(createdCategories.get(getRandomIntByBound(createdCategories.size())));
                }
            }
            if (!createdCatTypes.isEmpty()) {
                category.setClThreeEntityCategoryType(createdCatTypes.get(getRandomIntByBound(createdCatTypes.size())));
            }
            createdCategories.add(category);
            categoryRepo.save(category);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            catClass = new CLThreeEntityCategoryClassification();
            if (!createdEntities.isEmpty()) {
                catClass.setClThreeEntityCategoryClassificationEntity(createdEntities.get(getRandomIntByBound(createdEntities.size())));
            }
            if (!createdCategories.isEmpty()) {
                catClass.setClThreeEntityCategoryClassificationCategory(createdCategories.get(getRandomIntByBound(createdCategories.size())));
            }
            if (catClass.getClThreeEntityCategoryClassificationCategory() != null && catClass.getClThreeEntityCategoryClassificationEntity() != null) {
                LocalDate beginCatClass = getDateBetween(catClass.getClThreeEntityCategoryClassificationCategory().getClthreeEntityCategoryFromDate(), END_DATE);
                catClass.setClthreeEntityCategoryClassificationFromDate(beginCatClass);
                if (getRandomIntByBound(40) % 2 == 0) {
                    catClass.setClthreeEntityCategoryClassificationThruDate(getDateBetween(beginCatClass, END_DATE));
                }
                categoryClassificationRepo.save(catClass);
            }
        }

    }
}
