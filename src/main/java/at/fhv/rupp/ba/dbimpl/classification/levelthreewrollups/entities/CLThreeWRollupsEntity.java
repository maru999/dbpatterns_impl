package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_entity")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int clthreewrEntityId;


    @Column(name = "clthreewr_entity_name")
    private String clthreewrEntityName;

    public int getClthreewrEntityId() {
        return clthreewrEntityId;
    }

    public void setClthreewrEntityId(int clthreewrEntityId) {
        this.clthreewrEntityId = clthreewrEntityId;
    }

    public String getClthreewrEntityName() {
        return clthreewrEntityName;
    }

    public void setClthreewrEntityName(String clthreewrEntityName) {
        this.clthreewrEntityName = clthreewrEntityName;
    }
}
