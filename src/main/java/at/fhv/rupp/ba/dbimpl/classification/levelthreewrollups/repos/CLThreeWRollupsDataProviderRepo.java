package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsDataProvider;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLThreeWRollupsDataProviderRepo extends JpaRepository<CLThreeWRollupsDataProvider, Integer> {
}
