package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityAssocDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssoc;

import java.util.LinkedList;
import java.util.List;

public class RLThreeEntityAssocMapper {
    private RLThreeEntityAssocMapper() {
        //hidden constructor
    }

    public static RLThreeEntityAssocDTO toDTO(RLThreeEntityAssoc entity) {
        RLThreeEntityAssocDTO dto = new RLThreeEntityAssocDTO();
        dto.setId(entity.getRlThreeEntityAssocId());
        dto.setFromDate(entity.getRlThreeEntityAssocFromDate());
        if (entity.getRlThreeEntityAssocThruDate() != null) {
            dto.setThruDate(entity.getRlThreeEntityAssocThruDate());
        }
        if (entity.getRlThreeEntityAssocType() != null) {
            dto.setType(RLThreeEntityAssocTypeMapper.toDTO(entity.getRlThreeEntityAssocType()));
        }
        if (entity.getTlThreeAssocFromEntity() != null) {
            dto.setFromEntity(RLThreeEntityMapper.toDTO(entity.getTlThreeAssocFromEntity()));
        }
        if (entity.getTlThreeAssocToEntity() != null) {
            dto.setToEntity(RLThreeEntityMapper.toDTO(entity.getTlThreeAssocToEntity()));
        }


        return dto;
    }

    public static RLThreeEntityAssoc toEntity(RLThreeEntityAssocDTO dto) {
        RLThreeEntityAssoc entity = new RLThreeEntityAssoc();
        if (dto.getId() != null) {
            entity.setRlThreeEntityAssocId(dto.getId());
        }
        if (dto.getFromDate() != null) {
            entity.setRlThreeEntityAssocFromDate(dto.getFromDate());
        }
        if (dto.getThruDate() != null) {
            entity.setRlThreeEntityAssocThruDate(dto.getThruDate());
        }
        if (dto.getType() != null) {
            entity.setRlThreeEntityAssocType(RLThreeEntityAssocTypeMapper.toEntity(dto.getType()));
        }
        if (dto.getFromEntity() != null) {
            entity.setTlThreeAssocFromEntity(RLThreeEntityMapper.toEntity(dto.getFromEntity()));
        }
        if (dto.getToEntity() != null) {
            entity.setTlThreeAssocToEntity(RLThreeEntityMapper.toEntity(dto.getToEntity()));
        }
        return entity;
    }

    public static List<RLThreeEntityAssocDTO> toDTOs(List<RLThreeEntityAssoc> entities) {
        LinkedList<RLThreeEntityAssocDTO> result = new LinkedList<>();
        for (RLThreeEntityAssoc entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeEntityAssoc> toEntities(List<RLThreeEntityAssocDTO> dtos) {
        LinkedList<RLThreeEntityAssoc> result = new LinkedList<>();
        for (RLThreeEntityAssocDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
