package at.fhv.rupp.ba.dbimpl.status.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "sltwo_entity")
@EntityListeners(AuditingEntityListener.class)
public class SLTwoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sltwo_entity_id")
    private int slTwoEntityId;

    @Column(name = "sltwo_entity_status_datetime")
    private LocalDateTime slTwoEntityStatusDateTime;

    @ManyToOne
    @JoinColumn(name = "sltwo_entity_status_type_fk")
    private SLTwoEntityStatusType slTwoEntityStatusType;

    public int getSlTwoEntityId() {
        return slTwoEntityId;
    }

    public void setSlTwoEntityId(int slTwoEntityId) {
        this.slTwoEntityId = slTwoEntityId;
    }

    public LocalDateTime getSlTwoEntityStatusDateTime() {
        return slTwoEntityStatusDateTime;
    }

    public void setSlTwoEntityStatusDateTime(LocalDateTime slTwoEntityStatusDateTime) {
        this.slTwoEntityStatusDateTime = slTwoEntityStatusDateTime;
    }

    public SLTwoEntityStatusType getSlTwoEntityStatusType() {
        return slTwoEntityStatusType;
    }

    public void setSlTwoEntityStatusType(SLTwoEntityStatusType slTwoEntityStatusType) {
        this.slTwoEntityStatusType = slTwoEntityStatusType;
    }
}
