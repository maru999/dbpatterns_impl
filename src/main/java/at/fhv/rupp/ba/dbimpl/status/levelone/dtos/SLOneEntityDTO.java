package at.fhv.rupp.ba.dbimpl.status.levelone.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SLOneEntityDTO {
    private Integer id;
    private LocalDateTime eventOneDateTime;
    private LocalDateTime eventTwoDateTime;
    private LocalDateTime eventThreeDateTime;
    private String eventIndicator;
    private LocalDate eventFromDate;
    private LocalDate eventThruDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getEventOneDateTime() {
        return eventOneDateTime;
    }

    public void setEventOneDateTime(LocalDateTime eventOneDateTime) {
        this.eventOneDateTime = eventOneDateTime;
    }

    public LocalDateTime getEventTwoDateTime() {
        return eventTwoDateTime;
    }

    public void setEventTwoDateTime(LocalDateTime eventTwoDateTime) {
        this.eventTwoDateTime = eventTwoDateTime;
    }

    public LocalDateTime getEventThreeDateTime() {
        return eventThreeDateTime;
    }

    public void setEventThreeDateTime(LocalDateTime eventThreeDateTime) {
        this.eventThreeDateTime = eventThreeDateTime;
    }

    public String getEventIndicator() {
        return eventIndicator;
    }

    public void setEventIndicator(String eventIndicator) {
        this.eventIndicator = eventIndicator;
    }

    public LocalDate getEventFromDate() {
        return eventFromDate;
    }

    public void setEventFromDate(LocalDate eventFromDate) {
        this.eventFromDate = eventFromDate;
    }

    public LocalDate getEventThruDate() {
        return eventThruDate;
    }

    public void setEventThruDate(LocalDate eventThruDate) {
        this.eventThruDate = eventThruDate;
    }
}
