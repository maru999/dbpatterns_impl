package at.fhv.rupp.ba.dbimpl.status.leveltwo.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.datagen.SLTwoJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityWithStatusIProjection;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntity;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntityStatusType;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.exceptions.SLTwoEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.exceptions.SLTwoEntityStatusTypeNotFoundException;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.mappers.custom.SLTwoEntityMapper;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.mappers.custom.SLTwoEntityStatusTypeMapper;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.repos.SLTwoEntityRepo;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.repos.SLTwoEntityStatusTypeRepo;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/status/L2")
public class SLTwoController {

    private final SLTwoEntityRepo entityRepo;
    private final SLTwoEntityStatusTypeRepo statusTypeRepo;
    private final DataGenJavaFaker faker;

    public SLTwoController(SLTwoEntityRepo entityRepo, SLTwoEntityStatusTypeRepo statusTypeRepo) {
        this.entityRepo = entityRepo;
        this.statusTypeRepo = statusTypeRepo;
        this.faker = new SLTwoJavaFaker(entityRepo, statusTypeRepo);
    }

    @GetMapping("/entities")
    public List<SLTwoEntityDTO> getAllEntities() {
        return SLTwoEntityMapper.toDTOs(entityRepo.findAll());
    }

    @GetMapping("/entities/{id}")
    public ResponseEntity<SLTwoEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws SLTwoEntityNotFoundException {
        SLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new SLTwoEntityNotFoundException(getExceptionString("SLTwoEntity", entityId)));
        return ResponseEntity.ok().body(SLTwoEntityMapper.toDTO(entity));
    }

    @PostMapping("/entities")
    public SLTwoEntityDTO createEntity(@Valid @RequestBody SLTwoEntityDTO entityDTO) {
        return SLTwoEntityMapper.toDTO(entityRepo.save(SLTwoEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<SLTwoEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody SLTwoEntityDTO entityDetails) throws SLTwoEntityNotFoundException {
        SLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new SLTwoEntityNotFoundException(getExceptionString("SLTwoEntity", entityId)));
        SLTwoEntityDTO entityDTO = SLTwoEntityMapper.toDTO(entity);
        entityDTO.setStatusDateTime(entityDetails.getStatusDateTime());
        entityDTO.setStatusType(entityDetails.getStatusType());
        final SLTwoEntityDTO updatedEntity = SLTwoEntityMapper.toDTO(entityRepo.save(SLTwoEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws SLTwoEntityNotFoundException {
        SLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new SLTwoEntityNotFoundException(getExceptionString("SLTwoEntity", entityId)));
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }


    @GetMapping("/statusTypes")
    public List<SLTwoEntityStatusTypeDTO> getAllStatusTypes() {
        return SLTwoEntityStatusTypeMapper.toDTOs(statusTypeRepo.findAll());
    }

    @GetMapping("/statusTypes/{id}")
    public ResponseEntity<SLTwoEntityStatusTypeDTO> getStatusTypeById(@PathVariable(value = "id") Integer entityId) throws SLTwoEntityStatusTypeNotFoundException {
        SLTwoEntityStatusType entity = statusTypeRepo.findById(entityId).orElseThrow(() -> new SLTwoEntityStatusTypeNotFoundException(getExceptionString("SLTwoEntityStatus", entityId)));
        return ResponseEntity.ok().body(SLTwoEntityStatusTypeMapper.toDTO(entity));
    }

    @PostMapping("/statusTypes")
    public SLTwoEntityStatusTypeDTO createStatusType(@Valid @RequestBody SLTwoEntityStatusTypeDTO entityDTO) {
        return SLTwoEntityStatusTypeMapper.toDTO(statusTypeRepo.save(SLTwoEntityStatusTypeMapper.toEntity(entityDTO)));
    }

    @PutMapping("/statusTypes/{id}")
    public ResponseEntity<SLTwoEntityStatusTypeDTO> updateStatusType(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody SLTwoEntityStatusTypeDTO entityDetails) throws SLTwoEntityStatusTypeNotFoundException {
        SLTwoEntityStatusType entity = statusTypeRepo.findById(entityId).orElseThrow(() -> new SLTwoEntityStatusTypeNotFoundException(getExceptionString("SLTwoEntityStatus", entityId)));
        SLTwoEntityStatusTypeDTO entityDTO = SLTwoEntityStatusTypeMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        entityDTO.setSomeAttr(entityDetails.getSomeAttr());
        final SLTwoEntityStatusTypeDTO updatedEntity = SLTwoEntityStatusTypeMapper.toDTO(statusTypeRepo.save(SLTwoEntityStatusTypeMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/statusTypes/{id}")
    public Map<String, Boolean> deleteStatusType(@PathVariable(value = "id") Integer entityId) throws SLTwoEntityStatusTypeNotFoundException {
        SLTwoEntityStatusType entity = statusTypeRepo.findById(entityId).orElseThrow(() -> new SLTwoEntityStatusTypeNotFoundException(getExceptionString("SLTwoEntityStatus", entityId)));
        List<SLTwoEntity> refEntities = entityRepo.getEntitiesByStatusTypeId(entityId);
        for (SLTwoEntity refEntity : refEntities) {
            refEntity.setSlTwoEntityStatusType(null);
            entityRepo.save(refEntity);
        }
        statusTypeRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/status/statusleveltwo.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entitiesWStatus")
    public List<SLTwoEntityWithStatusIProjection> getAllEntitiesWithStatus() {
        return entityRepo.getEntitiesWithStatus();
    }

    @GetMapping("/entitiesWStatus/entityId/{id}")
    public List<SLTwoEntityWithStatusIProjection> getAllEntitiesWithStatusByEntityId(@PathVariable("id") Integer entityId) {
        return entityRepo.getEntitiesWithStatusByEntityId(entityId);
    }

    @GetMapping("/entitiesWStatus/statusTypeName/{name}")
    public List<SLTwoEntityWithStatusIProjection> getAllEntitiesWithStatusByStatusTypeName(@PathVariable("name") String statusTypeName) {
        return entityRepo.getEntitiesWithStatusStatusTypeName(statusTypeName);
    }

    @GetMapping("/entitiesWStatus/{startTime}/{endTime}")
    public List<SLTwoEntityWithStatusIProjection> getAllEntitiesWithStatusWithStatusDatetimeBetween(@PathVariable("startTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startTime, @PathVariable("endTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endTime) {
        return entityRepo.getEntitiesWithStatusByStatusDatetimeBetween(startTime, endTime);
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }


}
