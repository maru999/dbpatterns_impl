package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlthreewr_entity_type")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeWRulesEntityType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthreewr_entity_type_id")
    private int rlThreeWRulesEntityTypeId;

    @Column(name = "rlthreewr_entity_type_name")
    private String rlThreeWRulesEntityTypeName;

    @ManyToOne
    @JoinColumn(name = "rlthreewr_entity_type_parent")
    private RLThreeWRulesEntityType rlThreeWRulesEntityTypeParent;

    public int getRlThreeWRulesEntityTypeId() {
        return rlThreeWRulesEntityTypeId;
    }

    public void setRlThreeWRulesEntityTypeId(int rlThreeWRulesEntityTypeId) {
        this.rlThreeWRulesEntityTypeId = rlThreeWRulesEntityTypeId;
    }

    public String getRlThreeWRulesEntityTypeName() {
        return rlThreeWRulesEntityTypeName;
    }

    public void setRlThreeWRulesEntityTypeName(String rlThreeWRulesEntityTypeName) {
        this.rlThreeWRulesEntityTypeName = rlThreeWRulesEntityTypeName;
    }

    public RLThreeWRulesEntityType getRlThreeWRulesEntityTypeParent() {
        return rlThreeWRulesEntityTypeParent;
    }

    public void setRlThreeWRulesEntityTypeParent(RLThreeWRulesEntityType rlThreeWRulesEntityTypeParent) {
        this.rlThreeWRulesEntityTypeParent = rlThreeWRulesEntityTypeParent;
    }
}
