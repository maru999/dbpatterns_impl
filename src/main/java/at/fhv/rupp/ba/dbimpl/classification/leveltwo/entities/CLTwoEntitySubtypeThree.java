package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "cltwo_entity_subtype_three")
@EntityListeners(AuditingEntityListener.class)
@PrimaryKeyJoinColumn(name = "cltwo_entity_subtype_three_id", referencedColumnName = "cltwo_entity_id")
public class CLTwoEntitySubtypeThree extends CLTwoEntity {

    @Column(name = "cltwo_entity_subtype_three_info")
    private String clTwoEntitySubtypeThreeInfo;

    public String getClTwoEntitySubtypeThreeInfo() {
        return clTwoEntitySubtypeThreeInfo;
    }

    public void setClTwoEntitySubtypeThreeInfo(String clTwoEntitySubtypeThreeInfo) {
        this.clTwoEntitySubtypeThreeInfo = clTwoEntitySubtypeThreeInfo;
    }
}
