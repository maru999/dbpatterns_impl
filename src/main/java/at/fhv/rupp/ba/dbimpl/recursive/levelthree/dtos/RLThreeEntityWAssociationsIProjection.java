package at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos;

import java.time.LocalDate;

public interface RLThreeEntityWAssociationsIProjection {

    Integer getrlthree_entity_type_id();

    String getrlthree_entity_type_name();

    Integer getrlthree_entity_type_parent();

    Integer getrlthree_entity_assoc_type_id();

    String getrlthree_entity_assoc_type_name();

    Integer getrlthree_entity_assoc_type_parent();

    Integer getrlthree_entity_id();

    Integer getrlthree_entity_type_fk();

    Integer getrlthree_entity_assoc_id();

    LocalDate getrlthree_entity_assoc_from_date();

    LocalDate getrlthree_entity_assoc_thru_date();

    Integer getrlthree_entity_assoc_type_fk();

    Integer getrlthree_entity_assoc_from_ent();

    Integer getrlthree_entity_assoc_to_ent();
}
