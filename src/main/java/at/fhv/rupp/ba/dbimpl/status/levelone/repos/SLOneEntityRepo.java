package at.fhv.rupp.ba.dbimpl.status.levelone.repos;

import at.fhv.rupp.ba.dbimpl.status.levelone.entities.SLOneEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SLOneEntityRepo extends JpaRepository<SLOneEntity, Integer> {
}
