package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryTypeRollupType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLThreeWRollupsEntityCategoryTypeRollupTypeRepo extends JpaRepository<CLThreeWRollupsEntityCategoryTypeRollupType, Integer> {
}
