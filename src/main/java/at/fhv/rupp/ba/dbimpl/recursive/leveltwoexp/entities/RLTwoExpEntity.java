package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rltwoxp_entity")
@EntityListeners(AuditingEntityListener.class)
public class RLTwoExpEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rltwoxp_entity_id")
    private int rlTwoExpEntityId;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_assoc_to_entity_one")
    private RLTwoExpEntity rlTwoExpEntityAssocToEntityOne;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_assoc_to_entity_two")
    private RLTwoExpEntity rlTwoExpEntityAssocToEntityTwo;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_type_fk")
    private RLTwoExpEntityType rlTwoExpEntityType;

    public int getRlTwoExpEntityId() {
        return rlTwoExpEntityId;
    }

    public void setRlTwoExpEntityId(int rlTwoExpEntityId) {
        this.rlTwoExpEntityId = rlTwoExpEntityId;
    }

    public RLTwoExpEntity getRlTwoExpEntityAssocToEntityOne() {
        return rlTwoExpEntityAssocToEntityOne;
    }

    public void setRlTwoExpEntityAssocToEntityOne(RLTwoExpEntity rlTwoExpEntityAssocToEntityOne) {
        this.rlTwoExpEntityAssocToEntityOne = rlTwoExpEntityAssocToEntityOne;
    }

    public RLTwoExpEntity getRlTwoExpEntityAssocToEntityTwo() {
        return rlTwoExpEntityAssocToEntityTwo;
    }

    public void setRlTwoExpEntityAssocToEntityTwo(RLTwoExpEntity rlTwoExpEntityAssocToEntityTwo) {
        this.rlTwoExpEntityAssocToEntityTwo = rlTwoExpEntityAssocToEntityTwo;
    }

    public RLTwoExpEntityType getRlTwoExpEntityType() {
        return rlTwoExpEntityType;
    }

    public void setRlTwoExpEntityType(RLTwoExpEntityType rlTwoExpEntityType) {
        this.rlTwoExpEntityType = rlTwoExpEntityType;
    }
}
