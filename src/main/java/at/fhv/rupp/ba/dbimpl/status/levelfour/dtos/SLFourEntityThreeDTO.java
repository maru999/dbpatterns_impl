package at.fhv.rupp.ba.dbimpl.status.levelfour.dtos;

public class SLFourEntityThreeDTO {
    private Integer id;
    private String attr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttr() {
        return attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }
}
