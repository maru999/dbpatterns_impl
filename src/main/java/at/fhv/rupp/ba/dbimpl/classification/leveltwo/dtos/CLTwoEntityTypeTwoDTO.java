package at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos;

public class CLTwoEntityTypeTwoDTO {

    private Integer id;
    private String name;
    private CLTwoEntityTypeTwoDTO parentType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLTwoEntityTypeTwoDTO getParentType() {
        return parentType;
    }

    public void setParentType(CLTwoEntityTypeTwoDTO parentType) {
        this.parentType = parentType;
    }
}
