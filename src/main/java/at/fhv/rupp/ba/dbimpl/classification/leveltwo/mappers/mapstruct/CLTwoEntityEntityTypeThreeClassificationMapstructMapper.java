package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityEntityTypeThreeClassificationDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityEntityTypeThreeClassification;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLTwoEntityMapstructMapper.class, CLTwoEntityTypeThreeMapstructMapper.class})
public interface CLTwoEntityEntityTypeThreeClassificationMapstructMapper {

    @Mapping(source = "clTwoEntityEntityTypeThreeClassificationId", target = "id")
    @Mapping(source = "clTwoEntityEntityTypeThreeClassificationEntity", target = "entity")
    @Mapping(source = "clTwoEntityEntityTypeThreeTypeThree", target = "typeThree")
    CLTwoEntityEntityTypeThreeClassificationDTO toDTO(CLTwoEntityEntityTypeThreeClassification entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLTwoEntityEntityTypeThreeClassification toEntity(CLTwoEntityEntityTypeThreeClassificationDTO dto);

    @Mapping(source = "clTwoEntityEntityTypeThreeClassificationId", target = "id")
    @Mapping(source = "clTwoEntityEntityTypeThreeClassificationEntity", target = "entity")
    @Mapping(source = "clTwoEntityEntityTypeThreeTypeThree", target = "typeThree")
    void updateEntityFromDTO(CLTwoEntityEntityTypeThreeClassification entity, @MappingTarget CLTwoEntityEntityTypeThreeClassificationDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLTwoEntityEntityTypeThreeClassificationDTO dto, @MappingTarget CLTwoEntityEntityTypeThreeClassification entity);

    List<CLTwoEntityEntityTypeThreeClassificationDTO> toDTOs(List<CLTwoEntityEntityTypeThreeClassification> entities);

    List<CLTwoEntityEntityTypeThreeClassification> toEntities(List<CLTwoEntityEntityTypeThreeClassificationDTO> dtos);
}
