package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityAssocRuleDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssocRule;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLThreeWRulesEntityAssocRuleMapstructMapper {
    @Mapping(source = "rlThreeWRulesEntityAssocRuleId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityAssocRuleName", target = "name")
    RLThreeWRulesEntityAssocRuleDTO toDTO(RLThreeWRulesEntityAssocRule entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeWRulesEntityAssocRule toEntity(RLThreeWRulesEntityAssocRuleDTO dto);

    @Mapping(source = "rlThreeWRulesEntityAssocRuleId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityAssocRuleName", target = "name")
    void updateEntityFromDTO(RLThreeWRulesEntityAssocRule entity, @MappingTarget RLThreeWRulesEntityAssocRuleDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeWRulesEntityAssocRuleDTO dto, @MappingTarget RLThreeWRulesEntityAssocRule entity);

    List<RLThreeWRulesEntityAssocRuleDTO> toDTOs(List<RLThreeWRulesEntityAssocRule> entities);

    List<RLThreeWRulesEntityAssocRule> toEntities(List<RLThreeWRulesEntityAssocRuleDTO> dtos);
}
