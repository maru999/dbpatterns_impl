package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityTypesViewIProjection;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CLThreeWRollupsEntityRepo extends JpaRepository<CLThreeWRollupsEntity, Integer> {

    @Query(value = "SELECT * FROM clthreewr_ent_types_view", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypes();

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_id = :id AND (clthreewr_entity_category_rollup_parent_entity_category_id IS NOT NULL OR clthreewr_entity_category_rollup_child_entity_category_id IS NOT NULL)", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesHierarchyByEntityCatId(@Param("id") Integer catId);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_name = :name", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatName(@Param("name") String catName);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_type_name = :name", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatTypeName(@Param("name") String catTypeName);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_type_scheme_name = :name", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesBySchemeName(@Param("name") String schemeName);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_rollup_type_name = :name", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByRollupTypeName(@Param("name") String typeName);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_type_rollup_type_name = :name", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByTypeRollupTypeName(@Param("name") String rollupTypeName);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE dp_name = :name", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByDataProviderName(@Param("name") String dataProviderName);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_classification_from BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByClassificationFromDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_rollup_from BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatRollupFromDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query(value = "SELECT * FROM clthreewr_ent_types_view WHERE clthreewr_entity_category_type_rollup_from BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatTypeRollupFromDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

}
