package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityCategoryClassificationDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryClassification;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLThreeEntityCategoryMapstructMapper.class, CLThreeEntityMapstructMapper.class})
public interface CLThreeEntityCategoryClassificationMapstructMapper {

    @Mapping(source = "clthreeEntityCategoryClassificationId", target = "id")
    @Mapping(source = "clthreeEntityCategoryClassificationFromDate", target = "from")
    @Mapping(source = "clthreeEntityCategoryClassificationThruDate", target = "thru")
    @Mapping(source = "clThreeEntityCategoryClassificationCategory", target = "category")
    @Mapping(source = "clThreeEntityCategoryClassificationEntity", target = "entity")
    CLThreeEntityCategoryClassificationDTO toDTO(CLThreeEntityCategoryClassification entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeEntityCategoryClassification toEntity(CLThreeEntityCategoryClassificationDTO dto);

    @Mapping(source = "clthreeEntityCategoryClassificationId", target = "id")
    @Mapping(source = "clthreeEntityCategoryClassificationFromDate", target = "from")
    @Mapping(source = "clthreeEntityCategoryClassificationThruDate", target = "thru")
    @Mapping(source = "clThreeEntityCategoryClassificationCategory", target = "category")
    @Mapping(source = "clThreeEntityCategoryClassificationEntity", target = "entity")
    void updateEntityFromDTO(CLThreeEntityCategoryClassification entity, @MappingTarget CLThreeEntityCategoryClassificationDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeEntityCategoryClassificationDTO dto, @MappingTarget CLThreeEntityCategoryClassification entity);

    List<CLThreeEntityCategoryClassificationDTO> toDTOs(List<CLThreeEntityCategoryClassification> entities);

    List<CLThreeEntityCategoryClassification> toEntities(List<CLThreeEntityCategoryClassificationDTO> dtos);
}
