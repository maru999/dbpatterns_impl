package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityAssocDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssoc;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {RLThreeWRulesEntityMapstructMapper.class, RLThreeWRulesEntityAssocTypeMapstructMapper.class, RLThreeWRulesEntityAssocRuleMapstructMapper.class})
public interface RLThreeWRulesEntityAssocMapstructMapper {
    @Mapping(source = "rlThreeWRulesEntityAssocId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityAssocFromDate", target = "fromDate")
    @Mapping(source = "rlThreeWRulesEntityAssocThruDate", target = "thruDate")
    @Mapping(source = "rlThreeWRulesEntityAssocType", target = "type")
    @Mapping(source = "rlThreeWRulesAssocFromEntity", target = "fromEntity")
    @Mapping(source = "rlThreeWRulesAssocToEntity", target = "toEntity")
    @Mapping(source = "rlThreeWRulesEntityAssocRule", target = "rule")
    RLThreeWRulesEntityAssocDTO toDTO(RLThreeWRulesEntityAssoc entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeWRulesEntityAssoc toEntity(RLThreeWRulesEntityAssocDTO dto);

    @Mapping(source = "rlThreeWRulesEntityAssocId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityAssocFromDate", target = "fromDate")
    @Mapping(source = "rlThreeWRulesEntityAssocThruDate", target = "thruDate")
    @Mapping(source = "rlThreeWRulesEntityAssocType", target = "type")
    @Mapping(source = "rlThreeWRulesAssocFromEntity", target = "fromEntity")
    @Mapping(source = "rlThreeWRulesAssocToEntity", target = "toEntity")
    @Mapping(source = "rlThreeWRulesEntityAssocRule", target = "rule")
    void updateEntityFromDTO(RLThreeWRulesEntityAssoc entity, @MappingTarget RLThreeWRulesEntityAssocDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeWRulesEntityAssocDTO dto, @MappingTarget RLThreeWRulesEntityAssoc entity);

    List<RLThreeWRulesEntityAssocDTO> toDTOs(List<RLThreeWRulesEntityAssoc> entities);

    List<RLThreeWRulesEntityAssoc> toEntities(List<RLThreeWRulesEntityAssocDTO> dtos);
}
