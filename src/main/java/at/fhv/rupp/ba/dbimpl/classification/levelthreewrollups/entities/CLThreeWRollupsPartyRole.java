package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_party_role")
@EntityListeners(AuditingEntityListener.class)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class CLThreeWRollupsPartyRole {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_party_role_id")
    private int clthreewrPartyRoleId;

    @Column(name = "role_name")
    private String roleName;

    public int getClthreewrPartyRoleId() {
        return clthreewrPartyRoleId;
    }

    public void setClthreewrPartyRoleId(int clthreewrPartyRoleId) {
        this.clthreewrPartyRoleId = clthreewrPartyRoleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
