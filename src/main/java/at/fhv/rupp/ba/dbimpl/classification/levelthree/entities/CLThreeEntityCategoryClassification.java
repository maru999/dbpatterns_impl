package at.fhv.rupp.ba.dbimpl.classification.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "clthree_entity_category_classification")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeEntityCategoryClassification {
    //In Entity Category Classification
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int clthreeEntityCategoryClassificationId;

    @Column(name = "clthree_entity_category_classification_from", nullable = false)
    private LocalDate clthreeEntityCategoryClassificationFromDate;

    @Column(name = "clthree_entity_category_classification_thru")
    private LocalDate clthreeEntityCategoryClassificationThruDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="clthree_entity_category_classification_entity")
    private CLThreeEntity clThreeEntityCategoryClassificationEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="clthree_entity_category_classification_entity_category")
    private CLThreeEntityCategory clThreeEntityCategoryClassificationCategory;

    public int getClthreeEntityCategoryClassificationId() {
        return clthreeEntityCategoryClassificationId;
    }

    public void setClthreeEntityCategoryClassificationId(int clthreeEntityCategoryClassificationId) {
        this.clthreeEntityCategoryClassificationId = clthreeEntityCategoryClassificationId;
    }

    public LocalDate getClthreeEntityCategoryClassificationFromDate() {
        return clthreeEntityCategoryClassificationFromDate;
    }

    public void setClthreeEntityCategoryClassificationFromDate(LocalDate clthreeEntityCategoryClassificationFromDate) {
        this.clthreeEntityCategoryClassificationFromDate = clthreeEntityCategoryClassificationFromDate;
    }

    public LocalDate getClthreeEntityCategoryClassificationThruDate() {
        return clthreeEntityCategoryClassificationThruDate;
    }

    public void setClthreeEntityCategoryClassificationThruDate(LocalDate clthreeEntityCategoryClassificationThruDate) {
        this.clthreeEntityCategoryClassificationThruDate = clthreeEntityCategoryClassificationThruDate;
    }

    public CLThreeEntity getClThreeEntityCategoryClassificationEntity() {
        return clThreeEntityCategoryClassificationEntity;
    }

    public void setClThreeEntityCategoryClassificationEntity(CLThreeEntity clThreeEntityCategoryClassificationEntity) {
        this.clThreeEntityCategoryClassificationEntity = clThreeEntityCategoryClassificationEntity;
    }

    public CLThreeEntityCategory getClThreeEntityCategoryClassificationCategory() {
        return clThreeEntityCategoryClassificationCategory;
    }

    public void setClThreeEntityCategoryClassificationCategory(CLThreeEntityCategory clThreeEntityCategoryClassificationCategory) {
        this.clThreeEntityCategoryClassificationCategory = clThreeEntityCategoryClassificationCategory;
    }
}
