package at.fhv.rupp.ba.dbimpl.classification.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeThree;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLTwoEntityTypeThreeRepo extends JpaRepository<CLTwoEntityTypeThree, Integer> {
}
