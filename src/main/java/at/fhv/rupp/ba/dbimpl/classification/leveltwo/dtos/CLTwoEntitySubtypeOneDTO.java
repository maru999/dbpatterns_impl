package at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos;

public class CLTwoEntitySubtypeOneDTO extends CLTwoEntityDTO {
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
