package at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos;

public class RLThreeEntityDTO {
    private Integer id;
    private RLThreeEntityTypeDTO type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RLThreeEntityTypeDTO getType() {
        return type;
    }

    public void setType(RLThreeEntityTypeDTO type) {
        this.type = type;
    }
}
