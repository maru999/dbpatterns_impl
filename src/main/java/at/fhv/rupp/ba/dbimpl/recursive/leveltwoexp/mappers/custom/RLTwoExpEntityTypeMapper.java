package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityType;

import java.util.LinkedList;
import java.util.List;

public class RLTwoExpEntityTypeMapper {
    private RLTwoExpEntityTypeMapper() {
        //hidden constructor
    }

    public static RLTwoExpEntityTypeDTO toDTO(RLTwoExpEntityType entity) {
        RLTwoExpEntityTypeDTO dto = new RLTwoExpEntityTypeDTO();
        dto.setId(entity.getRlTwoExpEntityTypeId());
        dto.setName(entity.getRlTwoExpEntityTypeName());
        if (entity.getRlTwoExpEntityTypeParent() != null) {
            dto.setParent(toDTO(entity.getRlTwoExpEntityTypeParent()));
        }
        return dto;
    }

    public static RLTwoExpEntityType toEntity(RLTwoExpEntityTypeDTO dto) {
        RLTwoExpEntityType entity = new RLTwoExpEntityType();
        if (dto.getId() != null) {
            entity.setRlTwoExpEntityTypeId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setRlTwoExpEntityTypeName(dto.getName());
        }
        if (dto.getParent() != null) {
            entity.setRlTwoExpEntityTypeParent(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLTwoExpEntityTypeDTO> toDTOs(List<RLTwoExpEntityType> entities) {
        LinkedList<RLTwoExpEntityTypeDTO> result = new LinkedList<>();
        for (RLTwoExpEntityType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLTwoExpEntityType> toEntities(List<RLTwoExpEntityTypeDTO> dtos) {
        LinkedList<RLTwoExpEntityType> result = new LinkedList<>();
        for (RLTwoExpEntityTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
