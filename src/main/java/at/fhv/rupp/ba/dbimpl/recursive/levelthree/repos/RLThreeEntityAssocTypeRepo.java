package at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssocType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RLThreeEntityAssocTypeRepo extends JpaRepository<RLThreeEntityAssocType, Integer> {
}
