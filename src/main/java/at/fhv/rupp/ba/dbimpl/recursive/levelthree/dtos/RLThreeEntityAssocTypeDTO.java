package at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos;

public class RLThreeEntityAssocTypeDTO {
    private Integer id;
    private String name;
    private RLThreeEntityAssocTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLThreeEntityAssocTypeDTO getParent() {
        return parent;
    }

    public void setParent(RLThreeEntityAssocTypeDTO parent) {
        this.parent = parent;
    }
}
