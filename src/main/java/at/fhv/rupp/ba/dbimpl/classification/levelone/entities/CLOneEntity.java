package at.fhv.rupp.ba.dbimpl.classification.levelone.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clone_entity")
@EntityListeners(AuditingEntityListener.class)
public class CLOneEntity {
    public CLOneEntity() {
        //empty Constructor
    }
    //In Entity
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int cloneEntityID;

    @Column(name = "clone_entity_name", nullable = false)
    private String cloneEntityName;

    @Column(name = "clone_entity_type_one", nullable = false)
    private String cloneEntityTypeOne;

    @Column(name = "clone_entity_type_two")
    private String cloneEntityTypeTwo;

    @Column(name = "clone_entity_type_three", nullable = false)
    private String cloneEntityTypeThree;

    public int getCloneEntityID() {
        return cloneEntityID;
    }

    public void setCloneEntityID(int cloneEntityID) {
        this.cloneEntityID = cloneEntityID;
    }

    public String getCloneEntityName() {
        return cloneEntityName;
    }

    public void setCloneEntityName(String cloneEntityName) {
        this.cloneEntityName = cloneEntityName;
    }

    public String getCloneEntityTypeOne() {
        return cloneEntityTypeOne;
    }

    public void setCloneEntityTypeOne(String cloneEntityTypeOne) {
        this.cloneEntityTypeOne = cloneEntityTypeOne;
    }

    public String getCloneEntityTypeTwo() {
        return cloneEntityTypeTwo;
    }

    public void setCloneEntityTypeTwo(String cloneEntityTypeTwo) {
        this.cloneEntityTypeTwo = cloneEntityTypeTwo;
    }

    public String getCloneEntityTypeThree() {
        return cloneEntityTypeThree;
    }

    public void setCloneEntityTypeThree(String cloneEntityTypeThree) {
        this.cloneEntityTypeThree = cloneEntityTypeThree;
    }
}
