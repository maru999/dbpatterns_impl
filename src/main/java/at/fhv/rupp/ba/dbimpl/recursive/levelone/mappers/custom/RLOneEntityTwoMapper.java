package at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityTwoDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityTwo;

import java.util.LinkedList;
import java.util.List;

public class RLOneEntityTwoMapper {
    private RLOneEntityTwoMapper() {
        //hidden constructor
    }

    public static RLOneEntityTwoDTO toDTO(RLOneEntityTwo entity) {
        RLOneEntityTwoDTO dto = new RLOneEntityTwoDTO();
        dto.setId(entity.getRloneEntityTwoId());
        dto.setName(entity.getRloneEntityTwoName());
        if (entity.getRloneEntityTwoParent() != null) {
            dto.setParent(RLOneEntityOneMapper.toDTO(entity.getRloneEntityTwoParent()));
        }
        return dto;
    }

    public static RLOneEntityTwo toEntity(RLOneEntityTwoDTO dto) {
        RLOneEntityTwo entity = new RLOneEntityTwo();
        if (dto.getId() != null) {
            entity.setRloneEntityTwoId(dto.getId());
        }
        entity.setRloneEntityTwoName(dto.getName());
        if (dto.getParent() != null) {
            entity.setRloneEntityTwoParent(RLOneEntityOneMapper.toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLOneEntityTwoDTO> toDTOs(List<RLOneEntityTwo> entities) {
        LinkedList<RLOneEntityTwoDTO> result = new LinkedList<>();
        for (RLOneEntityTwo entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLOneEntityTwo> toEntities(List<RLOneEntityTwoDTO> dtos) {
        LinkedList<RLOneEntityTwo> result = new LinkedList<>();
        for (RLOneEntityTwoDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
