package at.fhv.rupp.ba.dbimpl.status.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityWithStatusIProjection;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface SLTwoEntityRepo extends JpaRepository<SLTwoEntity, Integer> {

    @Query(value = "SELECT * FROM sltwo_entity_status", nativeQuery = true)
    List<SLTwoEntityWithStatusIProjection> getEntitiesWithStatus();

    @Query(value = "SELECT * FROM sltwo_entity_status WHERE sltwo_entity_id = :id", nativeQuery = true)
    List<SLTwoEntityWithStatusIProjection> getEntitiesWithStatusByEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM sltwo_entity_status WHERE sltwo_status_type_name = :name", nativeQuery = true)
    List<SLTwoEntityWithStatusIProjection> getEntitiesWithStatusStatusTypeName(@Param("name") String statusTypeName);

    @Query(value = "SELECT * FROM sltwo_entity_status WHERE sltwo_entity_status_datetime BETWEEN :startTime AND :endTime", nativeQuery = true)
    List<SLTwoEntityWithStatusIProjection> getEntitiesWithStatusByStatusDatetimeBetween(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

    @Query(value = "SELECT * FROM sltwo_entity WHERE sltwo_entity_status_type_fk = :id", nativeQuery = true)
    List<SLTwoEntity> getEntitiesByStatusTypeId(@Param("id") Integer statusTypeId);
}
