package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = RLThreeEntityTypeMapstructMapper.class)
public interface RLThreeEntityMapstructMapper {
    @Mapping(source = "rlThreeEntityId", target = "id")
    @Mapping(source = "rlThreeEntityType", target = "type")
    RLThreeEntityDTO toDTO(RLThreeEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeEntity toEntity(RLThreeEntityDTO dto);

    @Mapping(source = "rlThreeEntityId", target = "id")
    @Mapping(source = "rlThreeEntityType", target = "type")
    void updateEntityFromDTO(RLThreeEntity entity, @MappingTarget RLThreeEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeEntityDTO dto, @MappingTarget RLThreeEntity entity);

    List<RLThreeEntityDTO> toDTOs(List<RLThreeEntity> entities);

    List<RLThreeEntity> toEntities(List<RLThreeEntityDTO> dtos);
}
