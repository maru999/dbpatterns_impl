package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourStatusType;

import java.util.LinkedList;
import java.util.List;

public class SLFourStatusTypeMapper {

    private SLFourStatusTypeMapper() {
        //hidden constructor
    }

    public static SLFourStatusTypeDTO toDTO(SLFourStatusType entity) {
        SLFourStatusTypeDTO dto = new SLFourStatusTypeDTO();
        dto.setId(entity.getSlFourStatusTypeId());
        dto.setName(entity.getSlFourStatusTypeName());
        return dto;
    }

    public static SLFourStatusType toEntity(SLFourStatusTypeDTO dto) {
        SLFourStatusType entity = new SLFourStatusType();
        if (dto.getId() != null) {
            entity.setSlFourStatusTypeId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setSlFourStatusTypeName(dto.getName());
        }
        return entity;
    }

    public static List<SLFourStatusTypeDTO> toDTOs(List<SLFourStatusType> entities) {
        LinkedList<SLFourStatusTypeDTO> result = new LinkedList<>();
        for (SLFourStatusType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLFourStatusType> toEntities(List<SLFourStatusTypeDTO> dtos) {
        LinkedList<SLFourStatusType> result = new LinkedList<>();
        for (SLFourStatusTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
