package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryClassification;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityCategoryClassificationDTO;

import java.util.LinkedList;
import java.util.List;

public class CLThreeEntityCategoryClassificationMapper {

    private CLThreeEntityCategoryClassificationMapper() {
        //hidden constructor
    }

    public static CLThreeEntityCategoryClassificationDTO toDTO(CLThreeEntityCategoryClassification entity) {
        CLThreeEntityCategoryClassificationDTO dto = new CLThreeEntityCategoryClassificationDTO();
        dto.setId(entity.getClthreeEntityCategoryClassificationId());
        dto.setFrom(entity.getClthreeEntityCategoryClassificationFromDate());
        if (entity.getClthreeEntityCategoryClassificationThruDate() != null) {
            dto.setThru(entity.getClthreeEntityCategoryClassificationThruDate());
        }
        if (entity.getClThreeEntityCategoryClassificationEntity() != null) {
            dto.setEntity(CLThreeEntityMapper.toDTO(entity.getClThreeEntityCategoryClassificationEntity()));
        }
        if (entity.getClThreeEntityCategoryClassificationCategory() != null) {
            dto.setCategory(CLThreeEntityCategoryMapper.toDTO(entity.getClThreeEntityCategoryClassificationCategory()));
        }
        return dto;
    }

    public static CLThreeEntityCategoryClassification toEntity(CLThreeEntityCategoryClassificationDTO dto) {
        CLThreeEntityCategoryClassification entity = new CLThreeEntityCategoryClassification();
        if (dto.getId() != null) {
            entity.setClthreeEntityCategoryClassificationId(dto.getId());
        }
        entity.setClthreeEntityCategoryClassificationFromDate(dto.getFrom());
        if (dto.getThru() != null) {
            entity.setClthreeEntityCategoryClassificationThruDate(dto.getThru());
        }
        if (dto.getEntity() != null) {
            entity.setClThreeEntityCategoryClassificationEntity(CLThreeEntityMapper.toEntity(dto.getEntity()));
        }
        if (dto.getCategory() != null) {
            entity.setClThreeEntityCategoryClassificationCategory(CLThreeEntityCategoryMapper.toEntity(dto.getCategory()));
        }
        return entity;
    }

    public static List<CLThreeEntityCategoryClassificationDTO> toDTOs(List<CLThreeEntityCategoryClassification> entities) {
        LinkedList<CLThreeEntityCategoryClassificationDTO> result = new LinkedList<>();
        for (CLThreeEntityCategoryClassification entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeEntityCategoryClassification> toEntities(List<CLThreeEntityCategoryClassificationDTO> dtos) {
        LinkedList<CLThreeEntityCategoryClassification> result = new LinkedList<>();
        for (CLThreeEntityCategoryClassificationDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
