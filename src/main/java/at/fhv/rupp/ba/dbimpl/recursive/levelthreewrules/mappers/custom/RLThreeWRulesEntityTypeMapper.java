package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityType;

import java.util.LinkedList;
import java.util.List;

public class RLThreeWRulesEntityTypeMapper {
    private RLThreeWRulesEntityTypeMapper() {
        //hidden constructor
    }

    public static RLThreeWRulesEntityTypeDTO toDTO(RLThreeWRulesEntityType entity) {
        RLThreeWRulesEntityTypeDTO dto = new RLThreeWRulesEntityTypeDTO();
        dto.setId(entity.getRlThreeWRulesEntityTypeId());
        dto.setName(entity.getRlThreeWRulesEntityTypeName());
        if (entity.getRlThreeWRulesEntityTypeParent() != null) {
            dto.setParent(toDTO(entity.getRlThreeWRulesEntityTypeParent()));
        }
        return dto;
    }

    public static RLThreeWRulesEntityType toEntity(RLThreeWRulesEntityTypeDTO dto) {
        RLThreeWRulesEntityType entity = new RLThreeWRulesEntityType();
        if (dto.getId() != null) {
            entity.setRlThreeWRulesEntityTypeId(dto.getId());
        }
        entity.setRlThreeWRulesEntityTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setRlThreeWRulesEntityTypeParent(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLThreeWRulesEntityTypeDTO> toDTOs(List<RLThreeWRulesEntityType> entities) {
        LinkedList<RLThreeWRulesEntityTypeDTO> result = new LinkedList<>();
        for (RLThreeWRulesEntityType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeWRulesEntityType> toEntities(List<RLThreeWRulesEntityTypeDTO> dtos) {
        LinkedList<RLThreeWRulesEntityType> result = new LinkedList<>();
        for (RLThreeWRulesEntityTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
