package at.fhv.rupp.ba.dbimpl.classification.levelone.datagen;

import at.fhv.rupp.ba.dbimpl.classification.levelone.entities.CLOneEntity;
import at.fhv.rupp.ba.dbimpl.classification.levelone.repos.CLOneEntityRepo;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;

public class CLOneJavaFaker extends DataGenJavaFaker {

    private final CLOneEntityRepo entityRepo;

    public CLOneJavaFaker(CLOneEntityRepo entityRepo) {
        this.entityRepo = entityRepo;
    }

    @Override
    public void fillDB() {
        String name;
        String typeOne;
        String typeTwo;
        String typeThree;
        for (int i = 0; i < HIGH_GEN_AMOUNT; i++) {
            name = trimStringIfTooLong(FAKER.beer().name(), MIDDLE_CHAR_LIMIT);
            typeOne = trimStringIfTooLong(FAKER.beer().hop(), LOW_CHAR_LIMIT);
            typeTwo = trimStringIfTooLong(FAKER.beer().malt(), LOW_CHAR_LIMIT);
            typeThree = trimStringIfTooLong(FAKER.beer().style(), MIDDLE_CHAR_LIMIT);
            CLOneEntity entity = new CLOneEntity();
            entity.setCloneEntityName(name);
            entity.setCloneEntityTypeOne(typeOne);
            entity.setCloneEntityTypeTwo(typeTwo);
            entity.setCloneEntityTypeThree(typeThree);
            entityRepo.save(entity);
        }
    }
}
