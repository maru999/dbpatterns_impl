package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntity;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityMapper {

    private CLThreeWRollupsEntityMapper() {
        //hidden Constructor
    }

    public static CLThreeWRollupsEntityDTO toDTO(CLThreeWRollupsEntity entity) {
        CLThreeWRollupsEntityDTO dto = new CLThreeWRollupsEntityDTO();
        dto.setId(entity.getClthreewrEntityId());
        dto.setName(entity.getClthreewrEntityName());
        return dto;
    }

    public static CLThreeWRollupsEntity toEntity(CLThreeWRollupsEntityDTO dto) {
        CLThreeWRollupsEntity entity = new CLThreeWRollupsEntity();
        if (dto.getId() != null) {
            entity.setClthreewrEntityId(dto.getId());
        }
        entity.setClthreewrEntityName(dto.getName());
        return entity;
    }

    public static List<CLThreeWRollupsEntityDTO> toDTOs(List<CLThreeWRollupsEntity> entities) {
        LinkedList<CLThreeWRollupsEntityDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntity> toEntities(List<CLThreeWRollupsEntityDTO> dtos) {
        LinkedList<CLThreeWRollupsEntity> result = new LinkedList<>();
        for (CLThreeWRollupsEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
