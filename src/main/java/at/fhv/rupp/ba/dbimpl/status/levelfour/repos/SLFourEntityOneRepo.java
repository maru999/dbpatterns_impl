package at.fhv.rupp.ba.dbimpl.status.levelfour.repos;

import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityOne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SLFourEntityOneRepo extends JpaRepository<SLFourEntityOne, Integer> {
}
