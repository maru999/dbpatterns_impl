package at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos;

public class CLTwoEntitySubtypeThreeDTO extends CLTwoEntityDTO {
    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
