package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocType;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLTwoExpEntityAssocTypeMapstructMapper {
    @Mapping(source = "rlTwoExpEntityAssocTypeId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocTypeName", target = "name")
    RLTwoExpEntityAssocTypeDTO toDTO(RLTwoExpEntityAssocType entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLTwoExpEntityAssocType toEntity(RLTwoExpEntityAssocTypeDTO dto);

    @Mapping(source = "rlTwoExpEntityAssocTypeId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocTypeName", target = "name")
    void updateEntityFromDTO(RLTwoExpEntityAssocType entity, @MappingTarget RLTwoExpEntityAssocTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLTwoExpEntityAssocTypeDTO dto, @MappingTarget RLTwoExpEntityAssocType entity);

    List<RLTwoExpEntityAssocTypeDTO> toDTOs(List<RLTwoExpEntityAssocType> entities);

    List<RLTwoExpEntityAssocType> toEntities(List<RLTwoExpEntityAssocTypeDTO> dtos);
}
