package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityAssocDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssoc;

import java.util.LinkedList;
import java.util.List;

public class RLThreeWRulesEntityAssocMapper {
    private RLThreeWRulesEntityAssocMapper() {
        //hidden constructor
    }

    public static RLThreeWRulesEntityAssocDTO toDTO(RLThreeWRulesEntityAssoc entity) {
        RLThreeWRulesEntityAssocDTO dto = new RLThreeWRulesEntityAssocDTO();
        dto.setId(entity.getRlThreeWRulesEntityAssocId());
        dto.setFromDate(entity.getRlThreeWRulesEntityAssocFromDate());
        if (entity.getRlThreeWRulesEntityAssocThruDate() != null) {
            dto.setThruDate(entity.getRlThreeWRulesEntityAssocThruDate());
        }
        if (entity.getRlThreeWRulesEntityAssocType() != null) {
            dto.setType(RLThreeWRulesEntityAssocTypeMapper.toDTO(entity.getRlThreeWRulesEntityAssocType()));
        }
        if (entity.getRlThreeWRulesAssocFromEntity() != null) {
            dto.setFromEntity(RLThreeWRulesEntityMapper.toDTO(entity.getRlThreeWRulesAssocFromEntity()));
        }
        if (entity.getRlThreeWRulesAssocToEntity() != null) {
            dto.setToEntity(RLThreeWRulesEntityMapper.toDTO(entity.getRlThreeWRulesAssocToEntity()));
        }
        if (entity.getRlThreeWRulesEntityAssocRule() != null) {
            dto.setRule(RLThreeWRulesEntityAssocRuleMapper.toDTO(entity.getRlThreeWRulesEntityAssocRule()));
        }


        return dto;
    }

    public static RLThreeWRulesEntityAssoc toEntity(RLThreeWRulesEntityAssocDTO dto) {
        RLThreeWRulesEntityAssoc entity = new RLThreeWRulesEntityAssoc();
        if (dto.getId() != null) {
            entity.setRlThreeWRulesEntityAssocId(dto.getId());
        }
        if (dto.getFromDate() != null) {
            entity.setRlThreeWRulesEntityAssocFromDate(dto.getFromDate());
        }
        if (dto.getThruDate() != null) {
            entity.setRlThreeWRulesEntityAssocThruDate(dto.getThruDate());
        }
        if (dto.getType() != null) {
            entity.setRlThreeWRulesEntityAssocType(RLThreeWRulesEntityAssocTypeMapper.toEntity(dto.getType()));
        }
        if (dto.getFromEntity() != null) {
            entity.setRlThreeWRulesAssocFromEntity(RLThreeWRulesEntityMapper.toEntity(dto.getFromEntity()));
        }
        if (dto.getToEntity() != null) {
            entity.setRlThreeWRulesAssocToEntity(RLThreeWRulesEntityMapper.toEntity(dto.getToEntity()));
        }
        if (dto.getRule() != null) {
            entity.setRlThreeWRulesEntityAssocRule(RLThreeWRulesEntityAssocRuleMapper.toEntity(dto.getRule()));
        }
        return entity;
    }

    public static List<RLThreeWRulesEntityAssocDTO> toDTOs(List<RLThreeWRulesEntityAssoc> entities) {
        LinkedList<RLThreeWRulesEntityAssocDTO> result = new LinkedList<>();
        for (RLThreeWRulesEntityAssoc entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeWRulesEntityAssoc> toEntities(List<RLThreeWRulesEntityAssocDTO> dtos) {
        LinkedList<RLThreeWRulesEntityAssoc> result = new LinkedList<>();
        for (RLThreeWRulesEntityAssocDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
