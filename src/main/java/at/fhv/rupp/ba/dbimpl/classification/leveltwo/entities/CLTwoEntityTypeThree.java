package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "cltwo_entity_type_three")
@EntityListeners(AuditingEntityListener.class)
public class CLTwoEntityTypeThree {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cltwo_entity_type_three_id")
    private int clTwoEntityTypeThreeId;

    @Column(name = "cltwo_entity_type_three_name", nullable = false)
    private String clTwoEntityTypeThreeName;

    public int getClTwoEntityTypeThreeId() {
        return clTwoEntityTypeThreeId;
    }

    public void setClTwoEntityTypeThreeId(int clTwoEntityTypeThreeId) {
        this.clTwoEntityTypeThreeId = clTwoEntityTypeThreeId;
    }

    public String getClTwoEntityTypeThreeName() {
        return clTwoEntityTypeThreeName;
    }

    public void setClTwoEntityTypeThreeName(String clTwoEntityTypeThreeName) {
        this.clTwoEntityTypeThreeName = clTwoEntityTypeThreeName;
    }
}