package at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos;

public class RLOneEntityThreeDTO {
    private Integer id;
    private String name;
    private RLOneEntityTwoDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLOneEntityTwoDTO getParent() {
        return parent;
    }

    public void setParent(RLOneEntityTwoDTO parent) {
        this.parent = parent;
    }
}
