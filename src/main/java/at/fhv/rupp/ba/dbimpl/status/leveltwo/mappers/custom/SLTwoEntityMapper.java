package at.fhv.rupp.ba.dbimpl.status.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntity;

import java.util.LinkedList;
import java.util.List;

public class SLTwoEntityMapper {

    private SLTwoEntityMapper() {
        //hidden constructor
    }

    public static SLTwoEntityDTO toDTO(SLTwoEntity entity) {
        SLTwoEntityDTO dto = new SLTwoEntityDTO();
        dto.setId(entity.getSlTwoEntityId());
        if (entity.getSlTwoEntityStatusDateTime() != null) {
            dto.setStatusDateTime(entity.getSlTwoEntityStatusDateTime());
        }
        if (entity.getSlTwoEntityStatusType() != null) {
            dto.setStatusType(SLTwoEntityStatusTypeMapper.toDTO(entity.getSlTwoEntityStatusType()));
        }
        return dto;
    }

    public static SLTwoEntity toEntity(SLTwoEntityDTO dto) {
        SLTwoEntity entity = new SLTwoEntity();
        if (dto.getId() != null) {
            entity.setSlTwoEntityId(dto.getId());
        }
        if (dto.getStatusDateTime() != null) {
            entity.setSlTwoEntityStatusDateTime(dto.getStatusDateTime());
        }
        if (dto.getStatusType() != null) {
            entity.setSlTwoEntityStatusType(SLTwoEntityStatusTypeMapper.toEntity(dto.getStatusType()));
        }
        return entity;
    }

    public static List<SLTwoEntityDTO> toDTOs(List<SLTwoEntity> entities) {
        LinkedList<SLTwoEntityDTO> result = new LinkedList<>();
        for (SLTwoEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLTwoEntity> toEntities(List<SLTwoEntityDTO> dtos) {
        LinkedList<SLTwoEntity> result = new LinkedList<>();
        for (SLTwoEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
