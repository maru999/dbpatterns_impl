package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

public class CLThreeWRollupsDataProviderDTO {
    private Integer id;
    private String name;
    private String roleName = "Data Provider";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
