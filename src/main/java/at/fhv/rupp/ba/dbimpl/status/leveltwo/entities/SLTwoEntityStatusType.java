package at.fhv.rupp.ba.dbimpl.status.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "sltwo_entity_status_type")
@EntityListeners(AuditingEntityListener.class)
@PrimaryKeyJoinColumn(name = "sltwo_entity_status_type_id", referencedColumnName = "sltwo_status_type_id")
public class SLTwoEntityStatusType extends SLTwoStatusType {

    @Column(name = "sltwo_entity_status_type_some_attr")
    private String slTwoEntityStatusTypeSomeAttr;

    public String getSlTwoEntityStatusTypeSomeAttr() {
        return slTwoEntityStatusTypeSomeAttr;
    }

    public void setSlTwoEntityStatusTypeSomeAttr(String slTwoEntityStatusTypeSomeAttr) {
        this.slTwoEntityStatusTypeSomeAttr = slTwoEntityStatusTypeSomeAttr;
    }
}
