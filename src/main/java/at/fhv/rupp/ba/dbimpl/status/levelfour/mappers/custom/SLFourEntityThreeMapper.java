package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityThreeDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityThree;

import java.util.LinkedList;
import java.util.List;

public class SLFourEntityThreeMapper {

    private SLFourEntityThreeMapper() {
        //hidden constructor
    }

    public static SLFourEntityThreeDTO toDTO(SLFourEntityThree entity) {
        SLFourEntityThreeDTO dto = new SLFourEntityThreeDTO();
        dto.setId(entity.getSlFourEntityThreeId());
        dto.setAttr(entity.getSlFourEntityThreeAttr());
        return dto;
    }

    public static SLFourEntityThree toEntity(SLFourEntityThreeDTO dto) {
        SLFourEntityThree entity = new SLFourEntityThree();
        if (dto.getId() != null) {
            entity.setSlFourEntityThreeId(dto.getId());
        }
        if (dto.getAttr() != null) {
            entity.setSlFourEntityThreeAttr(dto.getAttr());
        }
        return entity;
    }

    public static List<SLFourEntityThreeDTO> toDTOs(List<SLFourEntityThree> entities) {
        LinkedList<SLFourEntityThreeDTO> result = new LinkedList<>();
        for (SLFourEntityThree entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLFourEntityThree> toEntities(List<SLFourEntityThreeDTO> dtos) {
        LinkedList<SLFourEntityThree> result = new LinkedList<>();
        for (SLFourEntityThreeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
