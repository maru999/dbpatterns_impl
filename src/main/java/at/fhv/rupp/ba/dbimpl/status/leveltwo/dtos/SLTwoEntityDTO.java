package at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos;

import java.time.LocalDateTime;

public class SLTwoEntityDTO {
    private Integer id;
    private LocalDateTime statusDateTime;
    private SLTwoEntityStatusTypeDTO statusType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getStatusDateTime() {
        return statusDateTime;
    }

    public void setStatusDateTime(LocalDateTime statusDateTime) {
        this.statusDateTime = statusDateTime;
    }

    public SLTwoEntityStatusTypeDTO getStatusType() {
        return statusType;
    }

    public void setStatusType(SLTwoEntityStatusTypeDTO statusType) {
        this.statusType = statusType;
    }
}
