package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.exceptions;

public class RLTwoExpEntityTypeNotFoundException extends Exception {
    public RLTwoExpEntityTypeNotFoundException(String message) {
        super(message);
    }
}
