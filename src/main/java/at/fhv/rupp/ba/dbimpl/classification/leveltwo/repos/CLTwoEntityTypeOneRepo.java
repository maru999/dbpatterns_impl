package at.fhv.rupp.ba.dbimpl.classification.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeOne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLTwoEntityTypeOneRepo extends JpaRepository<CLTwoEntityTypeOne, Integer> {
}
