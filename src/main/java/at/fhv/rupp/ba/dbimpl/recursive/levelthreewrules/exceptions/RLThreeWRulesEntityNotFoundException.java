package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.exceptions;

public class RLThreeWRulesEntityNotFoundException extends Exception {
    public RLThreeWRulesEntityNotFoundException(String message) {
        super(message);
    }
}
