package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.repos;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RLTwoExpEntityAssocTypeRepo extends JpaRepository<RLTwoExpEntityAssocType, Integer> {
}
