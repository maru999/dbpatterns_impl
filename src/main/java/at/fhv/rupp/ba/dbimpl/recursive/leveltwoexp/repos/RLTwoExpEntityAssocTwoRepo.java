package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.repos;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocTwo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLTwoExpEntityAssocTwoRepo extends JpaRepository<RLTwoExpEntityAssocTwo, Integer> {
    @Query(value = "SELECT * FROM rltwoxp_entity_assoc_two WHERE rltwoxp_entity_assoc_two_to_entity = :id", nativeQuery = true)
    List<RLTwoExpEntityAssocTwo> getAssocTwosByToEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rltwoxp_entity_assoc_two WHERE rltwoxp_entity_assoc_two_from_entity = :id", nativeQuery = true)
    List<RLTwoExpEntityAssocTwo> getAssocTwosByFromEntityId(@Param("id") Integer entityId);

}
