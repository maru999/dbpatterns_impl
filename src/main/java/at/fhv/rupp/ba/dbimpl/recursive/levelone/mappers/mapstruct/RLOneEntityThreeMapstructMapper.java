package at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityThreeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityThree;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = RLOneEntityTwoMapstructMapper.class)
public interface RLOneEntityThreeMapstructMapper {
    @Mapping(source = "rloneEntityThreeId", target = "id")
    @Mapping(source = "rloneEntityThreeName", target = "name")
    @Mapping(source = "rloneEntityThreeParent", target = "parent")
    RLOneEntityThreeDTO toDTO(RLOneEntityThree entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLOneEntityThree toEntity(RLOneEntityThreeDTO dto);

    @Mapping(source = "rloneEntityThreeId", target = "id")
    @Mapping(source = "rloneEntityThreeName", target = "name")
    @Mapping(source = "rloneEntityThreeParent", target = "parent")
    void updateEntityFromDTO(RLOneEntityThree entity, @MappingTarget RLOneEntityThreeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLOneEntityThreeDTO dto, @MappingTarget RLOneEntityThree entity);

    List<RLOneEntityThreeDTO> toDTOs(List<RLOneEntityThree> entities);

    List<RLOneEntityThree> toEntities(List<RLOneEntityThreeDTO> dtos);
}
