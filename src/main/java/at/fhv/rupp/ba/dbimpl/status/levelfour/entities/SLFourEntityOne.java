package at.fhv.rupp.ba.dbimpl.status.levelfour.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "slfour_entity_one")
@EntityListeners(AuditingEntityListener.class)
public class SLFourEntityOne {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slfour_entity_one_id")
    private int slFourEntityOneId;

    @Column(name = "slfour_entity_one_desc")
    private String slFourEntityOneDesc;

    public int getSlFourEntityOneId() {
        return slFourEntityOneId;
    }

    public void setSlFourEntityOneId(int slFourEntityOneId) {
        this.slFourEntityOneId = slFourEntityOneId;
    }

    public String getSlFourEntityOneDesc() {
        return slFourEntityOneDesc;
    }

    public void setSlFourEntityOneDesc(String slFourEntityOneDesc) {
        this.slFourEntityOneDesc = slFourEntityOneDesc;
    }
}
