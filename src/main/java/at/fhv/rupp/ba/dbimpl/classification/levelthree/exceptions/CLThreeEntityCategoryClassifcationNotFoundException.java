package at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions;

public class CLThreeEntityCategoryClassifcationNotFoundException extends Exception {
    public CLThreeEntityCategoryClassifcationNotFoundException(String message) {
        super(message);
    }
}
