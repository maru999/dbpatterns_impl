package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos;

public class RLTwoExpEntityTypeDTO {
    private Integer id;
    private String name;
    private RLTwoExpEntityTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLTwoExpEntityTypeDTO getParent() {
        return parent;
    }

    public void setParent(RLTwoExpEntityTypeDTO parent) {
        this.parent = parent;
    }
}
