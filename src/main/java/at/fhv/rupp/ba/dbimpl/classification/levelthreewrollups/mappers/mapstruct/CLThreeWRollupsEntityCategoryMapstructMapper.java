package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategory;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = CLThreeWRollupsEntityCategoryTypeMapstructMapper.class)
public interface CLThreeWRollupsEntityCategoryMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryName", target = "name")
    @Mapping(source = "entityCategoryType", target = "type")
    CLThreeWRollupsEntityCategoryDTO toDTO(CLThreeWRollupsEntityCategory entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategory toEntity(CLThreeWRollupsEntityCategoryDTO dto);

    @Mapping(source = "clthreewrEntityCategoryId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryName", target = "name")
    @Mapping(source = "entityCategoryType", target = "type")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategory entity, @MappingTarget CLThreeWRollupsEntityCategoryDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryDTO dto, @MappingTarget CLThreeWRollupsEntityCategory entity);

    List<CLThreeWRollupsEntityCategoryDTO> toDTOs(List<CLThreeWRollupsEntityCategory> entities);

    List<CLThreeWRollupsEntityCategory> toEntities(List<CLThreeWRollupsEntityCategoryDTO> dtos);
}
