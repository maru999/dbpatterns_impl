package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryRollupDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryRollup;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLThreeWRollupsEntityCategoryMapstructMapper.class, CLThreeWRollupsEntityCategoryRollupTypeMapstructMapper.class})
public interface CLThreeWRollupsEntityCategoryRollupMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryRollupId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryRollupFromDate", target = "from")
    @Mapping(source = "clthreewrEntityCategoryRollupThruDate", target = "thru")
    @Mapping(source = "clthreeEntityCategoryRollupRollupType", target = "rollupType")
    @Mapping(source = "clthreewrEntityCategoryRollupParentCategory", target = "parent")
    @Mapping(source = "clthreewrEntityCategoryRollupChildCategory", target = "child")
    CLThreeWRollupsEntityCategoryRollupDTO toDTO(CLThreeWRollupsEntityCategoryRollup entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategoryRollup toEntity(CLThreeWRollupsEntityCategoryRollupDTO dto);

    @Mapping(source = "clthreewrEntityCategoryRollupId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryRollupFromDate", target = "from")
    @Mapping(source = "clthreewrEntityCategoryRollupThruDate", target = "thru")
    @Mapping(source = "clthreeEntityCategoryRollupRollupType", target = "rollupType")
    @Mapping(source = "clthreewrEntityCategoryRollupParentCategory", target = "parent")
    @Mapping(source = "clthreewrEntityCategoryRollupChildCategory", target = "child")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategoryRollup entity, @MappingTarget CLThreeWRollupsEntityCategoryRollupDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryRollupDTO dto, @MappingTarget CLThreeWRollupsEntityCategoryRollup entity);

    List<CLThreeWRollupsEntityCategoryRollupDTO> toDTOs(List<CLThreeWRollupsEntityCategoryRollup> entities);

    List<CLThreeWRollupsEntityCategoryRollup> toEntities(List<CLThreeWRollupsEntityCategoryRollupDTO> dtos);
}
