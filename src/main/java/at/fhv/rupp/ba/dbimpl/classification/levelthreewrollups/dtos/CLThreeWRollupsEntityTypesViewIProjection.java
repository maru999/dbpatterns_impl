package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

import java.time.LocalDate;

public interface CLThreeWRollupsEntityTypesViewIProjection {

    Integer getclthreewr_entity_id();

    String getclthreewr_entity_name();

    String getrole_name();

    Integer getdata_provider_id();

    String getdp_name();

    Integer getclthreewr_entity_category_rollup_type_id();

    String getclthreewr_entity_category_rollup_type_name();

    Integer getclthreewr_entity_category_rollup_type_parent_id();

    Integer getclthreewr_entity_category_type_rollup_type_id();

    String getclthreewr_entity_category_type_rollup_type_name();

    Integer getclthreewr_entity_category_trollup_type_parent_id();

    Integer getclthreewr_entity_category_type_scheme_id();

    String getclthreewr_entity_category_type_scheme_name();

    Integer getclthreewr_entity_category_type_scheme_dp_id();

    Integer getclthreewr_entity_category_type_id();

    String getclthreewr_entity_category_type_name();

    Integer getclthreewr_entity_category_type_entity_category_type_scheme_id();

    Integer getclthreewr_entity_category_id();

    String getclthreewr_entity_category_name();

    Integer getclthreewr_entity_category_entity_category_type_id();

    Integer getclthreewr_entity_category_classification_id();

    LocalDate getclthreewr_entity_category_classification_from();

    LocalDate getclthreewr_entity_category_classification_thru();

    Integer getclthreewr_entity_category_classification_entity_id();

    Integer getclthreewr_entity_category_classification_entity_category_id();

    Integer getclthreewr_entity_category_rollup_id();

    LocalDate getclthreewr_entity_category_rollup_from();

    LocalDate getclthreewr_entity_category_rollup_thru();

    Integer getclthreewr_entity_category_rollup_parent_entity_category_id();

    Integer getclthreewr_entity_category_rollup_child_entity_category_id();

    Integer getclthreewr_entity_category_rollup_entity_category_rollup_type_id();

    Integer getclthreewr_entity_category_type_rollup_id();

    LocalDate getclthreewr_entity_category_type_rollup_from();

    LocalDate getclthreewr_entity_category_type_rollup_thru();

    Integer getclthreewr_entity_category_type_rollup_parent_entity_category_id();

    Integer getclthreewr_entity_category_type_rollup_child_entity_category_id();

    Integer getclthreewr_entity_category_typer_entity_category_type_rtype_id();

}
