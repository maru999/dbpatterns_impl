package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssocType;

import java.util.LinkedList;
import java.util.List;

public class RLThreeWRulesEntityAssocTypeMapper {
    private RLThreeWRulesEntityAssocTypeMapper() {
        //hidden constructor
    }

    public static RLThreeWRulesEntityAssocTypeDTO toDTO(RLThreeWRulesEntityAssocType entity) {
        RLThreeWRulesEntityAssocTypeDTO dto = new RLThreeWRulesEntityAssocTypeDTO();
        dto.setId(entity.getRlThreeWRulesEntityAssocTypeId());
        dto.setName(entity.getRlThreeWRulesEntityAssocTypeName());
        if (entity.getRlThreeWRulesEntityAssocTypeParent() != null) {
            dto.setParent(toDTO(entity.getRlThreeWRulesEntityAssocTypeParent()));
        }
        return dto;
    }

    public static RLThreeWRulesEntityAssocType toEntity(RLThreeWRulesEntityAssocTypeDTO dto) {
        RLThreeWRulesEntityAssocType entity = new RLThreeWRulesEntityAssocType();
        if (dto.getId() != null) {
            entity.setRlThreeWRulesEntityAssocTypeId(dto.getId());
        }
        entity.setRlThreeWRulesEntityAssocTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setRlThreeWRulesEntityAssocTypeParent(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLThreeWRulesEntityAssocTypeDTO> toDTOs(List<RLThreeWRulesEntityAssocType> entities) {
        LinkedList<RLThreeWRulesEntityAssocTypeDTO> result = new LinkedList<>();
        for (RLThreeWRulesEntityAssocType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeWRulesEntityAssocType> toEntities(List<RLThreeWRulesEntityAssocTypeDTO> dtos) {
        LinkedList<RLThreeWRulesEntityAssocType> result = new LinkedList<>();
        for (RLThreeWRulesEntityAssocTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
