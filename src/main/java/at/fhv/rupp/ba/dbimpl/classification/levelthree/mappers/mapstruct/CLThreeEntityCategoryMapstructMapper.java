package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityCategoryDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategory;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = CLThreeEntityCategoryTypeMapstructMapper.class)
public interface CLThreeEntityCategoryMapstructMapper {

    @Mapping(source = "clthreeEntityCategoryId", target = "id")
    @Mapping(source = "clthreeEntityCategoryName", target = "name")
    @Mapping(source = "clThreeEntityCategoryParent", target = "parent")
    @Mapping(source = "clthreeEntityCategoryFromDate", target = "from")
    @Mapping(source = "clthreeEntityCategoryThruDate", target = "thru")
    @Mapping(source = "clThreeEntityCategoryType", target = "categoryType")
    CLThreeEntityCategoryDTO toDTO(CLThreeEntityCategory entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeEntityCategory toEntity(CLThreeEntityCategoryDTO dto);

    @Mapping(source = "clthreeEntityCategoryId", target = "id")
    @Mapping(source = "clthreeEntityCategoryName", target = "name")
    @Mapping(source = "clThreeEntityCategoryParent", target = "parent")
    @Mapping(source = "clthreeEntityCategoryFromDate", target = "from")
    @Mapping(source = "clthreeEntityCategoryThruDate", target = "thru")
    @Mapping(source = "clThreeEntityCategoryType", target = "categoryType")
    void updateEntityFromDTO(CLThreeEntityCategory entity, @MappingTarget CLThreeEntityCategoryDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeEntityCategoryDTO dto, @MappingTarget CLThreeEntityCategory entity);

    List<CLThreeEntityCategoryDTO> toDTOs(List<CLThreeEntityCategory> entities);

    List<CLThreeEntityCategory> toEntities(List<CLThreeEntityCategoryDTO> dtos);
}
