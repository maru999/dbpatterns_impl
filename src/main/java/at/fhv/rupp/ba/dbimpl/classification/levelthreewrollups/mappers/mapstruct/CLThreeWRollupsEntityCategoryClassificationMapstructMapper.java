package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryClassificationDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryClassification;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLThreeWRollupsEntityMapstructMapper.class, CLThreeWRollupsEntityCategoryMapstructMapper.class})
public interface CLThreeWRollupsEntityCategoryClassificationMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryClassificationId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryClassificationFromDate", target = "from")
    @Mapping(source = "clthreewrEntityCategoryClassificationThruDate", target = "thru")
    @Mapping(source = "clthreewrEntityCategoryClassifciationEntity", target = "entity")
    @Mapping(source = "clthreewrEntityCategoryClassificationEntityCategory", target = "category")
    CLThreeWRollupsEntityCategoryClassificationDTO toDTO(CLThreeWRollupsEntityCategoryClassification entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategoryClassification toEntity(CLThreeWRollupsEntityCategoryClassificationDTO dto);

    @Mapping(source = "clthreewrEntityCategoryClassificationId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryClassificationFromDate", target = "from")
    @Mapping(source = "clthreewrEntityCategoryClassificationThruDate", target = "thru")
    @Mapping(source = "clthreewrEntityCategoryClassifciationEntity", target = "entity")
    @Mapping(source = "clthreewrEntityCategoryClassificationEntityCategory", target = "category")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategoryClassification entity, @MappingTarget CLThreeWRollupsEntityCategoryClassificationDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryClassificationDTO dto, @MappingTarget CLThreeWRollupsEntityCategoryClassification entity);

    List<CLThreeWRollupsEntityCategoryClassificationDTO> toDTOs(List<CLThreeWRollupsEntityCategoryClassification> entities);

    List<CLThreeWRollupsEntityCategoryClassification> toEntities(List<CLThreeWRollupsEntityCategoryClassificationDTO> dtos);
}
