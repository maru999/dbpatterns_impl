package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryRollupDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryRollup;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryRollupMapper {

    private CLThreeWRollupsEntityCategoryRollupMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryRollupDTO toDTO(CLThreeWRollupsEntityCategoryRollup entity) {
        CLThreeWRollupsEntityCategoryRollupDTO dto = new CLThreeWRollupsEntityCategoryRollupDTO();
        dto.setId(entity.getClthreewrEntityCategoryRollupId());
        dto.setFrom(entity.getClthreewrEntityCategoryRollupFromDate());
        if (entity.getClthreewrEntityCategoryRollupThruDate() != null) {
            dto.setThru(entity.getClthreewrEntityCategoryRollupThruDate());
        }
        if (entity.getClthreewrEntityCategoryRollupParentCategory() != null) {
            dto.setParent(CLThreeWRollupsEntityCategoryMapper.toDTO(entity.getClthreewrEntityCategoryRollupParentCategory()));
        }
        if (entity.getClthreewrEntityCategoryRollupChildCategory() != null) {
            dto.setChild(CLThreeWRollupsEntityCategoryMapper.toDTO(entity.getClthreewrEntityCategoryRollupChildCategory()));
        }
        if (entity.getClthreeEntityCategoryRollupRollupType() != null) {
            dto.setRollupType(CLThreeWRollupsEntityCategoryRollupTypeMapper.toDTO(entity.getClthreeEntityCategoryRollupRollupType()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategoryRollup toEntity(CLThreeWRollupsEntityCategoryRollupDTO dto) {
        CLThreeWRollupsEntityCategoryRollup entity = new CLThreeWRollupsEntityCategoryRollup();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryRollupId(dto.getId());
        }
        entity.setClthreewrEntityCategoryRollupFromDate(dto.getFrom());
        if (dto.getThru() != null) {
            entity.setClthreewrEntityCategoryRollupThruDate(dto.getThru());
        }
        if (dto.getParent() != null) {
            entity.setClthreewrEntityCategoryRollupParentCategory(CLThreeWRollupsEntityCategoryMapper.toEntity(dto.getParent()));
        }
        if (dto.getChild() != null) {
            entity.setClthreewrEntityCategoryRollupChildCategory(CLThreeWRollupsEntityCategoryMapper.toEntity(dto.getChild()));
        }
        if (dto.getRollupType() != null) {
            entity.setClthreeEntityCategoryRollupRollupType(CLThreeWRollupsEntityCategoryRollupTypeMapper.toEntity(dto.getRollupType()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryRollupDTO> toDTOs(List<CLThreeWRollupsEntityCategoryRollup> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryRollupDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryRollup entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategoryRollup> toEntities(List<CLThreeWRollupsEntityCategoryRollupDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategoryRollup> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryRollupDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
