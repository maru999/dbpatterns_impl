package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.custom;


import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntity;

import java.util.LinkedList;
import java.util.List;

public class RLTwoExpEntityMapper {
    private RLTwoExpEntityMapper() {
        //hidden constructor
    }

    public static RLTwoExpEntityDTO toDTO(RLTwoExpEntity entity) {
        RLTwoExpEntityDTO dto = new RLTwoExpEntityDTO();
        dto.setId(entity.getRlTwoExpEntityId());
        if (entity.getRlTwoExpEntityAssocToEntityOne() != null) {
            dto.setAssocToOne(toDTO(entity.getRlTwoExpEntityAssocToEntityOne()));
        }
        if (entity.getRlTwoExpEntityAssocToEntityTwo() != null) {
            dto.setAssocToTwo(toDTO(entity.getRlTwoExpEntityAssocToEntityTwo()));
        }
        if (entity.getRlTwoExpEntityType() != null) {
            dto.setType(RLTwoExpEntityTypeMapper.toDTO(entity.getRlTwoExpEntityType()));
        }
        return dto;
    }

    public static RLTwoExpEntity toEntity(RLTwoExpEntityDTO dto) {
        RLTwoExpEntity entity = new RLTwoExpEntity();
        if (dto.getId() != null) {
            entity.setRlTwoExpEntityId(dto.getId());
        }
        if (dto.getAssocToOne() != null) {
            entity.setRlTwoExpEntityAssocToEntityOne(toEntity(dto.getAssocToOne()));
        }
        if (dto.getAssocToTwo() != null) {
            entity.setRlTwoExpEntityAssocToEntityTwo(toEntity(dto.getAssocToTwo()));
        }
        if (dto.getType() != null) {
            entity.setRlTwoExpEntityType(RLTwoExpEntityTypeMapper.toEntity(dto.getType()));
        }
        return entity;
    }

    public static List<RLTwoExpEntityDTO> toDTOs(List<RLTwoExpEntity> entities) {
        LinkedList<RLTwoExpEntityDTO> result = new LinkedList<>();
        for (RLTwoExpEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLTwoExpEntity> toEntities(List<RLTwoExpEntityDTO> dtos) {
        LinkedList<RLTwoExpEntity> result = new LinkedList<>();
        for (RLTwoExpEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }

}
