package at.fhv.rupp.ba.dbimpl.classification.levelthree.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.datagen.CLThreeJavaFaker;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.*;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom.CLThreeEntityCategoryClassificationMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom.CLThreeEntityCategoryMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom.CLThreeEntityCategoryTypeMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom.CLThreeEntityMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntity;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategory;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryClassification;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryType;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions.CLThreeEntityCategoryClassifcationNotFoundException;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions.CLThreeEntityCategoryNotFoundException;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions.CLThreeEntityCategoryTypeNotFoundException;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions.CLThreeEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityCategoryClassificationRepo;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityCategoryRepo;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityCategoryTypeRepo;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.repos.CLThreeEntityRepo;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/classification/L3")
public class CLThreeController {

    private final CLThreeEntityRepo entityRepo;
    private final CLThreeEntityCategoryRepo categoryRepo;
    private final CLThreeEntityCategoryTypeRepo categoryTypeRepo;
    private final CLThreeEntityCategoryClassificationRepo categoryClassificationRepo;
    private final DataGenJavaFaker faker;

    public CLThreeController(CLThreeEntityRepo entityRepo, CLThreeEntityCategoryRepo categoryRepo, CLThreeEntityCategoryTypeRepo categoryTypeRepo, CLThreeEntityCategoryClassificationRepo categoryClassificationRepo) {
        this.entityRepo = entityRepo;
        this.categoryRepo = categoryRepo;
        this.categoryTypeRepo = categoryTypeRepo;
        this.categoryClassificationRepo = categoryClassificationRepo;
        this.faker = new CLThreeJavaFaker(entityRepo, categoryRepo, categoryTypeRepo, categoryClassificationRepo);
    }

    @GetMapping("/entities")
    public List<CLThreeEntityDTO> getAllEntities() {
        return CLThreeEntityMapper.toDTOs(entityRepo.findAll());
    }

    @PostMapping("/entities")
    public CLThreeEntityDTO createEntity(@Valid @RequestBody CLThreeEntityDTO entityDTO) {
        return CLThreeEntityMapper.toDTO(entityRepo.save(CLThreeEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<CLThreeEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody CLThreeEntityDTO entityDetails) throws CLThreeEntityNotFoundException {
        CLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLThreeEntityNotFoundException(getExceptionString("CLThreeEntity", entityId)));
        CLThreeEntityDTO entityDTO = CLThreeEntityMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        final CLThreeEntityDTO updatedEntity = CLThreeEntityMapper.toDTO(entityRepo.save(CLThreeEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws CLThreeEntityNotFoundException {
        CLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLThreeEntityNotFoundException(getExceptionString("CLThreeEntity", entityId)));
        List<CLThreeEntityCategoryClassification> classifications = categoryClassificationRepo.getCategoryClassificationsByEntityId(entityId);
        for (CLThreeEntityCategoryClassification classification : classifications) {
            categoryClassificationRepo.delete(classification);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/entity/{id}")
    public ResponseEntity<CLThreeEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws CLThreeEntityNotFoundException {
        CLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLThreeEntityNotFoundException(getExceptionString("CLThreeEntity", entityId)));
        return ResponseEntity.ok().body(CLThreeEntityMapper.toDTO(entity));
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<CLThreeEntityCategoryDTO> getCategoryById(@PathVariable(value = "id") Integer categoryId) throws CLThreeEntityCategoryNotFoundException {
        CLThreeEntityCategory category = categoryRepo.findById(categoryId).orElseThrow(() -> new CLThreeEntityCategoryNotFoundException(getExceptionString("CLThreeEntityCategory", categoryId)));
        return ResponseEntity.ok().body(CLThreeEntityCategoryMapper.toDTO(category));
    }

    @GetMapping("/categoryType/{id}")
    public ResponseEntity<CLThreeEntityCategoryTypeDTO> getCategoryTypeById(@PathVariable(value = "id") Integer typeId) throws CLThreeEntityCategoryTypeNotFoundException {
        CLThreeEntityCategoryType type = categoryTypeRepo.findById(typeId).orElseThrow(() -> new CLThreeEntityCategoryTypeNotFoundException(getExceptionString("CLThreeEntityCategoryType", typeId)));
        return ResponseEntity.ok().body(CLThreeEntityCategoryTypeMapper.toDTO(type));
    }

    @GetMapping("/classification/{id}")
    public ResponseEntity<CLThreeEntityCategoryClassificationDTO> getCategoryClassificationById(@PathVariable(value = "id") Integer classificationId) throws CLThreeEntityCategoryClassifcationNotFoundException {
        CLThreeEntityCategoryClassification classification = categoryClassificationRepo.findById(classificationId).orElseThrow(() -> new CLThreeEntityCategoryClassifcationNotFoundException(getExceptionString("CLThreeEntityCategoryType", classificationId)));
        return ResponseEntity.ok().body(CLThreeEntityCategoryClassificationMapper.toDTO(classification));
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/classification/classificationlevelthree.sql");
        DbimplApplication.restart();
    }

    @GetMapping("entitiesWTypes")
    public List<CLThreeEntityWithTypesIProjection> getEntitiesWTypes() {
        return entityRepo.getAllEntitiesWTypes();
    }

    @GetMapping("entitiesWTypes/entityId/{id}")
    public List<CLThreeEntityWithTypesIProjection> getEntitiesWTypesByEntityId(@PathVariable("id") Integer entityId) {
        return entityRepo.getEntitiesWTypesByEntityId(entityId);
    }

    @GetMapping("entitiesWTypes/catName/{name}")
    public List<CLThreeEntityWithTypesIProjection> getEntitiesWTypesByCatName(@PathVariable("name") String catName) {
        return entityRepo.getEntitiesWTypesByCatName(catName);
    }

    @GetMapping("entitiesWTypes/{start}/{end}")
    public List<CLThreeEntityWithTypesIProjection> getEntitiesWTypesClassificationFromBetween(@PathVariable("start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start, @PathVariable("end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end) {
        return entityRepo.getEntitiesWTypesClassificationFromBetween(start, end);
    }

    @GetMapping("entitiesWTypes/catTopParents")
    public List<CLThreeEntityCategoryDTO> getTopParentsCatNames() {
        return CLThreeEntityCategoryMapper.toDTOs(categoryRepo.getTopParentCategoryNames());
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }
}