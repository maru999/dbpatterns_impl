package at.fhv.rupp.ba.dbimpl.classification.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthree_entity")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int clthreeEntityId;

    @Column(name = "clthree_entity_name")
    private String clthreeEntityName;

    public int getClthreeEntityId() {
        return clthreeEntityId;
    }

    public void setClthreeEntityId(int clthreeEntityId) {
        this.clthreeEntityId = clthreeEntityId;
    }

    public String getClthreeEntityName() {
        return clthreeEntityName;
    }

    public void setClthreeEntityName(String clthreeEntityName) {
        this.clthreeEntityName = clthreeEntityName;
    }
}
