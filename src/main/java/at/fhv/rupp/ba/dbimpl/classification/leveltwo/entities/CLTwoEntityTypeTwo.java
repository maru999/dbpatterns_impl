package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "cltwo_entity_type_two")
@EntityListeners(AuditingEntityListener.class)
public class CLTwoEntityTypeTwo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cltwo_entity_type_two_id")
    private int clTwoEntityTypeTwoId;

    @Column(name = "cltwo_entity_type_two_name", nullable = false)
    private String clTwoEntityTypeTwoName;

    @ManyToOne
    @JoinColumn(name = "cltwo_entity_type_two_parent")
    private CLTwoEntityTypeTwo clTwoEntityTypeTwoParent;

    public int getClTwoEntityTypeTwoId() {
        return clTwoEntityTypeTwoId;
    }

    public void setClTwoEntityTypeTwoId(int clTwoEntityTypeTwoId) {
        this.clTwoEntityTypeTwoId = clTwoEntityTypeTwoId;
    }

    public String getClTwoEntityTypeTwoName() {
        return clTwoEntityTypeTwoName;
    }

    public void setClTwoEntityTypeTwoName(String clTwoEntityTypeTwoName) {
        this.clTwoEntityTypeTwoName = clTwoEntityTypeTwoName;
    }

    public CLTwoEntityTypeTwo getClTwoEntityTypeTwoParent() {
        return clTwoEntityTypeTwoParent;
    }

    public void setClTwoEntityTypeTwoParent(CLTwoEntityTypeTwo clTwoEntityTypeTwoParent) {
        this.clTwoEntityTypeTwoParent = clTwoEntityTypeTwoParent;
    }
}
