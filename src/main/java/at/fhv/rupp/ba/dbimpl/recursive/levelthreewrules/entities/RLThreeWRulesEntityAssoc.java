package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "rlthreewr_entity_assoc")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeWRulesEntityAssoc {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthreewr_entity_assoc_id")
    private int rlThreeWRulesEntityAssocId;

    @Column(name = "rlthreewr_entity_assoc_from_date", nullable = false)
    private LocalDate rlThreeWRulesEntityAssocFromDate;

    @Column(name = "rlthreewr_entity_assoc_thru_date")
    private LocalDate rlThreeWRulesEntityAssocThruDate;

    @ManyToOne
    @JoinColumn(name = "rlthreewr_entity_assoc_type_fk")
    private RLThreeWRulesEntityAssocType rlThreeWRulesEntityAssocType;

    @ManyToOne
    @JoinColumn(name = "rlthreewr_entity_assoc_from_ent")
    private RLThreeWRulesEntity rlThreeWRulesAssocFromEntity;

    @ManyToOne
    @JoinColumn(name = "rlthreewr_entity_assoc_to_ent")
    private RLThreeWRulesEntity rlThreeWRulesAssocToEntity;

    @ManyToOne
    @JoinColumn(name = "rlthreewr_entity_assoc_rule_fk")
    private RLThreeWRulesEntityAssocRule rlThreeWRulesEntityAssocRule;

    public int getRlThreeWRulesEntityAssocId() {
        return rlThreeWRulesEntityAssocId;
    }

    public void setRlThreeWRulesEntityAssocId(int rlThreeWRulesEntityAssocId) {
        this.rlThreeWRulesEntityAssocId = rlThreeWRulesEntityAssocId;
    }

    public LocalDate getRlThreeWRulesEntityAssocFromDate() {
        return rlThreeWRulesEntityAssocFromDate;
    }

    public void setRlThreeWRulesEntityAssocFromDate(LocalDate rlThreeWRulesEntityAssocFromDate) {
        this.rlThreeWRulesEntityAssocFromDate = rlThreeWRulesEntityAssocFromDate;
    }

    public LocalDate getRlThreeWRulesEntityAssocThruDate() {
        return rlThreeWRulesEntityAssocThruDate;
    }

    public void setRlThreeWRulesEntityAssocThruDate(LocalDate rlThreeWRulesEntityAssocThruDate) {
        this.rlThreeWRulesEntityAssocThruDate = rlThreeWRulesEntityAssocThruDate;
    }

    public RLThreeWRulesEntityAssocType getRlThreeWRulesEntityAssocType() {
        return rlThreeWRulesEntityAssocType;
    }

    public void setRlThreeWRulesEntityAssocType(RLThreeWRulesEntityAssocType rlThreeWRulesEntityAssocType) {
        this.rlThreeWRulesEntityAssocType = rlThreeWRulesEntityAssocType;
    }

    public RLThreeWRulesEntity getRlThreeWRulesAssocFromEntity() {
        return rlThreeWRulesAssocFromEntity;
    }

    public void setRlThreeWRulesAssocFromEntity(RLThreeWRulesEntity rlThreeWRulesAssocFromEntity) {
        this.rlThreeWRulesAssocFromEntity = rlThreeWRulesAssocFromEntity;
    }

    public RLThreeWRulesEntity getRlThreeWRulesAssocToEntity() {
        return rlThreeWRulesAssocToEntity;
    }

    public void setRlThreeWRulesAssocToEntity(RLThreeWRulesEntity rlThreeWRulesAssocToEntity) {
        this.rlThreeWRulesAssocToEntity = rlThreeWRulesAssocToEntity;
    }

    public RLThreeWRulesEntityAssocRule getRlThreeWRulesEntityAssocRule() {
        return rlThreeWRulesEntityAssocRule;
    }

    public void setRlThreeWRulesEntityAssocRule(RLThreeWRulesEntityAssocRule rlThreeWRulesEntityAssocRule) {
        this.rlThreeWRulesEntityAssocRule = rlThreeWRulesEntityAssocRule;
    }
}
