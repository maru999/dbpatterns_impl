package at.fhv.rupp.ba.dbimpl.status.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntityStatusType;

import java.util.LinkedList;
import java.util.List;

public class SLTwoEntityStatusTypeMapper {

    private SLTwoEntityStatusTypeMapper() {
        //hidden constructor
    }

    public static SLTwoEntityStatusTypeDTO toDTO(SLTwoEntityStatusType entity) {
        SLTwoEntityStatusTypeDTO dto = new SLTwoEntityStatusTypeDTO();
        dto.setId(entity.getSlTwoStatusTypeId());
        dto.setName(entity.getSlTwoStatusTypeName());
        if (entity.getSlTwoEntityStatusTypeSomeAttr() != null) {
            dto.setSomeAttr(entity.getSlTwoEntityStatusTypeSomeAttr());
        }
        return dto;
    }

    public static SLTwoEntityStatusType toEntity(SLTwoEntityStatusTypeDTO dto) {
        SLTwoEntityStatusType entity = new SLTwoEntityStatusType();
        if (dto.getId() != null) {
            entity.setSlTwoStatusTypeId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setSlTwoStatusTypeName(dto.getName());
        }
        if (dto.getSomeAttr() != null) {
            entity.setSlTwoEntityStatusTypeSomeAttr(dto.getSomeAttr());
        }
        return entity;
    }

    public static List<SLTwoEntityStatusTypeDTO> toDTOs(List<SLTwoEntityStatusType> entities) {
        LinkedList<SLTwoEntityStatusTypeDTO> result = new LinkedList<>();
        for (SLTwoEntityStatusType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLTwoEntityStatusType> toEntities(List<SLTwoEntityStatusTypeDTO> dtos) {
        LinkedList<SLTwoEntityStatusType> result = new LinkedList<>();
        for (SLTwoEntityStatusTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
