package at.fhv.rupp.ba.dbimpl.recursive.levelone.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlone_entity_one")
@EntityListeners(AuditingEntityListener.class)
public class RLOneEntityOne {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlone_entity_one_id")
    private int rloneEntityOneId;

    @Column(name = "rlone_entity_one_name")
    private String rloneEntityOneName;

    public int getRloneEntityOneId() {
        return rloneEntityOneId;
    }

    public void setRloneEntityOneId(int rloneEntityOneId) {
        this.rloneEntityOneId = rloneEntityOneId;
    }

    public String getRloneEntityOneName() {
        return rloneEntityOneName;
    }

    public void setRloneEntityOneName(String rloneEntityOneName) {
        this.rloneEntityOneName = rloneEntityOneName;
    }
}
