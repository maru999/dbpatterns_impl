package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryRollupTypeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryRollupType;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryRollupTypeMapper {

    private CLThreeWRollupsEntityCategoryRollupTypeMapper() {
        // hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryRollupTypeDTO toDTO(CLThreeWRollupsEntityCategoryRollupType entity) {
        CLThreeWRollupsEntityCategoryRollupTypeDTO dto = new CLThreeWRollupsEntityCategoryRollupTypeDTO();
        dto.setId(entity.getClthreewrEntityCategoryRollupTypeId());
        dto.setName(entity.getClthreewrEntityCategoryRollupTypeName());
        if (entity.getParentRollupType() != null) {
            dto.setParent(toDTO(entity.getParentRollupType()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategoryRollupType toEntity(CLThreeWRollupsEntityCategoryRollupTypeDTO dto) {
        CLThreeWRollupsEntityCategoryRollupType entity = new CLThreeWRollupsEntityCategoryRollupType();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryRollupTypeId(dto.getId());
        }
        entity.setClthreewrEntityCategoryRollupTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setParentRollupType(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryRollupTypeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryRollupType> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryRollupTypeDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryRollupType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategoryRollupType> toEntities(List<CLThreeWRollupsEntityCategoryRollupTypeDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategoryRollupType> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryRollupTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
