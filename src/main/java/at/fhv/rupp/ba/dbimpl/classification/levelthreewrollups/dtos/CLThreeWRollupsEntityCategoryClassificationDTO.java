package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

import java.time.LocalDate;

public class CLThreeWRollupsEntityCategoryClassificationDTO {

    private Integer id;
    private LocalDate from;
    private LocalDate thru;
    private CLThreeWRollupsEntityDTO entity;
    private CLThreeWRollupsEntityCategoryDTO category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getThru() {
        return thru;
    }

    public void setThru(LocalDate thru) {
        this.thru = thru;
    }

    public CLThreeWRollupsEntityDTO getEntity() {
        return entity;
    }

    public void setEntity(CLThreeWRollupsEntityDTO entity) {
        this.entity = entity;
    }

    public CLThreeWRollupsEntityCategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CLThreeWRollupsEntityCategoryDTO category) {
        this.category = category;
    }
}
