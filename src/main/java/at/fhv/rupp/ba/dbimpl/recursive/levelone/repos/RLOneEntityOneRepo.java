package at.fhv.rupp.ba.dbimpl.recursive.levelone.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneAllEntitiesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityOne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLOneEntityOneRepo extends JpaRepository<RLOneEntityOne, Integer> {

    @Query(value = "SELECT * FROM rlone_all_entities", nativeQuery = true)
    List<RLOneAllEntitiesIProjection> getAllEntitiesInHierarchy();

    @Query(value = "SELECT * FROM rlone_all_entities WHERE rlone_entity_one_name = :name", nativeQuery = true)
    List<RLOneAllEntitiesIProjection> getAllEntitiesInHierarchyByEntityOneName(@Param("name") String entityOneName);

    @Query(value = "SELECT * FROM rlone_all_entities WHERE rlone_entity_two_name = :name", nativeQuery = true)
    List<RLOneAllEntitiesIProjection> getAllEntitiesInHierarchyByEntityTwoName(@Param("name") String entityTwoName);

    @Query(value = "SELECT * FROM rlone_all_entities WHERE rlone_entity_three_name = :name", nativeQuery = true)
    List<RLOneAllEntitiesIProjection> getAllEntitiesInHierarchyByEntityThreeName(@Param("name") String entityThreeName);
}
