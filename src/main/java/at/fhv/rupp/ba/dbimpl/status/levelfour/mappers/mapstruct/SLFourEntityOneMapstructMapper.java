package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityOneDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityOne;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLFourEntityOneMapstructMapper {

    @Mapping(source = "slFourEntityOneId", target = "id")
    @Mapping(source = "slFourEntityOneDesc", target = "desc")
    SLFourEntityOneDTO toDTO(SLFourEntityOne entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLFourEntityOne toEntity(SLFourEntityOneDTO dto);

    @Mapping(source = "slFourEntityOneId", target = "id")
    @Mapping(source = "slFourEntityOneDesc", target = "desc")
    void updateEntityFromDTO(SLFourEntityOne entity, @MappingTarget SLFourEntityOneDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLFourEntityOneDTO dto, @MappingTarget SLFourEntityOne entity);

    List<SLFourEntityOneDTO> toDTOs(List<SLFourEntityOne> entities);

    List<SLFourEntityOne> toEntities(List<SLFourEntityOneDTO> dtos);
}
