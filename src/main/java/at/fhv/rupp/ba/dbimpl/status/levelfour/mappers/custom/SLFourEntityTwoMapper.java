package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityTwoDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityTwo;

import java.util.LinkedList;
import java.util.List;

public class SLFourEntityTwoMapper {

    private SLFourEntityTwoMapper() {
        //hidden constructor
    }

    public static SLFourEntityTwoDTO toDTO(SLFourEntityTwo entity) {
        SLFourEntityTwoDTO dto = new SLFourEntityTwoDTO();
        dto.setId(entity.getSlFourEntityTwoId());
        dto.setName(entity.getSlFourEntityTwoName());
        return dto;
    }

    public static SLFourEntityTwo toEntity(SLFourEntityTwoDTO dto) {
        SLFourEntityTwo entity = new SLFourEntityTwo();
        if (dto.getId() != null) {
            entity.setSlFourEntityTwoId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setSlFourEntityTwoName(dto.getName());
        }
        return entity;
    }

    public static List<SLFourEntityTwoDTO> toDTOs(List<SLFourEntityTwo> entities) {
        LinkedList<SLFourEntityTwoDTO> result = new LinkedList<>();
        for (SLFourEntityTwo entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLFourEntityTwo> toEntities(List<SLFourEntityTwoDTO> dtos) {
        LinkedList<SLFourEntityTwo> result = new LinkedList<>();
        for (SLFourEntityTwoDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
