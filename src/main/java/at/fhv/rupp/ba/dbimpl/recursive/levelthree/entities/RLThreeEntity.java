package at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlthree_entity")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthree_entity_id")
    private int rlThreeEntityId;

    @ManyToOne
    @JoinColumn(name = "rlthree_entity_type_fk")
    private RLThreeEntityType rlThreeEntityType;

    public int getRlThreeEntityId() {
        return rlThreeEntityId;
    }

    public void setRlThreeEntityId(int rlThreeEntityId) {
        this.rlThreeEntityId = rlThreeEntityId;
    }

    public RLThreeEntityType getRlThreeEntityType() {
        return rlThreeEntityType;
    }

    public void setRlThreeEntityType(RLThreeEntityType rlThreeEntityType) {
        this.rlThreeEntityType = rlThreeEntityType;
    }
}
