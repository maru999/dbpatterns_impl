package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryRollup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLThreeWRollupsEntityCategoryRollupRepo extends JpaRepository<CLThreeWRollupsEntityCategoryRollup, Integer> {
}
