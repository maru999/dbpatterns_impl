package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocType;

import java.util.LinkedList;
import java.util.List;

public class RLTwoExpEntityAssocTypeMapper {
    private RLTwoExpEntityAssocTypeMapper() {
        //hidden constructor
    }

    public static RLTwoExpEntityAssocTypeDTO toDTO(RLTwoExpEntityAssocType entity) {
        RLTwoExpEntityAssocTypeDTO dto = new RLTwoExpEntityAssocTypeDTO();
        dto.setId(entity.getRlTwoExpEntityAssocTypeId());
        dto.setName(entity.getRlTwoExpEntityAssocTypeName());
        return dto;
    }

    public static RLTwoExpEntityAssocType toEntity(RLTwoExpEntityAssocTypeDTO dto) {
        RLTwoExpEntityAssocType entity = new RLTwoExpEntityAssocType();
        if (dto.getId() != null) {
            entity.setRlTwoExpEntityAssocTypeId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setRlTwoExpEntityAssocTypeName(dto.getName());
        }
        return entity;
    }

    public static List<RLTwoExpEntityAssocTypeDTO> toDTOs(List<RLTwoExpEntityAssocType> entities) {
        LinkedList<RLTwoExpEntityAssocTypeDTO> result = new LinkedList<>();
        for (RLTwoExpEntityAssocType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLTwoExpEntityAssocType> toEntities(List<RLTwoExpEntityAssocTypeDTO> dtos) {
        LinkedList<RLTwoExpEntityAssocType> result = new LinkedList<>();
        for (RLTwoExpEntityAssocTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
