package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssocType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RLThreeWRulesEntityAssocTypeRepo extends JpaRepository<RLThreeWRulesEntityAssocType, Integer> {
}
