package at.fhv.rupp.ba.dbimpl.classification.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLThreeEntityCategoryTypeRepo extends JpaRepository<CLThreeEntityCategoryType, Integer> {
}
