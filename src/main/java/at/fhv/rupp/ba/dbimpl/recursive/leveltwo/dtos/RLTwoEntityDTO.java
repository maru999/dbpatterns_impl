package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos;

public class RLTwoEntityDTO {
    private Integer id;
    private RLTwoEntityDTO parent;
    private RLTwoEntityTypeDTO type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RLTwoEntityDTO getParent() {
        return parent;
    }

    public void setParent(RLTwoEntityDTO parent) {
        this.parent = parent;
    }

    public RLTwoEntityTypeDTO getType() {
        return type;
    }

    public void setType(RLTwoEntityTypeDTO type) {
        this.type = type;
    }
}
