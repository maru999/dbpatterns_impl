package at.fhv.rupp.ba.dbimpl.classification.levelone.dtos;

public class CLOneEntityDTO {

    public CLOneEntityDTO() {
        //empty Constructor
    }

    private Integer id;
    private String name;
    private String typeOne;
    private String typeTwo;
    private String typeThree;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeOne() {
        return typeOne;
    }

    public void setTypeOne(String typeOne) {
        this.typeOne = typeOne;
    }

    public String getTypeTwo() {
        return typeTwo;
    }

    public void setTypeTwo(String typeTwo) {
        this.typeTwo = typeTwo;
    }

    public String getTypeThree() {
        return typeThree;
    }

    public void setTypeThree(String typeThree) {
        this.typeThree = typeThree;
    }
}
