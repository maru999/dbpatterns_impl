package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssocType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLThreeWRulesEntityAssocTypeMapstructMapper {
    @Mapping(source = "rlThreeWRulesEntityAssocTypeId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityAssocTypeName", target = "name")
    @Mapping(source = "rlThreeWRulesEntityAssocTypeParent", target = "parent")
    RLThreeWRulesEntityAssocTypeDTO toDTO(RLThreeWRulesEntityAssocType entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeWRulesEntityAssocType toEntity(RLThreeWRulesEntityAssocTypeDTO dto);

    @Mapping(source = "rlThreeWRulesEntityAssocTypeId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityAssocTypeName", target = "name")
    @Mapping(source = "rlThreeWRulesEntityAssocTypeParent", target = "parent")
    void updateEntityFromDTO(RLThreeWRulesEntityAssocType entity, @MappingTarget RLThreeWRulesEntityAssocTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeWRulesEntityAssocTypeDTO dto, @MappingTarget RLThreeWRulesEntityAssocType entity);

    List<RLThreeWRulesEntityAssocTypeDTO> toDTOs(List<RLThreeWRulesEntityAssocType> entities);

    List<RLThreeWRulesEntityAssocType> toEntities(List<RLThreeWRulesEntityAssocTypeDTO> dtos);
}
