package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.repos;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLTwoExpEntityTypeRepo extends JpaRepository<RLTwoExpEntityType, Integer> {
    @Query(value = "SELECT * FROM rltwoxp_entity_type WHERE rltwoxp_entity_type_parent = :id", nativeQuery = true)
    List<RLTwoExpEntityType> getAllTypesWhereTypeIsParent(@Param("id") Integer typeId);
}
