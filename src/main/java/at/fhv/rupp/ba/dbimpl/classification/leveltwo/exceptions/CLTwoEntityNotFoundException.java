package at.fhv.rupp.ba.dbimpl.classification.leveltwo.exceptions;

public class CLTwoEntityNotFoundException extends Exception {
    public CLTwoEntityNotFoundException(String message) {
        super(message);
    }
}
