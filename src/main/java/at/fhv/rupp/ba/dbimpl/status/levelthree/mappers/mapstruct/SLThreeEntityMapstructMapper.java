package at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntity;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeStatusType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLThreeEntityMapstructMapper {

    @Mapping(source = "slThreeEntityId", target = "id")
    SLThreeEntityDTO toDTO(SLThreeEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLThreeEntity toEntity(SLThreeEntityDTO dto);

    @Mapping(source = "slThreeEntityId", target = "id")
    void updateEntityFromDTO(SLThreeEntity entity, @MappingTarget SLThreeEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLThreeEntityDTO dto, @MappingTarget SLThreeEntity entity);

    List<SLThreeEntityDTO> toDTOs(List<SLThreeEntity> entities);

    List<SLThreeEntity> toEntities(List<SLThreeEntityDTO> dtos);
}
