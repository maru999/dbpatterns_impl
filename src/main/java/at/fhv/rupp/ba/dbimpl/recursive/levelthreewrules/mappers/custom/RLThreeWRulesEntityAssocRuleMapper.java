package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityAssocRuleDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssocRule;

import java.util.LinkedList;
import java.util.List;

public class RLThreeWRulesEntityAssocRuleMapper {

    private RLThreeWRulesEntityAssocRuleMapper() {
        //hidden constructor
    }

    public static RLThreeWRulesEntityAssocRuleDTO toDTO(RLThreeWRulesEntityAssocRule entity) {
        RLThreeWRulesEntityAssocRuleDTO dto = new RLThreeWRulesEntityAssocRuleDTO();
        dto.setId(entity.getRlThreeWRulesEntityAssocRuleId());
        dto.setName(entity.getRlThreeWRulesEntityAssocRuleName());
        return dto;
    }

    public static RLThreeWRulesEntityAssocRule toEntity(RLThreeWRulesEntityAssocRuleDTO dto) {
        RLThreeWRulesEntityAssocRule entity = new RLThreeWRulesEntityAssocRule();
        if (dto.getId() != null) {
            entity.setRlThreeWRulesEntityAssocRuleId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setRlThreeWRulesEntityAssocRuleName(dto.getName());
        }
        return entity;
    }

    public static List<RLThreeWRulesEntityAssocRuleDTO> toDTOs(List<RLThreeWRulesEntityAssocRule> entities) {
        LinkedList<RLThreeWRulesEntityAssocRuleDTO> result = new LinkedList<>();
        for (RLThreeWRulesEntityAssocRule entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeWRulesEntityAssocRule> toEntities(List<RLThreeWRulesEntityAssocRuleDTO> dtos) {
        LinkedList<RLThreeWRulesEntityAssocRule> result = new LinkedList<>();
        for (RLThreeWRulesEntityAssocRuleDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
