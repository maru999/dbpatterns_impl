package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

import java.time.LocalDate;

public class CLThreeWRollupsEntityCategoryRollupDTO {

    private Integer id;
    private LocalDate from;
    private LocalDate thru;
    private CLThreeWRollupsEntityCategoryDTO parent;
    private CLThreeWRollupsEntityCategoryDTO child;
    private CLThreeWRollupsEntityCategoryRollupTypeDTO rollupType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getThru() {
        return thru;
    }

    public void setThru(LocalDate thru) {
        this.thru = thru;
    }

    public CLThreeWRollupsEntityCategoryDTO getParent() {
        return parent;
    }

    public void setParent(CLThreeWRollupsEntityCategoryDTO parent) {
        this.parent = parent;
    }

    public CLThreeWRollupsEntityCategoryDTO getChild() {
        return child;
    }

    public void setChild(CLThreeWRollupsEntityCategoryDTO child) {
        this.child = child;
    }

    public CLThreeWRollupsEntityCategoryRollupTypeDTO getRollupType() {
        return rollupType;
    }

    public void setRollupType(CLThreeWRollupsEntityCategoryRollupTypeDTO rollupType) {
        this.rollupType = rollupType;
    }
}
