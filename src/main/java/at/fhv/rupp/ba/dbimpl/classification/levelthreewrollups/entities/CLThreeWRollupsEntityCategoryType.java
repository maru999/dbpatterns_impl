package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_entity_category_type")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategoryType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_type_id")
    private int clthreewrEntityCategoryTypeId;

    @Column(name = "clthreewr_entity_category_type_name")
    private String clthreewrEntityCategoryTypeName;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_type_entity_category_type_scheme_id")
    private CLThreeWRollupsEntityCategoryTypeScheme clthreeCategoryTypeScheme;

    public int getClthreewrEntityCategoryTypeId() {
        return clthreewrEntityCategoryTypeId;
    }

    public void setClthreewrEntityCategoryTypeId(int clthreewrEntityCategoryTypeId) {
        this.clthreewrEntityCategoryTypeId = clthreewrEntityCategoryTypeId;
    }

    public String getClthreewrEntityCategoryTypeName() {
        return clthreewrEntityCategoryTypeName;
    }

    public void setClthreewrEntityCategoryTypeName(String clthreewrEntityCategoryTypeName) {
        this.clthreewrEntityCategoryTypeName = clthreewrEntityCategoryTypeName;
    }

    public CLThreeWRollupsEntityCategoryTypeScheme getClthreeCategoryTypeScheme() {
        return clthreeCategoryTypeScheme;
    }

    public void setClthreeCategoryTypeScheme(CLThreeWRollupsEntityCategoryTypeScheme clthreeCategoryTypeScheme) {
        this.clthreeCategoryTypeScheme = clthreeCategoryTypeScheme;
    }
}
