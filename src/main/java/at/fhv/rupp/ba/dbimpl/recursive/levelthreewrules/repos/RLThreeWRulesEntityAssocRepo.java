package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLThreeWRulesEntityAssocRepo extends JpaRepository<RLThreeWRulesEntityAssoc, Integer> {
    @Query(value = "SELECT * FROM rlthreewr_entity_assoc WHERE rlthreewr_entity_assoc_from_ent = :id", nativeQuery = true)
    List<RLThreeWRulesEntityAssoc> getAssociationsByEntityFromId(@Param("id") Integer fromId);

    @Query(value = "SELECT * FROM rlthreewr_entity_assoc WHERE rlthreewr_entity_assoc_to_ent = :id", nativeQuery = true)
    List<RLThreeWRulesEntityAssoc> getAssociationsByEntityToId(@Param("id") Integer fromId);

}
