package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

public class CLThreeWRollupsEntityCategoryDTO {

    private Integer id;
    private String name;
    private CLThreeWRollupsEntityCategoryTypeDTO type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLThreeWRollupsEntityCategoryTypeDTO getType() {
        return type;
    }

    public void setType(CLThreeWRollupsEntityCategoryTypeDTO type) {
        this.type = type;
    }
}
