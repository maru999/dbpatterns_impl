package at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlthree_entity_type")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeEntityType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthree_entity_type_id")
    private int rlThreeEntityTypeId;

    @Column(name = "rlthree_entity_type_name")
    private String rlThreeEntityTypeName;

    @ManyToOne
    @JoinColumn(name = "rlthree_entity_type_parent")
    private RLThreeEntityType rlThreeEntityTypeParent;

    public int getRlThreeEntityTypeId() {
        return rlThreeEntityTypeId;
    }

    public void setRlThreeEntityTypeId(int rlThreeEntityTypeId) {
        this.rlThreeEntityTypeId = rlThreeEntityTypeId;
    }

    public String getRlThreeEntityTypeName() {
        return rlThreeEntityTypeName;
    }

    public void setRlThreeEntityTypeName(String rlThreeEntityTypeName) {
        this.rlThreeEntityTypeName = rlThreeEntityTypeName;
    }

    public RLThreeEntityType getRlThreeEntityTypeParent() {
        return rlThreeEntityTypeParent;
    }

    public void setRlThreeEntityTypeParent(RLThreeEntityType rlThreeEntityTypeParent) {
        this.rlThreeEntityTypeParent = rlThreeEntityTypeParent;
    }
}
