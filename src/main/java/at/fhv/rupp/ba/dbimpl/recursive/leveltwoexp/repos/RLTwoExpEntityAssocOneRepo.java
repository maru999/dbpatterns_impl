package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.repos;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocOne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLTwoExpEntityAssocOneRepo extends JpaRepository<RLTwoExpEntityAssocOne, Integer> {
    @Query(value = "SELECT * FROM rltwoxp_entity_assoc_one WHERE rltwoxp_entity_assoc_one_to_entity = :id", nativeQuery = true)
    List<RLTwoExpEntityAssocOne> getAssocOnesByToEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rltwoxp_entity_assoc_one WHERE rltwoxp_entity_assoc_one_from_entity = :id", nativeQuery = true)
    List<RLTwoExpEntityAssocOne> getAssocOnesByFromEntityId(@Param("id") Integer entityId);

}
