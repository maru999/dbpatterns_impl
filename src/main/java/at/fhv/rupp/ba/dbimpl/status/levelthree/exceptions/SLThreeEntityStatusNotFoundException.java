package at.fhv.rupp.ba.dbimpl.status.levelthree.exceptions;

public class SLThreeEntityStatusNotFoundException extends Exception {
    public SLThreeEntityStatusNotFoundException(String message) {
        super(message);
    }
}
