package at.fhv.rupp.ba.dbimpl.recursive.levelone.exceptions;

public class RLOneEntityOneNotFoundException extends Exception{
    public RLOneEntityOneNotFoundException(String message) {
        super(message);
    }
}
