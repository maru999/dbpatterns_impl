package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_entity_category_rollup_type")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategoryRollupType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_rollup_type_id")
    private int clthreewrEntityCategoryRollupTypeId;

    @Column(name = "clthreewr_entity_category_rollup_type_name")
    private String clthreewrEntityCategoryRollupTypeName;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_rollup_type_parent_id")
    private CLThreeWRollupsEntityCategoryRollupType parentRollupType;

    public int getClthreewrEntityCategoryRollupTypeId() {
        return clthreewrEntityCategoryRollupTypeId;
    }

    public void setClthreewrEntityCategoryRollupTypeId(int clthreewrEntityCategoryRollupTypeId) {
        this.clthreewrEntityCategoryRollupTypeId = clthreewrEntityCategoryRollupTypeId;
    }

    public String getClthreewrEntityCategoryRollupTypeName() {
        return clthreewrEntityCategoryRollupTypeName;
    }

    public void setClthreewrEntityCategoryRollupTypeName(String clthreewrEntityCategoryRollupTypeName) {
        this.clthreewrEntityCategoryRollupTypeName = clthreewrEntityCategoryRollupTypeName;
    }

    public CLThreeWRollupsEntityCategoryRollupType getParentRollupType() {
        return parentRollupType;
    }

    public void setParentRollupType(CLThreeWRollupsEntityCategoryRollupType parentRollupType) {
        this.parentRollupType = parentRollupType;
    }
}
