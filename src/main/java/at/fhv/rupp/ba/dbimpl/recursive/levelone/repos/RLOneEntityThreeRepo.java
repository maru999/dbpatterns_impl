package at.fhv.rupp.ba.dbimpl.recursive.levelone.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityThree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLOneEntityThreeRepo extends JpaRepository<RLOneEntityThree, Integer> {
    @Query(value = "SELECT * FROM rlone_entity_three WHERE rlone_entity_three_parent = :id", nativeQuery = true)
    List<RLOneEntityThree> getAllEntityThreesByEntityTwoId(@Param("id") Integer entityTwoId);
}
