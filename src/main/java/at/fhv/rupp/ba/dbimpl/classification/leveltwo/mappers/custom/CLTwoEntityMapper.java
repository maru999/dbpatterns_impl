package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeOneDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeThreeDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeTwoDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.*;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntityMapper {

    private CLTwoEntityMapper() {
        //hidden constructor
    }

    public static CLTwoEntityDTO toDTO(CLTwoEntity entity) {
        if (entity instanceof CLTwoEntitySubtypeOne)
            return CLTwoEntitySubtypeOneMapper.toDTO((CLTwoEntitySubtypeOne) entity);
        else if (entity instanceof CLTwoEntitySubtypeTwo)
            return CLTwoEntitySubtypeTwoMapper.toDTO((CLTwoEntitySubtypeTwo) entity);
        else return CLTwoEntitySubtypeThreeMapper.toDTO((CLTwoEntitySubtypeThree) entity);
    }

    public static CLTwoEntity toEntity(CLTwoEntityDTO dto) {
        if (dto instanceof CLTwoEntitySubtypeOneDTO)
            return CLTwoEntitySubtypeOneMapper.toEntity((CLTwoEntitySubtypeOneDTO) dto);
        else if (dto instanceof CLTwoEntitySubtypeTwoDTO)
            return CLTwoEntitySubtypeTwoMapper.toEntity((CLTwoEntitySubtypeTwoDTO) dto);
        else return CLTwoEntitySubtypeThreeMapper.toEntity((CLTwoEntitySubtypeThreeDTO) dto);
    }

    public static List<CLTwoEntityDTO> toDTOs(List<CLTwoEntity> entities) {
        LinkedList<CLTwoEntityDTO> result = new LinkedList<>();
        for (CLTwoEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLTwoEntity> toEntities(List<CLTwoEntityDTO> dtos, boolean isNew) {
        LinkedList<CLTwoEntity> result = new LinkedList<>();
        for (CLTwoEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
