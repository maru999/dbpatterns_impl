package at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos;

import java.time.LocalDate;

public class RLThreeEntityAssocDTO {
    private Integer id;
    private LocalDate fromDate;
    private LocalDate thruDate;
    private RLThreeEntityAssocTypeDTO type;
    private RLThreeEntityDTO fromEntity;
    private RLThreeEntityDTO toEntity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getThruDate() {
        return thruDate;
    }

    public void setThruDate(LocalDate thruDate) {
        this.thruDate = thruDate;
    }

    public RLThreeEntityAssocTypeDTO getType() {
        return type;
    }

    public void setType(RLThreeEntityAssocTypeDTO type) {
        this.type = type;
    }

    public RLThreeEntityDTO getFromEntity() {
        return fromEntity;
    }

    public void setFromEntity(RLThreeEntityDTO fromEntity) {
        this.fromEntity = fromEntity;
    }

    public RLThreeEntityDTO getToEntity() {
        return toEntity;
    }

    public void setToEntity(RLThreeEntityDTO toEntity) {
        this.toEntity = toEntity;
    }
}
