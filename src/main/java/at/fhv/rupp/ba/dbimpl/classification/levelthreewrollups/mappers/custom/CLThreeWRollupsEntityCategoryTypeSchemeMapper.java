package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeSchemeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryTypeScheme;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryTypeSchemeMapper {

    private CLThreeWRollupsEntityCategoryTypeSchemeMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryTypeSchemeDTO toDTO(CLThreeWRollupsEntityCategoryTypeScheme entity) {
        CLThreeWRollupsEntityCategoryTypeSchemeDTO dto = new CLThreeWRollupsEntityCategoryTypeSchemeDTO();
        dto.setId(entity.getClthreewrEntityCategoryTypeSchemeId());
        dto.setName(entity.getClthreewrEntityCategoryTypeSchemeName());
        if (entity.getSchemeDataProvider() != null) {
            dto.setDataProvider(CLThreeWRollupsDataProviderMapper.toDTO(entity.getSchemeDataProvider()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategoryTypeScheme toEntity(CLThreeWRollupsEntityCategoryTypeSchemeDTO dto) {
        CLThreeWRollupsEntityCategoryTypeScheme entity = new CLThreeWRollupsEntityCategoryTypeScheme();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryTypeSchemeId(dto.getId());
        }
        entity.setClthreewrEntityCategoryTypeSchemeName(dto.getName());
        if (dto.getDataProvider() != null) {
            entity.setSchemeDataProvider(CLThreeWRollupsDataProviderMapper.toEntity(dto.getDataProvider()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryTypeSchemeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryTypeScheme> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryTypeSchemeDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryTypeScheme entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategoryTypeScheme> toEntities(List<CLThreeWRollupsEntityCategoryTypeSchemeDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategoryTypeScheme> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryTypeSchemeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
