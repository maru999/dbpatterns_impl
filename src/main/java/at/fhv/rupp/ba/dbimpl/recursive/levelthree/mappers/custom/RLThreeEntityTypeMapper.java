package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityType;

import java.util.LinkedList;
import java.util.List;

public class RLThreeEntityTypeMapper {
    private RLThreeEntityTypeMapper() {
        //hidden constructor
    }

    public static RLThreeEntityTypeDTO toDTO(RLThreeEntityType entity) {
        RLThreeEntityTypeDTO dto = new RLThreeEntityTypeDTO();
        dto.setId(entity.getRlThreeEntityTypeId());
        dto.setName(entity.getRlThreeEntityTypeName());
        if (entity.getRlThreeEntityTypeParent() != null) {
            dto.setParent(toDTO(entity.getRlThreeEntityTypeParent()));
        }
        return dto;
    }

    public static RLThreeEntityType toEntity(RLThreeEntityTypeDTO dto) {
        RLThreeEntityType entity = new RLThreeEntityType();
        if (dto.getId() != null) {
            entity.setRlThreeEntityTypeId(dto.getId());
        }
        entity.setRlThreeEntityTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setRlThreeEntityTypeParent(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLThreeEntityTypeDTO> toDTOs(List<RLThreeEntityType> entities) {
        LinkedList<RLThreeEntityTypeDTO> result = new LinkedList<>();
        for (RLThreeEntityType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeEntityType> toEntities(List<RLThreeEntityTypeDTO> dtos) {
        LinkedList<RLThreeEntityType> result = new LinkedList<>();
        for (RLThreeEntityTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
