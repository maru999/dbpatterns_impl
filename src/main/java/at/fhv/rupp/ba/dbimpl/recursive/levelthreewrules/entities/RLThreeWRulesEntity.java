package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlthreewr_entity")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeWRulesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthreewr_entity_id")
    private int rlThreeWRulesEntityId;

    @ManyToOne
    @JoinColumn(name = "rlthreewr_entity_type_fk")
    private RLThreeWRulesEntityType rlThreeWRulesEntityType;

    public int getRlThreeWRulesEntityId() {
        return rlThreeWRulesEntityId;
    }

    public void setRlThreeWRulesEntityId(int rlThreeWRulesEntityId) {
        this.rlThreeWRulesEntityId = rlThreeWRulesEntityId;
    }

    public RLThreeWRulesEntityType getRlThreeWRulesEntityType() {
        return rlThreeWRulesEntityType;
    }

    public void setRlThreeWRulesEntityType(RLThreeWRulesEntityType rlThreeWRulesEntityType) {
        this.rlThreeWRulesEntityType = rlThreeWRulesEntityType;
    }
}
