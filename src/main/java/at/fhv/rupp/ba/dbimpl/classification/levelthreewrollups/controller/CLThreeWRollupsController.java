package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.datagen.CLThreeWRollupsJavaFaker;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsDataProviderDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityTypesViewIProjection;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsDataProvider;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntity;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryClassification;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.exceptions.CLThreeWRollupsDataProviderNotFoundException;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.exceptions.CLThreeWRollupsEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom.CLThreeWRollupsDataProviderMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom.CLThreeWRollupsEntityMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos.*;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/classification/L3Rollups")
public class CLThreeWRollupsController {
    private final CLThreeWRollupsEntityRepo entityRepo;
    private final CLThreeWRollupsDataProviderRepo dataProviderRepo;
    private final CLThreeWRollupsEntityCategoryRepo categoryRepo;
    private final CLThreeWRollupsEntityCategoryClassificationRepo categoryClassificationRepo;
    private final CLThreeWRollupsEntityCategoryRollupRepo categoryRollupRepo;
    private final CLThreeWRollupsEntityCategoryRollupTypeRepo rollupTypeRepo;
    private final CLThreeWRollupsEntityCategoryTypeRepo categoryTypeRepo;
    private final CLThreeWRollupsEntityCategoryTypeRollupRepo typeRollupRepo;
    private final CLThreeWRollupsEntityCategoryTypeRollupTypeRepo typeRollupTypeRepo;
    private final CLThreeWRollupsEntityCategoryTypeSchemeRepo typeSchemeRepo;
    private final DataGenJavaFaker faker;

    public CLThreeWRollupsController(CLThreeWRollupsEntityRepo entityRepo, CLThreeWRollupsDataProviderRepo dataProviderRepo, CLThreeWRollupsEntityCategoryRepo categoryRepo, CLThreeWRollupsEntityCategoryClassificationRepo categoryClassificationRepo, CLThreeWRollupsEntityCategoryRollupRepo categoryRollupRepo, CLThreeWRollupsEntityCategoryRollupTypeRepo rollupTypeRepo, CLThreeWRollupsEntityCategoryTypeRepo categoryTypeRepo, CLThreeWRollupsEntityCategoryTypeRollupRepo typeRollupRepo, CLThreeWRollupsEntityCategoryTypeRollupTypeRepo typeRollupTypeRepo, CLThreeWRollupsEntityCategoryTypeSchemeRepo typeSchemeRepo) {
        this.entityRepo = entityRepo;
        this.dataProviderRepo = dataProviderRepo;
        this.categoryRepo = categoryRepo;
        this.categoryClassificationRepo = categoryClassificationRepo;
        this.categoryRollupRepo = categoryRollupRepo;
        this.rollupTypeRepo = rollupTypeRepo;
        this.categoryTypeRepo = categoryTypeRepo;
        this.typeRollupRepo = typeRollupRepo;
        this.typeRollupTypeRepo = typeRollupTypeRepo;
        this.typeSchemeRepo = typeSchemeRepo;
        this.faker = new CLThreeWRollupsJavaFaker(entityRepo, dataProviderRepo, categoryRepo, categoryClassificationRepo, categoryRollupRepo, rollupTypeRepo, categoryTypeRepo, typeRollupRepo, typeRollupTypeRepo, typeSchemeRepo);
    }

    @GetMapping("/dataProvider/{id}")
    public ResponseEntity<CLThreeWRollupsDataProviderDTO> getDataProviderById(@PathVariable(value = "id") Integer entityId) throws CLThreeWRollupsDataProviderNotFoundException {
        CLThreeWRollupsDataProvider entity = dataProviderRepo.findById(entityId).orElseThrow(() -> new CLThreeWRollupsDataProviderNotFoundException(getExceptionString("CLThreeWRollupsDataProvider", entityId)));
        return ResponseEntity.ok().body(CLThreeWRollupsDataProviderMapper.toDTO(entity));
    }

    @PostMapping("/dataProvider")
    public CLThreeWRollupsDataProviderDTO createDataProvider(@Valid @RequestBody CLThreeWRollupsDataProviderDTO entityDTO) {
        return CLThreeWRollupsDataProviderMapper.toDTO(dataProviderRepo.save(CLThreeWRollupsDataProviderMapper.toEntity(entityDTO)));
    }

    @GetMapping("/entities")
    public List<CLThreeWRollupsEntityDTO> getAllEntities() {
        return CLThreeWRollupsEntityMapper.toDTOs(entityRepo.findAll());
    }

    @GetMapping("/entities/{id}")
    public ResponseEntity<CLThreeWRollupsEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws CLThreeWRollupsEntityNotFoundException {
        CLThreeWRollupsEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLThreeWRollupsEntityNotFoundException(getExceptionString("CLThreeWRollupsEntity", entityId)));
        return ResponseEntity.ok().body(CLThreeWRollupsEntityMapper.toDTO(entity));
    }

    @PostMapping("/entities")
    public CLThreeWRollupsEntityDTO createEntity(@Valid @RequestBody CLThreeWRollupsEntityDTO entityDTO) {
        return CLThreeWRollupsEntityMapper.toDTO(entityRepo.save(CLThreeWRollupsEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<CLThreeWRollupsEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody CLThreeWRollupsEntityDTO entityDetails) throws CLThreeWRollupsEntityNotFoundException {
        CLThreeWRollupsEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLThreeWRollupsEntityNotFoundException(getExceptionString("CLThreeWRollupsEntity", entityId)));
        CLThreeWRollupsEntityDTO entityDTO = CLThreeWRollupsEntityMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        final CLThreeWRollupsEntityDTO updatedEntity = CLThreeWRollupsEntityMapper.toDTO(entityRepo.save(CLThreeWRollupsEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws CLThreeWRollupsEntityNotFoundException {
        CLThreeWRollupsEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new CLThreeWRollupsEntityNotFoundException(getExceptionString("CLThreeWRollupsEntity", entityId)));
        List<CLThreeWRollupsEntityCategoryClassification> classifications = categoryClassificationRepo.getClassificationsByEntityId(entityId);
        for (CLThreeWRollupsEntityCategoryClassification classification : classifications) {
            categoryClassificationRepo.delete(classification);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/classification/classificationlevelthreewrollups.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entitiesWTypes")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypes() {
        return entityRepo.getAllEntitiesWTypes();
    }

    @GetMapping("/entitiesWTypes/catHierarchy/{id}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesHierarchyByCatId(@PathVariable("id") Integer catId) {
        return entityRepo.getAllEntitiesWTypesHierarchyByEntityCatId(catId);
    }

    @GetMapping("/entitiesWTypes/catName/{name}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatName(@PathVariable("name") String catName) {
        return entityRepo.getAllEntitiesWTypesByCatName(catName);
    }

    @GetMapping("/entitiesWTypes/catTypeName/{name}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatTypeName(@PathVariable("name") String catTypeName) {
        return entityRepo.getAllEntitiesWTypesByCatTypeName(catTypeName);
    }

    @GetMapping("/entitiesWTypes/dpName/{name}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByDataProviderName(@PathVariable("name") String dpName) {
        return entityRepo.getAllEntitiesWTypesByDataProviderName(dpName);
    }

    @GetMapping("/entitiesWTypes/schemeName/{name}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesBySchemeName(@PathVariable("name") String schemeName) {
        return entityRepo.getAllEntitiesWTypesBySchemeName(schemeName);
    }

    @GetMapping("/entitiesWTypes/rollupTypeName/{name}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByRollupType(@PathVariable("name") String rollupTypeName) {
        return entityRepo.getAllEntitiesWTypesByRollupTypeName(rollupTypeName);
    }

    @GetMapping("/entitiesWTypes/typeRollupTypeName/{name}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByTypeRollupTypeName(@PathVariable("name") String typeRollupTypeName) {
        return entityRepo.getAllEntitiesWTypesByTypeRollupTypeName(typeRollupTypeName);
    }

    @GetMapping("/entitiesWTypes/classificationFrom/{startDate}/{endDate}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByClassificationFromDate(@PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return entityRepo.getAllEntitiesWTypesByClassificationFromDate(startDate, endDate);
    }

    @GetMapping("/entitiesWTypes/catRollupFrom/{startDate}/{endDate}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatRollupFromDate(@PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return entityRepo.getAllEntitiesWTypesByCatRollupFromDate(startDate, endDate);
    }

    @GetMapping("/entitiesWTypes/catTypeRollupFrom/{startDate}/{endDate}")
    public List<CLThreeWRollupsEntityTypesViewIProjection> getAllEntitiesWTypesByCatTypeRollupFromDate(@PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return entityRepo.getAllEntitiesWTypesByCatTypeRollupFromDate(startDate, endDate);
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }
}
