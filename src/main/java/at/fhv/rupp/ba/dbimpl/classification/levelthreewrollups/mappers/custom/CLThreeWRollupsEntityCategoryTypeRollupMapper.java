package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeRollupDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryTypeRollup;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryTypeRollupMapper {

    private CLThreeWRollupsEntityCategoryTypeRollupMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryTypeRollupDTO toDTO(CLThreeWRollupsEntityCategoryTypeRollup entity) {
        CLThreeWRollupsEntityCategoryTypeRollupDTO dto = new CLThreeWRollupsEntityCategoryTypeRollupDTO();
        dto.setId(entity.getClthreewrEntityCategoryTypeRollupId());
        dto.setFrom(entity.getClthreewrEntityCategoryTypeRollupFromDate());
        if (entity.getClthreewrEntityCategoryTypeRollupThruDate() != null) {
            dto.setThru(entity.getClthreewrEntityCategoryTypeRollupThruDate());
        }
        if (entity.getClthreewrEntityCategoryTypeRollupParentCategory() != null) {
            dto.setParent(CLThreeWRollupsEntityCategoryTypeMapper.toDTO(entity.getClthreewrEntityCategoryTypeRollupParentCategory()));
        }
        if (entity.getClthreewrEntityCategoryTypeRollupChildCategory() != null) {
            dto.setChild(CLThreeWRollupsEntityCategoryTypeMapper.toDTO(entity.getClthreewrEntityCategoryTypeRollupChildCategory()));
        }
        if (entity.getClthreewrEntityCategoryTypeRollupType() != null) {
            dto.setType(CLThreeWRollupsEntityCategoryTypeRollupTypeMapper.toDTO(entity.getClthreewrEntityCategoryTypeRollupType()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategoryTypeRollup toEntity(CLThreeWRollupsEntityCategoryTypeRollupDTO dto) {
        CLThreeWRollupsEntityCategoryTypeRollup entity = new CLThreeWRollupsEntityCategoryTypeRollup();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryTypeRollupId(dto.getId());
        }
        entity.setClthreewrEntityCategoryTypeRollupFromDate(dto.getFrom());
        if (dto.getThru() != null) {
            entity.setClthreewrEntityCategoryTypeRollupThruDate(dto.getThru());
        }
        if (dto.getParent() != null) {
            entity.setClthreewrEntityCategoryTypeRollupParentCategory(CLThreeWRollupsEntityCategoryTypeMapper.toEntity(dto.getParent()));
        }
        if (dto.getChild() != null) {
            entity.setClthreewrEntityCategoryTypeRollupChildCategory(CLThreeWRollupsEntityCategoryTypeMapper.toEntity(dto.getChild()));
        }
        if (dto.getType() != null) {
            entity.setClthreewrEntityCategoryTypeRollupType(CLThreeWRollupsEntityCategoryTypeRollupTypeMapper.toEntity(dto.getType()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryTypeRollupDTO> toDTOs(List<CLThreeWRollupsEntityCategoryTypeRollup> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryTypeRollupDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryTypeRollup entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategoryTypeRollup> toEntities(List<CLThreeWRollupsEntityCategoryTypeRollupDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategoryTypeRollup> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryTypeRollupDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
