package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityTypeThreeDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeThree;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntityTypeThreeMapper {

    private CLTwoEntityTypeThreeMapper() {
        //hidden constructor
    }

    public static CLTwoEntityTypeThreeDTO toDTO(CLTwoEntityTypeThree entity) {
        CLTwoEntityTypeThreeDTO dto = new CLTwoEntityTypeThreeDTO();
        dto.setId(entity.getClTwoEntityTypeThreeId());
        dto.setName(entity.getClTwoEntityTypeThreeName());
        return dto;
    }

    public static List<CLTwoEntityTypeThreeDTO> toDTOs(List<CLTwoEntityTypeThree> entities) {
        LinkedList<CLTwoEntityTypeThreeDTO> result = new LinkedList<>();
        for (CLTwoEntityTypeThree entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static CLTwoEntityTypeThree toEntity(CLTwoEntityTypeThreeDTO dto) {
        CLTwoEntityTypeThree entity = new CLTwoEntityTypeThree();
        if (dto.getId() != null) {
            entity.setClTwoEntityTypeThreeId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setClTwoEntityTypeThreeName(dto.getName());
        }
        return entity;
    }

    public static List<CLTwoEntityTypeThree> toEntities(List<CLTwoEntityTypeThreeDTO> dtos) {
        LinkedList<CLTwoEntityTypeThree> result = new LinkedList<>();
        for (CLTwoEntityTypeThreeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
