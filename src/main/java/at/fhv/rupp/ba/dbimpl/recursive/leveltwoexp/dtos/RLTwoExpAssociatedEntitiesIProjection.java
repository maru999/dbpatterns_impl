package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos;

import java.time.LocalDate;

public interface RLTwoExpAssociatedEntitiesIProjection {

    Integer getrltwoxp_entity_type_id();

    String getrltwoxp_entity_type_name();

    Integer getrltwoxp_entity_type_parent();

    Integer getrltwoxp_entity_assoc_type_id();

    String getrltwoxp_entity_assoc_type_name();

    Integer getrltwoxp_entity_id();

    Integer getrltwoxp_entity_assoc_to_entity_one();

    Integer getrltwoxp_entity_assoc_to_entity_two();

    Integer getrltwoxp_entity_type_fk();

    Integer getrltwoxp_entity_assoc_one_id();

    LocalDate getrltwoxp_entity_assoc_one_from_date();

    LocalDate getrltwoxp_entity_assoc_one_thru_date();

    Integer getrltwoxp_entity_assoc_one_from_entity();

    Integer getrltwoxp_entity_assoc_one_to_entity();

    Integer getrltwoxp_entity_assoc_two_id();

    LocalDate getrltwoxp_entity_assoc_two_from_date();

    LocalDate getrltwoxp_entity_assoc_two_thru_date();

    Integer getrltwoxp_entity_assoc_two_from_entity();

    Integer getrltwoxp_entity_assoc_two_to_entity();

    Integer getrltwoxp_entity_assoc_two_type();
}
