package at.fhv.rupp.ba.dbimpl.status.levelfour.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SLFourStatusApplicationDTO {
    private Integer id;
    private LocalDateTime dateTime;
    private LocalDate statusFrom;
    private LocalDate statusThru;
    private LocalDate from;
    private LocalDate thru;
    private SLFourStatusTypeDTO type;
    private SLFourEntityOneDTO entityOne;
    private SLFourEntityTwoDTO entityTwo;
    private SLFourEntityThreeDTO entityThree;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public LocalDate getStatusFrom() {
        return statusFrom;
    }

    public void setStatusFrom(LocalDate statusFrom) {
        this.statusFrom = statusFrom;
    }

    public LocalDate getStatusThru() {
        return statusThru;
    }

    public void setStatusThru(LocalDate statusThru) {
        this.statusThru = statusThru;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getThru() {
        return thru;
    }

    public void setThru(LocalDate thru) {
        this.thru = thru;
    }

    public SLFourStatusTypeDTO getType() {
        return type;
    }

    public void setType(SLFourStatusTypeDTO type) {
        this.type = type;
    }

    public SLFourEntityOneDTO getEntityOne() {
        return entityOne;
    }

    public void setEntityOne(SLFourEntityOneDTO entityOne) {
        this.entityOne = entityOne;
    }

    public SLFourEntityTwoDTO getEntityTwo() {
        return entityTwo;
    }

    public void setEntityTwo(SLFourEntityTwoDTO entityTwo) {
        this.entityTwo = entityTwo;
    }

    public SLFourEntityThreeDTO getEntityThree() {
        return entityThree;
    }

    public void setEntityThree(SLFourEntityThreeDTO entityThree) {
        this.entityThree = entityThree;
    }
}
