package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

public class CLThreeWRollupsEntityCategoryTypeDTO {

    private Integer id;
    private String name;
    private CLThreeWRollupsEntityCategoryTypeSchemeDTO scheme;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLThreeWRollupsEntityCategoryTypeSchemeDTO getScheme() {
        return scheme;
    }

    public void setScheme(CLThreeWRollupsEntityCategoryTypeSchemeDTO scheme) {
        this.scheme = scheme;
    }
}
