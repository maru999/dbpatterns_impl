package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityWAssociationsIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntitiesWAssociationsIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface RLThreeWRulesEntityRepo extends JpaRepository<RLThreeWRulesEntity, Integer> {

    @Query(value = "SELECT * FROM rlthreewr_entity_associations", nativeQuery = true)
    List<RLThreeWRulesEntitiesWAssociationsIProjection> getAllEntitiesWAssociations();

    @Query(value = "SELECT * FROM rlthreewr_entity_associations WHERE rlthreewr_entity_id = :id", nativeQuery = true)
    List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWAssociationsByEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rlthreewr_entity_associations WHERE rlthreewr_entity_assoc_rule_name = :name", nativeQuery = true)
    List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWAssociationsByRuleName(@Param("name") String ruleName);

    @Query(value = "SELECT * FROM rlthreewr_entity_associations WHERE rlthreewr_entity_type_name = :name", nativeQuery = true)
    List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWithAssociationsByTypeName(@Param("name") String typeName);

    @Query(value = "SELECT * FROM rlthreewr_entity_associations WHERE rlthreewr_entity_assoc_thru_date IS NULL", nativeQuery = true)
    List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWithAssociationsStillInUse();

    @Query(value = "SELECT * FROM rlthreewr_entity_associations WHERE rlthreewr_entity_assoc_thru_date IS NOT NULL", nativeQuery = true)
    List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWithAssociationsNoLongerInUse();

    @Query(value = "SELECT * FROM rlthreewr_entity_associations WHERE rlthreewr_entity_assoc_from_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<RLThreeWRulesEntitiesWAssociationsIProjection> getEntitiesWithAssociationsWhereFromDateBetween(@Param("startDate") LocalDate start, @Param("endDate") LocalDate end);

    @Query(value = "SELECT * FROM rlthreewr_entity WHERE rlthreewr_entity_type_fk = :id", nativeQuery = true)
    List<RLThreeWRulesEntity> getEntitiesByTypeId(@Param("id") Integer typeId);

}
