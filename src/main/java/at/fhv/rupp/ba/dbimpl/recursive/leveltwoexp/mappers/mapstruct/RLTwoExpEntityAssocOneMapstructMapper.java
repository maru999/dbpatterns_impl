package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityAssocOneDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocOne;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = RLTwoExpEntityMapstructMapper.class)
public interface RLTwoExpEntityAssocOneMapstructMapper {
    @Mapping(source = "rlTwoExpEntityAssocOneId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocOneFromDate", target = "fromDate")
    @Mapping(source = "rlTwoExpEntityAssocOneThruDate", target = "thruDate")
    @Mapping(source = "rlTwoExpEntityAssocOneFromEntity", target = "fromEntity")
    @Mapping(source = "rlTwoExpEntityAssocOneToEntity", target = "toEntity")
    RLTwoExpEntityAssocOneDTO toDTO(RLTwoExpEntityAssocOne entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLTwoExpEntityAssocOne toEntity(RLTwoExpEntityAssocOneDTO dto);

    @Mapping(source = "rlTwoExpEntityAssocOneId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocOneFromDate", target = "fromDate")
    @Mapping(source = "rlTwoExpEntityAssocOneThruDate", target = "thruDate")
    @Mapping(source = "rlTwoExpEntityAssocOneFromEntity", target = "fromEntity")
    @Mapping(source = "rlTwoExpEntityAssocOneToEntity", target = "toEntity")
    void updateEntityFromDTO(RLTwoExpEntityAssocOne entity, @MappingTarget RLTwoExpEntityAssocOneDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLTwoExpEntityAssocOneDTO dto, @MappingTarget RLTwoExpEntityAssocOne entity);

    List<RLTwoExpEntityAssocOneDTO> toDTOs(List<RLTwoExpEntityAssocOne> entities);

    List<RLTwoExpEntityAssocOne> toEntities(List<RLTwoExpEntityAssocOneDTO> dtos);
}
