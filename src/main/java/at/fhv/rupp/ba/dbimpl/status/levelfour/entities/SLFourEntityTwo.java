package at.fhv.rupp.ba.dbimpl.status.levelfour.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "slfour_entity_two")
@EntityListeners(AuditingEntityListener.class)
public class SLFourEntityTwo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slfour_entity_two_id")
    private int slFourEntityTwoId;

    @Column(name = "slfour_entity_two_name")
    private String slFourEntityTwoName;

    public int getSlFourEntityTwoId() {
        return slFourEntityTwoId;
    }

    public void setSlFourEntityTwoId(int slFourEntityTwoId) {
        this.slFourEntityTwoId = slFourEntityTwoId;
    }

    public String getSlFourEntityTwoName() {
        return slFourEntityTwoName;
    }

    public void setSlFourEntityTwoName(String slFourEntityTwoName) {
        this.slFourEntityTwoName = slFourEntityTwoName;
    }
}
