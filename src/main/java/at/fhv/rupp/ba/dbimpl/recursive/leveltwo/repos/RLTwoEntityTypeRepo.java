package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLTwoEntityTypeRepo extends JpaRepository<RLTwoEntityType, Integer> {
    @Query(value = "SELECT * FROM rltwo_entity_type WHERE rltwo_entity_type_parent = :id", nativeQuery = true)
    List<RLTwoEntityType> getAllTypesWhereTypeIsParent(@Param("id") Integer typeId);
}
