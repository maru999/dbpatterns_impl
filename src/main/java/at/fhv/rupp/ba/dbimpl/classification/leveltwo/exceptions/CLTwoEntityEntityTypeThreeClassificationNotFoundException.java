package at.fhv.rupp.ba.dbimpl.classification.leveltwo.exceptions;

public class CLTwoEntityEntityTypeThreeClassificationNotFoundException extends Exception {
    public CLTwoEntityEntityTypeThreeClassificationNotFoundException(String message) {
        super(message);
    }
}
