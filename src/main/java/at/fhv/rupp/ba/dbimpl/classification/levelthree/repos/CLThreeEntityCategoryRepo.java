package at.fhv.rupp.ba.dbimpl.classification.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CLThreeEntityCategoryRepo extends JpaRepository<CLThreeEntityCategory, Integer> {

    @Query(value = "SELECT * FROM clthree_entity_category WHERE clthree_entity_category_parent_category IS NULL", nativeQuery = true)
    List<CLThreeEntityCategory> getTopParentCategoryNames();
}
