package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityTypeTwoDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeTwo;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntityTypeTwoMapper {

    private CLTwoEntityTypeTwoMapper() {
        //hidden constructor
    }

    public static CLTwoEntityTypeTwoDTO toDTO(CLTwoEntityTypeTwo entity) {
        CLTwoEntityTypeTwoDTO dto = new CLTwoEntityTypeTwoDTO();
        dto.setId(entity.getClTwoEntityTypeTwoId());
        dto.setName(entity.getClTwoEntityTypeTwoName());
        if (entity.getClTwoEntityTypeTwoParent() != null) {
            dto.setParentType(toDTO(entity.getClTwoEntityTypeTwoParent()));
        }
        return dto;
    }

    public static CLTwoEntityTypeTwo toEntity(CLTwoEntityTypeTwoDTO dto) {
        CLTwoEntityTypeTwo entity = new CLTwoEntityTypeTwo();
        if (dto.getId() != null) {
            entity.setClTwoEntityTypeTwoId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setClTwoEntityTypeTwoName(dto.getName());
        }
        if (dto.getParentType() != null) {
            entity.setClTwoEntityTypeTwoParent(toEntity(dto.getParentType()));
        }
        return entity;
    }

    public static List<CLTwoEntityTypeTwoDTO> toDTOs(List<CLTwoEntityTypeTwo> entities) {
        LinkedList<CLTwoEntityTypeTwoDTO> result = new LinkedList<>();
        for (CLTwoEntityTypeTwo entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLTwoEntityTypeTwo> toEntities(List<CLTwoEntityTypeTwoDTO> dtos) {
        LinkedList<CLTwoEntityTypeTwo> result = new LinkedList<>();
        for (CLTwoEntityTypeTwoDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
