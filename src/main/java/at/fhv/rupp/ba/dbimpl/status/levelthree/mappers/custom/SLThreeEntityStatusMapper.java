package at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityStatusDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntityStatus;

import java.util.LinkedList;
import java.util.List;

public class SLThreeEntityStatusMapper {

    private SLThreeEntityStatusMapper() {
        //hidden constructor
    }

    public static SLThreeEntityStatusDTO toDTO(SLThreeEntityStatus entity) {
        SLThreeEntityStatusDTO dto = new SLThreeEntityStatusDTO();
        dto.setId(entity.getSlThreeEntityStatusId());
        if (entity.getSlThreeEntityStatusDateTime() != null) {
            dto.setStatusDateTime(entity.getSlThreeEntityStatusDateTime());
        }
        dto.setStatusFrom(entity.getSlThreeEntityStatusStatusFromDate());
        if (entity.getSlThreeEntityStatusStatusThruDate() != null) {
            dto.setStatusThru(entity.getSlThreeEntityStatusStatusThruDate());
        }
        dto.setEntityStatusFrom(entity.getSlThreeEntityStatusFromDate());
        if (entity.getSlThreeEntityStatusThruDate() != null) {
            dto.setEntityStatusThru(entity.getSlThreeEntityStatusThruDate());
        }
        if (entity.getSlThreeEntityStatusStatusType() != null) {
            dto.setStatusType(SLThreeStatusTypeMapper.toDTO(entity.getSlThreeEntityStatusStatusType()));
        }
        if (entity.getSlThreeEntityStatusEntity() != null) {
            dto.setEntity(SLThreeEntityMapper.toDTO(entity.getSlThreeEntityStatusEntity()));
        }
        return dto;
    }

    public static SLThreeEntityStatus toEntity(SLThreeEntityStatusDTO dto) {
        SLThreeEntityStatus entity = new SLThreeEntityStatus();
        if (dto.getId() != null) {
            entity.setSlThreeEntityStatusId(dto.getId());
        }
        if (dto.getStatusDateTime() != null) {
            entity.setSlThreeEntityStatusDateTime(dto.getStatusDateTime());
        }
        if (dto.getStatusFrom() != null) {
            entity.setSlThreeEntityStatusStatusFromDate(dto.getStatusFrom());
        }
        if (dto.getStatusThru() != null) {
            entity.setSlThreeEntityStatusStatusThruDate(dto.getStatusThru());
        }
        if (dto.getEntityStatusFrom() != null) {
            entity.setSlThreeEntityStatusFromDate(dto.getEntityStatusFrom());
        }
        if (dto.getEntityStatusThru() != null) {
            entity.setSlThreeEntityStatusThruDate(dto.getEntityStatusThru());
        }
        if (dto.getStatusType() != null) {
            entity.setSlThreeEntityStatusStatusType(SLThreeStatusTypeMapper.toEntity(dto.getStatusType()));
        }
        if (dto.getEntity() != null) {
            entity.setSlThreeEntityStatusEntity(SLThreeEntityMapper.toEntity(dto.getEntity()));
        }
        return entity;
    }

    public static List<SLThreeEntityStatusDTO> toDTOs(List<SLThreeEntityStatus> entities) {
        LinkedList<SLThreeEntityStatusDTO> result = new LinkedList<>();
        for (SLThreeEntityStatus entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLThreeEntityStatus> toEntities(List<SLThreeEntityStatusDTO> dtos) {
        LinkedList<SLThreeEntityStatus> result = new LinkedList<>();
        for (SLThreeEntityStatusDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
