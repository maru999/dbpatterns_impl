package at.fhv.rupp.ba.dbimpl.status.levelfour.repos;

import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityTwo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SLFourEntityTwoRepo extends JpaRepository<SLFourEntityTwo, Integer> {
}
