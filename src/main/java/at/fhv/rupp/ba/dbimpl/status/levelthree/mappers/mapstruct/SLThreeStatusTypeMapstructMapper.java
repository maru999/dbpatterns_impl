package at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeStatusType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLThreeStatusTypeMapstructMapper {

    @Mapping(source = "slThreeStatusTypeId", target = "id")
    @Mapping(source = "slThreeStatusTypeName", target = "name")
    SLThreeStatusTypeDTO toDTO(SLThreeStatusType entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLThreeStatusType toEntity(SLThreeStatusTypeDTO dto);

    @Mapping(source = "slThreeStatusTypeId", target = "id")
    @Mapping(source = "slThreeStatusTypeName", target = "name")
    void updateEntityFromDTO(SLThreeStatusType entity, @MappingTarget SLThreeStatusTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLThreeStatusTypeDTO dto, @MappingTarget SLThreeStatusType entity);

    List<SLThreeStatusTypeDTO> toDTOs(List<SLThreeStatusType> entities);

    List<SLThreeStatusType> toEntities(List<SLThreeStatusTypeDTO> dtos);
}
