package at.fhv.rupp.ba.dbimpl.status.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntityStatusType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SLTwoEntityStatusTypeRepo extends JpaRepository<SLTwoEntityStatusType, Integer> {
}
