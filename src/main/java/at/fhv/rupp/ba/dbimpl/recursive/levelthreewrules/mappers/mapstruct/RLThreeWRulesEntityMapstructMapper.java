package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = RLThreeWRulesEntityTypeMapstructMapper.class)
public interface RLThreeWRulesEntityMapstructMapper {
    @Mapping(source = "rlThreeWRulesEntityId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityType", target = "type")
    RLThreeWRulesEntityDTO toDTO(RLThreeWRulesEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeWRulesEntity toEntity(RLThreeWRulesEntityDTO dto);

    @Mapping(source = "rlThreeWRulesEntityId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityType", target = "type")
    void updateEntityFromDTO(RLThreeWRulesEntity entity, @MappingTarget RLThreeWRulesEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeWRulesEntityDTO dto, @MappingTarget RLThreeWRulesEntity entity);

    List<RLThreeWRulesEntityDTO> toDTOs(List<RLThreeWRulesEntity> entities);

    List<RLThreeWRulesEntity> toEntities(List<RLThreeWRulesEntityDTO> dtos);
}
