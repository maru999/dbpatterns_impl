package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

public class CLThreeWRollupsEntityCategoryTypeSchemeDTO {
    private Integer id;
    private String name;
    private CLThreeWRollupsDataProviderDTO dataProvider;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLThreeWRollupsDataProviderDTO getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(CLThreeWRollupsDataProviderDTO dataProvider) {
        this.dataProvider = dataProvider;
    }
}
