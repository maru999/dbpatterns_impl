package at.fhv.rupp.ba.dbimpl.status.levelone.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelone.dtos.SLOneEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.levelone.entities.SLOneEntity;

import java.util.LinkedList;
import java.util.List;

public class SLOneEntityMapper {

    private SLOneEntityMapper() {
        //hidden constructor
    }

    public static SLOneEntityDTO toDTO(SLOneEntity entity) {
        SLOneEntityDTO dto = new SLOneEntityDTO();
        dto.setId(entity.getSlOneEntityId());
        if (entity.getSlOneEntityEventOneDateTime() != null) {
            dto.setEventOneDateTime(entity.getSlOneEntityEventOneDateTime());
        }
        if (entity.getSlOneEntityEventTwoDateTime() != null) {
            dto.setEventTwoDateTime(entity.getSlOneEntityEventTwoDateTime());
        }
        if (entity.getSlOneEntityEventThreeDateTime() != null) {
            dto.setEventThreeDateTime(entity.getSlOneEntityEventThreeDateTime());
        }
        if (entity.getSlOneEntityEventIndicator() != null) {
            dto.setEventIndicator(entity.getSlOneEntityEventIndicator());
        }
        dto.setEventFromDate(entity.getSlOneEntityEventFromDate());
        if (entity.getSlOneEntityEventThruDate() != null) {
            dto.setEventThruDate(entity.getSlOneEntityEventThruDate());
        }
        return dto;
    }

    public static SLOneEntity toEntity(SLOneEntityDTO dto) {
        SLOneEntity entity = new SLOneEntity();
        if (dto.getId() != null) {
            entity.setSlOneEntityId(dto.getId());
        }
        if (dto.getEventOneDateTime() != null) {
            entity.setSlOneEntityEventOneDateTime(dto.getEventOneDateTime());
        }
        if (dto.getEventTwoDateTime() != null) {
            entity.setSlOneEntityEventTwoDateTime(dto.getEventTwoDateTime());
        }
        if (dto.getEventThreeDateTime() != null) {
            entity.setSlOneEntityEventThreeDateTime(dto.getEventThreeDateTime());
        }
        if (dto.getEventIndicator() != null) {
            entity.setSlOneEntityEventIndicator(dto.getEventIndicator());
        }
        if (dto.getEventFromDate() != null) {
            entity.setSlOneEntityEventFromDate(dto.getEventFromDate());
        }
        if (dto.getEventThruDate() != null) {
            entity.setSlOneEntityEventThruDate(dto.getEventThruDate());
        }
        return entity;
    }

    public static List<SLOneEntityDTO> toDTOs(List<SLOneEntity> entities) {
        LinkedList<SLOneEntityDTO> result = new LinkedList<>();
        for (SLOneEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLOneEntity> toEntities(List<SLOneEntityDTO> dtos) {
        LinkedList<SLOneEntity> result = new LinkedList<>();
        for (SLOneEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
