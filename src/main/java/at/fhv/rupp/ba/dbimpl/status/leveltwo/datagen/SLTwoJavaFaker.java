package at.fhv.rupp.ba.dbimpl.status.leveltwo.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntity;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntityStatusType;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.repos.SLTwoEntityRepo;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.repos.SLTwoEntityStatusTypeRepo;

import java.util.LinkedList;
import java.util.List;

public class SLTwoJavaFaker extends DataGenJavaFaker {
    private final SLTwoEntityRepo entityRepo;
    private final SLTwoEntityStatusTypeRepo statusRepo;

    public SLTwoJavaFaker(SLTwoEntityRepo entityRepo, SLTwoEntityStatusTypeRepo statusRepo) {
        this.entityRepo = entityRepo;
        this.statusRepo = statusRepo;
    }

    @Override
    public void fillDB() {
        SLTwoEntity entity;
        SLTwoEntityStatusType entityStatusType;
        List<SLTwoEntityStatusType> createdStatusTypes = new LinkedList<>();
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityStatusType = new SLTwoEntityStatusType();
            entityStatusType.setSlTwoStatusTypeName(trimStringIfTooLong(FAKER.name().firstName(), MIDDLE_CHAR_LIMIT));
            entityStatusType.setSlTwoEntityStatusTypeSomeAttr(trimStringIfTooLong(FAKER.name().lastName(), MIDDLE_CHAR_LIMIT));
            createdStatusTypes.add(entityStatusType);
            statusRepo.save(entityStatusType);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new SLTwoEntity();
            entity.setSlTwoEntityStatusDateTime(getDateTimeBetween(START_DATETIME, END_DATETIME));
            if (!createdStatusTypes.isEmpty()) {
                entity.setSlTwoEntityStatusType(createdStatusTypes.get(getRandomIntByBound(createdStatusTypes.size())));
            }
            entityRepo.save(entity);
        }
    }
}
