package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourStatusType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLFourStatusTypeMapstructMapper {

    @Mapping(source = "slFourStatusTypeId", target = "id")
    @Mapping(source = "slFourStatusTypeName", target = "name")
    SLFourStatusTypeDTO toDTO(SLFourStatusType entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLFourStatusType toEntity(SLFourStatusTypeDTO dto);

    @Mapping(source = "slFourStatusTypeId", target = "id")
    @Mapping(source = "slFourStatusTypeName", target = "name")
    void updateEntityFromDTO(SLFourStatusType entity, @MappingTarget SLFourStatusTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLFourStatusTypeDTO dto, @MappingTarget SLFourStatusType entity);

    List<SLFourStatusTypeDTO> toDTOs(List<SLFourStatusType> entities);

    List<SLFourStatusType> toEntities(List<SLFourStatusTypeDTO> dtos);
}
