package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityTypeThreeDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeThree;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLTwoEntityTypeThreeMapstructMapper {

    @Mapping(source = "clTwoEntityTypeThreeId", target = "id")
    @Mapping(source = "clTwoEntityTypeThreeName", target = "name")
    CLTwoEntityTypeThreeDTO toDTO(CLTwoEntityTypeThree entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLTwoEntityTypeThree toEntity(CLTwoEntityTypeThreeDTO dto);

    @Mapping(source = "clTwoEntityTypeThreeId", target = "id")
    @Mapping(source = "clTwoEntityTypeThreeName", target = "name")
    void updateEntityFromDTO(CLTwoEntityTypeThree entity, @MappingTarget CLTwoEntityTypeThreeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLTwoEntityTypeThreeDTO dto, @MappingTarget CLTwoEntityTypeThree entity);

    List<CLTwoEntityTypeThreeDTO> toDTOs(List<CLTwoEntityTypeThree> entities);

    List<CLTwoEntityTypeThree> toEntities(List<CLTwoEntityTypeThreeDTO> dtos);
}
