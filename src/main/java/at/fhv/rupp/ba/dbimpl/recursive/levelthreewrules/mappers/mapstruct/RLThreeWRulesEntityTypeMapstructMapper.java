package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLThreeWRulesEntityTypeMapstructMapper {
    @Mapping(source = "rlThreeWRulesEntityTypeId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityTypeName", target = "name")
    @Mapping(source = "rlThreeWRulesEntityTypeParent", target = "parent")
    RLThreeWRulesEntityTypeDTO toDTO(RLThreeWRulesEntityType entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeWRulesEntityType toEntity(RLThreeWRulesEntityTypeDTO dto);

    @Mapping(source = "rlThreeWRulesEntityTypeId", target = "id")
    @Mapping(source = "rlThreeWRulesEntityTypeName", target = "name")
    @Mapping(source = "rlThreeWRulesEntityTypeParent", target = "parent")
    void updateEntityFromDTO(RLThreeWRulesEntityType entity, @MappingTarget RLThreeWRulesEntityTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeWRulesEntityTypeDTO dto, @MappingTarget RLThreeWRulesEntityType entity);

    List<RLThreeWRulesEntityTypeDTO> toDTOs(List<RLThreeWRulesEntityType> entities);

    List<RLThreeWRulesEntityType> toEntities(List<RLThreeWRulesEntityTypeDTO> dtos);
}
