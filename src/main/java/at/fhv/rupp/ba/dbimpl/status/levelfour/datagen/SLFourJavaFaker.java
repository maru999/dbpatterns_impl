package at.fhv.rupp.ba.dbimpl.status.levelfour.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.*;
import at.fhv.rupp.ba.dbimpl.status.levelfour.repos.*;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class SLFourJavaFaker extends DataGenJavaFaker {
    private final SLFourEntityOneRepo entityOneRepo;
    private final SLFourEntityTwoRepo entityTwoRepo;
    private final SLFourEntityThreeRepo entityThreeRepo;
    private final SLFourStatusApplicationRepo applicationRepo;
    private final SLFourStatusTypeRepo statusTypeRepo;

    public SLFourJavaFaker(SLFourEntityOneRepo entityOneRepo, SLFourEntityTwoRepo entityTwoRepo, SLFourEntityThreeRepo entityThreeRepo, SLFourStatusApplicationRepo applicationRepo, SLFourStatusTypeRepo statusTypeRepo) {
        this.entityOneRepo = entityOneRepo;
        this.entityTwoRepo = entityTwoRepo;
        this.entityThreeRepo = entityThreeRepo;
        this.applicationRepo = applicationRepo;
        this.statusTypeRepo = statusTypeRepo;
    }

    @Override
    public void fillDB() {
        SLFourEntityOne entityOne;
        SLFourEntityTwo entityTwo;
        SLFourEntityThree entityThree;
        SLFourStatusType statusType;
        SLFourStatusApplication application;
        List<SLFourEntityOne> createdOnes = new LinkedList<>();
        List<SLFourEntityTwo> createdTwos = new LinkedList<>();
        List<SLFourEntityThree> createdThrees = new LinkedList<>();
        List<SLFourStatusType> createdStatusTypes = new LinkedList<>();
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            statusType = new SLFourStatusType();
            statusType.setSlFourStatusTypeName(trimStringIfTooLong(FAKER.internet().domainName(), LOW_CHAR_LIMIT));
            createdStatusTypes.add(statusType);
            statusTypeRepo.save(statusType);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityOne = new SLFourEntityOne();
            entityOne.setSlFourEntityOneDesc(trimStringIfTooLong(FAKER.internet().emailAddress(), MIDDLE_CHAR_LIMIT));
            createdOnes.add(entityOne);
            entityOneRepo.save(entityOne);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityTwo = new SLFourEntityTwo();
            entityTwo.setSlFourEntityTwoName(trimStringIfTooLong(FAKER.internet().url(), LOW_CHAR_LIMIT));
            createdTwos.add(entityTwo);
            entityTwoRepo.save(entityTwo);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityThree = new SLFourEntityThree();
            entityThree.setSlFourEntityThreeAttr(trimStringIfTooLong(FAKER.internet().domainSuffix(), LOW_CHAR_LIMIT));
            createdThrees.add(entityThree);
            entityThreeRepo.save(entityThree);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            application = new SLFourStatusApplication();
            LocalDate fromDate = getDateBetween(START_DATE, END_DATE);
            application.setSlFourStatusApplicationFrom(fromDate);
            if (getRandomIntByBound(20) % 2 == 0) {
                application.setSlFourStatusApplicationThru(getDateBetween(fromDate, END_DATE));
            }
            LocalDate fromStatusDate = getDateBetween(fromDate, END_DATE);
            application.setSlFourStatusApplicationStatusFrom(fromStatusDate);
            if (getRandomIntByBound(10) % 2 == 0) {
                application.setSlFourStatusApplicationStatusThru(getDateBetween(fromStatusDate, END_DATE));
            }
            application.setSlFourStatusApplicationDateTime(getDateTimeBetween(fromStatusDate.atStartOfDay(), END_DATETIME));
            if (!createdStatusTypes.isEmpty()) {
                application.setSlFourStatusApplicationType(createdStatusTypes.get(getRandomIntByBound(createdStatusTypes.size())));
            }
            if (!createdOnes.isEmpty() && !createdTwos.isEmpty() && !createdThrees.isEmpty()) {
                if (getRandomIntByBound(10) % 2 == 0) {
                    application.setSlFourStatusApplicationEntityOne(createdOnes.get(getRandomIntByBound(createdOnes.size())));
                } else if (getRandomIntByBound(15) % 2 == 0) {
                    application.setSlFourStatusApplicationEntityTwo(createdTwos.get(getRandomIntByBound(createdTwos.size())));
                } else {
                    application.setSlFourStatusApplicationEntityThree(createdThrees.get(getRandomIntByBound(createdThrees.size())));
                }
            }
            applicationRepo.save(application);
        }
    }
}
