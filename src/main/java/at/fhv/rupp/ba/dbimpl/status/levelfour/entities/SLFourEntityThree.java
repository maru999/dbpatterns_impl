package at.fhv.rupp.ba.dbimpl.status.levelfour.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "slfour_entity_three")
@EntityListeners(AuditingEntityListener.class)
public class SLFourEntityThree {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slfour_entity_three_id")
    private int slFourEntityThreeId;

    @Column(name = "slfour_entity_three_attr")
    private String slFourEntityThreeAttr;

    public int getSlFourEntityThreeId() {
        return slFourEntityThreeId;
    }

    public void setSlFourEntityThreeId(int slFourEntityThreeId) {
        this.slFourEntityThreeId = slFourEntityThreeId;
    }

    public String getSlFourEntityThreeAttr() {
        return slFourEntityThreeAttr;
    }

    public void setSlFourEntityThreeAttr(String slFourEntityThreeAttr) {
        this.slFourEntityThreeAttr = slFourEntityThreeAttr;
    }
}