package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "cltwo_entity_subtype_one")
@EntityListeners(AuditingEntityListener.class)
@PrimaryKeyJoinColumn(name = "cltwo_entity_subtype_one_id", referencedColumnName = "cltwo_entity_id")
public class CLTwoEntitySubtypeOne extends CLTwoEntity {

    @Column(name = "cltwo_entity_subtype_one_desc")
    private String clTwoEntitySubtypeOneDesc;

    public String getClTwoEntitySubtypeOneDesc() {
        return clTwoEntitySubtypeOneDesc;
    }

    public void setClTwoEntitySubtypeOneDesc(String clTwoEntitySubtypeOneDesc) {
        this.clTwoEntitySubtypeOneDesc = clTwoEntitySubtypeOneDesc;
    }
}
