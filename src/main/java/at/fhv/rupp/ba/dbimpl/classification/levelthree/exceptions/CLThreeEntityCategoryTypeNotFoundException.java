package at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions;

public class CLThreeEntityCategoryTypeNotFoundException extends Exception {
    public CLThreeEntityCategoryTypeNotFoundException(String message) {
        super(message);
    }
}
