package at.fhv.rupp.ba.dbimpl.recursive.levelone.exceptions;

public class RLOneEntityTwoNotFoundException extends Exception {
    public RLOneEntityTwoNotFoundException(String message) {
        super(message);
    }
}
