package at.fhv.rupp.ba.dbimpl.classification.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthree_entity_category_type")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeEntityCategoryType {
    //In Entity Category Type
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int clthreeEntityCategoryTypeId;

    @Column(name = "clthree_entity_category_type_name", nullable = false)
    private String clthreeEntityCategoryTypeName;

    @ManyToOne(fetch = FetchType.EAGER) //Recursive
    @JoinColumn(name = "clthree_parent_entity_category_type")
    private CLThreeEntityCategoryType clThreeEntityCategoryTypeParent;

    public int getClthreeEntityCategoryTypeId() {
        return clthreeEntityCategoryTypeId;
    }

    public void setClthreeEntityCategoryTypeId(int clthreeEntityCategoryTypeId) {
        this.clthreeEntityCategoryTypeId = clthreeEntityCategoryTypeId;
    }

    public String getClthreeEntityCategoryTypeName() {
        return clthreeEntityCategoryTypeName;
    }

    public void setClthreeEntityCategoryTypeName(String clthreeEntityCategoryTypeName) {
        this.clthreeEntityCategoryTypeName = clthreeEntityCategoryTypeName;
    }

    public CLThreeEntityCategoryType getClThreeEntityCategoryTypeParent() {
        return clThreeEntityCategoryTypeParent;
    }

    public void setClThreeEntityCategoryTypeParent(CLThreeEntityCategoryType clThreeEntityCategoryTypeParent) {
        this.clThreeEntityCategoryTypeParent = clThreeEntityCategoryTypeParent;
    }
}