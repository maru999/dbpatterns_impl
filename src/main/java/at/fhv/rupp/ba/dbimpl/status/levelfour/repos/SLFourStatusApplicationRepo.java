package at.fhv.rupp.ba.dbimpl.status.levelfour.repos;

import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourStatusApplication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SLFourStatusApplicationRepo extends JpaRepository<SLFourStatusApplication, Integer> {
}
