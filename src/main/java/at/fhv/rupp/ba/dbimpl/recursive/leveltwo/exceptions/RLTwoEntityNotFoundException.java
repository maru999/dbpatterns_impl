package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.exceptions;

public class RLTwoEntityNotFoundException extends Exception {
    public RLTwoEntityNotFoundException(String message) {
        super(message);
    }
}
