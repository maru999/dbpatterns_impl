package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name="cltwo_entity_type_one")
@EntityListeners(AuditingEntityListener.class)
public class CLTwoEntityTypeOne {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cltwo_entity_type_one_id")
    private int clTwoEntityTypeOneId;

    @Column(name = "cltwo_entity_type_one_name", nullable = false)
    private String clTwoEntityTypeOneName;

    public int getClTwoEntityTypeOneId() {
        return clTwoEntityTypeOneId;
    }

    public void setClTwoEntityTypeOneId(int clTwoEntityTypeOneId) {
        this.clTwoEntityTypeOneId = clTwoEntityTypeOneId;
    }

    public String getClTwoEntityTypeOneName() {
        return clTwoEntityTypeOneName;
    }

    public void setClTwoEntityTypeOneName(String clTwoEntityTypeOneName) {
        this.clTwoEntityTypeOneName = clTwoEntityTypeOneName;
    }
}
