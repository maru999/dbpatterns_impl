package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeThreeDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeThree;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntitySubtypeThreeMapper {
    private CLTwoEntitySubtypeThreeMapper() {
        //hidden constructor
    }

    public static CLTwoEntitySubtypeThreeDTO toDTO(CLTwoEntitySubtypeThree entity) {
        CLTwoEntitySubtypeThreeDTO dto = new CLTwoEntitySubtypeThreeDTO();
        dto.setId(entity.getClTwoEntityId());
        dto.setName(entity.getClTwoEntityName());
        if (entity.getClTwoEntityEntityTypeOne() != null) {
            dto.setTypeOne(CLTwoEntityTypeOneMapper.toDTO(entity.getClTwoEntityEntityTypeOne()));
        }
        if (entity.getClTwoEntityEntityTypeTwo() != null) {
            dto.setTypeTwo(CLTwoEntityTypeTwoMapper.toDTO(entity.getClTwoEntityEntityTypeTwo()));
        }
        if (entity.getClTwoEntitySubtypeThreeInfo() != null) {
            dto.setInfo(entity.getClTwoEntitySubtypeThreeInfo());
        }
        return dto;
    }

    public static CLTwoEntitySubtypeThree toEntity(CLTwoEntitySubtypeThreeDTO dto) {
        CLTwoEntitySubtypeThree entity = new CLTwoEntitySubtypeThree();
        if (dto.getId() != null) {
            entity.setClTwoEntityId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setClTwoEntityName(dto.getName());
        }
        if (dto.getTypeOne() != null) {
            entity.setClTwoEntityEntityTypeOne(CLTwoEntityTypeOneMapper.toEntity(dto.getTypeOne()));
        }
        if (dto.getTypeTwo() != null) {
            entity.setClTwoEntityEntityTypeTwo(CLTwoEntityTypeTwoMapper.toEntity(dto.getTypeTwo()));
        }
        if (dto.getInfo() != null) {
            entity.setClTwoEntitySubtypeThreeInfo(dto.getInfo());
        }
        return entity;
    }

    public static List<CLTwoEntitySubtypeThreeDTO> toDTOs(List<CLTwoEntitySubtypeThree> entities) {
        LinkedList<CLTwoEntitySubtypeThreeDTO> result = new LinkedList<>();
        for (CLTwoEntitySubtypeThree entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLTwoEntitySubtypeThree> toEntities(List<CLTwoEntitySubtypeThreeDTO> dtos) {
        LinkedList<CLTwoEntitySubtypeThree> result = new LinkedList<>();
        for (CLTwoEntitySubtypeThreeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
