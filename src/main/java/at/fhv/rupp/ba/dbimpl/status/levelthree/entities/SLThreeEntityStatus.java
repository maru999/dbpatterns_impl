package at.fhv.rupp.ba.dbimpl.status.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "slthree_entity_status")
@EntityListeners(AuditingEntityListener.class)
public class SLThreeEntityStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slthree_entity_status_id")
    private int slThreeEntityStatusId;

    @Column(name = "slthree_entity_status_date_time")
    private LocalDateTime slThreeEntityStatusDateTime;

    @Column(name = "slthree_entity_status_status_from_date", nullable = false)
    private LocalDate slThreeEntityStatusStatusFromDate;

    @Column(name = "slthree_entity_status_status_thru_date")
    private LocalDate slThreeEntityStatusStatusThruDate;

    @Column(name = "slthree_entity_status_from_date", nullable = false)
    private LocalDate slThreeEntityStatusFromDate;

    @Column(name = "slthree_entity_status_thru_date")
    private LocalDate slThreeEntityStatusThruDate;

    @ManyToOne
    @JoinColumn(name = "slthree_entity_status_type_fk")
    private SLThreeStatusType slThreeEntityStatusStatusType;

    @ManyToOne
    @JoinColumn(name = "slthree_entity_status_entity_fk")
    private SLThreeEntity slThreeEntityStatusEntity;

    public int getSlThreeEntityStatusId() {
        return slThreeEntityStatusId;
    }

    public void setSlThreeEntityStatusId(int slThreeEntityStatusId) {
        this.slThreeEntityStatusId = slThreeEntityStatusId;
    }

    public LocalDateTime getSlThreeEntityStatusDateTime() {
        return slThreeEntityStatusDateTime;
    }

    public void setSlThreeEntityStatusDateTime(LocalDateTime slThreeEntityStatusDateTime) {
        this.slThreeEntityStatusDateTime = slThreeEntityStatusDateTime;
    }

    public LocalDate getSlThreeEntityStatusStatusFromDate() {
        return slThreeEntityStatusStatusFromDate;
    }

    public void setSlThreeEntityStatusStatusFromDate(LocalDate slThreeEntityStatusStatusFromDate) {
        this.slThreeEntityStatusStatusFromDate = slThreeEntityStatusStatusFromDate;
    }

    public LocalDate getSlThreeEntityStatusStatusThruDate() {
        return slThreeEntityStatusStatusThruDate;
    }

    public void setSlThreeEntityStatusStatusThruDate(LocalDate slThreeEntityStatusStatusThruDate) {
        this.slThreeEntityStatusStatusThruDate = slThreeEntityStatusStatusThruDate;
    }

    public LocalDate getSlThreeEntityStatusFromDate() {
        return slThreeEntityStatusFromDate;
    }

    public void setSlThreeEntityStatusFromDate(LocalDate slThreeEntityStatusFromDate) {
        this.slThreeEntityStatusFromDate = slThreeEntityStatusFromDate;
    }

    public LocalDate getSlThreeEntityStatusThruDate() {
        return slThreeEntityStatusThruDate;
    }

    public void setSlThreeEntityStatusThruDate(LocalDate slThreeEntityStatusThruDate) {
        this.slThreeEntityStatusThruDate = slThreeEntityStatusThruDate;
    }

    public SLThreeStatusType getSlThreeEntityStatusStatusType() {
        return slThreeEntityStatusStatusType;
    }

    public void setSlThreeEntityStatusStatusType(SLThreeStatusType slThreeEntityStatusStatusType) {
        this.slThreeEntityStatusStatusType = slThreeEntityStatusStatusType;
    }

    public SLThreeEntity getSlThreeEntityStatusEntity() {
        return slThreeEntityStatusEntity;
    }

    public void setSlThreeEntityStatusEntity(SLThreeEntity slThreeEntityStatusEntity) {
        this.slThreeEntityStatusEntity = slThreeEntityStatusEntity;
    }
}
