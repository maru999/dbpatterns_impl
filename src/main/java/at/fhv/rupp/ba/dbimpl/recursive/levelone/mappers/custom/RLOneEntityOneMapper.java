package at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityOneDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityOne;

import java.util.LinkedList;
import java.util.List;

public class RLOneEntityOneMapper {
    private RLOneEntityOneMapper() {
        //hidden constructor
    }

    public static RLOneEntityOneDTO toDTO(RLOneEntityOne entity) {
        RLOneEntityOneDTO dto = new RLOneEntityOneDTO();
        dto.setId(entity.getRloneEntityOneId());
        dto.setName(entity.getRloneEntityOneName());
        return dto;
    }

    public static RLOneEntityOne toEntity(RLOneEntityOneDTO dto) {
        RLOneEntityOne entity = new RLOneEntityOne();
        if (dto.getId() != null) {
            entity.setRloneEntityOneId(dto.getId());
        }
        entity.setRloneEntityOneName(dto.getName());
        return entity;
    }

    public static List<RLOneEntityOneDTO> toDTOs(List<RLOneEntityOne> entities) {
        LinkedList<RLOneEntityOneDTO> result = new LinkedList<>();
        for (RLOneEntityOne entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLOneEntityOne> toEntities(List<RLOneEntityOneDTO> dtos) {
        LinkedList<RLOneEntityOne> result = new LinkedList<>();
        for (RLOneEntityOneDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
