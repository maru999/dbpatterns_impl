package at.fhv.rupp.ba.dbimpl.status.levelfour.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface SLFourEntityStatusViewIProjection {
Integer getslfour_status_type_id();

String getslfour_status_type_name();

String getslfour_entity_one_desc();

String getslfour_entity_two_name();

String getslfour_entity_three_attr();

Integer getslfour_status_appli_id();

LocalDateTime getslfour_status_appli_datetime();

LocalDate getslfour_status_appli_status_from();

LocalDate getslfour_status_appli_status_thru();

LocalDate getslfour_status_appli_from();

LocalDate getslfour_status_appli_thru();

Integer getslfour_status_appli_type();

Integer getslfour_status_appli_entity_one();

Integer getslfour_status_appli_entity_two();

Integer getslfour_status_appli_entity_three();
}
