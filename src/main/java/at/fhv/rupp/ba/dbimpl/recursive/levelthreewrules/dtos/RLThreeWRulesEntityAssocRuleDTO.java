package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos;

public class RLThreeWRulesEntityAssocRuleDTO {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
