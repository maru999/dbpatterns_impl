package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeOneDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeOne;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntitySubtypeOneMapper {
    private CLTwoEntitySubtypeOneMapper() {
        //hidden constructor
    }

    public static CLTwoEntitySubtypeOneDTO toDTO(CLTwoEntitySubtypeOne entity) {
        CLTwoEntitySubtypeOneDTO dto = new CLTwoEntitySubtypeOneDTO();
        dto.setId(entity.getClTwoEntityId());
        dto.setName(entity.getClTwoEntityName());
        if (entity.getClTwoEntityEntityTypeOne() != null) {
            dto.setTypeOne(CLTwoEntityTypeOneMapper.toDTO(entity.getClTwoEntityEntityTypeOne()));
        }
        if (entity.getClTwoEntityEntityTypeTwo() != null) {
            dto.setTypeTwo(CLTwoEntityTypeTwoMapper.toDTO(entity.getClTwoEntityEntityTypeTwo()));
        }
        if (entity.getClTwoEntitySubtypeOneDesc() != null) {
            dto.setDesc(entity.getClTwoEntitySubtypeOneDesc());
        }
        return dto;
    }

    public static CLTwoEntitySubtypeOne toEntity(CLTwoEntitySubtypeOneDTO dto) {
        CLTwoEntitySubtypeOne entity = new CLTwoEntitySubtypeOne();
        if (dto.getId() != null) {
            entity.setClTwoEntityId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setClTwoEntityName(dto.getName());
        }
        if (dto.getTypeOne() != null) {
            entity.setClTwoEntityEntityTypeOne(CLTwoEntityTypeOneMapper.toEntity(dto.getTypeOne()));
        }
        if (dto.getTypeTwo() != null) {
            entity.setClTwoEntityEntityTypeTwo(CLTwoEntityTypeTwoMapper.toEntity(dto.getTypeTwo()));
        }
        if (dto.getDesc() != null) {
            entity.setClTwoEntitySubtypeOneDesc(dto.getDesc());
        }
        return entity;
    }

    public static List<CLTwoEntitySubtypeOneDTO> toDTOs(List<CLTwoEntitySubtypeOne> entities) {
        LinkedList<CLTwoEntitySubtypeOneDTO> result = new LinkedList<>();
        for (CLTwoEntitySubtypeOne entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLTwoEntitySubtypeOne> toEntities(List<CLTwoEntitySubtypeOneDTO> dtos) {
        LinkedList<CLTwoEntitySubtypeOne> result = new LinkedList<>();
        for (CLTwoEntitySubtypeOneDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
