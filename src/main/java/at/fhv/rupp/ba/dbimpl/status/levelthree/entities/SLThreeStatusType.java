package at.fhv.rupp.ba.dbimpl.status.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "slthree_status_type")
@EntityListeners(AuditingEntityListener.class)
public class SLThreeStatusType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slthree_status_type_id")
    private int slThreeStatusTypeId;

    @Column(name = "slthree_status_type_name")
    private String slThreeStatusTypeName;

    public int getSlThreeStatusTypeId() {
        return slThreeStatusTypeId;
    }

    public void setSlThreeStatusTypeId(int slThreeStatusTypeId) {
        this.slThreeStatusTypeId = slThreeStatusTypeId;
    }

    public String getSlThreeStatusTypeName() {
        return slThreeStatusTypeName;
    }

    public void setSlThreeStatusTypeName(String slThreeStatusTypeName) {
        this.slThreeStatusTypeName = slThreeStatusTypeName;
    }
}
