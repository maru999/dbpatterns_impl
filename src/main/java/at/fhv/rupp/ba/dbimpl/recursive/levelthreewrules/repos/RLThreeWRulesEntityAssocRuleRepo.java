package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityAssocRule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RLThreeWRulesEntityAssocRuleRepo extends JpaRepository<RLThreeWRulesEntityAssocRule, Integer> {
}
