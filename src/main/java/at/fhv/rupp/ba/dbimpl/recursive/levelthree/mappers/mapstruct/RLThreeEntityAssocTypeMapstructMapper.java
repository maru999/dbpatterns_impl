package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssocType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLThreeEntityAssocTypeMapstructMapper {
    @Mapping(source = "rlThreeEntityAssocTypeId", target = "id")
    @Mapping(source = "rlThreeEntityAssocTypeName", target = "name")
    @Mapping(source = "rlThreeEntityAssocTypeParent", target = "parent")
    RLThreeEntityAssocTypeDTO toDTO(RLThreeEntityAssocType entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeEntityAssocType toEntity(RLThreeEntityAssocTypeDTO dto);

    @Mapping(source = "rlThreeEntityAssocTypeId", target = "id")
    @Mapping(source = "rlThreeEntityAssocTypeName", target = "name")
    @Mapping(source = "rlThreeEntityAssocTypeParent", target = "parent")
    void updateEntityFromDTO(RLThreeEntityAssocType entity, @MappingTarget RLThreeEntityAssocTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeEntityAssocTypeDTO dto, @MappingTarget RLThreeEntityAssocType entity);

    List<RLThreeEntityAssocTypeDTO> toDTOs(List<RLThreeEntityAssocType> entities);

    List<RLThreeEntityAssocType> toEntities(List<RLThreeEntityAssocTypeDTO> dtos);
}
