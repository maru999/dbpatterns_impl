package at.fhv.rupp.ba.dbimpl.classification.levelone.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.classification.levelone.dtos.CLOneEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelone.mappers.custom.CLOneEntityMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelone.entities.CLOneEntity;
import at.fhv.rupp.ba.dbimpl.classification.levelone.exceptions.CLOneEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.classification.levelone.mappers.mapstruct.CLOneEntityMapstructMapper;
import at.fhv.rupp.ba.dbimpl.classification.levelone.repos.CLOneEntityRepo;
import at.fhv.rupp.ba.dbimpl.classification.levelone.datagen.CLOneJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/classification/L1")
public class CLOneController {
    private final DataGenJavaFaker faker;
    private final CLOneEntityRepo repository;
    private final CLOneEntityMapstructMapper mapstructMapper;

    public CLOneController(CLOneEntityRepo repository, CLOneEntityMapstructMapper mapstructMapper) {
        this.faker = new CLOneJavaFaker(repository);
        this.repository = repository;
        this.mapstructMapper = mapstructMapper;
    }

    @GetMapping("/entities")
    public List<CLOneEntityDTO> getAllEntities() {
        return CLOneEntityMapper.toDTOs(repository.findAll());
    }

    @GetMapping("/entities/mapstruct")
    public List<CLOneEntityDTO> getAllEntitiesWMapstruct() {
        return mapstructMapper.toDTOs(repository.findAll());
    }

    @GetMapping("/entities/{id}")
    public ResponseEntity<CLOneEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws CLOneEntityNotFoundException {
        CLOneEntity entity = repository.findById(entityId).orElseThrow(() -> new CLOneEntityNotFoundException(getExceptionString(entityId)));
        return ResponseEntity.ok().body(CLOneEntityMapper.toDTO(entity));
    }

    @PostMapping("/entities")
    public CLOneEntityDTO createEntity(@Valid @RequestBody CLOneEntityDTO entityDTO) {
        return CLOneEntityMapper.toDTO(repository.save(CLOneEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<CLOneEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody CLOneEntityDTO entityDetails) throws CLOneEntityNotFoundException {
        CLOneEntity entity = repository.findById(entityId).orElseThrow(() -> new CLOneEntityNotFoundException(getExceptionString(entityId)));
        CLOneEntityDTO entityDTO = CLOneEntityMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        entityDTO.setTypeOne(entityDetails.getTypeOne());
        entityDTO.setTypeTwo(entityDetails.getTypeTwo());
        entityDTO.setTypeThree(entityDetails.getTypeThree());
        final CLOneEntityDTO updatedEntity = CLOneEntityMapper.toDTO(repository.save(CLOneEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws CLOneEntityNotFoundException {
        CLOneEntity entity = repository.findById(entityId).orElseThrow(() -> new CLOneEntityNotFoundException(getExceptionString(entityId)));
        repository.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/classification/classificationlevelone.sql");
        DbimplApplication.restart();
    }

    private String getExceptionString(Integer id) {
        return "Level one Entity with ID " + id + " not found";
    }

}
