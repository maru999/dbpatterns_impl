package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_entity_category")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_id")
    private int clthreewrEntityCategoryId;

    @Column(name = "clthreewr_entity_category_name")
    private String clthreewrEntityCategoryName;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_entity_category_type_id")
    private CLThreeWRollupsEntityCategoryType entityCategoryType;

    public int getClthreewrEntityCategoryId() {
        return clthreewrEntityCategoryId;
    }

    public void setClthreewrEntityCategoryId(int clthreewrEntityCategoryId) {
        this.clthreewrEntityCategoryId = clthreewrEntityCategoryId;
    }

    public String getClthreewrEntityCategoryName() {
        return clthreewrEntityCategoryName;
    }

    public void setClthreewrEntityCategoryName(String clthreewrEntityCategoryName) {
        this.clthreewrEntityCategoryName = clthreewrEntityCategoryName;
    }

    public CLThreeWRollupsEntityCategoryType getEntityCategoryType() {
        return entityCategoryType;
    }

    public void setEntityCategoryType(CLThreeWRollupsEntityCategoryType entityCategoryType) {
        this.entityCategoryType = entityCategoryType;
    }
}
