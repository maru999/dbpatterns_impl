package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "cltwo_entity_subtype_two")
@EntityListeners(AuditingEntityListener.class)
@PrimaryKeyJoinColumn(name = "cltwo_entity_subtype_two_id", referencedColumnName = "cltwo_entity_id")
public class CLTwoEntitySubtypeTwo extends CLTwoEntity {

    @Column(name = "cltwo_entity_subtype_two_attr")
    private String clTwoEntitySubtypeTwoAttribute;

    public String getClTwoEntitySubtypeTwoAttribute() {
        return clTwoEntitySubtypeTwoAttribute;
    }

    public void setClTwoEntitySubtypeTwoAttribute(String clTwoEntitySubtypeTwoAttribute) {
        this.clTwoEntitySubtypeTwoAttribute = clTwoEntitySubtypeTwoAttribute;
    }
}
