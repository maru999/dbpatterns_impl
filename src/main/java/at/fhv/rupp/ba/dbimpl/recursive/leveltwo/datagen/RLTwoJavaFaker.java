package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntity;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntityType;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.repos.RLTwoEntityRepo;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.repos.RLTwoEntityTypeRepo;

import java.util.LinkedList;
import java.util.List;

public class RLTwoJavaFaker extends DataGenJavaFaker {
    private final RLTwoEntityRepo entityRepo;
    private final RLTwoEntityTypeRepo entityTypeRepo;

    public RLTwoJavaFaker(RLTwoEntityRepo entityRepo, RLTwoEntityTypeRepo entityTypeRepo) {
        this.entityRepo = entityRepo;
        this.entityTypeRepo = entityTypeRepo;
    }

    @Override
    public void fillDB() {
        RLTwoEntity entity;
        RLTwoEntityType entityType;
        List<RLTwoEntity> createdEntities = new LinkedList<>();
        List<RLTwoEntityType> createdTypes = new LinkedList<>();
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entityType = new RLTwoEntityType();
            entityType.setRlTwoEntityTypeName(trimStringIfTooLong(FAKER.dragonBall().character(), MIDDLE_CHAR_LIMIT));
            if (!createdTypes.isEmpty()) {
                if (getRandomIntByBound(90) % 2 == 0) {
                    entityType.setRlTwoEntityTypeParent(createdTypes.get(getRandomIntByBound(createdTypes.size())));
                }
            }
            createdTypes.add(entityType);
            entityTypeRepo.save(entityType);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new RLTwoEntity();
            if (!createdTypes.isEmpty()) {
                entity.setRlTwoEntityType(createdTypes.get(getRandomIntByBound(createdTypes.size())));
            }
            if (!createdEntities.isEmpty()) {
                if (getRandomIntByBound(50) % 2 == 0) {
                    entity.setRlTwoEntityParent(createdEntities.get(getRandomIntByBound(createdEntities.size())));
                }
            }
            createdEntities.add(entity);
            entityRepo.save(entity);
        }

    }
}
