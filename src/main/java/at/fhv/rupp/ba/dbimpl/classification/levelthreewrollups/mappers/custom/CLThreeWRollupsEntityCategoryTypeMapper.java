package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryType;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryTypeMapper {

    private CLThreeWRollupsEntityCategoryTypeMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryTypeDTO toDTO(CLThreeWRollupsEntityCategoryType entity) {
        CLThreeWRollupsEntityCategoryTypeDTO dto = new CLThreeWRollupsEntityCategoryTypeDTO();
        dto.setId(entity.getClthreewrEntityCategoryTypeId());
        dto.setName(entity.getClthreewrEntityCategoryTypeName());
        if (entity.getClthreeCategoryTypeScheme() != null) {
            dto.setScheme(CLThreeWRollupsEntityCategoryTypeSchemeMapper.toDTO(entity.getClthreeCategoryTypeScheme()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategoryType toEntity(CLThreeWRollupsEntityCategoryTypeDTO dto) {
        CLThreeWRollupsEntityCategoryType entity = new CLThreeWRollupsEntityCategoryType();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryTypeId(dto.getId());
        }
        entity.setClthreewrEntityCategoryTypeName(dto.getName());
        if (dto.getScheme() != null) {
            entity.setClthreeCategoryTypeScheme(CLThreeWRollupsEntityCategoryTypeSchemeMapper.toEntity(dto.getScheme()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryTypeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryType> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryTypeDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategoryType> toEntities(List<CLThreeWRollupsEntityCategoryTypeDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategoryType> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }

}
