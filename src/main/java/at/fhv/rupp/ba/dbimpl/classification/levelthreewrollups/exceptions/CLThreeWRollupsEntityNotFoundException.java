package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.exceptions;

public class CLThreeWRollupsEntityNotFoundException extends Exception {
    public CLThreeWRollupsEntityNotFoundException(String message) {
        super(message);
    }
}
