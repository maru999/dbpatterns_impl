package at.fhv.rupp.ba.dbimpl.status.levelone.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "slone_entity")
@EntityListeners(AuditingEntityListener.class)
public class SLOneEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slone_entity_id")
    private int slOneEntityId;

    @Column(name = "slone_entity_event_one_dt")
    private LocalDateTime slOneEntityEventOneDateTime;

    @Column(name = "slone_entity_event_two_dt")
    private LocalDateTime slOneEntityEventTwoDateTime;

    @Column(name = "slone_entity_event_three_dt")
    private LocalDateTime slOneEntityEventThreeDateTime;

    @Column(name = "slone_entity_event_indicator")
    private String slOneEntityEventIndicator;

    @Column(name = "slone_entity_event_from_date")
    private LocalDate slOneEntityEventFromDate;

    @Column(name = "slone_entity_event_thru_date")
    private LocalDate slOneEntityEventThruDate;

    public int getSlOneEntityId() {
        return slOneEntityId;
    }

    public void setSlOneEntityId(int slOneEntityId) {
        this.slOneEntityId = slOneEntityId;
    }

    public LocalDateTime getSlOneEntityEventOneDateTime() {
        return slOneEntityEventOneDateTime;
    }

    public void setSlOneEntityEventOneDateTime(LocalDateTime slOneEntityEventOneDateTime) {
        this.slOneEntityEventOneDateTime = slOneEntityEventOneDateTime;
    }

    public LocalDateTime getSlOneEntityEventTwoDateTime() {
        return slOneEntityEventTwoDateTime;
    }

    public void setSlOneEntityEventTwoDateTime(LocalDateTime slOneEntityEventTwoDateTime) {
        this.slOneEntityEventTwoDateTime = slOneEntityEventTwoDateTime;
    }

    public LocalDateTime getSlOneEntityEventThreeDateTime() {
        return slOneEntityEventThreeDateTime;
    }

    public void setSlOneEntityEventThreeDateTime(LocalDateTime slOneEntityEventThreeDateTime) {
        this.slOneEntityEventThreeDateTime = slOneEntityEventThreeDateTime;
    }

    public String getSlOneEntityEventIndicator() {
        return slOneEntityEventIndicator;
    }

    public void setSlOneEntityEventIndicator(String slOneEntityEventIndicator) {
        this.slOneEntityEventIndicator = slOneEntityEventIndicator;
    }

    public LocalDate getSlOneEntityEventFromDate() {
        return slOneEntityEventFromDate;
    }

    public void setSlOneEntityEventFromDate(LocalDate slOneEntityEventFromDate) {
        this.slOneEntityEventFromDate = slOneEntityEventFromDate;
    }

    public LocalDate getSlOneEntityEventThruDate() {
        return slOneEntityEventThruDate;
    }

    public void setSlOneEntityEventThruDate(LocalDate slOneEntityEventThruDate) {
        this.slOneEntityEventThruDate = slOneEntityEventThruDate;
    }
}
