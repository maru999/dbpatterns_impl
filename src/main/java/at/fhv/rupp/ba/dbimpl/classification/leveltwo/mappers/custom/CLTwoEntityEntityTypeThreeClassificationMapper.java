package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityEntityTypeThreeClassificationDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityEntityTypeThreeClassification;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntityEntityTypeThreeClassificationMapper {

    private CLTwoEntityEntityTypeThreeClassificationMapper() {
        //hidden constructor
    }

    public static CLTwoEntityEntityTypeThreeClassificationDTO toDTO(CLTwoEntityEntityTypeThreeClassification entity) {
        CLTwoEntityEntityTypeThreeClassificationDTO dto = new CLTwoEntityEntityTypeThreeClassificationDTO();
        dto.setId(entity.getClTwoEntityEntityTypeThreeClassificationId());
        if (entity.getClTwoEntityEntityTypeThreeClassificationEntity() != null) {
            dto.setEntity(CLTwoEntityMapper.toDTO(entity.getClTwoEntityEntityTypeThreeClassificationEntity()));
        }
        if (entity.getClTwoEntityEntityTypeThreeTypeThree() != null) {
            dto.setTypeThree(CLTwoEntityTypeThreeMapper.toDTO(entity.getClTwoEntityEntityTypeThreeTypeThree()));
        }
        return dto;
    }

    public static List<CLTwoEntityEntityTypeThreeClassificationDTO> toDTOs(List<CLTwoEntityEntityTypeThreeClassification> entities) {
        LinkedList<CLTwoEntityEntityTypeThreeClassificationDTO> result = new LinkedList<>();
        for (CLTwoEntityEntityTypeThreeClassification entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static CLTwoEntityEntityTypeThreeClassification toEntity(CLTwoEntityEntityTypeThreeClassificationDTO dto) {
        CLTwoEntityEntityTypeThreeClassification entity = new CLTwoEntityEntityTypeThreeClassification();
        if (dto.getId() != null) {
            entity.setClTwoEntityEntityTypeThreeClassificationId(dto.getId());
        }
        if (dto.getEntity() != null) {
            entity.setClTwoEntityEntityTypeThreeClassificationEntity(CLTwoEntityMapper.toEntity(dto.getEntity()));
        }
        if (dto.getTypeThree() != null) {
            entity.setClTwoEntityEntityTypeThreeTypeThree(CLTwoEntityTypeThreeMapper.toEntity(dto.getTypeThree()));
        }
        return entity;
    }

    public static List<CLTwoEntityEntityTypeThreeClassification> toEntities(List<CLTwoEntityEntityTypeThreeClassificationDTO> dtos) {
        LinkedList<CLTwoEntityEntityTypeThreeClassification> result = new LinkedList<>();
        for (CLTwoEntityEntityTypeThreeClassificationDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
