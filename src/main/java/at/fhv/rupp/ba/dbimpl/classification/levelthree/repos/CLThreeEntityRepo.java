package at.fhv.rupp.ba.dbimpl.classification.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityWithTypesIProjection;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntity;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CLThreeEntityRepo extends JpaRepository<CLThreeEntity, Integer> {

    @Query(value = "SELECT * FROM clthree_entity_types", nativeQuery = true)
    List<CLThreeEntityWithTypesIProjection> getAllEntitiesWTypes();

    @Query(value = "SELECT * FROM clthree_entity_types WHERE clthree_entity_id = :id", nativeQuery = true)
    List<CLThreeEntityWithTypesIProjection> getEntitiesWTypesByEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM clthree_entity_types WHERE clthree_entity_category_name = :name", nativeQuery = true)
    List<CLThreeEntityWithTypesIProjection> getEntitiesWTypesByCatName(@Param("name") String catName);

    @Query(value = "SELECT * FROM clthree_entity_types WHERE clthree_entity_category_classification_from BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<CLThreeEntityWithTypesIProjection> getEntitiesWTypesClassificationFromBetween(@Param("startDate") LocalDate start, @Param("endDate") LocalDate end);



}
