package at.fhv.rupp.ba.dbimpl.status.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityStatusViewIProjection;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface SLThreeEntityRepo extends JpaRepository<SLThreeEntity, Integer> {

    @Query(value = "SELECT * FROM slthree_status_view", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatus();

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_entity_id = :id", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusByEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_status_type_id = :id", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusByStatusId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_status_type_name = :name", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusByStatusTypeName(@Param("name") String statusTypeName);

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_entity_status_date_time BETWEEN :startTime AND :endTime", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusByStatusDateTimeBetween(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_entity_status_from_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusByFromDateBetween(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_entity_status_status_from_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusByStatusFromDateBetween(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_entity_status_thru_date IS NOT NULL", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusWhereEntityNotThru();

    @Query(value = "SELECT * FROM slthree_status_view WHERE slthree_entity_status_status_thru_date IS NOT NULL", nativeQuery = true)
    List<SLThreeEntityStatusViewIProjection> getAllEntitesWithStatusWhereEntityStatusNotThru();
}
