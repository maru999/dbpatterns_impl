package at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos;

public interface CLTwoEntityNamedTypesIProjection {

    Integer getcltwo_entity_id();

    String getcltwo_entity_name();

    String getcltwo_entity_type_one_name();

    String getcltwo_entity_type_two_name();

    String getcltwo_entity_type_three_name();


}
