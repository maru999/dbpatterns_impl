package at.fhv.rupp.ba.dbimpl.status.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "sltwo_status_type")
@EntityListeners(AuditingEntityListener.class)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class SLTwoStatusType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sltwo_status_type_id")
    private int slTwoStatusTypeId;

    @Column(name = "sltwo_status_type_name")
    private String slTwoStatusTypeName;

    public int getSlTwoStatusTypeId() {
        return slTwoStatusTypeId;
    }

    public void setSlTwoStatusTypeId(int slTwoStatusTypeId) {
        this.slTwoStatusTypeId = slTwoStatusTypeId;
    }

    public String getSlTwoStatusTypeName() {
        return slTwoStatusTypeName;
    }

    public void setSlTwoStatusTypeName(String slTwoStatusTypeName) {
        this.slTwoStatusTypeName = slTwoStatusTypeName;
    }
}
