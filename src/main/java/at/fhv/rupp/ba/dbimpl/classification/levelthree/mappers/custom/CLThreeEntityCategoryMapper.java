package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategory;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityCategoryDTO;

import java.util.LinkedList;
import java.util.List;

public class CLThreeEntityCategoryMapper {

    private CLThreeEntityCategoryMapper() {
        //hidden constructor
    }

    public static CLThreeEntityCategoryDTO toDTO(CLThreeEntityCategory entity) {
        CLThreeEntityCategoryDTO dto = new CLThreeEntityCategoryDTO();
        dto.setId(entity.getClthreeEntityCategoryId());
        dto.setName(entity.getClthreeEntityCategoryName());
        dto.setFrom(entity.getClthreeEntityCategoryFromDate());
        if (entity.getClthreeEntityCategoryThruDate() != null) {
            dto.setThru(entity.getClthreeEntityCategoryThruDate());
        }
        if (entity.getClThreeEntityCategoryParent() != null) {
            dto.setParent(toDTO(entity.getClThreeEntityCategoryParent()));
        }
        if (entity.getClThreeEntityCategoryType() != null) {
            dto.setCategoryType(CLThreeEntityCategoryTypeMapper.toDTO(entity.getClThreeEntityCategoryType()));
        }
        return dto;
    }

    public static CLThreeEntityCategory toEntity(CLThreeEntityCategoryDTO dto) {
        CLThreeEntityCategory entity = new CLThreeEntityCategory();
        if (dto.getId() != null) {
            entity.setClthreeEntityCategoryId(dto.getId());
        }
        entity.setClthreeEntityCategoryName(dto.getName());
        entity.setClthreeEntityCategoryFromDate(dto.getFrom());
        if (dto.getThru() != null) {
            entity.setClthreeEntityCategoryThruDate(dto.getThru());
        }
        if (dto.getParent() != null) {
            entity.setClThreeEntityCategoryParent(toEntity(dto.getParent()));
        }
        if (dto.getCategoryType() != null) {
            entity.setClThreeEntityCategoryType(CLThreeEntityCategoryTypeMapper.toEntity(dto.getCategoryType()));
        }
        return entity;
    }

    public static List<CLThreeEntityCategoryDTO> toDTOs(List<CLThreeEntityCategory> entities) {
        LinkedList<CLThreeEntityCategoryDTO> result = new LinkedList<>();
        for (CLThreeEntityCategory entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeEntityCategory> toEntities(List<CLThreeEntityCategoryDTO> dtos) {
        LinkedList<CLThreeEntityCategory> result = new LinkedList<>();
        for (CLThreeEntityCategoryDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
