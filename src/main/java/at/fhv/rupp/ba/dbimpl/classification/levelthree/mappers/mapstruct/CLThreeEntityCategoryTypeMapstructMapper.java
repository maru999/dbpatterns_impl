package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityCategoryTypeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLThreeEntityCategoryTypeMapstructMapper {

    @Mapping(source = "clthreeEntityCategoryTypeId", target = "id")
    @Mapping(source = "clthreeEntityCategoryTypeName", target = "name")
    @Mapping(source = "clThreeEntityCategoryTypeParent", target = "parent")
    CLThreeEntityCategoryTypeDTO toDTO(CLThreeEntityCategoryType entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeEntityCategoryType toEntity(CLThreeEntityCategoryTypeDTO dto);

    @Mapping(source = "clthreeEntityCategoryTypeId", target = "id")
    @Mapping(source = "clthreeEntityCategoryTypeName", target = "name")
    @Mapping(source = "clThreeEntityCategoryTypeParent", target = "parent")
    void updateEntityFromDTO(CLThreeEntityCategoryType entity, @MappingTarget CLThreeEntityCategoryTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeEntityCategoryTypeDTO dto, @MappingTarget CLThreeEntityCategoryType entity);

    List<CLThreeEntityCategoryTypeDTO> toDTOs(List<CLThreeEntityCategoryType> entities);

    List<CLThreeEntityCategoryType> toEntities(List<CLThreeEntityCategoryTypeDTO> dtos);
}
