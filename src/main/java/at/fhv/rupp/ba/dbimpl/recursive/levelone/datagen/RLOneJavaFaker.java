package at.fhv.rupp.ba.dbimpl.recursive.levelone.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityOne;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityThree;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityTwo;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.repos.RLOneEntityOneRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.repos.RLOneEntityThreeRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.repos.RLOneEntityTwoRepo;

import java.util.LinkedList;
import java.util.List;

public class RLOneJavaFaker extends DataGenJavaFaker {
    private final RLOneEntityOneRepo entityOneRepo;
    private final RLOneEntityTwoRepo entityTwoRepo;
    private final RLOneEntityThreeRepo entityThreeRepo;

    public RLOneJavaFaker(RLOneEntityOneRepo entityOneRepo, RLOneEntityTwoRepo entityTwoRepo, RLOneEntityThreeRepo entityThreeRepo) {
        this.entityOneRepo = entityOneRepo;
        this.entityTwoRepo = entityTwoRepo;
        this.entityThreeRepo = entityThreeRepo;
    }

    @Override
    public void fillDB() {
        RLOneEntityOne entityOne;
        RLOneEntityTwo entityTwo;
        RLOneEntityThree entityThree;
        List<RLOneEntityOne> createdEntityOnes = new LinkedList<>();
        List<RLOneEntityTwo> createdEntityTwos = new LinkedList<>();
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entityOne = new RLOneEntityOne();
            entityOne.setRloneEntityOneName(trimStringIfTooLong(FAKER.hitchhikersGuideToTheGalaxy().character(), MIDDLE_CHAR_LIMIT));
            createdEntityOnes.add(entityOne);
            entityOneRepo.save(entityOne);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entityTwo = new RLOneEntityTwo();
            entityTwo.setRloneEntityTwoName(trimStringIfTooLong(FAKER.hitchhikersGuideToTheGalaxy().planet(), MIDDLE_CHAR_LIMIT));
            if (!createdEntityOnes.isEmpty()) {
                entityTwo.setRloneEntityTwoParent(createdEntityOnes.get(getRandomIntByBound(createdEntityOnes.size())));
                createdEntityTwos.add(entityTwo);
                entityTwoRepo.save(entityTwo);
            }
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entityThree = new RLOneEntityThree();
            entityThree.setRloneEntityThreeName(trimStringIfTooLong(FAKER.hitchhikersGuideToTheGalaxy().starship(), MIDDLE_CHAR_LIMIT));
            if (!createdEntityTwos.isEmpty()) {
                entityThree.setRloneEntityThreeParent(createdEntityTwos.get(getRandomIntByBound(createdEntityTwos.size())));
                entityThreeRepo.save(entityThree);
            }
        }
    }
}
