package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.exceptions;

public class RLTwoExpEntityNotFoundException extends Exception {
    public RLTwoExpEntityNotFoundException(String message) {
        super(message);
    }
}
