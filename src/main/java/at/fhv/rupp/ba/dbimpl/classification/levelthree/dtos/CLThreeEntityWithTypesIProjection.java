package at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos;

import java.time.LocalDate;

public interface CLThreeEntityWithTypesIProjection {
    Integer getclthree_entity_id();

    String getclthree_entity_name();

    Integer getclthree_entity_category_type_id();

    String getclthree_entity_category_type_name();

    Integer getclthree_parent_entity_category_type();

    Integer getclthree_entity_category_id();

    String getclthree_entity_category_name();

    LocalDate getclthree_entity_category_from();

    LocalDate getclthree_entity_category_thru();

    Integer getclthree_entity_category_parent_category();

    Integer getclthree_entity_category_category_type();

    Integer getclthree_entity_category_classification_id();

    LocalDate getclthree_entity_category_classification_from();

    LocalDate getclthree_entity_category_classification_thru();

    Integer getclthree_entity_category_classification_entity();

    Integer getclthree_entity_category_classification_entity_category();
}
