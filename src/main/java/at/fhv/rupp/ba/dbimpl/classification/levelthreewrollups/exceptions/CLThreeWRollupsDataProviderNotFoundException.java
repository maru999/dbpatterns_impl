package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.exceptions;

public class CLThreeWRollupsDataProviderNotFoundException extends Exception{
    public CLThreeWRollupsDataProviderNotFoundException(String message) {
        super(message);
    }
}
