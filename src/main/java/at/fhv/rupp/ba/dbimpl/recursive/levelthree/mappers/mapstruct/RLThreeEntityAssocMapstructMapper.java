package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityAssocDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssoc;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssocType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {RLThreeEntityMapstructMapper.class, RLThreeEntityAssocTypeMapstructMapper.class})
public interface RLThreeEntityAssocMapstructMapper {
    @Mapping(source = "rlThreeEntityAssocId", target = "id")
    @Mapping(source = "rlThreeEntityAssocFromDate", target = "fromDate")
    @Mapping(source = "rlThreeEntityAssocThruDate", target = "thruDate")
    @Mapping(source = "rlThreeEntityAssocType", target = "type")
    @Mapping(source = "tlThreeAssocFromEntity", target = "fromEntity")
    @Mapping(source = "tlThreeAssocToEntity", target = "toEntity")
    RLThreeEntityAssocDTO toDTO(RLThreeEntityAssoc entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLThreeEntityAssoc toEntity(RLThreeEntityAssocDTO dto);

    @Mapping(source = "rlThreeEntityAssocId", target = "id")
    @Mapping(source = "rlThreeEntityAssocFromDate", target = "fromDate")
    @Mapping(source = "rlThreeEntityAssocThruDate", target = "thruDate")
    @Mapping(source = "rlThreeEntityAssocType", target = "type")
    @Mapping(source = "tlThreeAssocFromEntity", target = "fromEntity")
    @Mapping(source = "tlThreeAssocToEntity", target = "toEntity")
    void updateEntityFromDTO(RLThreeEntityAssoc entity, @MappingTarget RLThreeEntityAssocDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLThreeEntityAssocDTO dto, @MappingTarget RLThreeEntityAssoc entity);

    List<RLThreeEntityAssocDTO> toDTOs(List<RLThreeEntityAssoc> entities);

    List<RLThreeEntityAssoc> toEntities(List<RLThreeEntityAssocDTO> dtos);
}
