package at.fhv.rupp.ba.dbimpl;

import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;

public class ScriptMain {
    public static void main(String[] args) {
        ScriptExecutor.runAllScripts();
    }
}
