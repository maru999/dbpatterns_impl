package at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos;

import java.time.LocalDate;

public class CLThreeEntityCategoryClassificationDTO {

    private Integer id;
    private LocalDate from;
    private LocalDate thru;
    private CLThreeEntityDTO entity;
    private CLThreeEntityCategoryDTO category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getThru() {
        return thru;
    }

    public void setThru(LocalDate thru) {
        this.thru = thru;
    }

    public CLThreeEntityDTO getEntity() {
        return entity;
    }

    public void setEntity(CLThreeEntityDTO entity) {
        this.entity = entity;
    }

    public CLThreeEntityCategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CLThreeEntityCategoryDTO category) {
        this.category = category;
    }
}
