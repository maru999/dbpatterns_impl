package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos.RLThreeWRulesEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntity;

import java.util.LinkedList;
import java.util.List;

public class RLThreeWRulesEntityMapper {
    private RLThreeWRulesEntityMapper() {
        //hidden constructor
    }

    public static RLThreeWRulesEntityDTO toDTO(RLThreeWRulesEntity entity) {
        RLThreeWRulesEntityDTO dto = new RLThreeWRulesEntityDTO();
        dto.setId(entity.getRlThreeWRulesEntityId());
        if (entity.getRlThreeWRulesEntityType() != null) {
            dto.setType(RLThreeWRulesEntityTypeMapper.toDTO(entity.getRlThreeWRulesEntityType()));
        }
        return dto;
    }

    public static RLThreeWRulesEntity toEntity(RLThreeWRulesEntityDTO dto) {
        RLThreeWRulesEntity entity = new RLThreeWRulesEntity();
        if (dto.getId() != null) {
            entity.setRlThreeWRulesEntityId(dto.getId());
        }
        if (dto.getType() != null) {
            entity.setRlThreeWRulesEntityType(RLThreeWRulesEntityTypeMapper.toEntity(dto.getType()));
        }
        return entity;
    }

    public static List<RLThreeWRulesEntityDTO> toDTOs(List<RLThreeWRulesEntity> entities) {
        LinkedList<RLThreeWRulesEntityDTO> result = new LinkedList<>();
        for (RLThreeWRulesEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeWRulesEntity> toEntities(List<RLThreeWRulesEntityDTO> dtos) {
        LinkedList<RLThreeWRulesEntity> result = new LinkedList<>();
        for (RLThreeWRulesEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
