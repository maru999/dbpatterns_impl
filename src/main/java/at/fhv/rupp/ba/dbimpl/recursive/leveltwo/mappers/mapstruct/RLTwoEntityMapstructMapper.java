package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = RLTwoEntityTypeMapstructMapper.class)
public interface RLTwoEntityMapstructMapper {
    @Mapping(source = "rlTwoEntityId", target = "id")
    @Mapping(source = "rlTwoEntityParent", target = "parent")
    @Mapping(source = "rlTwoEntityType", target = "type")
    RLTwoEntityDTO toDTO(RLTwoEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLTwoEntity toEntity(RLTwoEntityDTO dto);

    @Mapping(source = "rlTwoEntityId", target = "id")
    @Mapping(source = "rlTwoEntityParent", target = "parent")
    @Mapping(source = "rlTwoEntityType", target = "type")
    void updateEntityFromDTO(RLTwoEntity entity, @MappingTarget RLTwoEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLTwoEntityDTO dto, @MappingTarget RLTwoEntity entity);

    List<RLTwoEntityDTO> toDTOs(List<RLTwoEntity> entities);

    List<RLTwoEntity> toEntities(List<RLTwoEntityDTO> dtos);
}
