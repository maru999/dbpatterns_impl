package at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLThreeEntityTypeRepo extends JpaRepository<RLThreeEntityType, Integer> {
    @Query(value = "SELECT * FROM rlthree_entity_type WHERE rlthree_entity_type_parent = :id", nativeQuery = true)
    List<RLThreeEntityType> getTypesWhereTypeIsParent(@Param("id") Integer typeId);
}
