package at.fhv.rupp.ba.dbimpl.recursive.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLThreeEntityAssocRepo extends JpaRepository<RLThreeEntityAssoc, Integer> {
    @Query(value = "SELECT * FROM rlthree_entity_assoc WHERE rlthree_entity_assoc_from_ent = :id", nativeQuery = true)
    List<RLThreeEntityAssoc> getAssociationsByFromEntityId(@Param("id") Integer fromId);

    @Query(value = "SELECT * FROM rlthree_entity_assoc WHERE rlthree_entity_assoc_to_ent = :id", nativeQuery = true)
    List<RLThreeEntityAssoc> getAssociationsByToEntityId(@Param("id") Integer ToId);

}
