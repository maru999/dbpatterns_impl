package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name="cltwo_entity_type_three_class")
@EntityListeners(AuditingEntityListener.class)
public class CLTwoEntityEntityTypeThreeClassification {
    //In Entity Classification
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cltwo_entity_type_three_class_id")
    private int clTwoEntityEntityTypeThreeClassificationId;

    @ManyToOne
    @JoinColumn(name = "cltwo_entity_type_three_class_entid")
    private CLTwoEntity clTwoEntityEntityTypeThreeClassificationEntity;

    @ManyToOne
    @JoinColumn(name = "cltwo_entity_type_three_class_threeid")
    private CLTwoEntityTypeThree clTwoEntityEntityTypeThreeTypeThree;

    public int getClTwoEntityEntityTypeThreeClassificationId() {
        return clTwoEntityEntityTypeThreeClassificationId;
    }

    public void setClTwoEntityEntityTypeThreeClassificationId(int clTwoEntityEntityTypeThreeClassificationId) {
        this.clTwoEntityEntityTypeThreeClassificationId = clTwoEntityEntityTypeThreeClassificationId;
    }

    public CLTwoEntity getClTwoEntityEntityTypeThreeClassificationEntity() {
        return clTwoEntityEntityTypeThreeClassificationEntity;
    }

    public void setClTwoEntityEntityTypeThreeClassificationEntity(CLTwoEntity clTwoEntityEntityTypeThreeClassificationEntity) {
        this.clTwoEntityEntityTypeThreeClassificationEntity = clTwoEntityEntityTypeThreeClassificationEntity;
    }

    public CLTwoEntityTypeThree getClTwoEntityEntityTypeThreeTypeThree() {
        return clTwoEntityEntityTypeThreeTypeThree;
    }

    public void setClTwoEntityEntityTypeThreeTypeThree(CLTwoEntityTypeThree clTwoEntityEntityTypeThreeTypeThree) {
        this.clTwoEntityEntityTypeThreeTypeThree = clTwoEntityEntityTypeThreeTypeThree;
    }
}
