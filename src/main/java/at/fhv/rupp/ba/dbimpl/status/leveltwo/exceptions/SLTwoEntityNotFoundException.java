package at.fhv.rupp.ba.dbimpl.status.leveltwo.exceptions;

public class SLTwoEntityNotFoundException extends Exception {
    public SLTwoEntityNotFoundException(String message) {
        super(message);
    }
}
