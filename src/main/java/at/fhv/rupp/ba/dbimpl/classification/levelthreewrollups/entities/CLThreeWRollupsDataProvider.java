package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_data_provider")
@EntityListeners(AuditingEntityListener.class)
@PrimaryKeyJoinColumn(name = "data_provider_id", referencedColumnName = "clthreewr_party_role_id")
public class CLThreeWRollupsDataProvider extends CLThreeWRollupsPartyRole {

    @Column(name = "dp_name")
    private String clthreewrDataProviderName;

    public String getClthreewrDataProviderName() {
        return clthreewrDataProviderName;
    }

    public void setClthreewrDataProviderName(String clthreewrDataProviderName) {
        this.clthreewrDataProviderName = clthreewrDataProviderName;
    }
}
