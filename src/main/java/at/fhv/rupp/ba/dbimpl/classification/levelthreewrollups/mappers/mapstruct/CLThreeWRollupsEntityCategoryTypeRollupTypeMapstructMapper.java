package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeRollupTypeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryTypeRollupType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLThreeWRollupsEntityCategoryTypeRollupTypeMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryTypeRollupTypeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupTypeName", target = "name")
    @Mapping(source = "parentTypeRollupType", target = "parent")
    CLThreeWRollupsEntityCategoryTypeRollupTypeDTO toDTO(CLThreeWRollupsEntityCategoryTypeRollupType entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategoryTypeRollupType toEntity(CLThreeWRollupsEntityCategoryTypeRollupTypeDTO dto);

    @Mapping(source = "clthreewrEntityCategoryTypeRollupTypeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeRollupTypeName", target = "name")
    @Mapping(source = "parentTypeRollupType", target = "parent")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategoryTypeRollupType entity, @MappingTarget CLThreeWRollupsEntityCategoryTypeRollupTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryTypeRollupTypeDTO dto, @MappingTarget CLThreeWRollupsEntityCategoryTypeRollupType entity);

    List<CLThreeWRollupsEntityCategoryTypeRollupTypeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryTypeRollupType> entities);

    List<CLThreeWRollupsEntityCategoryTypeRollupType> toEntities(List<CLThreeWRollupsEntityCategoryTypeRollupTypeDTO> dtos);
}
