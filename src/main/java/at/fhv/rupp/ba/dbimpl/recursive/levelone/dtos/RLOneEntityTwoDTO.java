package at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos;

public class RLOneEntityTwoDTO {
    private Integer id;
    private String name;
    private RLOneEntityOneDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLOneEntityOneDTO getParent() {
        return parent;
    }

    public void setParent(RLOneEntityOneDTO parent) {
        this.parent = parent;
    }
}
