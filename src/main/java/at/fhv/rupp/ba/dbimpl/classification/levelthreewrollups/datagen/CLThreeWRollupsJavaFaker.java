package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.datagen;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.*;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos.*;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsJavaFaker extends DataGenJavaFaker {
    private final CLThreeWRollupsEntityRepo entityRepo;
    private final CLThreeWRollupsDataProviderRepo dataProviderRepo;
    private final CLThreeWRollupsEntityCategoryRepo categoryRepo;
    private final CLThreeWRollupsEntityCategoryClassificationRepo categoryClassificationRepo;
    private final CLThreeWRollupsEntityCategoryRollupRepo categoryRollupRepo;
    private final CLThreeWRollupsEntityCategoryRollupTypeRepo rollupTypeRepo;
    private final CLThreeWRollupsEntityCategoryTypeRepo categoryTypeRepo;
    private final CLThreeWRollupsEntityCategoryTypeRollupRepo typeRollupRepo;
    private final CLThreeWRollupsEntityCategoryTypeRollupTypeRepo typeRollupTypeRepo;
    private final CLThreeWRollupsEntityCategoryTypeSchemeRepo typeSchemeRepo;

    public CLThreeWRollupsJavaFaker(CLThreeWRollupsEntityRepo entityRepo, CLThreeWRollupsDataProviderRepo dataProviderRepo, CLThreeWRollupsEntityCategoryRepo categoryRepo, CLThreeWRollupsEntityCategoryClassificationRepo categoryClassificationRepo, CLThreeWRollupsEntityCategoryRollupRepo categoryRollupRepo, CLThreeWRollupsEntityCategoryRollupTypeRepo rollupTypeRepo, CLThreeWRollupsEntityCategoryTypeRepo categoryTypeRepo, CLThreeWRollupsEntityCategoryTypeRollupRepo typeRollupRepo, CLThreeWRollupsEntityCategoryTypeRollupTypeRepo typeRollupTypeRepo, CLThreeWRollupsEntityCategoryTypeSchemeRepo typeSchemeRepo) {
        this.entityRepo = entityRepo;
        this.dataProviderRepo = dataProviderRepo;
        this.categoryRepo = categoryRepo;
        this.categoryClassificationRepo = categoryClassificationRepo;
        this.categoryRollupRepo = categoryRollupRepo;
        this.rollupTypeRepo = rollupTypeRepo;
        this.categoryTypeRepo = categoryTypeRepo;
        this.typeRollupRepo = typeRollupRepo;
        this.typeRollupTypeRepo = typeRollupTypeRepo;
        this.typeSchemeRepo = typeSchemeRepo;
    }

    @Override
    public void fillDB() {
        CLThreeWRollupsDataProvider dataProvider;
        CLThreeWRollupsEntity entity;
        CLThreeWRollupsEntityCategory entityCategory;
        CLThreeWRollupsEntityCategoryClassification entityCategoryClassification;
        CLThreeWRollupsEntityCategoryRollup categoryRollup;
        CLThreeWRollupsEntityCategoryRollupType categoryRollupType;
        CLThreeWRollupsEntityCategoryType categoryType;
        CLThreeWRollupsEntityCategoryTypeRollup categoryTypeRollup;
        CLThreeWRollupsEntityCategoryTypeRollupType categoryTypeRollupType;
        CLThreeWRollupsEntityCategoryTypeScheme typeScheme;
        List<CLThreeWRollupsEntityCategoryRollupType> rollupTypes = new LinkedList<>();
        List<CLThreeWRollupsEntityCategoryTypeRollupType> typeRollupTypes = new LinkedList<>();
        List<CLThreeWRollupsDataProvider> dataProviders = new LinkedList<>();
        List<CLThreeWRollupsEntityCategoryTypeScheme> categoryTypeSchemes = new LinkedList<>();
        List<CLThreeWRollupsEntityCategoryType> categoryTypes = new LinkedList<>();
        List<CLThreeWRollupsEntity> entities = new LinkedList<>();
        List<CLThreeWRollupsEntityCategory> entityCategories = new LinkedList<>();
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new CLThreeWRollupsEntity();
            entity.setClthreewrEntityName(trimStringIfTooLong(FAKER.dog().name(), MIDDLE_CHAR_LIMIT));
            entities.add(entity);
            entityRepo.save(entity);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            dataProvider = new CLThreeWRollupsDataProvider();
            dataProvider.setRoleName(trimStringIfTooLong(FAKER.cat().breed(), MIDDLE_CHAR_LIMIT));
            dataProvider.setClthreewrDataProviderName(trimStringIfTooLong(FAKER.cat().name(), MIDDLE_CHAR_LIMIT));
            dataProviders.add(dataProvider);
            dataProviderRepo.save(dataProvider);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            categoryRollupType = new CLThreeWRollupsEntityCategoryRollupType();
            categoryRollupType.setClthreewrEntityCategoryRollupTypeName(trimStringIfTooLong(FAKER.cat().registry(), MIDDLE_CHAR_LIMIT));
            if (!rollupTypes.isEmpty()) {
                if (getRandomIntByBound(10) % 2 == 0) {
                    categoryRollupType.setParentRollupType(rollupTypes.get(getRandomIntByBound(rollupTypes.size())));
                }
            }
            rollupTypes.add(categoryRollupType);
            rollupTypeRepo.save(categoryRollupType);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            categoryTypeRollupType = new CLThreeWRollupsEntityCategoryTypeRollupType();
            categoryTypeRollupType.setClthreewrEntityCategoryTypeRollupTypeName(trimStringIfTooLong(FAKER.dog().breed(), MIDDLE_CHAR_LIMIT));
            if (!typeRollupTypes.isEmpty()) {
                if (getRandomIntByBound(20) % 2 == 0) {
                    categoryTypeRollupType.setParentTypeRollupType(typeRollupTypes.get(getRandomIntByBound(typeRollupTypes.size())));
                }
            }
            typeRollupTypes.add(categoryTypeRollupType);
            typeRollupTypeRepo.save(categoryTypeRollupType);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            typeScheme = new CLThreeWRollupsEntityCategoryTypeScheme();
            typeScheme.setClthreewrEntityCategoryTypeSchemeName(trimStringIfTooLong(FAKER.dog().sound(), MIDDLE_CHAR_LIMIT));
            if (!dataProviders.isEmpty()) {
                typeScheme.setSchemeDataProvider(dataProviders.get(getRandomIntByBound(dataProviders.size())));
            }
            categoryTypeSchemes.add(typeScheme);
            typeSchemeRepo.save(typeScheme);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            categoryType = new CLThreeWRollupsEntityCategoryType();
            categoryType.setClthreewrEntityCategoryTypeName(trimStringIfTooLong(FAKER.dog().coatLength(), MIDDLE_CHAR_LIMIT));
            if (!categoryTypeSchemes.isEmpty()) {
                categoryType.setClthreeCategoryTypeScheme(categoryTypeSchemes.get(getRandomIntByBound(categoryTypeSchemes.size())));
            }
            categoryTypes.add(categoryType);
            categoryTypeRepo.save(categoryType);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityCategory = new CLThreeWRollupsEntityCategory();
            entityCategory.setClthreewrEntityCategoryName(trimStringIfTooLong(FAKER.dog().memePhrase(), MIDDLE_CHAR_LIMIT));
            if (!categoryTypes.isEmpty()) {
                entityCategory.setEntityCategoryType(categoryTypes.get(getRandomIntByBound(categoryTypes.size())));
            }
            entityCategories.add(entityCategory);
            categoryRepo.save(entityCategory);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityCategoryClassification = new CLThreeWRollupsEntityCategoryClassification();
            LocalDate fromDate = getDateBetween(START_DATE, END_DATE);
            entityCategoryClassification.setClthreewrEntityCategoryClassificationFromDate(fromDate);
            if (getRandomIntByBound(30) % 2 == 0) {
                entityCategoryClassification.setClthreewrEntityCategoryClassificationThruDate(getDateBetween(fromDate, END_DATE));
            }
            if (!entities.isEmpty()) {
                entityCategoryClassification.setClthreewrEntityCategoryClassifciationEntity(entities.get(getRandomIntByBound(entities.size())));
            }
            if (!entityCategories.isEmpty()) {
                entityCategoryClassification.setClthreewrEntityCategoryClassificationEntityCategory(entityCategories.get(getRandomIntByBound(entityCategories.size())));
            }
            categoryClassificationRepo.save(entityCategoryClassification);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            categoryRollup = new CLThreeWRollupsEntityCategoryRollup();
            LocalDate fromDate = getDateBetween(START_DATE, END_DATE);
            categoryRollup.setClthreewrEntityCategoryRollupFromDate(fromDate);
            if (getRandomIntByBound(10) % 2 == 0) {
                categoryRollup.setClthreewrEntityCategoryRollupThruDate(getDateBetween(fromDate, END_DATE));
            }
            if (entityCategories.size() >= 2) {
                CLThreeWRollupsEntityCategory parentCat = entityCategories.get(getRandomIntByBound(entityCategories.size()));
                categoryRollup.setClthreewrEntityCategoryRollupParentCategory(parentCat);
                CLThreeWRollupsEntityCategory childCat = null;
                while (childCat == null) {
                    CLThreeWRollupsEntityCategory temp = entityCategories.get(getRandomIntByBound(entityCategories.size()));
                    if (temp != parentCat) childCat = temp;
                }
                categoryRollup.setClthreewrEntityCategoryRollupChildCategory(childCat);
            }
            if (!rollupTypes.isEmpty()) {
                categoryRollup.setClthreeEntityCategoryRollupRollupType(rollupTypes.get(getRandomIntByBound(rollupTypes.size())));
            }
            categoryRollupRepo.save(categoryRollup);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            categoryTypeRollup = new CLThreeWRollupsEntityCategoryTypeRollup();
            LocalDate fromDate = getDateBetween(START_DATE, END_DATE);
            categoryTypeRollup.setClthreewrEntityCategoryTypeRollupFromDate(fromDate);
            if (getRandomIntByBound(10) % 2 == 0) {
                categoryTypeRollup.setClthreewrEntityCategoryTypeRollupThruDate(getDateBetween(fromDate, END_DATE));
            }
            if (categoryTypes.size() >= 2) {
                CLThreeWRollupsEntityCategoryType parentType = categoryTypes.get(getRandomIntByBound(categoryTypes.size()));
                categoryTypeRollup.setClthreewrEntityCategoryTypeRollupParentCategory(parentType);
                CLThreeWRollupsEntityCategoryType childType = null;
                while (childType == null) {
                    CLThreeWRollupsEntityCategoryType temp = categoryTypes.get(getRandomIntByBound(categoryTypes.size()));
                    if (temp != parentType) childType = temp;
                }
                categoryTypeRollup.setClthreewrEntityCategoryTypeRollupChildCategory(childType);
                if (!typeRollupTypes.isEmpty()) {
                    categoryTypeRollup.setClthreewrEntityCategoryTypeRollupType(typeRollupTypes.get(getRandomIntByBound(typeRollupTypes.size())));
                }
                typeRollupRepo.save(categoryTypeRollup);
            }
        }
    }
}
