package at.fhv.rupp.ba.dbimpl.recursive.levelone.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityTwo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RLOneEntityTwoRepo extends JpaRepository<RLOneEntityTwo, Integer> {
}
