package at.fhv.rupp.ba.dbimpl.recursive.levelthree.exceptions;

public class RLThreeEntityTypeNotFoundException extends Exception {
    public RLThreeEntityTypeNotFoundException(String message) {
        super(message);
    }
}
