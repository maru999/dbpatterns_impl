package at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeStatusType;

import java.util.LinkedList;
import java.util.List;

public class SLThreeStatusTypeMapper {

    private SLThreeStatusTypeMapper() {
        //hidden constructor
    }

    public static SLThreeStatusTypeDTO toDTO(SLThreeStatusType entity) {
        SLThreeStatusTypeDTO dto = new SLThreeStatusTypeDTO();
        dto.setId(entity.getSlThreeStatusTypeId());
        dto.setName(entity.getSlThreeStatusTypeName());
        return dto;
    }

    public static SLThreeStatusType toEntity(SLThreeStatusTypeDTO dto) {
        SLThreeStatusType entity = new SLThreeStatusType();
        if (dto.getId() != null) {
            entity.setSlThreeStatusTypeId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setSlThreeStatusTypeName(dto.getName());
        }
        return entity;
    }

    public static List<SLThreeStatusTypeDTO> toDTOs(List<SLThreeStatusType> entities) {
        LinkedList<SLThreeStatusTypeDTO> result = new LinkedList<>();
        for (SLThreeStatusType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLThreeStatusType> toEntities(List<SLThreeStatusTypeDTO> dtos) {
        LinkedList<SLThreeStatusType> result = new LinkedList<>();
        for (SLThreeStatusTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
