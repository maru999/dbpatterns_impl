package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlthreewr_entity_assoc_rule")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeWRulesEntityAssocRule {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthreewr_entity_assoc_rule_id")
    private int rlThreeWRulesEntityAssocRuleId;

    @Column(name = "rlthreewr_entity_assoc_rule_name")
    private String rlThreeWRulesEntityAssocRuleName;

    public int getRlThreeWRulesEntityAssocRuleId() {
        return rlThreeWRulesEntityAssocRuleId;
    }

    public void setRlThreeWRulesEntityAssocRuleId(int rlThreeWRulesEntityAssocRuleId) {
        this.rlThreeWRulesEntityAssocRuleId = rlThreeWRulesEntityAssocRuleId;
    }

    public String getRlThreeWRulesEntityAssocRuleName() {
        return rlThreeWRulesEntityAssocRuleName;
    }

    public void setRlThreeWRulesEntityAssocRuleName(String rlThreeWRulesEntityAssocRuleName) {
        this.rlThreeWRulesEntityAssocRuleName = rlThreeWRulesEntityAssocRuleName;
    }
}
