package at.fhv.rupp.ba.dbimpl.classification.leveltwo.exceptions;

public class CLTwoEntityTypeThreeNotFoundException extends Exception {
    public CLTwoEntityTypeThreeNotFoundException(String message) {
        super(message);
    }
}
