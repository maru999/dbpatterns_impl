package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos;

import java.time.LocalDate;

public interface RLThreeWRulesEntitiesWAssociationsIProjection {
    Integer getrlthreewr_entity_assoc_rule_id();

    String getrlthreewr_entity_assoc_rule_name();

    Integer getrlthreewr_entity_type_id();

    String getrlthreewr_entity_type_name();

    Integer getrlthreewr_entity_type_parent();

    Integer getrlthreewr_entity_assoc_type_id();

    String getrlthreewr_entity_assoc_type_name();

    Integer getrlthreewr_entity_assoc_type_parent();

    Integer getrlthreewr_entity_id();

    Integer getrlthreewr_entity_type_fk();

    Integer getrlthreewr_entity_assoc_id();

    LocalDate getrlthreewr_entity_assoc_from_date();

    LocalDate getrlthreewr_entity_assoc_thru_date();

    Integer getrlthreewr_entity_assoc_type_fk();

    Integer getrlthreewr_entity_assoc_from_ent();

    Integer getrlthreewr_entity_assoc_to_ent();

    Integer getrlthreewr_entity_assoc_rule_fk();
}
