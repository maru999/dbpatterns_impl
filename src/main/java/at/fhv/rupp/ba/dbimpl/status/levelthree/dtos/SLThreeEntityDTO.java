package at.fhv.rupp.ba.dbimpl.status.levelthree.dtos;

public class SLThreeEntityDTO {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
