package at.fhv.rupp.ba.dbimpl.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.DriverManager;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.jdbc.ScriptRunner;

public class ScriptExecutor {

    public static void runScriptByName(String scriptName) {
        String dbUrl = "jdbc:postgresql://localhost:5432/referenceimpl";
        ClassLoader classLoader = ScriptExecutor.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(scriptName)).getFile());
        String script = file.getPath();
        try {
            Class.forName("org.postgresql.Driver");
            //Connection con = DriverManager.getConnection(dbUrl, props);
            new ScriptRunner(DriverManager.getConnection(
                    dbUrl, "postgres", "postgres"))
                    .runScript(new BufferedReader(new FileReader(script)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void runAllScripts() {
        List<String> scripts = new LinkedList<>();
        scripts.add("sql/classification/classificationlevelone.sql");
        scripts.add("sql/classification/classificationleveltwo.sql");
        scripts.add("sql/classification/classificationlevelthree.sql");
        scripts.add("sql/classification/classificationlevelthreewrollups.sql");
        scripts.add("sql/recursive/recursivelevelone.sql");
        scripts.add("sql/recursive/recursiveleveltwo.sql");
        scripts.add("sql/recursive/recursiveleveltwoexpanded.sql");
        scripts.add("sql/recursive/recursivelevelthree.sql");
        scripts.add("sql/recursive/recursivelevelthreewithrules.sql");
        scripts.add("sql/status/statuslevelone.sql");
        scripts.add("sql/status/statusleveltwo.sql");
        scripts.add("sql/status/statuslevelthree.sql");
        scripts.add("sql/status/statuslevelfour.sql");
        for (String script : scripts) {
            ScriptExecutor.runScriptByName(script);
        }
    }
}
