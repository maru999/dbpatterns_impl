package at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityThreeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityThree;

import java.util.LinkedList;
import java.util.List;

public class RLOneEntityThreeMapper {
    private RLOneEntityThreeMapper() {
        //hidden constructor
    }

    public static RLOneEntityThreeDTO toDTO(RLOneEntityThree entity) {
        RLOneEntityThreeDTO dto = new RLOneEntityThreeDTO();
        dto.setId(entity.getRloneEntityThreeId());
        dto.setName(entity.getRloneEntityThreeName());
        if (entity.getRloneEntityThreeParent() != null) {
            dto.setParent(RLOneEntityTwoMapper.toDTO(entity.getRloneEntityThreeParent()));
        }
        return dto;
    }


    public static RLOneEntityThree toEntity(RLOneEntityThreeDTO dto) {
        RLOneEntityThree entity = new RLOneEntityThree();
        if (dto.getId() != null) {
            entity.setRloneEntityThreeId(dto.getId());
        }
        entity.setRloneEntityThreeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setRloneEntityThreeParent(RLOneEntityTwoMapper.toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLOneEntityThreeDTO> toDTOs(List<RLOneEntityThree> entities) {
        LinkedList<RLOneEntityThreeDTO> result = new LinkedList<>();
        for (RLOneEntityThree entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLOneEntityThree> toEntities(List<RLOneEntityThreeDTO> dtos) {
        LinkedList<RLOneEntityThree> result = new LinkedList<>();
        for (RLOneEntityThreeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
