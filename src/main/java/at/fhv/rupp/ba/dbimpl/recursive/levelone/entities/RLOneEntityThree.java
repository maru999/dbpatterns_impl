package at.fhv.rupp.ba.dbimpl.recursive.levelone.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlone_entity_three")
@EntityListeners(AuditingEntityListener.class)
public class RLOneEntityThree {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlone_entity_three_id")
    private int rloneEntityThreeId;

    @Column(name = "rlone_entity_three_name")
    private String rloneEntityThreeName;

    @ManyToOne
    @JoinColumn(name = "rlone_entity_three_parent")
    private RLOneEntityTwo rloneEntityThreeParent;

    public int getRloneEntityThreeId() {
        return rloneEntityThreeId;
    }

    public void setRloneEntityThreeId(int rloneEntityThreeId) {
        this.rloneEntityThreeId = rloneEntityThreeId;
    }

    public String getRloneEntityThreeName() {
        return rloneEntityThreeName;
    }

    public void setRloneEntityThreeName(String rloneEntityThreeName) {
        this.rloneEntityThreeName = rloneEntityThreeName;
    }

    public RLOneEntityTwo getRloneEntityThreeParent() {
        return rloneEntityThreeParent;
    }

    public void setRloneEntityThreeParent(RLOneEntityTwo rloneEntityThreeParent) {
        this.rloneEntityThreeParent = rloneEntityThreeParent;
    }
}
