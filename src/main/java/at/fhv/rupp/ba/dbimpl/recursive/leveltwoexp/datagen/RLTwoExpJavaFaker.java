package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.datagen;

import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.*;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.repos.*;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class RLTwoExpJavaFaker extends DataGenJavaFaker {
    private final RLTwoExpEntityRepo entityRepo;
    private final RLTwoExpEntityAssocOneRepo assocOneRepo;
    private final RLTwoExpEntityAssocTwoRepo assocTwoRepo;
    private final RLTwoExpEntityAssocTypeRepo assocTypeRepo;
    private final RLTwoExpEntityTypeRepo typeRepo;

    public RLTwoExpJavaFaker(RLTwoExpEntityRepo entityRepo, RLTwoExpEntityAssocOneRepo assocOneRepo, RLTwoExpEntityAssocTwoRepo assocTwoRepo, RLTwoExpEntityAssocTypeRepo assocTypeRepo, RLTwoExpEntityTypeRepo typeRepo) {
        this.entityRepo = entityRepo;
        this.assocOneRepo = assocOneRepo;
        this.assocTwoRepo = assocTwoRepo;
        this.assocTypeRepo = assocTypeRepo;
        this.typeRepo = typeRepo;
    }


    @Override
    public void fillDB() {
        RLTwoExpEntity entity;
        RLTwoExpEntityAssocOne assocOne;
        RLTwoExpEntityAssocTwo assocTwo;
        RLTwoExpEntityAssocType assocType;
        RLTwoExpEntityType entityType;
        List<RLTwoExpEntity> createdEntities = new LinkedList<>();
        List<RLTwoExpEntityType> createdEntityTypes = new LinkedList<>();
        List<RLTwoExpEntityAssocType> createdAssocTypes = new LinkedList<>();
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entityType = new RLTwoExpEntityType();
            entityType.setRlTwoExpEntityTypeName(trimStringIfTooLong(FAKER.lordOfTheRings().character(), MIDDLE_CHAR_LIMIT));
            if (!createdEntityTypes.isEmpty()) {
                if (getRandomIntByBound(20) % 2 == 0) {
                    entityType.setRlTwoExpEntityTypeParent(createdEntityTypes.get(getRandomIntByBound(createdEntityTypes.size())));
                }
            }
            createdEntityTypes.add(entityType);
            typeRepo.save(entityType);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assocType = new RLTwoExpEntityAssocType();
            assocType.setRlTwoExpEntityAssocTypeName(trimStringIfTooLong(FAKER.lordOfTheRings().location(), MIDDLE_CHAR_LIMIT));
            createdAssocTypes.add(assocType);
            assocTypeRepo.save(assocType);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            entity = new RLTwoExpEntity();
            if (!createdEntities.isEmpty()) {
                RLTwoExpEntity entityOne = null;
                if (getRandomIntByBound(30) % 2 == 0) {
                    entityOne = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                    entity.setRlTwoExpEntityAssocToEntityOne(entityOne);
                }
                if (getRandomIntByBound(30) % 2 == 0) {
                    RLTwoExpEntity entityTwo;
                    entityTwo = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                    if (entityOne != null) {
                        if (entityOne != entityTwo) {
                            entity.setRlTwoExpEntityAssocToEntityTwo(createdEntities.get(getRandomIntByBound(createdEntities.size())));
                        }
                    } else {
                        entity.setRlTwoExpEntityAssocToEntityTwo(createdEntities.get(getRandomIntByBound(createdEntities.size())));
                    }
                }
            }
            if (!createdEntityTypes.isEmpty()) {
                entity.setRlTwoExpEntityType(createdEntityTypes.get(getRandomIntByBound(createdEntityTypes.size())));
            }
            createdEntities.add(entity);
            entityRepo.save(entity);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assocOne = new RLTwoExpEntityAssocOne();
            LocalDate from = getDateBetween(START_DATE, END_DATE);
            assocOne.setRlTwoExpEntityAssocOneFromDate(from);
            if (getRandomIntByBound(20) % 2 == 0) {
                assocOne.setRlTwoExpEntityAssocOneThruDate(getDateBetween(from, END_DATE));
            }
            if (createdEntities.size() >= 2) {
                RLTwoExpEntity fromEnt = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                assocOne.setRlTwoExpEntityAssocOneFromEntity(fromEnt);
                RLTwoExpEntity toEnt = null;
                while (toEnt == null) {
                    RLTwoExpEntity temp = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                    if (temp != fromEnt) toEnt = temp;
                }
                assocOne.setRlTwoExpEntityAssocOneToEntity(toEnt);
            }
            assocOneRepo.save(assocOne);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            assocTwo = new RLTwoExpEntityAssocTwo();
            LocalDate from = getDateBetween(START_DATE, END_DATE);
            assocTwo.setRlTwoExpEntityAssocTwoFromDate(from);
            if (getRandomIntByBound(20) % 2 == 0) {
                assocTwo.setRlTwoExpEntityAssocTwoThruDate(getDateBetween(from, END_DATE));
            }
            if (createdEntities.size() >= 2) {
                RLTwoExpEntity fromEnt = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                assocTwo.setRlTwoExpEntityAssocTwoFromEntity(fromEnt);
                RLTwoExpEntity toEnt = null;
                while (toEnt == null) {
                    RLTwoExpEntity temp = createdEntities.get(getRandomIntByBound(createdEntities.size()));
                    if (temp != fromEnt) toEnt = temp;
                }
                assocTwo.setRlTwoExpEntityAssocTwoToEntity(toEnt);
            }
            if (!createdAssocTypes.isEmpty()) {
                assocTwo.setRlTwoExpEntityAssocTwoType(createdAssocTypes.get(getRandomIntByBound(createdAssocTypes.size())));
            }
            assocTwoRepo.save(assocTwo);
        }
    }
}
