package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityTwoDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityTwo;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLFourEntityTwoMapstructMapper {

    @Mapping(source = "slFourEntityTwoId", target = "id")
    @Mapping(source = "slFourEntityTwoName", target = "name")
    SLFourEntityTwoDTO toDTO(SLFourEntityTwo entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLFourEntityTwo toEntity(SLFourEntityTwoDTO dto);

    @Mapping(source = "slFourEntityTwoId", target = "id")
    @Mapping(source = "slFourEntityTwoName", target = "name")
    void updateEntityFromDTO(SLFourEntityTwo entity, @MappingTarget SLFourEntityTwoDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLFourEntityTwoDTO dto, @MappingTarget SLFourEntityTwo entity);

    List<SLFourEntityTwoDTO> toDTOs(List<SLFourEntityTwo> entities);

    List<SLFourEntityTwo> toEntities(List<SLFourEntityTwoDTO> dtos);
}
