package at.fhv.rupp.ba.dbimpl.status.levelfour.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "slfour_status_type")
@EntityListeners(AuditingEntityListener.class)
public class SLFourStatusType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slfour_status_type_id")
    private int slFourStatusTypeId;

    @Column(name = "slfour_status_type_name", nullable = false)
    private String slFourStatusTypeName;

    public int getSlFourStatusTypeId() {
        return slFourStatusTypeId;
    }

    public void setSlFourStatusTypeId(int slFourStatusTypeId) {
        this.slFourStatusTypeId = slFourStatusTypeId;
    }

    public String getSlFourStatusTypeName() {
        return slFourStatusTypeName;
    }

    public void setSlFourStatusTypeName(String slFourStatusTypeName) {
        this.slFourStatusTypeName = slFourStatusTypeName;
    }
}
