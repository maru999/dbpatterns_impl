package at.fhv.rupp.ba.dbimpl.classification.levelone.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelone.entities.CLOneEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLOneEntityRepo extends JpaRepository<CLOneEntity, Integer> {
}
