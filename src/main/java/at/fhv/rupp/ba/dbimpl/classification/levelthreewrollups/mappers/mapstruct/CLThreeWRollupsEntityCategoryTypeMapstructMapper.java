package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = CLThreeWRollupsEntityCategoryTypeSchemeMapstructMapper.class)
public interface CLThreeWRollupsEntityCategoryTypeMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryTypeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeName", target = "name")
    @Mapping(source = "clthreeCategoryTypeScheme", target = "scheme")
    CLThreeWRollupsEntityCategoryTypeDTO toDTO(CLThreeWRollupsEntityCategoryType entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategoryType toEntity(CLThreeWRollupsEntityCategoryTypeDTO dto);

    @Mapping(source = "clthreewrEntityCategoryTypeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeName", target = "name")
    @Mapping(source = "clthreeCategoryTypeScheme", target = "scheme")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategoryType entity, @MappingTarget CLThreeWRollupsEntityCategoryTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryTypeDTO dto, @MappingTarget CLThreeWRollupsEntityCategoryType entity);

    List<CLThreeWRollupsEntityCategoryTypeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryType> entities);

    List<CLThreeWRollupsEntityCategoryType> toEntities(List<CLThreeWRollupsEntityCategoryTypeDTO> dtos);
}
