package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.datagen.RLTwoExpJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpAssociatedEntitiesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpFromEntitiesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntity;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocOne;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocTwo;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityType;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.exceptions.RLTwoExpEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.exceptions.RLTwoExpEntityTypeNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.custom.RLTwoExpEntityMapper;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.custom.RLTwoExpEntityTypeMapper;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.repos.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/recursion/L2Exp")
public class RLTwoExpController {
    private final RLTwoExpEntityRepo entityRepo;
    private final RLTwoExpEntityTypeRepo entityTypeRepo;
    private final RLTwoExpEntityAssocOneRepo assocOneRepo;
    private final RLTwoExpEntityAssocTwoRepo assocTwoRepo;
    private final RLTwoExpEntityAssocTypeRepo assocTypeRepo;
    private final DataGenJavaFaker faker;


    public RLTwoExpController(RLTwoExpEntityRepo entityRepo, RLTwoExpEntityTypeRepo entityTypeRepo, RLTwoExpEntityAssocOneRepo assocOneRepo, RLTwoExpEntityAssocTwoRepo assocTwoRepo, RLTwoExpEntityAssocTypeRepo assocTypeRepo) {
        this.entityRepo = entityRepo;
        this.entityTypeRepo = entityTypeRepo;
        this.assocOneRepo = assocOneRepo;
        this.assocTwoRepo = assocTwoRepo;
        this.assocTypeRepo = assocTypeRepo;
        this.faker = new RLTwoExpJavaFaker(entityRepo, assocOneRepo, assocTwoRepo, assocTypeRepo, entityTypeRepo);
    }

    @GetMapping("/entity/{id}")
    public ResponseEntity<RLTwoExpEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws RLTwoExpEntityNotFoundException {
        RLTwoExpEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLTwoExpEntityNotFoundException(getExceptionString("RLTwoExpEntity", entityId)));
        return ResponseEntity.ok().body(RLTwoExpEntityMapper.toDTO(entity));
    }

    @GetMapping("/entities/all")
    public List<RLTwoExpEntityDTO> getAllEntities() {
        return RLTwoExpEntityMapper.toDTOs(entityRepo.findAll());
    }

    @PostMapping("/entities")
    public RLTwoExpEntityDTO createEntity(@Valid @RequestBody RLTwoExpEntityDTO entityDTO) {
        return RLTwoExpEntityMapper.toDTO(entityRepo.save(RLTwoExpEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<RLTwoExpEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLTwoExpEntityDTO entityDetails) throws RLTwoExpEntityNotFoundException {
        RLTwoExpEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLTwoExpEntityNotFoundException(getExceptionString("RLTwoExpEntity", entityId)));
        RLTwoExpEntityDTO entityDTO = RLTwoExpEntityMapper.toDTO(entity);
        if (entityDetails.getAssocToOne() != null) {
            entityDTO.setAssocToOne(entityDetails.getAssocToOne());
        }
        if (entityDetails.getAssocToTwo() != null) {
            entityDTO.setAssocToTwo(entityDetails.getAssocToTwo());
        }
        if (entityDetails.getType() != null) {
            entityDTO.setType(entityDetails.getType());
        }
        final RLTwoExpEntityDTO updatedEntity = RLTwoExpEntityMapper.toDTO(entityRepo.save(RLTwoExpEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws RLTwoExpEntityNotFoundException {
        RLTwoExpEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLTwoExpEntityNotFoundException(getExceptionString("RLTwoExpEntity", entityId)));
        List<RLTwoExpEntity> associatedOneEntities = entityRepo.getEntitiesByAssocOneToEntityId(entityId);
        for (RLTwoExpEntity associatedOneEntity : associatedOneEntities) {
            associatedOneEntity.setRlTwoExpEntityAssocToEntityOne(null);
            entityRepo.save(associatedOneEntity);
        }
        List<RLTwoExpEntity> associatedTwoEntities = entityRepo.getEntitiesByAssocTwoToEntityId(entityId);
        for (RLTwoExpEntity associatedTwoEntity : associatedTwoEntities) {
            associatedTwoEntity.setRlTwoExpEntityAssocToEntityTwo(null);
            entityRepo.save(associatedTwoEntity);
        }
        List<RLTwoExpEntityAssocOne> assocOnes = assocOneRepo.getAssocOnesByFromEntityId(entityId);
        for (RLTwoExpEntityAssocOne assocOne : assocOnes) {
            assocOneRepo.delete(assocOne);
        }
        assocOnes = assocOneRepo.getAssocOnesByToEntityId(entityId);
        for (RLTwoExpEntityAssocOne assocOne : assocOnes) {
            assocOneRepo.delete(assocOne);
        }
        List<RLTwoExpEntityAssocTwo> assocTwos = assocTwoRepo.getAssocTwosByToEntityId(entityId);
        for (RLTwoExpEntityAssocTwo assocTwo : assocTwos) {
            assocTwoRepo.delete(assocTwo);
        }
        assocTwos = assocTwoRepo.getAssocTwosByFromEntityId(entityId);
        for (RLTwoExpEntityAssocTwo assocTwo : assocTwos) {
            assocTwoRepo.delete(assocTwo);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }


    @GetMapping("/type/{id}")
    public ResponseEntity<RLTwoExpEntityTypeDTO> getTypeById(@PathVariable(value = "id") Integer entityId) throws RLTwoExpEntityTypeNotFoundException {
        RLTwoExpEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLTwoExpEntityTypeNotFoundException(getExceptionString("RLTwoExpEntityType", entityId)));
        return ResponseEntity.ok().body(RLTwoExpEntityTypeMapper.toDTO(entity));
    }

    @GetMapping("/types/all")
    public List<RLTwoExpEntityTypeDTO> getAllTypes() {
        return RLTwoExpEntityTypeMapper.toDTOs(entityTypeRepo.findAll());
    }

    @PostMapping("/types")
    public RLTwoExpEntityTypeDTO createType(@Valid @RequestBody RLTwoExpEntityTypeDTO entityDTO) {
        return RLTwoExpEntityTypeMapper.toDTO(entityTypeRepo.save(RLTwoExpEntityTypeMapper.toEntity(entityDTO)));
    }

    @PutMapping("/types/{id}")
    public ResponseEntity<RLTwoExpEntityTypeDTO> updateType(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLTwoExpEntityTypeDTO entityDetails) throws RLTwoExpEntityTypeNotFoundException {
        RLTwoExpEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLTwoExpEntityTypeNotFoundException(getExceptionString("RLTwoExpEntityType", entityId)));
        RLTwoExpEntityTypeDTO entityDTO = RLTwoExpEntityTypeMapper.toDTO(entity);
        if (entityDetails.getName() != null) {
            entityDTO.setName(entityDetails.getName());
        }
        if (entityDetails.getParent() != null) {
            entityDTO.setParent(entityDetails.getParent());
        }
        final RLTwoExpEntityTypeDTO updatedEntity = RLTwoExpEntityTypeMapper.toDTO(entityTypeRepo.save(RLTwoExpEntityTypeMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/types/{id}")
    public Map<String, Boolean> deleteType(@PathVariable(value = "id") Integer entityId) throws RLTwoExpEntityTypeNotFoundException {
        RLTwoExpEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLTwoExpEntityTypeNotFoundException(getExceptionString("RLTwoExpEntityType", entityId)));
        List<RLTwoExpEntityType> childTypes = entityTypeRepo.getAllTypesWhereTypeIsParent(entityId);
        for (RLTwoExpEntityType childType : childTypes) {
            childType.setRlTwoExpEntityTypeParent(null);
            entityTypeRepo.save(childType);
        }
        List<RLTwoExpEntity> referencingEntities = entityRepo.getEntitiesByTypeId(entityId);
        for (RLTwoExpEntity referencingEntity : referencingEntities) {
            referencingEntity.setRlTwoExpEntityType(null);
            entityRepo.save(referencingEntity);
        }
        entityTypeRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/recursive/recursiveleveltwoexpanded.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entitiesWAssociations")
    public List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesWithAssociatedEntities() {
        return entityRepo.getAllEntitiesWithAssociations();
    }

    @GetMapping("/entitiesWAssociations/entityId/{id}")
    public List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesAssociatedWEntityById(@PathVariable(value = "id") Integer entityId) {
        return entityRepo.getAllEntitiesAssociatedToEntityById(entityId);
    }

    @GetMapping("/entitiesWAssociations/assocOneFrom/{id}")
    public List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesAssociatedWEntityByAssocOneFrom(@PathVariable(value = "id") Integer fromId) {
        return entityRepo.getAllAssociatedEntitiesWithAssocOneFromEntity(fromId);
    }

    @GetMapping("/entitiesWAssociations/assocOneTo/{id}")
    public List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesAssociatedWEntityByAssocOneTo(@PathVariable(value = "id") Integer toId) {
        return entityRepo.getAllAssociatedEntitiesWithAssocOneToEntity(toId);
    }

    @GetMapping("/entitiesWAssociations/assocTwoFrom/{id}")
    public List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesAssociatedWEntityByAssocTwoFrom(@PathVariable(value = "id") Integer fromId) {
        return entityRepo.getAllAssociatedEntitiesWithAssocTwoFromEntity(fromId);
    }

    @GetMapping("/entitiesWAssociations/assocTwoTo/{id}")
    public List<RLTwoExpAssociatedEntitiesIProjection> getAllEntitiesAssociatedWEntityByAssocTwoTo(@PathVariable(value = "id") Integer toId) {
        return entityRepo.getAllAssociatedEntitiesWithAssocTwoToEntity(toId);
    }

    @GetMapping("/entitiesWAssociations/entityInfoOnly")
    public List<RLTwoExpFromEntitiesIProjection> getAllEntitiesAssociationsEntityInfoOnly() {
        return entityRepo.getAssociatedEntitiesEntityInfoOnly();
    }
}
