package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos;

import java.time.LocalDate;

public class RLTwoExpEntityAssocTwoDTO {
    private Integer id;
    private LocalDate fromDate;
    private LocalDate thruDate;
    private RLTwoExpEntityDTO toEntity;
    private RLTwoExpEntityDTO fromEntity;
    private RLTwoExpEntityAssocTypeDTO type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getThruDate() {
        return thruDate;
    }

    public void setThruDate(LocalDate thruDate) {
        this.thruDate = thruDate;
    }

    public RLTwoExpEntityDTO getToEntity() {
        return toEntity;
    }

    public void setToEntity(RLTwoExpEntityDTO toEntity) {
        this.toEntity = toEntity;
    }

    public RLTwoExpEntityDTO getFromEntity() {
        return fromEntity;
    }

    public void setFromEntity(RLTwoExpEntityDTO fromEntity) {
        this.fromEntity = fromEntity;
    }

    public RLTwoExpEntityAssocTypeDTO getType() {
        return type;
    }

    public void setType(RLTwoExpEntityAssocTypeDTO type) {
        this.type = type;
    }
}
