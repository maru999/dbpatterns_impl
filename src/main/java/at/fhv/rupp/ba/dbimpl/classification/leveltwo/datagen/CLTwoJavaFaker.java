package at.fhv.rupp.ba.dbimpl.classification.leveltwo.datagen;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.*;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.repos.*;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;

import java.util.*;


public class CLTwoJavaFaker extends DataGenJavaFaker {

    private final CLTwoEntityRepo entityRepo;
    private final CLTwoEntityEntityTypeThreeClassificationRepo entityClassificationRepo;
    private final CLTwoEntityTypeOneRepo typeOneRepo;
    private final CLTwoEntityTypeTwoRepo typeTwoRepo;
    private final CLTwoEntityTypeThreeRepo typeThreeRepo;

    public CLTwoJavaFaker(CLTwoEntityRepo entityRepo, CLTwoEntityEntityTypeThreeClassificationRepo entityClassificationRepo, CLTwoEntityTypeOneRepo typeOneRepo, CLTwoEntityTypeTwoRepo typeTwoRepo, CLTwoEntityTypeThreeRepo typeThreeRepo) {
        this.entityRepo = entityRepo;
        this.entityClassificationRepo = entityClassificationRepo;
        this.typeOneRepo = typeOneRepo;
        this.typeTwoRepo = typeTwoRepo;
        this.typeThreeRepo = typeThreeRepo;
    }

    @Override
    public void fillDB() {
        CLTwoEntitySubtypeOne entitySubOne;
        CLTwoEntitySubtypeTwo entitySubTwo;
        CLTwoEntitySubtypeThree entitySubThree;
        CLTwoEntityTypeOne typeOne;
        CLTwoEntityTypeTwo typeTwo;
        CLTwoEntityTypeThree typeThree;
        CLTwoEntityEntityTypeThreeClassification classification;
        List<CLTwoEntity> createdEntities = new LinkedList<>();
        List<CLTwoEntityTypeOne> createdTypeOnes = new LinkedList<>();
        List<CLTwoEntityTypeTwo> createdTypeTwos = new LinkedList<>();
        List<CLTwoEntityTypeThree> createdTypeThrees = new LinkedList<>();
        HashMap<CLTwoEntity, CLTwoEntityTypeThree> entityToTypeThree = new HashMap<>();
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            typeOne = new CLTwoEntityTypeOne();
            typeOne.setClTwoEntityTypeOneName(trimStringIfTooLong(FAKER.harryPotter().book(), MIDDLE_CHAR_LIMIT));
            createdTypeOnes.add(typeOne);
            typeOneRepo.save(typeOne);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            typeThree = new CLTwoEntityTypeThree();
            typeThree.setClTwoEntityTypeThreeName(trimStringIfTooLong(FAKER.harryPotter().location(), MIDDLE_CHAR_LIMIT));
            createdTypeThrees.add(typeThree);
            typeThreeRepo.save(typeThree);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            typeTwo = new CLTwoEntityTypeTwo();
            typeTwo.setClTwoEntityTypeTwoName(trimStringIfTooLong(FAKER.harryPotter().spell(), MIDDLE_CHAR_LIMIT));
            int length = createdTypeTwos.size();
            Random random = new Random();
            if (!createdTypeTwos.isEmpty()) {
                int randomInt = random.nextInt(length);
                typeTwo.setClTwoEntityTypeTwoParent(createdTypeTwos.get(randomInt));
            }
            createdTypeTwos.add(typeTwo);
            typeTwoRepo.save(typeTwo);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entitySubOne = new CLTwoEntitySubtypeOne();
            entitySubOne.setClTwoEntityName(trimStringIfTooLong(FAKER.harryPotter().character(), MIDDLE_CHAR_LIMIT));
            if (!createdTypeOnes.isEmpty()) {
                entitySubOne.setClTwoEntityEntityTypeOne(createdTypeOnes.get(getRandomIntByBound(createdTypeOnes.size())));
            }
            if (!createdTypeTwos.isEmpty()) {
                entitySubOne.setClTwoEntityEntityTypeTwo(createdTypeTwos.get(getRandomIntByBound(createdTypeTwos.size())));
            }
            entitySubOne.setClTwoEntitySubtypeOneDesc(trimStringIfTooLong(FAKER.harryPotter().house(), MIDDLE_CHAR_LIMIT));
            createdEntities.add(entitySubOne);
            entityRepo.save(entitySubOne);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entitySubTwo = new CLTwoEntitySubtypeTwo();
            entitySubTwo.setClTwoEntityName(trimStringIfTooLong(FAKER.harryPotter().character(), MIDDLE_CHAR_LIMIT));
            if (!createdTypeOnes.isEmpty()) {
                entitySubTwo.setClTwoEntityEntityTypeOne(createdTypeOnes.get(getRandomIntByBound(createdTypeOnes.size())));
            }
            if (!createdTypeTwos.isEmpty()) {
                entitySubTwo.setClTwoEntityEntityTypeTwo(createdTypeTwos.get(getRandomIntByBound(createdTypeTwos.size())));
            }
            entitySubTwo.setClTwoEntitySubtypeTwoAttribute(trimStringIfTooLong(FAKER.harryPotter().quote(), MIDDLE_CHAR_LIMIT));
            createdEntities.add(entitySubTwo);
            entityRepo.save(entitySubTwo);
        }
        for (int i = 0; i < TINY_GEN_AMOUNT; i++) {
            entitySubThree = new CLTwoEntitySubtypeThree();
            entitySubThree.setClTwoEntityName(trimStringIfTooLong(FAKER.harryPotter().character(), MIDDLE_CHAR_LIMIT));
            if (!createdTypeOnes.isEmpty()) {
                entitySubThree.setClTwoEntityEntityTypeOne(createdTypeOnes.get(getRandomIntByBound(createdTypeOnes.size())));
            }
            if (!createdTypeTwos.isEmpty()) {
                entitySubThree.setClTwoEntityEntityTypeTwo(createdTypeTwos.get(getRandomIntByBound(createdTypeTwos.size())));
            }
            entitySubThree.setClTwoEntitySubtypeThreeInfo(trimStringIfTooLong(FAKER.harryPotter().quote(), MIDDLE_CHAR_LIMIT));
            createdEntities.add(entitySubThree);
            entityRepo.save(entitySubThree);
        }
        for (int i = 0; i < LOW_GEN_AMOUNT; i++) {
            classification = new CLTwoEntityEntityTypeThreeClassification();
            CLTwoEntity entityTemp = null;
            CLTwoEntityTypeThree typeThreeTemp = null;
            if (!createdEntities.isEmpty()) {
                entityTemp = createdEntities.get(getRandomIntByBound(createdEntities.size()));
            }
            if (!createdTypeThrees.isEmpty()) {
                typeThreeTemp = createdTypeThrees.get(getRandomIntByBound(createdTypeThrees.size()));
            }
            if (entityTemp != null && typeThreeTemp != null) {
                if (entityToTypeThree.isEmpty() || !entityToTypeThree.containsKey(entityTemp) || !entityToTypeThree.containsValue(typeThreeTemp) || !entityToTypeThree.get(entityTemp).equals(typeThreeTemp)) {
                    entityToTypeThree.put(entityTemp, typeThreeTemp);
                    classification.setClTwoEntityEntityTypeThreeClassificationEntity(entityTemp);
                    classification.setClTwoEntityEntityTypeThreeTypeThree(typeThreeTemp);
                    entityClassificationRepo.save(classification);
                }
            }
        }

    }
}
