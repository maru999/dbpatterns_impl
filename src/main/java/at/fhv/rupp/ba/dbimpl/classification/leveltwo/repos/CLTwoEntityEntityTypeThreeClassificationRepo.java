package at.fhv.rupp.ba.dbimpl.classification.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityEntityTypeThreeClassification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CLTwoEntityEntityTypeThreeClassificationRepo extends JpaRepository<CLTwoEntityEntityTypeThreeClassification, Integer> {

    @Query(value = "SELECT * FROM cltwo_entity_type_three_class WHERE cltwo_entity_type_three_class_entid = :id", nativeQuery = true)
    List<CLTwoEntityEntityTypeThreeClassification> getAllClassificationsByEntityId(@Param("id") Integer entityId);
}
