package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryRollupTypeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryRollupType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLThreeWRollupsEntityCategoryRollupTypeMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryRollupTypeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryRollupTypeName", target = "name")
    @Mapping(source = "parentRollupType", target = "parent")
    CLThreeWRollupsEntityCategoryRollupTypeDTO toDTO(CLThreeWRollupsEntityCategoryRollupType entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategoryRollupType toEntity(CLThreeWRollupsEntityCategoryRollupTypeDTO dto);

    @Mapping(source = "clthreewrEntityCategoryRollupTypeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryRollupTypeName", target = "name")
    @Mapping(source = "parentRollupType", target = "parent")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategoryRollupType entity, @MappingTarget CLThreeWRollupsEntityCategoryRollupTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryRollupTypeDTO dto, @MappingTarget CLThreeWRollupsEntityCategoryRollupType entity);

    List<CLThreeWRollupsEntityCategoryRollupTypeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryRollupType> entities);

    List<CLThreeWRollupsEntityCategoryRollupType> toEntities(List<CLThreeWRollupsEntityCategoryRollupTypeDTO> dtos);
}
