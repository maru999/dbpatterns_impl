package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos;

public class RLTwoExpEntityDTO {
    private Integer id;
    private RLTwoExpEntityDTO assocToOne;
    private RLTwoExpEntityDTO assocToTwo;
    private RLTwoExpEntityTypeDTO type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RLTwoExpEntityDTO getAssocToOne() {
        return assocToOne;
    }

    public void setAssocToOne(RLTwoExpEntityDTO assocToOne) {
        this.assocToOne = assocToOne;
    }

    public RLTwoExpEntityDTO getAssocToTwo() {
        return assocToTwo;
    }

    public void setAssocToTwo(RLTwoExpEntityDTO assocToTwo) {
        this.assocToTwo = assocToTwo;
    }

    public RLTwoExpEntityTypeDTO getType() {
        return type;
    }

    public void setType(RLTwoExpEntityTypeDTO type) {
        this.type = type;
    }
}
