package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityTypeDTO;

public class RLThreeWRulesEntityDTO {
    private Integer id;
    private RLThreeWRulesEntityTypeDTO type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RLThreeWRulesEntityTypeDTO getType() {
        return type;
    }

    public void setType(RLThreeWRulesEntityTypeDTO type) {
        this.type = type;
    }
}
