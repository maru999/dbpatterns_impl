package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rltwoxp_entity_type")
@EntityListeners(AuditingEntityListener.class)
public class RLTwoExpEntityType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rltwoxp_entity_type_id")
    private int rlTwoExpEntityTypeId;

    @Column(name = "rltwoxp_entity_type_name")
    private String rlTwoExpEntityTypeName;

    @ManyToOne
    @JoinColumn(name = "rltwoxp_entity_type_parent")
    private RLTwoExpEntityType rlTwoExpEntityTypeParent;

    public int getRlTwoExpEntityTypeId() {
        return rlTwoExpEntityTypeId;
    }

    public void setRlTwoExpEntityTypeId(int rlTwoExpEntityTypeId) {
        this.rlTwoExpEntityTypeId = rlTwoExpEntityTypeId;
    }

    public String getRlTwoExpEntityTypeName() {
        return rlTwoExpEntityTypeName;
    }

    public void setRlTwoExpEntityTypeName(String rlTwoExpEntityTypeName) {
        this.rlTwoExpEntityTypeName = rlTwoExpEntityTypeName;
    }

    public RLTwoExpEntityType getRlTwoExpEntityTypeParent() {
        return rlTwoExpEntityTypeParent;
    }

    public void setRlTwoExpEntityTypeParent(RLTwoExpEntityType rlTwoExpEntityTypeParent) {
        this.rlTwoExpEntityTypeParent = rlTwoExpEntityTypeParent;
    }
}
