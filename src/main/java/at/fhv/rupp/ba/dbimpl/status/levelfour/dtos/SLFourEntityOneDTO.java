package at.fhv.rupp.ba.dbimpl.status.levelfour.dtos;

public class SLFourEntityOneDTO {
    private Integer id;
    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
