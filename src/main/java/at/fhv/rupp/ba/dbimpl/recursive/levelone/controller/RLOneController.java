package at.fhv.rupp.ba.dbimpl.recursive.levelone.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.datagen.RLOneJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneAllEntitiesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityOneDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityThreeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityTwoDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityOne;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityThree;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityTwo;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.exceptions.RLOneEntityOneNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.exceptions.RLOneEntityThreeNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.exceptions.RLOneEntityTwoNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.custom.RLOneEntityOneMapper;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.custom.RLOneEntityThreeMapper;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.custom.RLOneEntityTwoMapper;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.repos.RLOneEntityOneRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.repos.RLOneEntityThreeRepo;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.repos.RLOneEntityTwoRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/recursion/L1")
public class RLOneController {
    private final RLOneEntityOneRepo entityOneRepo;
    private final RLOneEntityTwoRepo entityTwoRepo;
    private final RLOneEntityThreeRepo entityThreeRepo;
    private final DataGenJavaFaker faker;

    public RLOneController(RLOneEntityOneRepo entityOneRepo, RLOneEntityTwoRepo entityTwoRepo, RLOneEntityThreeRepo entityThreeRepo) {
        this.entityOneRepo = entityOneRepo;
        this.entityTwoRepo = entityTwoRepo;
        this.entityThreeRepo = entityThreeRepo;
        this.faker = new RLOneJavaFaker(entityOneRepo, entityTwoRepo, entityThreeRepo);
    }

    @GetMapping("/entities/one/{id}")
    public ResponseEntity<RLOneEntityOneDTO> getEntityOneById(@PathVariable(value = "id") Integer entityId) throws RLOneEntityOneNotFoundException {
        RLOneEntityOne entity = entityOneRepo.findById(entityId).orElseThrow(() -> new RLOneEntityOneNotFoundException(getExceptionString("RLOneEntityOne", entityId)));
        return ResponseEntity.ok().body(RLOneEntityOneMapper.toDTO(entity));
    }

    @GetMapping("/entities/one/all")
    public List<RLOneEntityOneDTO> getAllEntitiesOne() {
        return RLOneEntityOneMapper.toDTOs(entityOneRepo.findAll());
    }

    @PostMapping("/entities/one")
    public RLOneEntityOneDTO createEntityOne(@Valid @RequestBody RLOneEntityOneDTO entityDTO) {
        return RLOneEntityOneMapper.toDTO(entityOneRepo.save(RLOneEntityOneMapper.toEntity(entityDTO)));
    }

    @GetMapping("/entities/two/{id}")
    public ResponseEntity<RLOneEntityTwoDTO> getEntityTwoById(@PathVariable(value = "id") Integer entityId) throws RLOneEntityTwoNotFoundException {
        RLOneEntityTwo entity = entityTwoRepo.findById(entityId).orElseThrow(() -> new RLOneEntityTwoNotFoundException(getExceptionString("RLOneEntityTwo", entityId)));
        return ResponseEntity.ok().body(RLOneEntityTwoMapper.toDTO(entity));
    }

    @GetMapping("/entities/two/all")
    public List<RLOneEntityTwoDTO> getAllEntitiesTwo() {
        return RLOneEntityTwoMapper.toDTOs(entityTwoRepo.findAll());
    }

    @PostMapping("/entities/two")
    public RLOneEntityTwoDTO createEntityTwo(@Valid @RequestBody RLOneEntityTwoDTO entityDTO) {
        return RLOneEntityTwoMapper.toDTO(entityTwoRepo.save(RLOneEntityTwoMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/two/{id}")
    public ResponseEntity<RLOneEntityTwoDTO> updateEntityTwo(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLOneEntityTwoDTO entityDetails) throws RLOneEntityTwoNotFoundException {
        RLOneEntityTwo entity = entityTwoRepo.findById(entityId).orElseThrow(() -> new RLOneEntityTwoNotFoundException(getExceptionString("RLOneEntityTwo", entityId)));
        RLOneEntityTwoDTO entityDTO = RLOneEntityTwoMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        if (entityDetails.getParent() != null) {
            entityDTO.setParent(entityDetails.getParent());
        }
        final RLOneEntityTwoDTO updatedEntity = RLOneEntityTwoMapper.toDTO(entityTwoRepo.save(RLOneEntityTwoMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/two/{id}")
    public Map<String, Boolean> deleteEntityTwo(@PathVariable(value = "id") Integer entityId) throws RLOneEntityTwoNotFoundException {
        RLOneEntityTwo entity = entityTwoRepo.findById(entityId).orElseThrow(() -> new RLOneEntityTwoNotFoundException(getExceptionString("RLOneEntityTwo", entityId)));
        List<RLOneEntityThree> threes = entityThreeRepo.getAllEntityThreesByEntityTwoId(entityId);
        for (RLOneEntityThree three : threes) {
            three.setRloneEntityThreeParent(null);
            entityThreeRepo.save(three);
        }
        entityTwoRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/entities/three/{id}")
    public ResponseEntity<RLOneEntityThreeDTO> getEntityThreeById(@PathVariable(value = "id") Integer entityId) throws RLOneEntityThreeNotFoundException {
        RLOneEntityThree entity = entityThreeRepo.findById(entityId).orElseThrow(() -> new RLOneEntityThreeNotFoundException(getExceptionString("RLOneEntityThree", entityId)));
        return ResponseEntity.ok().body(RLOneEntityThreeMapper.toDTO(entity));
    }

    @GetMapping("/entities/three/all")
    public List<RLOneEntityThreeDTO> getAllEntitiesThree() {
        return RLOneEntityThreeMapper.toDTOs(entityThreeRepo.findAll());
    }

    @PostMapping("/entities/three")
    public RLOneEntityThreeDTO createEntityThree(@Valid @RequestBody RLOneEntityThreeDTO entityDTO) {
        return RLOneEntityThreeMapper.toDTO(entityThreeRepo.save(RLOneEntityThreeMapper.toEntity(entityDTO)));
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/recursive/recursivelevelone.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entities/allHierarchy")
    public List<RLOneAllEntitiesIProjection> getAllEntitiesInHierarchy() {
        return entityOneRepo.getAllEntitiesInHierarchy();
    }

    @GetMapping("/entities/allHierarchy/oneName/{name}")
    public List<RLOneAllEntitiesIProjection> getEntitiesInHierarchyByOneName(@PathVariable(value = "name") String entityOneName) {
        return entityOneRepo.getAllEntitiesInHierarchyByEntityOneName(entityOneName);
    }

    @GetMapping("/entities/allHierarchy/twoName/{name}")
    public List<RLOneAllEntitiesIProjection> getEntitiesInHierarchyByTwoName(@PathVariable(value = "name") String entityTwoName) {
        return entityOneRepo.getAllEntitiesInHierarchyByEntityTwoName(entityTwoName);
    }

    @GetMapping("/entities/allHierarchy/threeName/{name}")
    public List<RLOneAllEntitiesIProjection> getEntitiesInHierarchyByThreeName(@PathVariable(value = "name") String entityThreeName) {
        return entityOneRepo.getAllEntitiesInHierarchyByEntityThreeName(entityThreeName);
    }


    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }
}
