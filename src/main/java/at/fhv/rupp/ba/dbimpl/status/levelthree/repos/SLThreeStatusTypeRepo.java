package at.fhv.rupp.ba.dbimpl.status.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeStatusType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SLThreeStatusTypeRepo extends JpaRepository<SLThreeStatusType, Integer> {
}
