package at.fhv.rupp.ba.dbimpl.classification.levelone.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelone.dtos.CLOneEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelone.entities.CLOneEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLOneEntityMapstructMapper {

    @Mapping(source = "cloneEntityID", target = "id")
    @Mapping(source = "cloneEntityName", target = "name")
    @Mapping(source = "cloneEntityTypeOne", target = "typeOne")
    @Mapping(source = "cloneEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "cloneEntityTypeThree", target = "typeThree")
    CLOneEntityDTO toDTO(CLOneEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLOneEntity toEntity(CLOneEntityDTO dto);

    @Mapping(source = "cloneEntityID", target = "id")
    @Mapping(source = "cloneEntityName", target = "name")
    @Mapping(source = "cloneEntityTypeOne", target = "typeOne")
    @Mapping(source = "cloneEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "cloneEntityTypeThree", target = "typeThree")
    void updateEntityFromDTO(CLOneEntity entity, @MappingTarget CLOneEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLOneEntityDTO dto, @MappingTarget CLOneEntity entity);

    List<CLOneEntityDTO> toDTOs(List<CLOneEntity> entities);

    List<CLOneEntity> toEntities(List<CLOneEntityDTO> dtos);
}
