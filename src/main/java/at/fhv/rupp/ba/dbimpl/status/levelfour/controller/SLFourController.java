package at.fhv.rupp.ba.dbimpl.status.levelfour.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.status.levelfour.datagen.SLFourJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityStatusViewIProjection;
import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourStatusApplicationDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourStatusApplication;
import at.fhv.rupp.ba.dbimpl.status.levelfour.exceptions.SLFourStatusApplicationNotFoundException;
import at.fhv.rupp.ba.dbimpl.status.levelfour.exceptions.SLFourXORConstraintViolationException;
import at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.custom.SLFourStatusApplicationMapper;
import at.fhv.rupp.ba.dbimpl.status.levelfour.repos.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/status/L4")
public class SLFourController {

    private final SLFourStatusApplicationRepo statusAppRepo;
    private final SLFourStatusTypeRepo statusTypeRepo;
    private final SLFourEntityOneRepo entityOneRepo;
    private final SLFourEntityTwoRepo entityTwoRepo;
    private final SLFourEntityThreeRepo entityThreeRepo;
    private final DataGenJavaFaker faker;

    public SLFourController(SLFourStatusApplicationRepo statusAppRepo, SLFourStatusTypeRepo statusTypeRepo, SLFourEntityOneRepo entityOneRepo, SLFourEntityTwoRepo entityTwoRepo, SLFourEntityThreeRepo entityThreeRepo) {
        this.statusAppRepo = statusAppRepo;
        this.statusTypeRepo = statusTypeRepo;
        this.entityOneRepo = entityOneRepo;
        this.entityTwoRepo = entityTwoRepo;
        this.entityThreeRepo = entityThreeRepo;
        this.faker = new SLFourJavaFaker(entityOneRepo, entityTwoRepo, entityThreeRepo, statusAppRepo, statusTypeRepo);
    }

    @GetMapping("/statusApplications")
    public List<SLFourStatusApplicationDTO> getAllStatusApplications() {
        return SLFourStatusApplicationMapper.toDTOs(statusAppRepo.findAll());
    }

    @GetMapping("/statusApplications/{id}")
    public ResponseEntity<SLFourStatusApplicationDTO> getStatusApplicationById(@PathVariable(value = "id") Integer entityId) throws SLFourStatusApplicationNotFoundException, SLFourXORConstraintViolationException {
        SLFourStatusApplication entity = statusAppRepo.findById(entityId).orElseThrow(() -> new SLFourStatusApplicationNotFoundException(getExceptionString("SLFourStatusApplication", entityId)));
        return ResponseEntity.ok().body(SLFourStatusApplicationMapper.toDTO(entity));
    }

    @PostMapping("/statusApplications")
    public SLFourStatusApplicationDTO createStatusApplication(@Valid @RequestBody SLFourStatusApplicationDTO entityDTO) throws SLFourXORConstraintViolationException {
        return SLFourStatusApplicationMapper.toDTO(statusAppRepo.save(SLFourStatusApplicationMapper.toEntity(entityDTO)));
    }

    @PutMapping("/statusApplications/{id}")
    public ResponseEntity<SLFourStatusApplicationDTO> updateStatusApplication(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody SLFourStatusApplicationDTO entityDetails) throws SLFourStatusApplicationNotFoundException, SLFourXORConstraintViolationException {
        SLFourStatusApplication entity = statusAppRepo.findById(entityId).orElseThrow(() -> new SLFourStatusApplicationNotFoundException(getExceptionString("SLFourStatusApplication", entityId)));
        SLFourStatusApplicationDTO entityDTO = SLFourStatusApplicationMapper.toDTO(entity);
        entityDTO.setDateTime(entityDetails.getDateTime());
        entityDTO.setStatusFrom(entityDetails.getStatusFrom());
        entityDTO.setStatusThru(entityDetails.getStatusThru());
        entityDTO.setFrom(entityDetails.getFrom());
        entityDTO.setThru(entityDetails.getThru());
        entityDTO.setType(entityDetails.getType());
        if (entityDetails.getEntityOne() != null) {
            entityDTO.setEntityOne(entityDetails.getEntityOne());
            entityDTO.setEntityTwo(null);
            entityDTO.setEntityThree(null);
        } else if (entityDetails.getEntityTwo() != null) {
            entityDTO.setEntityTwo(entityDetails.getEntityTwo());
            entityDTO.setEntityOne(null);
            entityDTO.setEntityThree(null);
        } else if (entityDetails.getEntityThree() != null) {
            entityDTO.setEntityThree(entityDetails.getEntityThree());
            entityDTO.setEntityOne(null);
            entityDTO.setEntityTwo(null);
        }
        final SLFourStatusApplicationDTO updatedEntity = SLFourStatusApplicationMapper.toDTO(statusAppRepo.save(SLFourStatusApplicationMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/statusApplications/{id}")
    public Map<String, Boolean> deleteStatusApplication(@PathVariable(value = "id") Integer entityId) throws SLFourStatusApplicationNotFoundException {
        SLFourStatusApplication entity = statusAppRepo.findById(entityId).orElseThrow(() -> new SLFourStatusApplicationNotFoundException(getExceptionString("SLFourStatusApplication", entityId)));
        statusAppRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/status/statuslevelfour.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entitiesWStatus")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatus() {
        return statusTypeRepo.getAllEntitiesWStatus();
    }

    @GetMapping("/entitiesWStatus/entityOne")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusWithEntityOne() {
        return statusTypeRepo.getAllEntitiesWStatusEntityOne();
    }

    @GetMapping("/entitiesWStatus/entityTwo")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusEntityTwo() {
        return statusTypeRepo.getAllEntitiesWStatusEntityTwo();
    }

    @GetMapping("/entitiesWStatus/entityThree")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusEntityThree() {
        return statusTypeRepo.getAllEntitiesWStatusEntityThree();
    }

    @GetMapping("/entitiesWStatus/noEntityOne")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusNoEntityOne() {
        return statusTypeRepo.getAllEntitiesWStatusNoEntityOne();
    }

    @GetMapping("/entitiesWStatus/noEntityTwo")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusNoEntityTwo() {
        return statusTypeRepo.getAllEntitiesWStatusNoEntityTwo();
    }

    @GetMapping("/entitiesWStatus/noEntityThree")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusNoEntityThree() {
        return statusTypeRepo.getAllEntitiesWStatusNoEntityThree();
    }

    @GetMapping("/entitiesWStatus/datetime/{startTime}/{endTime}")
    public List<SLFourEntityStatusViewIProjection> getAllEntitiesWStatusDateTimeBetween(@PathVariable("startTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startTime, @PathVariable("endTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endTime) {
        return statusTypeRepo.getAllEntitiesWStatusWhereStatusDateTimeBetween(startTime, endTime);
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }

}
