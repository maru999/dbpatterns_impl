package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rltwo_entity")
@EntityListeners(AuditingEntityListener.class)
public class RLTwoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rltwo_entity_id")
    private int rlTwoEntityId;

    @ManyToOne
    @JoinColumn(name = "rltwo_entity_parent")
    private RLTwoEntity rlTwoEntityParent;

    @ManyToOne
    @JoinColumn(name = "rltwo_entity_type_fk")
    private RLTwoEntityType rlTwoEntityType;

    public int getRlTwoEntityId() {
        return rlTwoEntityId;
    }

    public void setRlTwoEntityId(int rlTwoEntityId) {
        this.rlTwoEntityId = rlTwoEntityId;
    }

    public RLTwoEntity getRlTwoEntityParent() {
        return rlTwoEntityParent;
    }

    public void setRlTwoEntityParent(RLTwoEntity rlTwoEntityParent) {
        this.rlTwoEntityParent = rlTwoEntityParent;
    }

    public RLTwoEntityType getRlTwoEntityType() {
        return rlTwoEntityType;
    }

    public void setRlTwoEntityType(RLTwoEntityType rlTwoEntityType) {
        this.rlTwoEntityType = rlTwoEntityType;
    }
}
