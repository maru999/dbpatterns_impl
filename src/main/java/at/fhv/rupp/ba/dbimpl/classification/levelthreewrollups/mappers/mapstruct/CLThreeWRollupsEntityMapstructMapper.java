package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLThreeWRollupsEntityMapstructMapper {

    @Mapping(source = "clthreewrEntityId", target = "id")
    @Mapping(source = "clthreewrEntityName", target = "name")
    CLThreeWRollupsEntityDTO toDTO(CLThreeWRollupsEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntity toEntity(CLThreeWRollupsEntityDTO dto);

    @Mapping(source = "clthreewrEntityId", target = "id")
    @Mapping(source = "clthreewrEntityName", target = "name")
    void updateEntityFromDTO(CLThreeWRollupsEntity entity, @MappingTarget CLThreeWRollupsEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityDTO dto, @MappingTarget CLThreeWRollupsEntity entity);

    List<CLThreeWRollupsEntityDTO> toDTOs(List<CLThreeWRollupsEntity> entities);

    List<CLThreeWRollupsEntity> toEntities(List<CLThreeWRollupsEntityDTO> dtos);
}
