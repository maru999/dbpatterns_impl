package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourEntityThreeDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityThree;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLFourEntityThreeMapstructMapper {

    @Mapping(source = "slFourEntityThreeId", target = "id")
    @Mapping(source = "slFourEntityThreeAttr", target = "attr")
    SLFourEntityThreeDTO toDTO(SLFourEntityThree entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLFourEntityThree toEntity(SLFourEntityThreeDTO dto);

    @Mapping(source = "slFourEntityThreeId", target = "id")
    @Mapping(source = "slFourEntityThreeAttr", target = "attr")
    void updateEntityFromDTO(SLFourEntityThree entity, @MappingTarget SLFourEntityThreeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLFourEntityThreeDTO dto, @MappingTarget SLFourEntityThree entity);

    List<SLFourEntityThreeDTO> toDTOs(List<SLFourEntityThree> entities);

    List<SLFourEntityThree> toEntities(List<SLFourEntityThreeDTO> dtos);
}
