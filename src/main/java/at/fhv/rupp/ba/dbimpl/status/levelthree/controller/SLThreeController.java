package at.fhv.rupp.ba.dbimpl.status.levelthree.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.status.levelthree.datagen.SLThreeJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityStatusDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityStatusViewIProjection;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntity;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntityStatus;
import at.fhv.rupp.ba.dbimpl.status.levelthree.exceptions.SLThreeEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.status.levelthree.exceptions.SLThreeEntityStatusNotFoundException;
import at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.custom.SLThreeEntityMapper;
import at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.custom.SLThreeEntityStatusMapper;
import at.fhv.rupp.ba.dbimpl.status.levelthree.repos.SLThreeEntityRepo;
import at.fhv.rupp.ba.dbimpl.status.levelthree.repos.SLThreeEntityStatusRepo;
import at.fhv.rupp.ba.dbimpl.status.levelthree.repos.SLThreeStatusTypeRepo;
import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/status/L3")
public class SLThreeController {

    private final SLThreeEntityRepo entityRepo;
    private final SLThreeEntityStatusRepo entityStatusRepo;
    private final SLThreeStatusTypeRepo statusTypeRepo;
    private final DataGenJavaFaker faker;

    public SLThreeController(SLThreeEntityRepo entityRepo, SLThreeEntityStatusRepo entityStatusRepo, SLThreeStatusTypeRepo statusTypeRepo) {
        this.entityRepo = entityRepo;
        this.entityStatusRepo = entityStatusRepo;
        this.statusTypeRepo = statusTypeRepo;
        faker = new SLThreeJavaFaker(entityRepo, entityStatusRepo, statusTypeRepo);
    }

    @GetMapping("/entities")
    public List<SLThreeEntityDTO> getAllEntities() {
        return SLThreeEntityMapper.toDTOs(entityRepo.findAll());
    }

    @GetMapping("/entities/{id}")
    public ResponseEntity<SLThreeEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws SLThreeEntityNotFoundException {
        SLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new SLThreeEntityNotFoundException(getExceptionString("SLThreeEntity", entityId)));
        return ResponseEntity.ok().body(SLThreeEntityMapper.toDTO(entity));
    }

    @PostMapping("/entities")
    public SLThreeEntityDTO createEntity(@Valid @RequestBody SLThreeEntityDTO entityDTO) {
        return SLThreeEntityMapper.toDTO(entityRepo.save(SLThreeEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<SLThreeEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody SLThreeEntityDTO entityDetails) throws SLThreeEntityNotFoundException {
        SLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new SLThreeEntityNotFoundException(getExceptionString("SLThreeEntity", entityId)));
        SLThreeEntityDTO entityDTO = SLThreeEntityMapper.toDTO(entity);
        final SLThreeEntityDTO updatedEntity = SLThreeEntityMapper.toDTO(entityRepo.save(SLThreeEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws SLThreeEntityNotFoundException {
        SLThreeEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new SLThreeEntityNotFoundException(getExceptionString("SLThreeEntity", entityId)));
        List<SLThreeEntityStatus> statuses = entityStatusRepo.getEntityStatusByEntityId(entityId);
        for (SLThreeEntityStatus status : statuses) {
            entityStatusRepo.delete(status);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }


    @GetMapping("/entityStatus")
    public List<SLThreeEntityStatusDTO> getAllEntitiyStatus() {
        return SLThreeEntityStatusMapper.toDTOs(entityStatusRepo.findAll());
    }

    @GetMapping("/entityStatus/{id}")
    public ResponseEntity<SLThreeEntityStatusDTO> getEntityStatusById(@PathVariable(value = "id") Integer entityId) throws SLThreeEntityStatusNotFoundException {
        SLThreeEntityStatus entity = entityStatusRepo.findById(entityId).orElseThrow(() -> new SLThreeEntityStatusNotFoundException(getExceptionString("SLThreeEntityStatus", entityId)));
        return ResponseEntity.ok().body(SLThreeEntityStatusMapper.toDTO(entity));
    }

    @PostMapping("/entityStatus")
    public SLThreeEntityStatusDTO createEntityStatus(@Valid @RequestBody SLThreeEntityStatusDTO entityDTO) {
        return SLThreeEntityStatusMapper.toDTO(entityStatusRepo.save(SLThreeEntityStatusMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entityStatus/{id}")
    public ResponseEntity<SLThreeEntityStatusDTO> updateEntityStatus(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody SLThreeEntityStatusDTO entityDetails) throws SLThreeEntityStatusNotFoundException {
        SLThreeEntityStatus entity = entityStatusRepo.findById(entityId).orElseThrow(() -> new SLThreeEntityStatusNotFoundException(getExceptionString("SLThreeEntityStatus", entityId)));
        SLThreeEntityStatusDTO entityDTO = SLThreeEntityStatusMapper.toDTO(entity);
        if (entityDetails.getEntity() != null) {
            entityDTO.setEntity(entityDetails.getEntity());
        }
        if (entityDetails.getStatusType() != null) {
            entityDTO.setStatusType(entityDetails.getStatusType());
        }
        if (entityDetails.getEntityStatusThru() != null) {
            entityDTO.setEntityStatusThru(entityDetails.getEntityStatusThru());
        }
        if (entityDetails.getEntityStatusFrom() != null) {
            entityDTO.setEntityStatusFrom(entityDetails.getEntityStatusFrom());
        }
        if (entityDetails.getStatusThru() != null) {
            entityDTO.setStatusThru(entityDetails.getStatusThru());
        }
        if (entityDetails.getStatusFrom() != null) {
            entityDTO.setStatusFrom(entityDetails.getStatusFrom());
        }
        if (entityDetails.getStatusDateTime() != null) {
            entityDTO.setStatusDateTime(entityDetails.getStatusDateTime());
        }
        final SLThreeEntityStatusDTO updatedEntity = SLThreeEntityStatusMapper.toDTO(entityStatusRepo.save(SLThreeEntityStatusMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entityStatus/{id}")
    public Map<String, Boolean> deleteEntityStatus(@PathVariable(value = "id") Integer entityId) throws SLThreeEntityStatusNotFoundException {
        SLThreeEntityStatus entity = entityStatusRepo.findById(entityId).orElseThrow(() -> new SLThreeEntityStatusNotFoundException(getExceptionString("SLThreeEntityStatus", entityId)));
        entityStatusRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/status/statuslevelthree.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entityWStatus")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatus() {
        return entityRepo.getAllEntitesWithStatus();
    }

    @GetMapping("/entityWStatus/entityNotThru")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusWhereEntityNotThru() {
        return entityRepo.getAllEntitesWithStatusWhereEntityNotThru();
    }

    @GetMapping("/entityWStatus/entityStatusNotThru")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusWhereEntityStatusNotThru() {
        return entityRepo.getAllEntitesWithStatusWhereEntityStatusNotThru();
    }

    @GetMapping("/entityWStatus/entityId/{id}")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusByEntityId(@PathVariable("id") Integer entityId) {
        return entityRepo.getAllEntitesWithStatusByEntityId(entityId);
    }

    @GetMapping("/entityWStatus/statusTypeId/{id}")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusByStatusTypeId(@PathVariable("id") Integer typeId) {
        return entityRepo.getAllEntitesWithStatusByStatusId(typeId);
    }

    @GetMapping("/entityWStatus/statusTypeName/{name}")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusByStatusTypeName(@PathVariable("name") String typeName) {
        return entityRepo.getAllEntitesWithStatusByStatusTypeName(typeName);
    }

    @GetMapping("/entityWStatus/entityFromDate/{startDate}/{endDate}")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusByEntityFromDate(@PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return entityRepo.getAllEntitesWithStatusByFromDateBetween(startDate, endDate);
    }

    @GetMapping("/entityWStatus/entityStatusFromDate/{startDate}/{endDate}")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusByEntityStatusFromDate(@PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return entityRepo.getAllEntitesWithStatusByStatusFromDateBetween(startDate, endDate);
    }

    @GetMapping("/entityWStatus/statusDatetime/{startTime}/{endTime}")
    public List<SLThreeEntityStatusViewIProjection> getAllEntitiesWithStatusByEntityStatusDateTime(@PathVariable("startTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startTime, @PathVariable("endTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endTime) {
        return entityRepo.getAllEntitesWithStatusByStatusDateTimeBetween(startTime, endTime);
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }

}
