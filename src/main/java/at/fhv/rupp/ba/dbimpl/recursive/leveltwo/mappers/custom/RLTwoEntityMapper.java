package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityThreeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityThree;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntity;

import java.util.LinkedList;
import java.util.List;

public class RLTwoEntityMapper {
    private RLTwoEntityMapper() {
        //hidden constructor
    }

    public static RLTwoEntityDTO toDTO(RLTwoEntity entity) {
        RLTwoEntityDTO dto = new RLTwoEntityDTO();
        dto.setId(entity.getRlTwoEntityId());
        if (entity.getRlTwoEntityParent() != null) {
            dto.setParent(toDTO(entity.getRlTwoEntityParent()));
        }
        if (entity.getRlTwoEntityType() != null) {
            dto.setType(RLTwoEntityTypeMapper.toDTO(entity.getRlTwoEntityType()));
        }
        return dto;
    }

    public static RLTwoEntity toEntity(RLTwoEntityDTO dto) {
        RLTwoEntity entity = new RLTwoEntity();
        if (dto.getId() != null) {
            entity.setRlTwoEntityId(dto.getId());
        }
        if (dto.getParent() != null) {
            entity.setRlTwoEntityParent(toEntity(dto.getParent()));
        }
        if (dto.getType() != null) {
            entity.setRlTwoEntityType(RLTwoEntityTypeMapper.toEntity(dto.getType()));
        }
        return entity;
    }

    public static List<RLTwoEntityDTO> toDTOs(List<RLTwoEntity> entities) {
        LinkedList<RLTwoEntityDTO> result = new LinkedList<>();
        for (RLTwoEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLTwoEntity> toEntities(List<RLTwoEntityDTO> dtos) {
        LinkedList<RLTwoEntity> result = new LinkedList<>();
        for (RLTwoEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
