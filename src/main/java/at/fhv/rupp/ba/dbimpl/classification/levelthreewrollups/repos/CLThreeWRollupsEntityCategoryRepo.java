package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CLThreeWRollupsEntityCategoryRepo extends JpaRepository<CLThreeWRollupsEntityCategory, Integer> {
}
