package at.fhv.rupp.ba.dbimpl.classification.levelone.mappers.custom;


import at.fhv.rupp.ba.dbimpl.classification.levelone.dtos.CLOneEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelone.entities.CLOneEntity;

import java.util.LinkedList;
import java.util.List;

public class CLOneEntityMapper {

    private CLOneEntityMapper() {
        //empty private constructor to hide public one
    }

    public static CLOneEntityDTO toDTO(CLOneEntity entity) {
        CLOneEntityDTO dto = new CLOneEntityDTO();
        dto.setId(entity.getCloneEntityID());
        dto.setName(entity.getCloneEntityName());
        dto.setTypeOne(entity.getCloneEntityTypeOne());
        dto.setTypeTwo(entity.getCloneEntityTypeTwo());
        dto.setTypeThree(entity.getCloneEntityTypeThree());
        return dto;
    }

    public static List<CLOneEntityDTO> toDTOs(List<CLOneEntity> list) {
        List<CLOneEntityDTO> resultList = new LinkedList<>();
        for (CLOneEntity entity : list) {
            resultList.add(toDTO(entity));
        }
        return resultList;
    }

    public static CLOneEntity toEntity(CLOneEntityDTO dto) {
        CLOneEntity entity = new CLOneEntity();
        if (dto.getId() != null) {
            entity.setCloneEntityID(dto.getId());
        }
        entity.setCloneEntityName(dto.getName());
        entity.setCloneEntityTypeOne(dto.getTypeOne());
        if (dto.getTypeTwo() != null) {
            entity.setCloneEntityTypeTwo(dto.getTypeTwo());
        }
        entity.setCloneEntityTypeThree(dto.getTypeThree());
        return entity;
    }

    public static List<CLOneEntity> toEntities(List<CLOneEntityDTO> dtoList) {
        List<CLOneEntity> resultList = new LinkedList<>();
        for (CLOneEntityDTO dto : dtoList) {
            resultList.add(toEntity(dto));
        }
        return resultList;
    }
}
