package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryTypeSchemeDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategoryTypeScheme;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = CLThreeWRollupsDataProviderMapstructMapper.class)
public interface CLThreeWRollupsEntityCategoryTypeSchemeMapstructMapper {

    @Mapping(source = "clthreewrEntityCategoryTypeSchemeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeSchemeName", target = "name")
    @Mapping(source = "schemeDataProvider", target = "dataProvider")
    CLThreeWRollupsEntityCategoryTypeSchemeDTO toDTO(CLThreeWRollupsEntityCategoryTypeScheme entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsEntityCategoryTypeScheme toEntity(CLThreeWRollupsEntityCategoryTypeSchemeDTO dto);

    @Mapping(source = "clthreewrEntityCategoryTypeSchemeId", target = "id")
    @Mapping(source = "clthreewrEntityCategoryTypeSchemeName", target = "name")
    @Mapping(source = "schemeDataProvider", target = "dataProvider")
    void updateEntityFromDTO(CLThreeWRollupsEntityCategoryTypeScheme entity, @MappingTarget CLThreeWRollupsEntityCategoryTypeSchemeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsEntityCategoryTypeSchemeDTO dto, @MappingTarget CLThreeWRollupsEntityCategoryTypeScheme entity);

    List<CLThreeWRollupsEntityCategoryTypeSchemeDTO> toDTOs(List<CLThreeWRollupsEntityCategoryTypeScheme> entities);

    List<CLThreeWRollupsEntityCategoryTypeScheme> toEntities(List<CLThreeWRollupsEntityCategoryTypeSchemeDTO> dtos);
}
