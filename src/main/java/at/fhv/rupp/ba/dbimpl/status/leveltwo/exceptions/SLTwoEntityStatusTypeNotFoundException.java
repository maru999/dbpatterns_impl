package at.fhv.rupp.ba.dbimpl.status.leveltwo.exceptions;

public class SLTwoEntityStatusTypeNotFoundException extends Exception {
    public SLTwoEntityStatusTypeNotFoundException(String message) {
        super(message);
    }
}
