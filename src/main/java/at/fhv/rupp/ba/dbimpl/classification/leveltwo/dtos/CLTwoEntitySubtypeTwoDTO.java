package at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos;

public class CLTwoEntitySubtypeTwoDTO extends CLTwoEntityDTO {
    private String attr;

    public String getAttr() {
        return attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }
}
