package at.fhv.rupp.ba.dbimpl.core;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ba/core")
public class CoreController {

    @GetMapping("/RESET")
    public void resetAllTables() {
        ScriptExecutor.runAllScripts();
        DbimplApplication.restart();
    }

    @GetMapping("/RESTART")
    public void restartApplication() {
        DbimplApplication.restart();
    }

    @GetMapping("/STOP")
    public void exitApplication() {
        DbimplApplication.close();
    }
}
