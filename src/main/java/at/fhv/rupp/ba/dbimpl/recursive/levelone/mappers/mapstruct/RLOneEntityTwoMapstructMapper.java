package at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityTwoDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityTwo;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = RLOneEntityOneMapstructMapper.class)
public interface RLOneEntityTwoMapstructMapper {
    @Mapping(source = "rloneEntityTwoId", target = "id")
    @Mapping(source = "rloneEntityTwoName", target = "name")
    @Mapping(source = "rloneEntityTwoParent", target = "parent")
    RLOneEntityTwoDTO toDTO(RLOneEntityTwo entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLOneEntityTwo toEntity(RLOneEntityTwoDTO dto);

    @Mapping(source = "rloneEntityTwoId", target = "id")
    @Mapping(source = "rloneEntityTwoName", target = "name")
    @Mapping(source = "rloneEntityTwoParent", target = "parent")
    void updateEntityFromDTO(RLOneEntityTwo entity, @MappingTarget RLOneEntityTwoDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLOneEntityTwoDTO dto, @MappingTarget RLOneEntityTwo entity);

    List<RLOneEntityTwoDTO> toDTOs(List<RLOneEntityTwo> entities);

    List<RLOneEntityTwo> toEntities(List<RLOneEntityTwoDTO> dtos);
}
