package at.fhv.rupp.ba.dbimpl.classification.leveltwo.exceptions;

public class CLTwoEntityTypeTwoNotFoundException extends Exception {
    public CLTwoEntityTypeTwoNotFoundException(String message) {
        super(message);
    }
}
