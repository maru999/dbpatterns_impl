package at.fhv.rupp.ba.dbimpl.status.levelthree.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface SLThreeEntityStatusViewIProjection {
    Integer getslthree_status_type_id();

    String getslthree_status_type_name();

    Integer getslthree_entity_id();

    Integer getslthree_entity_status_id();

    LocalDateTime getslthree_entity_status_date_time();

    LocalDate getslthree_entity_status_status_from_date();

    LocalDate getslthree_entity_status_status_thru_date();

    LocalDate getslthree_entity_status_from_date();

    LocalDate getslthree_entity_status_thru_date();

    Integer getslthree_entity_status_type_fk();

    Integer getslthree_entity_status_entity_fk();
}
