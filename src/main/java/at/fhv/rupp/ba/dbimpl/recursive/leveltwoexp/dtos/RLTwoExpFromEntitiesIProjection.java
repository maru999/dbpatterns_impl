package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos;

public interface RLTwoExpFromEntitiesIProjection {
    Integer getrltwoxp_entity_type_id();

    String getrltwoxp_entity_type_name();
}
