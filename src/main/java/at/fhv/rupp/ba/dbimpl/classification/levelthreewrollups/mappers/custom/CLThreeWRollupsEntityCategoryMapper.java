package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsEntityCategoryDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsEntityCategory;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsEntityCategoryMapper {

    private CLThreeWRollupsEntityCategoryMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsEntityCategoryDTO toDTO(CLThreeWRollupsEntityCategory entity) {
        CLThreeWRollupsEntityCategoryDTO dto = new CLThreeWRollupsEntityCategoryDTO();
        dto.setId(entity.getClthreewrEntityCategoryId());
        dto.setName(entity.getClthreewrEntityCategoryName());
        if (entity.getEntityCategoryType() != null) {
            dto.setType(CLThreeWRollupsEntityCategoryTypeMapper.toDTO(entity.getEntityCategoryType()));
        }
        return dto;
    }

    public static CLThreeWRollupsEntityCategory toEntity(CLThreeWRollupsEntityCategoryDTO dto) {
        CLThreeWRollupsEntityCategory entity = new CLThreeWRollupsEntityCategory();
        if (dto.getId() != null) {
            entity.setClthreewrEntityCategoryId(dto.getId());
        }
        entity.setClthreewrEntityCategoryName(dto.getName());
        if (dto.getType() != null) {
            entity.setEntityCategoryType(CLThreeWRollupsEntityCategoryTypeMapper.toEntity(dto.getType()));
        }
        return entity;
    }

    public static List<CLThreeWRollupsEntityCategoryDTO> toDTOs(List<CLThreeWRollupsEntityCategory> entities) {
        LinkedList<CLThreeWRollupsEntityCategoryDTO> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategory entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsEntityCategory> toEntities(List<CLThreeWRollupsEntityCategoryDTO> dtos) {
        LinkedList<CLThreeWRollupsEntityCategory> result = new LinkedList<>();
        for (CLThreeWRollupsEntityCategoryDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
