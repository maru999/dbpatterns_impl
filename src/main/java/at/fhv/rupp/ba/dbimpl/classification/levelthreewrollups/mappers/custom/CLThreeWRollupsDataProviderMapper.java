package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsDataProviderDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsDataProvider;

import java.util.LinkedList;
import java.util.List;

public class CLThreeWRollupsDataProviderMapper {

    private CLThreeWRollupsDataProviderMapper() {
        //hidden constructor
    }

    public static CLThreeWRollupsDataProviderDTO toDTO(CLThreeWRollupsDataProvider entity) {
        CLThreeWRollupsDataProviderDTO dto = new CLThreeWRollupsDataProviderDTO();
        dto.setId(entity.getClthreewrPartyRoleId());
        dto.setName(entity.getClthreewrDataProviderName());
        dto.setRoleName(entity.getRoleName());
        return dto;
    }

    public static CLThreeWRollupsDataProvider toEntity(CLThreeWRollupsDataProviderDTO dto) {
        CLThreeWRollupsDataProvider entity = new CLThreeWRollupsDataProvider();
        if (dto.getId() != null) {
            entity.setClthreewrPartyRoleId(dto.getId());
        }
        entity.setRoleName(dto.getRoleName());
        entity.setClthreewrDataProviderName(dto.getName());
        return entity;
    }

    public static List<CLThreeWRollupsDataProviderDTO> toDTOs(List<CLThreeWRollupsDataProvider> entities) {
        LinkedList<CLThreeWRollupsDataProviderDTO> result = new LinkedList<>();
        for (CLThreeWRollupsDataProvider entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeWRollupsDataProvider> toEntities(List<CLThreeWRollupsDataProviderDTO> dtos) {
        LinkedList<CLThreeWRollupsDataProvider> result = new LinkedList<>();
        for (CLThreeWRollupsDataProviderDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
