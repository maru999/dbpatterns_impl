package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeTwoDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeTwo;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLTwoEntityTypeOneMapstructMapper.class, CLTwoEntityTypeTwoMapstructMapper.class})
public interface CLTwoEntitySubtypeTwoMapstructMapper {

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeTwoAttribute", target = "attr")
    CLTwoEntitySubtypeTwoDTO toDTO(CLTwoEntitySubtypeTwo entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLTwoEntitySubtypeTwo toEntity(CLTwoEntitySubtypeTwoDTO dto);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeTwoAttribute", target = "attr")
    void updateEntityFromDTO(CLTwoEntitySubtypeTwo entity, @MappingTarget CLTwoEntitySubtypeTwoDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLTwoEntitySubtypeTwoDTO dto, @MappingTarget CLTwoEntitySubtypeTwo entity);

    List<CLTwoEntitySubtypeTwoDTO> toDTOs(List<CLTwoEntitySubtypeTwo> entities);

    List<CLTwoEntitySubtypeTwo> toEntities(List<CLTwoEntitySubtypeTwoDTO> dtos);
}
