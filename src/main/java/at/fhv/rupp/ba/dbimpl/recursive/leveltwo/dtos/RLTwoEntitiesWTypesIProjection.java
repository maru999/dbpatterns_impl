package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos;

public interface RLTwoEntitiesWTypesIProjection {
    String getrltwo_entity_type_name();

    String getrltwo_entity_id();
}
