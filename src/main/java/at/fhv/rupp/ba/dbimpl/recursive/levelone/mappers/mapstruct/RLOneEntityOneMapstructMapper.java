package at.fhv.rupp.ba.dbimpl.recursive.levelone.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.levelone.dtos.RLOneEntityOneDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelone.entities.RLOneEntityOne;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLOneEntityOneMapstructMapper {
    @Mapping(source = "rloneEntityOneId", target = "id")
    @Mapping(source = "rloneEntityOneName", target = "name")
    RLOneEntityOneDTO toDTO(RLOneEntityOne entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLOneEntityOne toEntity(RLOneEntityOneDTO dto);

    @Mapping(source = "rloneEntityOneId", target = "id")
    @Mapping(source = "rloneEntityOneName", target = "name")
    void updateEntityFromDTO(RLOneEntityOne entity, @MappingTarget RLOneEntityOneDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLOneEntityOneDTO dto, @MappingTarget RLOneEntityOne entity);

    List<RLOneEntityOneDTO> toDTOs(List<RLOneEntityOne> entities);

    List<RLOneEntityOne> toEntities(List<RLOneEntityOneDTO> dtos);
}
