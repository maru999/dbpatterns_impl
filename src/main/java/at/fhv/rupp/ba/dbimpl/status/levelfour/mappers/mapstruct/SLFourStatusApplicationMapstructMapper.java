package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourStatusApplicationDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourStatusApplication;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {SLFourStatusTypeMapstructMapper.class, SLFourEntityOneMapstructMapper.class, SLFourEntityTwoMapstructMapper.class, SLFourEntityThreeMapstructMapper.class})
public interface SLFourStatusApplicationMapstructMapper {

    @Mapping(source = "slFourStatusApplicationId", target = "id")
    @Mapping(source = "slFourStatusApplicationDateTime", target = "dateTime")
    @Mapping(source = "slFourStatusApplicationFrom", target = "from")
    @Mapping(source = "slFourStatusApplicationThru", target = "thru")
    @Mapping(source = "slFourStatusApplicationStatusFrom", target = "statusFrom")
    @Mapping(source = "slFourStatusApplicationStatusThru", target = "statusThru")
    @Mapping(source = "slFourStatusApplicationType", target = "type")
    @Mapping(source = "slFourStatusApplicationEntityOne", target = "entityOne")
    @Mapping(source = "slFourStatusApplicationEntityTwo", target = "entityTwo")
    @Mapping(source = "slFourStatusApplicationEntityThree", target = "entityThree")
    SLFourStatusApplicationDTO toDTO(SLFourStatusApplication entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLFourStatusApplication toEntity(SLFourStatusApplicationDTO dto);

    @Mapping(source = "slFourStatusApplicationId", target = "id")
    @Mapping(source = "slFourStatusApplicationDateTime", target = "dateTime")
    @Mapping(source = "slFourStatusApplicationFrom", target = "from")
    @Mapping(source = "slFourStatusApplicationThru", target = "thru")
    @Mapping(source = "slFourStatusApplicationStatusFrom", target = "statusFrom")
    @Mapping(source = "slFourStatusApplicationStatusThru", target = "statusThru")
    @Mapping(source = "slFourStatusApplicationType", target = "type")
    @Mapping(source = "slFourStatusApplicationEntityOne", target = "entityOne")
    @Mapping(source = "slFourStatusApplicationEntityTwo", target = "entityTwo")
    @Mapping(source = "slFourStatusApplicationEntityThree", target = "entityThree")
    void updateEntityFromDTO(SLFourStatusApplication entity, @MappingTarget SLFourStatusApplicationDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLFourStatusApplicationDTO dto, @MappingTarget SLFourStatusApplication entity);

    List<SLFourStatusApplicationDTO> toDTOs(List<SLFourStatusApplication> entities);

    List<SLFourStatusApplication> toEntities(List<SLFourStatusApplicationDTO> dtos);
}
