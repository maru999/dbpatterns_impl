package at.fhv.rupp.ba.dbimpl.status.levelfour.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelfour.dtos.SLFourStatusApplicationDTO;
import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourStatusApplication;
import at.fhv.rupp.ba.dbimpl.status.levelfour.exceptions.SLFourXORConstraintViolationException;

import java.util.LinkedList;
import java.util.List;

public class SLFourStatusApplicationMapper {

    private SLFourStatusApplicationMapper() {
        //hidden constructor
    }

    public static SLFourStatusApplicationDTO toDTO(SLFourStatusApplication entity) throws SLFourXORConstraintViolationException {
        SLFourStatusApplicationDTO dto = new SLFourStatusApplicationDTO();
        dto.setId(entity.getSlFourStatusApplicationId());
        dto.setFrom(entity.getSlFourStatusApplicationFrom());
        if (entity.getSlFourStatusApplicationDateTime() != null) {
            dto.setDateTime(entity.getSlFourStatusApplicationDateTime());
        }
        if (entity.getSlFourStatusApplicationStatusFrom() != null) {
            dto.setStatusFrom(entity.getSlFourStatusApplicationStatusFrom());
        }
        if (entity.getSlFourStatusApplicationStatusThru() != null) {
            dto.setStatusThru(entity.getSlFourStatusApplicationStatusThru());
        }
        if (entity.getSlFourStatusApplicationThru() != null) {
            dto.setThru(entity.getSlFourStatusApplicationThru());
        }
        if (entity.getSlFourStatusApplicationType() != null) {
            dto.setType(SLFourStatusTypeMapper
                    .toDTO(entity.getSlFourStatusApplicationType()));
        }
        if (entity.getSlFourStatusApplicationEntityOne() != null) {
            dto.setEntityOne(SLFourEntityOneMapper
                    .toDTO(entity.getSlFourStatusApplicationEntityOne()));
            dto.setEntityTwo(null);
            dto.setEntityThree(null);
        } else if (entity.getSlFourStatusApplicationEntityTwo() != null) {
            dto.setEntityTwo(SLFourEntityTwoMapper
                    .toDTO(entity.getSlFourStatusApplicationEntityTwo()));
            dto.setEntityOne(null);
            dto.setEntityThree(null);
        } else if (entity.getSlFourStatusApplicationEntityThree() != null) {
            dto.setEntityThree(SLFourEntityThreeMapper
                    .toDTO(entity.getSlFourStatusApplicationEntityThree()));
            dto.setEntityOne(null);
            dto.setEntityTwo(null);
        }
        else if(entity.getSlFourStatusApplicationEntityOne() == null
                && entity.getSlFourStatusApplicationEntityTwo() == null
                && entity.getSlFourStatusApplicationEntityThree() == null)
            throw new SLFourXORConstraintViolationException(
                    "SLFourStatusApplication must have exactly one valid Entity!");
        return dto;
    }

    public static SLFourStatusApplication toEntity(SLFourStatusApplicationDTO dto) throws SLFourXORConstraintViolationException {
        SLFourStatusApplication entity = new SLFourStatusApplication();
        if (dto.getId() != null) {
            entity.setSlFourStatusApplicationId(dto.getId());
        }
        if (dto.getDateTime() != null) {
            entity.setSlFourStatusApplicationDateTime(dto.getDateTime());
        }
        if (dto.getStatusFrom() != null) {
            entity.setSlFourStatusApplicationStatusFrom(dto.getStatusFrom());
        }
        if (dto.getStatusThru() != null) {
            entity.setSlFourStatusApplicationStatusThru(dto.getStatusThru());
        }
        if (dto.getFrom() != null) {
            entity.setSlFourStatusApplicationFrom(dto.getFrom());
        }
        if (dto.getThru() != null) {
            entity.setSlFourStatusApplicationThru(dto.getThru());
        }
        if (dto.getType() != null) {
            entity.setSlFourStatusApplicationType(SLFourStatusTypeMapper.toEntity(dto.getType()));
        }
        if (dto.getEntityOne() != null) {
            entity.setSlFourStatusApplicationEntityOne(SLFourEntityOneMapper
                    .toEntity(dto.getEntityOne()));
            entity.setSlFourStatusApplicationEntityTwo(null);
            entity.setSlFourStatusApplicationEntityThree(null);
        } else if (dto.getEntityTwo() != null) {
            entity.setSlFourStatusApplicationEntityTwo(SLFourEntityTwoMapper
                    .toEntity(dto.getEntityTwo()));
            entity.setSlFourStatusApplicationEntityOne(null);
            entity.setSlFourStatusApplicationEntityThree(null);
        } else if (dto.getEntityThree() != null) {
            entity.setSlFourStatusApplicationEntityThree(SLFourEntityThreeMapper
                    .toEntity(dto.getEntityThree()));
            entity.setSlFourStatusApplicationEntityOne(null);
            entity.setSlFourStatusApplicationEntityTwo(null);
        } else if(dto.getEntityOne() == null && dto.getEntityTwo() == null && dto.getEntityThree() == null) throw new SLFourXORConstraintViolationException("SLFourStatusApplication must have exactly one valid Entity!");
        return entity;
    }

    public static List<SLFourStatusApplicationDTO> toDTOs(List<SLFourStatusApplication> entities) {
        LinkedList<SLFourStatusApplicationDTO> result = new LinkedList<>();
        for (SLFourStatusApplication entity : entities) {
            try {
                result.add(toDTO(entity));
            } catch (SLFourXORConstraintViolationException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static List<SLFourStatusApplication> toEntities(List<SLFourStatusApplicationDTO> dtos) {
        LinkedList<SLFourStatusApplication> result = new LinkedList<>();
        for (SLFourStatusApplicationDTO dto : dtos) {
            try {
                result.add(toEntity(dto));
            } catch (SLFourXORConstraintViolationException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
