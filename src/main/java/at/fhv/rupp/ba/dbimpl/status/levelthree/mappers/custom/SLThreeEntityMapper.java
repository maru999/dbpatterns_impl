package at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntity;

import java.util.LinkedList;
import java.util.List;

public class SLThreeEntityMapper {

    private SLThreeEntityMapper() {
        //hidden constructor
    }

    public static SLThreeEntityDTO toDTO(SLThreeEntity entity) {
        SLThreeEntityDTO dto = new SLThreeEntityDTO();
        dto.setId(entity.getSlThreeEntityId());
        return dto;
    }

    public static SLThreeEntity toEntity(SLThreeEntityDTO dto) {
        SLThreeEntity entity = new SLThreeEntity();
        if (dto.getId() != null) {
            entity.setSlThreeEntityId(dto.getId());
        }
        return entity;
    }

    public static List<SLThreeEntityDTO> toDTOs(List<SLThreeEntity> entities) {
        LinkedList<SLThreeEntityDTO> result = new LinkedList<>();
        for (SLThreeEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<SLThreeEntity> toEntities(List<SLThreeEntityDTO> dtos) {
        LinkedList<SLThreeEntity> result = new LinkedList<>();
        for (SLThreeEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
