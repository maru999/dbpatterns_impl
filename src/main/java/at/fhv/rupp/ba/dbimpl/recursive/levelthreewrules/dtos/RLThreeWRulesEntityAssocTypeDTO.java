package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.dtos;

public class RLThreeWRulesEntityAssocTypeDTO {
    private Integer id;
    private String name;
    private RLThreeWRulesEntityAssocTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RLThreeWRulesEntityAssocTypeDTO getParent() {
        return parent;
    }

    public void setParent(RLThreeWRulesEntityAssocTypeDTO parent) {
        this.parent = parent;
    }
}
