package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

public class CLThreeWRollupsEntityCategoryTypeRollupTypeDTO {
    private Integer id;
    private String name;
    private CLThreeWRollupsEntityCategoryTypeRollupTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLThreeWRollupsEntityCategoryTypeRollupTypeDTO getParent() {
        return parent;
    }

    public void setParent(CLThreeWRollupsEntityCategoryTypeRollupTypeDTO parent) {
        this.parent = parent;
    }
}
