package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityTypeTwoDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntityTypeTwo;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLTwoEntityTypeTwoMapstructMapper {

    @Mapping(source = "clTwoEntityTypeTwoId", target = "id")
    @Mapping(source = "clTwoEntityTypeTwoName", target = "name")
    @Mapping(source = "clTwoEntityTypeTwoParent", target = "parentType")
    CLTwoEntityTypeTwoDTO toDTO(CLTwoEntityTypeTwo entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLTwoEntityTypeTwo toEntity(CLTwoEntityTypeTwoDTO dto);

    @Mapping(source = "clTwoEntityTypeTwoId", target = "id")
    @Mapping(source = "clTwoEntityTypeTwoName", target = "name")
    @Mapping(source = "clTwoEntityTypeTwoParent", target = "parentType")
    void updateEntityFromDTO(CLTwoEntityTypeTwo entity, @MappingTarget CLTwoEntityTypeTwoDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLTwoEntityTypeTwoDTO dto, @MappingTarget CLTwoEntityTypeTwo entity);

    List<CLTwoEntityTypeTwoDTO> toDTOs(List<CLTwoEntityTypeTwo> entities);

    List<CLTwoEntityTypeTwo> toEntities(List<CLTwoEntityTypeTwoDTO> dtos);
}
