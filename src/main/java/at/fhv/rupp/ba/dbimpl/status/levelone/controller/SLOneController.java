package at.fhv.rupp.ba.dbimpl.status.levelone.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.status.levelone.datagen.SLOneJavaFaker;
import at.fhv.rupp.ba.dbimpl.status.levelone.dtos.SLOneEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.levelone.entities.SLOneEntity;
import at.fhv.rupp.ba.dbimpl.status.levelone.exceptions.SLOneEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.status.levelone.mappers.custom.SLOneEntityMapper;
import at.fhv.rupp.ba.dbimpl.status.levelone.repos.SLOneEntityRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/status/L1")
public class SLOneController {

    private final SLOneEntityRepo repository;
    private final DataGenJavaFaker faker;

    public SLOneController(SLOneEntityRepo repository) {
        this.repository = repository;
        this.faker = new SLOneJavaFaker(repository);
    }

    @GetMapping("/entities")
    public List<SLOneEntityDTO> getAllEntities() {
        return SLOneEntityMapper.toDTOs(repository.findAll());
    }

    @GetMapping("/entities/{id}")
    public ResponseEntity<SLOneEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws SLOneEntityNotFoundException {
        SLOneEntity entity = repository.findById(entityId).orElseThrow(() -> new SLOneEntityNotFoundException(getExceptionString("SLOneEntity", entityId)));
        return ResponseEntity.ok().body(SLOneEntityMapper.toDTO(entity));
    }

    @PostMapping("/entities")
    public SLOneEntityDTO createEntity(@Valid @RequestBody SLOneEntityDTO entityDTO) {
        return SLOneEntityMapper.toDTO(repository.save(SLOneEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<SLOneEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody SLOneEntityDTO entityDetails) throws SLOneEntityNotFoundException {
        SLOneEntity entity = repository.findById(entityId).orElseThrow(() -> new SLOneEntityNotFoundException(getExceptionString("SLOneEntity", entityId)));
        SLOneEntityDTO entityDTO = SLOneEntityMapper.toDTO(entity);
        entityDTO.setEventOneDateTime(entityDetails.getEventOneDateTime());
        entityDTO.setEventTwoDateTime(entityDetails.getEventTwoDateTime());
        entityDTO.setEventThreeDateTime(entityDetails.getEventThreeDateTime());
        entityDTO.setEventIndicator(entityDetails.getEventIndicator());
        entityDTO.setEventFromDate(entityDetails.getEventFromDate());
        entityDTO.setEventThruDate(entityDetails.getEventThruDate());
        final SLOneEntityDTO updatedEntity = SLOneEntityMapper.toDTO(repository.save(SLOneEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws SLOneEntityNotFoundException {
        SLOneEntity entity = repository.findById(entityId).orElseThrow(() -> new SLOneEntityNotFoundException(getExceptionString("SLOneEntity", entityId)));
        repository.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/status/statuslevelone.sql");
        DbimplApplication.restart();
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }

}
