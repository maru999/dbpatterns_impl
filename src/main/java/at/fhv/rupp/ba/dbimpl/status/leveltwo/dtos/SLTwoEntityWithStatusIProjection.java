package at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos;

import java.time.LocalDateTime;

public interface SLTwoEntityWithStatusIProjection {
    Integer getsltwo_status_type_id();

    String getsltwo_status_type_name();

    Integer getsltwo_entity_status_type_id();

    String getsltwo_entity_status_type_some_attr();

    Integer getsltwo_entity_id();

    LocalDateTime getsltwo_entity_status_datetime();

    Integer getsltwo_entity_status_type_fk();
}
