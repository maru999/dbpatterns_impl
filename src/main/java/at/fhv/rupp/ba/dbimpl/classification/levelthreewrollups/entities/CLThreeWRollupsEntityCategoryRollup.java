package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "clthreewr_entity_category_rollup")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategoryRollup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_rollup_id")
    private int clthreewrEntityCategoryRollupId;

    @Column(name = "clthreewr_entity_category_rollup_from", nullable = false)
    private LocalDate clthreewrEntityCategoryRollupFromDate;

    @Column(name = "clthreewr_entity_category_rollup_thru")
    private LocalDate clthreewrEntityCategoryRollupThruDate;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_rollup_parent_entity_category_id")
    private CLThreeWRollupsEntityCategory clthreewrEntityCategoryRollupParentCategory;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_rollup_child_entity_category_id")
    private CLThreeWRollupsEntityCategory clthreewrEntityCategoryRollupChildCategory;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_rollup_entity_category_rollup_type_id")
    private CLThreeWRollupsEntityCategoryRollupType clthreeEntityCategoryRollupRollupType;

    public int getClthreewrEntityCategoryRollupId() {
        return clthreewrEntityCategoryRollupId;
    }

    public void setClthreewrEntityCategoryRollupId(int clthreewrEntityCategoryRollupId) {
        this.clthreewrEntityCategoryRollupId = clthreewrEntityCategoryRollupId;
    }

    public LocalDate getClthreewrEntityCategoryRollupFromDate() {
        return clthreewrEntityCategoryRollupFromDate;
    }

    public void setClthreewrEntityCategoryRollupFromDate(LocalDate clthreewrEntityCategoryRollupFromDate) {
        this.clthreewrEntityCategoryRollupFromDate = clthreewrEntityCategoryRollupFromDate;
    }

    public LocalDate getClthreewrEntityCategoryRollupThruDate() {
        return clthreewrEntityCategoryRollupThruDate;
    }

    public void setClthreewrEntityCategoryRollupThruDate(LocalDate clthreewrEntityCategoryRollupThruDate) {
        this.clthreewrEntityCategoryRollupThruDate = clthreewrEntityCategoryRollupThruDate;
    }

    public CLThreeWRollupsEntityCategory getClthreewrEntityCategoryRollupParentCategory() {
        return clthreewrEntityCategoryRollupParentCategory;
    }

    public void setClthreewrEntityCategoryRollupParentCategory(CLThreeWRollupsEntityCategory clthreewrEntityCategoryRollupParentCategory) {
        this.clthreewrEntityCategoryRollupParentCategory = clthreewrEntityCategoryRollupParentCategory;
    }

    public CLThreeWRollupsEntityCategory getClthreewrEntityCategoryRollupChildCategory() {
        return clthreewrEntityCategoryRollupChildCategory;
    }

    public void setClthreewrEntityCategoryRollupChildCategory(CLThreeWRollupsEntityCategory clthreewrEntityCategoryRollupChildCategory) {
        this.clthreewrEntityCategoryRollupChildCategory = clthreewrEntityCategoryRollupChildCategory;
    }

    public CLThreeWRollupsEntityCategoryRollupType getClthreeEntityCategoryRollupRollupType() {
        return clthreeEntityCategoryRollupRollupType;
    }

    public void setClthreeEntityCategoryRollupRollupType(CLThreeWRollupsEntityCategoryRollupType clthreeEntityCategoryRollupRollupType) {
        this.clthreeEntityCategoryRollupRollupType = clthreeEntityCategoryRollupRollupType;
    }
}
