package at.fhv.rupp.ba.dbimpl.status.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntityStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SLThreeEntityStatusRepo extends JpaRepository<SLThreeEntityStatus, Integer> {
    @Query(value = "SELECT * FROM slthree_entity_status WHERE slthree_entity_status_entity_fk = :id", nativeQuery = true)
    List<SLThreeEntityStatus> getEntityStatusByEntityId(@Param("id") Integer entityId);
}
