package at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions;

public class CLThreeEntityCategoryNotFoundException extends Exception {
    public CLThreeEntityCategoryNotFoundException(String message) {
        super(message);
    }
}
