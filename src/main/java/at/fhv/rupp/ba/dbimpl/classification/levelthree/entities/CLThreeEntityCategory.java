package at.fhv.rupp.ba.dbimpl.classification.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "clthree_entity_category")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeEntityCategory {
    //In Entity Category
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int clthreeEntityCategoryId;

    @Column(name = "clthree_entity_category_name", nullable = false)
    private String clthreeEntityCategoryName;

    @Column(name = "clthree_entity_category_from", nullable = false)
    private LocalDate clthreeEntityCategoryFromDate;

    @Column(name = "clthree_entity_category_thru")
    private LocalDate clthreeEntityCategoryThruDate;

    @ManyToOne(fetch = FetchType.EAGER) //Recursive
    @JoinColumn(name="clthree_entity_category_parent_category")
    private CLThreeEntityCategory clThreeEntityCategoryParent;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="clthree_entity_category_category_type")
    private CLThreeEntityCategoryType clThreeEntityCategoryType;

    public int getClthreeEntityCategoryId() {
        return clthreeEntityCategoryId;
    }

    public void setClthreeEntityCategoryId(int clthreeEntityCategoryId) {
        this.clthreeEntityCategoryId = clthreeEntityCategoryId;
    }

    public String getClthreeEntityCategoryName() {
        return clthreeEntityCategoryName;
    }

    public void setClthreeEntityCategoryName(String clthreeEntityCategoryName) {
        this.clthreeEntityCategoryName = clthreeEntityCategoryName;
    }

    public LocalDate getClthreeEntityCategoryFromDate() {
        return clthreeEntityCategoryFromDate;
    }

    public void setClthreeEntityCategoryFromDate(LocalDate clthreeEntityCategoryFromDate) {
        this.clthreeEntityCategoryFromDate = clthreeEntityCategoryFromDate;
    }

    public LocalDate getClthreeEntityCategoryThruDate() {
        return clthreeEntityCategoryThruDate;
    }

    public void setClthreeEntityCategoryThruDate(LocalDate clthreeEntityCategoryThruDate) {
        this.clthreeEntityCategoryThruDate = clthreeEntityCategoryThruDate;
    }

    public CLThreeEntityCategory getClThreeEntityCategoryParent() {
        return clThreeEntityCategoryParent;
    }

    public void setClThreeEntityCategoryParent(CLThreeEntityCategory clThreeEntityCategoryParent) {
        this.clThreeEntityCategoryParent = clThreeEntityCategoryParent;
    }

    public CLThreeEntityCategoryType getClThreeEntityCategoryType() {
        return clThreeEntityCategoryType;
    }

    public void setClThreeEntityCategoryType(CLThreeEntityCategoryType clThreeEntityCategoryType) {
        this.clThreeEntityCategoryType = clThreeEntityCategoryType;
    }
}

