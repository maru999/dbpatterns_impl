package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.exceptions;

public class RLThreeWRulesEntityTypeNotFoundException extends Exception {
    public RLThreeWRulesEntityTypeNotFoundException(String message) {
        super(message);
    }
}
