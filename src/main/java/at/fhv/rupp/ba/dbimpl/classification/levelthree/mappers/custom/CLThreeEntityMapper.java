package at.fhv.rupp.ba.dbimpl.classification.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntity;
import at.fhv.rupp.ba.dbimpl.classification.levelthree.dtos.CLThreeEntityDTO;

import java.util.LinkedList;
import java.util.List;

public class CLThreeEntityMapper {

    private CLThreeEntityMapper() {
        //hidden constructor
    }

    public static CLThreeEntityDTO toDTO(CLThreeEntity entity) {
        CLThreeEntityDTO dto = new CLThreeEntityDTO();
        dto.setId(entity.getClthreeEntityId());
        dto.setName(entity.getClthreeEntityName());
        return dto;
    }

    public static CLThreeEntity toEntity(CLThreeEntityDTO dto) {
        CLThreeEntity entity = new CLThreeEntity();
        if (dto.getId() != null) {
            entity.setClthreeEntityId(dto.getId());
        }
        entity.setClthreeEntityName(dto.getName());
        return entity;
    }

    public static List<CLThreeEntityDTO> toDTOs(List<CLThreeEntity> entities) {
        LinkedList<CLThreeEntityDTO> result = new LinkedList<>();
        for (CLThreeEntity entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLThreeEntity> toEntities(List<CLThreeEntityDTO> dtos) {
        LinkedList<CLThreeEntity> result = new LinkedList<>();
        for (CLThreeEntityDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
