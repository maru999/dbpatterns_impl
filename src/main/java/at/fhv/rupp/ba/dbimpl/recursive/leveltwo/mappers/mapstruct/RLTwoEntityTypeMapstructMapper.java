package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntityType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RLTwoEntityTypeMapstructMapper {
    @Mapping(source = "rlTwoEntityTypeId", target = "id")
    @Mapping(source = "rlTwoEntityTypeName", target = "name")
    @Mapping(source = "rlTwoEntityTypeParent", target = "parent")
    RLTwoEntityTypeDTO toDTO(RLTwoEntityType entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLTwoEntityType toEntity(RLTwoEntityTypeDTO dto);

    @Mapping(source = "rlTwoEntityTypeId", target = "id")
    @Mapping(source = "rlTwoEntityTypeName", target = "name")
    @Mapping(source = "rlTwoEntityTypeParent", target = "parent")
    void updateEntityFromDTO(RLTwoEntityType entity, @MappingTarget RLTwoEntityTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLTwoEntityTypeDTO dto, @MappingTarget RLTwoEntityType entity);

    List<RLTwoEntityTypeDTO> toDTOs(List<RLTwoEntityType> entities);

    List<RLTwoEntityType> toEntities(List<RLTwoEntityTypeDTO> dtos);
}
