package at.fhv.rupp.ba.dbimpl.classification.levelthree.exceptions;

public class CLThreeEntityNotFoundException extends Exception {
    public CLThreeEntityNotFoundException(String message) {
        super(message);
    }
}
