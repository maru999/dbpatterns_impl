package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "clthreewr_entity_category_classification")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategoryClassification {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_classification_id")
    private int clthreewrEntityCategoryClassificationId;

    @Column(name = "clthreewr_entity_category_classification_from", nullable = false)
    private LocalDate clthreewrEntityCategoryClassificationFromDate;

    @Column(name = "clthreewr_entity_category_classification_thru")
    private LocalDate clthreewrEntityCategoryClassificationThruDate;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_classification_entity_id")
    private CLThreeWRollupsEntity clthreewrEntityCategoryClassifciationEntity;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_classification_entity_category_id")
    private CLThreeWRollupsEntityCategory clthreewrEntityCategoryClassificationEntityCategory;

    public int getClthreewrEntityCategoryClassificationId() {
        return clthreewrEntityCategoryClassificationId;
    }

    public void setClthreewrEntityCategoryClassificationId(int clthreewrEntityCategoryClassificationId) {
        this.clthreewrEntityCategoryClassificationId = clthreewrEntityCategoryClassificationId;
    }

    public LocalDate getClthreewrEntityCategoryClassificationFromDate() {
        return clthreewrEntityCategoryClassificationFromDate;
    }

    public void setClthreewrEntityCategoryClassificationFromDate(LocalDate clthreewrEntityCategoryClassificationFromDate) {
        this.clthreewrEntityCategoryClassificationFromDate = clthreewrEntityCategoryClassificationFromDate;
    }

    public LocalDate getClthreewrEntityCategoryClassificationThruDate() {
        return clthreewrEntityCategoryClassificationThruDate;
    }

    public void setClthreewrEntityCategoryClassificationThruDate(LocalDate clthreewrEntityCategoryClassificationThruDate) {
        this.clthreewrEntityCategoryClassificationThruDate = clthreewrEntityCategoryClassificationThruDate;
    }

    public CLThreeWRollupsEntity getClthreewrEntityCategoryClassifciationEntity() {
        return clthreewrEntityCategoryClassifciationEntity;
    }

    public void setClthreewrEntityCategoryClassifciationEntity(CLThreeWRollupsEntity clthreewrEntityCategoryClassifciationEntity) {
        this.clthreewrEntityCategoryClassifciationEntity = clthreewrEntityCategoryClassifciationEntity;
    }

    public CLThreeWRollupsEntityCategory getClthreewrEntityCategoryClassificationEntityCategory() {
        return clthreewrEntityCategoryClassificationEntityCategory;
    }

    public void setClthreewrEntityCategoryClassificationEntityCategory(CLThreeWRollupsEntityCategory clthreewrEntityCategoryClassificationEntityCategory) {
        this.clthreewrEntityCategoryClassificationEntityCategory = clthreewrEntityCategoryClassificationEntityCategory;
    }
}
