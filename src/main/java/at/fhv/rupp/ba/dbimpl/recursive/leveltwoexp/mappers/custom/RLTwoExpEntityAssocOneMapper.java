package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityAssocOneDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntityAssocOne;

import java.util.LinkedList;
import java.util.List;

public class RLTwoExpEntityAssocOneMapper {
    private RLTwoExpEntityAssocOneMapper() {
        //hidden constructor
    }

    public static RLTwoExpEntityAssocOneDTO toDTO(RLTwoExpEntityAssocOne entity) {
        RLTwoExpEntityAssocOneDTO dto = new RLTwoExpEntityAssocOneDTO();
        dto.setId(entity.getRlTwoExpEntityAssocOneId());
        dto.setFromDate(entity.getRlTwoExpEntityAssocOneFromDate());
        if (entity.getRlTwoExpEntityAssocOneThruDate() != null) {
            dto.setThruDate(entity.getRlTwoExpEntityAssocOneThruDate());
        }
        if (entity.getRlTwoExpEntityAssocOneFromEntity() != null) {
            dto.setFromEntity(RLTwoExpEntityMapper.toDTO(entity.getRlTwoExpEntityAssocOneFromEntity()));
        }
        if (entity.getRlTwoExpEntityAssocOneToEntity() != null) {
            dto.setToEntity(RLTwoExpEntityMapper.toDTO(entity.getRlTwoExpEntityAssocOneToEntity()));
        }
        return dto;
    }

    public static RLTwoExpEntityAssocOne toEntity(RLTwoExpEntityAssocOneDTO dto) {
        RLTwoExpEntityAssocOne entity = new RLTwoExpEntityAssocOne();
        if (dto.getId() != null) {
            entity.setRlTwoExpEntityAssocOneId(dto.getId());
        }
        if (dto.getFromDate() != null) {
            entity.setRlTwoExpEntityAssocOneFromDate(dto.getFromDate());
        }
        if (dto.getThruDate() != null) {
            entity.setRlTwoExpEntityAssocOneThruDate(dto.getThruDate());
        }
        if (dto.getFromEntity() != null) {
            entity.setRlTwoExpEntityAssocOneFromEntity(RLTwoExpEntityMapper.toEntity(dto.getFromEntity()));
        }
        if (dto.getToEntity() != null) {
            entity.setRlTwoExpEntityAssocOneToEntity(RLTwoExpEntityMapper.toEntity(dto.getToEntity()));
        }
        return entity;
    }

    public static List<RLTwoExpEntityAssocOneDTO> toDTOs(List<RLTwoExpEntityAssocOne> entities) {
        LinkedList<RLTwoExpEntityAssocOneDTO> result = new LinkedList<>();
        for (RLTwoExpEntityAssocOne entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLTwoExpEntityAssocOne> toEntities(List<RLTwoExpEntityAssocOneDTO> dtos) {
        LinkedList<RLTwoExpEntityAssocOne> result = new LinkedList<>();
        for (RLTwoExpEntityAssocOneDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
