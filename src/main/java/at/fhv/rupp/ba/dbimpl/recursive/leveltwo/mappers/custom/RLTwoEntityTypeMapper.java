package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntity;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntityType;

import java.util.LinkedList;
import java.util.List;

public class RLTwoEntityTypeMapper {
    private RLTwoEntityTypeMapper() {
        //hidden constructor
    }

    public static RLTwoEntityTypeDTO toDTO(RLTwoEntityType entity) {
        RLTwoEntityTypeDTO dto = new RLTwoEntityTypeDTO();
        dto.setId(entity.getRlTwoEntityTypeId());
        dto.setName(entity.getRlTwoEntityTypeName());
        if (entity.getRlTwoEntityTypeParent() != null) {
            dto.setParent(toDTO(entity.getRlTwoEntityTypeParent()));
        }
        return dto;
    }

    public static RLTwoEntityType toEntity(RLTwoEntityTypeDTO dto) {
        RLTwoEntityType entity = new RLTwoEntityType();
        if (dto.getId() != null) {
            entity.setRlTwoEntityTypeId(dto.getId());
        }
        entity.setRlTwoEntityTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setRlTwoEntityTypeParent(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLTwoEntityTypeDTO> toDTOs(List<RLTwoEntityType> entities) {
        LinkedList<RLTwoEntityTypeDTO> result = new LinkedList<>();
        for (RLTwoEntityType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLTwoEntityType> toEntities(List<RLTwoEntityTypeDTO> dtos) {
        LinkedList<RLTwoEntityType> result = new LinkedList<>();
        for (RLTwoEntityTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
