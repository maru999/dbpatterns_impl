package at.fhv.rupp.ba.dbimpl.recursive.levelthree.mappers.custom;

import at.fhv.rupp.ba.dbimpl.recursive.levelthree.dtos.RLThreeEntityAssocTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.levelthree.entities.RLThreeEntityAssocType;

import java.util.LinkedList;
import java.util.List;

public class RLThreeEntityAssocTypeMapper {
    private RLThreeEntityAssocTypeMapper() {
        //hidden constructor
    }

    public static RLThreeEntityAssocTypeDTO toDTO(RLThreeEntityAssocType entity) {
        RLThreeEntityAssocTypeDTO dto = new RLThreeEntityAssocTypeDTO();
        dto.setId(entity.getRlThreeEntityAssocTypeId());
        dto.setName(entity.getRlThreeEntityAssocTypeName());
        if (entity.getRlThreeEntityAssocTypeParent() != null) {
            dto.setParent(toDTO(entity.getRlThreeEntityAssocTypeParent()));
        }
        return dto;
    }

    public static RLThreeEntityAssocType toEntity(RLThreeEntityAssocTypeDTO dto) {
        RLThreeEntityAssocType entity = new RLThreeEntityAssocType();
        if (dto.getId() != null) {
            entity.setRlThreeEntityAssocTypeId(dto.getId());
        }
        entity.setRlThreeEntityAssocTypeName(dto.getName());
        if (dto.getParent() != null) {
            entity.setRlThreeEntityAssocTypeParent(toEntity(dto.getParent()));
        }
        return entity;
    }

    public static List<RLThreeEntityAssocTypeDTO> toDTOs(List<RLThreeEntityAssocType> entities) {
        LinkedList<RLThreeEntityAssocTypeDTO> result = new LinkedList<>();
        for (RLThreeEntityAssocType entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<RLThreeEntityAssocType> toEntities(List<RLThreeEntityAssocTypeDTO> dtos) {
        LinkedList<RLThreeEntityAssocType> result = new LinkedList<>();
        for (RLThreeEntityAssocTypeDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
