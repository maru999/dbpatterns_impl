package at.fhv.rupp.ba.dbimpl.status.levelfour.repos;

import at.fhv.rupp.ba.dbimpl.status.levelfour.entities.SLFourEntityThree;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SLFourEntityThreeRepo extends JpaRepository<SLFourEntityThree, Integer> {
}
