package at.fhv.rupp.ba.dbimpl.recursive.levelone.exceptions;

public class RLOneEntityThreeNotFoundException extends Exception{
    public RLOneEntityThreeNotFoundException(String message) {
        super(message);
    }
}
