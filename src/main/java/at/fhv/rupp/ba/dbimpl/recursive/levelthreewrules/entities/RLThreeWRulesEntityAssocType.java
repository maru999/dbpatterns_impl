package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlthreewr_entity_assoc_type")
@EntityListeners(AuditingEntityListener.class)
public class RLThreeWRulesEntityAssocType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlthreewr_entity_assoc_type_id")
    private int rlThreeWRulesEntityAssocTypeId;

    @Column(name = "rlthreewr_entity_assoc_type_name")
    private String rlThreeWRulesEntityAssocTypeName;

    @ManyToOne
    @JoinColumn(name = "rlthreewr_entity_assoc_type_parent")
    private RLThreeWRulesEntityAssocType rlThreeWRulesEntityAssocTypeParent;

    public int getRlThreeWRulesEntityAssocTypeId() {
        return rlThreeWRulesEntityAssocTypeId;
    }

    public void setRlThreeWRulesEntityAssocTypeId(int rlThreeWRulesEntityAssocTypeId) {
        this.rlThreeWRulesEntityAssocTypeId = rlThreeWRulesEntityAssocTypeId;
    }

    public String getRlThreeWRulesEntityAssocTypeName() {
        return rlThreeWRulesEntityAssocTypeName;
    }

    public void setRlThreeWRulesEntityAssocTypeName(String rlThreeWRulesEntityAssocTypeName) {
        this.rlThreeWRulesEntityAssocTypeName = rlThreeWRulesEntityAssocTypeName;
    }

    public RLThreeWRulesEntityAssocType getRlThreeWRulesEntityAssocTypeParent() {
        return rlThreeWRulesEntityAssocTypeParent;
    }

    public void setRlThreeWRulesEntityAssocTypeParent(RLThreeWRulesEntityAssocType rlThreeWRulesEntityAssocTypeParent) {
        this.rlThreeWRulesEntityAssocTypeParent = rlThreeWRulesEntityAssocTypeParent;
    }
}
