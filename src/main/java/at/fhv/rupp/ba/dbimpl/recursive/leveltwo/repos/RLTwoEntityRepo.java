package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntitiesWTypesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntity;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLTwoEntityRepo extends JpaRepository<RLTwoEntity, Integer> {

    @Query(value = "SELECT * from rltwo_entities_with_type", nativeQuery = true)
    List<RLTwoEntitiesWTypesIProjection> getAllEntitiesWithType();

    @Query(value = "SELECT * from rltwo_entities_with_type WHERE rltwo_entity_type_name = :name", nativeQuery = true)
    List<RLTwoEntitiesWTypesIProjection> getAllEntitiesWithTypeName(@Param("name") String typeName);

    @Query(value = "SELECT * from rltwo_entities_with_type WHERE rltwo_entity_id = :id", nativeQuery = true)
    List<RLTwoEntitiesWTypesIProjection> getAllTypesByEntityId(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rltwo_entity WHERE rltwo_entity_parent = :id", nativeQuery = true)
    List<RLTwoEntity> getAllEntitiesWhereEntityIsParent(@Param("id") Integer entityId);

    @Query(value = "SELECT * FROM rltwo_entity WHERE rltwo_entity_type_fk = :id", nativeQuery = true)
    List<RLTwoEntity> getAllEntitiesByTypeId(@Param("id") Integer typeId);

}
