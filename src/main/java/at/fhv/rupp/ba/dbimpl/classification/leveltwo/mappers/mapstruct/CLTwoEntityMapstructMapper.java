package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeOneDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeThreeDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeTwoDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntity;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeOne;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeThree;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeTwo;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {CLTwoEntityTypeOneMapstructMapper.class, CLTwoEntityTypeTwoMapstructMapper.class})
public interface CLTwoEntityMapstructMapper {

    default CLTwoEntityDTO toDTO(CLTwoEntity entity) {
        if (entity == null) return null;
        else if (entity instanceof CLTwoEntitySubtypeOne) {
            return toSubOneDTO((CLTwoEntitySubtypeOne) entity);
        } else if (entity instanceof CLTwoEntitySubtypeTwo) {
            return toSubTwoDTO((CLTwoEntitySubtypeTwo) entity);
        } else {
            return toSubThreeDTO((CLTwoEntitySubtypeThree) entity);
        }
    }

    default CLTwoEntity toEntity(CLTwoEntityDTO dto) {
        if (dto == null) return null;
        else if (dto instanceof CLTwoEntitySubtypeOneDTO) {
            return toSubOneEntity((CLTwoEntitySubtypeOneDTO) dto);
        } else if (dto instanceof CLTwoEntitySubtypeTwoDTO) {
            return toSubTwoEntity((CLTwoEntitySubtypeTwoDTO) dto);
        } else {
            return toSubThreeEntity((CLTwoEntitySubtypeThreeDTO) dto);
        }
    }

    default void updateDTOFromEntity(CLTwoEntityDTO dto, CLTwoEntity entity) {
        if (entity instanceof CLTwoEntitySubtypeOne && dto instanceof CLTwoEntitySubtypeOneDTO) {
            updateDTOFromEntitySubOne((CLTwoEntitySubtypeOneDTO) dto, (CLTwoEntitySubtypeOne) entity);
        } else if (entity instanceof CLTwoEntitySubtypeTwo && dto instanceof CLTwoEntitySubtypeTwoDTO) {
            updateDTOFromEntitySubTwo((CLTwoEntitySubtypeTwoDTO) dto, (CLTwoEntitySubtypeTwo) entity);
        } else if (entity instanceof CLTwoEntitySubtypeThree && dto instanceof CLTwoEntitySubtypeThreeDTO) {
            updateDTOFromEntitySubThree((CLTwoEntitySubtypeThreeDTO) dto, (CLTwoEntitySubtypeThree) entity);
        }
    }

    default void updateEntityFromDTO(CLTwoEntity entity, CLTwoEntityDTO dto) {
        if (dto instanceof CLTwoEntitySubtypeOneDTO && entity instanceof CLTwoEntitySubtypeOne) {
            updateEntityFromDTOSubOne((CLTwoEntitySubtypeOne) entity, (CLTwoEntitySubtypeOneDTO) dto);
        } else if (dto instanceof CLTwoEntitySubtypeTwoDTO && entity instanceof CLTwoEntitySubtypeTwo) {
            updateEntityFromDTOSubTwo((CLTwoEntitySubtypeTwo) entity, (CLTwoEntitySubtypeTwoDTO) dto);
        } else if (dto instanceof CLTwoEntitySubtypeThreeDTO && entity instanceof CLTwoEntitySubtypeThree) {
            updateEntityFromDTOSubThree((CLTwoEntitySubtypeThree) entity, (CLTwoEntitySubtypeThreeDTO) dto);
        }
    }


    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeOneDesc", target = "desc")
    CLTwoEntitySubtypeOneDTO toSubOneDTO(CLTwoEntitySubtypeOne entity);

    @InheritInverseConfiguration(name = "toSubOneDTO")
    CLTwoEntitySubtypeOne toSubOneEntity(CLTwoEntitySubtypeOneDTO dto);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeTwoAttribute", target = "attr")
    CLTwoEntitySubtypeTwoDTO toSubTwoDTO(CLTwoEntitySubtypeTwo entity);

    @InheritInverseConfiguration(name = "toSubTwoDTO")
    CLTwoEntitySubtypeTwo toSubTwoEntity(CLTwoEntitySubtypeTwoDTO dto);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeThreeInfo", target = "info")
    CLTwoEntitySubtypeThreeDTO toSubThreeDTO(CLTwoEntitySubtypeThree entity);

    @InheritInverseConfiguration(name = "toSubThreeDTO")
    CLTwoEntitySubtypeThree toSubThreeEntity(CLTwoEntitySubtypeThreeDTO dto);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeOneDesc", target = "desc")
    void updateEntityFromDTOSubOne(CLTwoEntitySubtypeOne entity, @MappingTarget CLTwoEntitySubtypeOneDTO dto);

    @InheritInverseConfiguration(name = "toSubOneDTO")
    void updateDTOFromEntitySubOne(CLTwoEntitySubtypeOneDTO dto, @MappingTarget CLTwoEntitySubtypeOne entity);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeTwoAttribute", target = "attr")
    void updateEntityFromDTOSubTwo(CLTwoEntitySubtypeTwo entity, @MappingTarget CLTwoEntitySubtypeTwoDTO dto);

    @InheritInverseConfiguration(name = "toSubTwoDTO")
    void updateDTOFromEntitySubTwo(CLTwoEntitySubtypeTwoDTO dto, @MappingTarget CLTwoEntitySubtypeTwo entity);

    @Mapping(source = "clTwoEntityId", target = "id")
    @Mapping(source = "clTwoEntityName", target = "name")
    @Mapping(source = "clTwoEntityEntityTypeOne", target = "typeOne")
    @Mapping(source = "clTwoEntityEntityTypeTwo", target = "typeTwo")
    @Mapping(source = "clTwoEntitySubtypeThreeInfo", target = "info")
    void updateEntityFromDTOSubThree(CLTwoEntitySubtypeThree entity, @MappingTarget CLTwoEntitySubtypeThreeDTO dto);

    @InheritInverseConfiguration(name = "toSubThreeDTO")
    void updateDTOFromEntitySubThree(CLTwoEntitySubtypeThreeDTO dto, @MappingTarget CLTwoEntitySubtypeThree entity);


    List<CLTwoEntityDTO> toDTOs(List<CLTwoEntity> entities);

    List<CLTwoEntity> toEntities(List<CLTwoEntityDTO> dtos);
}
