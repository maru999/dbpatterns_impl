package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "clthreewr_entity_category_type_rollup_type")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategoryTypeRollupType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_type_rollup_type_id")
    private int clthreewrEntityCategoryTypeRollupTypeId;

    @Column(name = "clthreewr_entity_category_type_rollup_type_name")
    private String clthreewrEntityCategoryTypeRollupTypeName;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_trollup_type_parent_id")
    private CLThreeWRollupsEntityCategoryTypeRollupType parentTypeRollupType;

    public int getClthreewrEntityCategoryTypeRollupTypeId() {
        return clthreewrEntityCategoryTypeRollupTypeId;
    }

    public void setClthreewrEntityCategoryTypeRollupTypeId(int clthreewrEntityCategoryTypeRollupTypeId) {
        this.clthreewrEntityCategoryTypeRollupTypeId = clthreewrEntityCategoryTypeRollupTypeId;
    }

    public String getClthreewrEntityCategoryTypeRollupTypeName() {
        return clthreewrEntityCategoryTypeRollupTypeName;
    }

    public void setClthreewrEntityCategoryTypeRollupTypeName(String clthreewrEntityCategoryTypeRollupTypeName) {
        this.clthreewrEntityCategoryTypeRollupTypeName = clthreewrEntityCategoryTypeRollupTypeName;
    }

    public CLThreeWRollupsEntityCategoryTypeRollupType getParentTypeRollupType() {
        return parentTypeRollupType;
    }

    public void setParentTypeRollupType(CLThreeWRollupsEntityCategoryTypeRollupType parentTypeRollupType) {
        this.parentTypeRollupType = parentTypeRollupType;
    }
}
