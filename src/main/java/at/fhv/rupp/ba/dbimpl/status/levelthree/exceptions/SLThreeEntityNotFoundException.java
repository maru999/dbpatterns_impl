package at.fhv.rupp.ba.dbimpl.status.levelthree.exceptions;

public class SLThreeEntityNotFoundException extends Exception {
    public SLThreeEntityNotFoundException(String message) {
        super(message);
    }
}
