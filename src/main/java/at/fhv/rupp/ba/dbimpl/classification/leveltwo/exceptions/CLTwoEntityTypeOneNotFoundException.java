package at.fhv.rupp.ba.dbimpl.classification.leveltwo.exceptions;

public class CLTwoEntityTypeOneNotFoundException extends Exception {
    public CLTwoEntityTypeOneNotFoundException(String message) {
        super(message);
    }
}
