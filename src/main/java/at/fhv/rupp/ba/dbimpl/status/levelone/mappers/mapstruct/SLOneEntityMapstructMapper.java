package at.fhv.rupp.ba.dbimpl.status.levelone.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelone.dtos.SLOneEntityDTO;
import at.fhv.rupp.ba.dbimpl.status.levelone.entities.SLOneEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLOneEntityMapstructMapper {

    @Mapping(source = "slOneEntityId", target = "id")
    @Mapping(source = "slOneEntityEventFromDate", target = "eventFromDate")
    @Mapping(source = "slOneEntityEventThruDate", target = "eventThruDate")
    @Mapping(source = "slOneEntityEventIndicator", target = "eventIndicator")
    @Mapping(source = "slOneEntityEventOneDateTime", target = "eventOneDateTime")
    @Mapping(source = "slOneEntityEventThreeDateTime", target = "eventThreeDateTime")
    @Mapping(source = "slOneEntityEventTwoDateTime", target = "eventTwoDateTime")
    SLOneEntityDTO toDTO(SLOneEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLOneEntity toEntity(SLOneEntityDTO dto);

    @Mapping(source = "slOneEntityId", target = "id")
    @Mapping(source = "slOneEntityEventFromDate", target = "eventFromDate")
    @Mapping(source = "slOneEntityEventThruDate", target = "eventThruDate")
    @Mapping(source = "slOneEntityEventIndicator", target = "eventIndicator")
    @Mapping(source = "slOneEntityEventOneDateTime", target = "eventOneDateTime")
    @Mapping(source = "slOneEntityEventThreeDateTime", target = "eventThreeDateTime")
    @Mapping(source = "slOneEntityEventTwoDateTime", target = "eventTwoDateTime")
    void updateEntityFromDTO(SLOneEntity entity, @MappingTarget SLOneEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLOneEntityDTO dto, @MappingTarget SLOneEntity entity);

    List<SLOneEntityDTO> toDTOs(List<SLOneEntity> entities);

    List<SLOneEntity> toEntities(List<SLOneEntityDTO> dtos);
}
