package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

public class CLThreeWRollupsEntityCategoryRollupTypeDTO {
    private Integer id;
    private String name;
    private CLThreeWRollupsEntityCategoryRollupTypeDTO parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CLThreeWRollupsEntityCategoryRollupTypeDTO getParent() {
        return parent;
    }

    public void setParent(CLThreeWRollupsEntityCategoryRollupTypeDTO parent) {
        this.parent = parent;
    }
}
