package at.fhv.rupp.ba.dbimpl.status.levelthree.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "slthree_entity")
@EntityListeners(AuditingEntityListener.class)
public class SLThreeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slthree_entity_id")
    private int slThreeEntityId;

    public int getSlThreeEntityId() {
        return slThreeEntityId;
    }

    public void setSlThreeEntityId(int slThreeEntityId) {
        this.slThreeEntityId = slThreeEntityId;
    }
}
