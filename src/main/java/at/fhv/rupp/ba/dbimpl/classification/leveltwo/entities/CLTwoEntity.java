package at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "cltwo_entity")
@EntityListeners(AuditingEntityListener.class)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class CLTwoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cltwo_entity_id")
    private int clTwoEntityId;

    @Column(name = "cltwo_entity_name", nullable = false)
    private String clTwoEntityName;

    @ManyToOne
    @JoinColumn(name="cltwo_entity_type_one_fk")
    private CLTwoEntityTypeOne clTwoEntityEntityTypeOne;

    @ManyToOne
    @JoinColumn(name="cltwo_entity_type_two_fk")
    private CLTwoEntityTypeTwo clTwoEntityEntityTypeTwo;

    public int getClTwoEntityId() {
        return clTwoEntityId;
    }

    public void setClTwoEntityId(int clTwoEntityId) {
        this.clTwoEntityId = clTwoEntityId;
    }

    public String getClTwoEntityName() {
        return clTwoEntityName;
    }

    public void setClTwoEntityName(String clTwoEntityName) {
        this.clTwoEntityName = clTwoEntityName;
    }

    public CLTwoEntityTypeOne getClTwoEntityEntityTypeOne() {
        return clTwoEntityEntityTypeOne;
    }

    public void setClTwoEntityEntityTypeOne(CLTwoEntityTypeOne clTwoEntityEntityTypeOne) {
        this.clTwoEntityEntityTypeOne = clTwoEntityEntityTypeOne;
    }

    public CLTwoEntityTypeTwo getClTwoEntityEntityTypeTwo() {
        return clTwoEntityEntityTypeTwo;
    }

    public void setClTwoEntityEntityTypeTwo(CLTwoEntityTypeTwo clTwoEntityEntityTypeTwo) {
        this.clTwoEntityEntityTypeTwo = clTwoEntityEntityTypeTwo;
    }
}
