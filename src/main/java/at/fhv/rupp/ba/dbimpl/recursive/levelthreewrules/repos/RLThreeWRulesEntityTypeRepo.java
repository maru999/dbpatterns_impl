package at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.repos;

import at.fhv.rupp.ba.dbimpl.recursive.levelthreewrules.entities.RLThreeWRulesEntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RLThreeWRulesEntityTypeRepo extends JpaRepository<RLThreeWRulesEntityType, Integer> {
    @Query(value = "SELECT * FROM rlthreewr_entity_type WHERE rlthreewr_entity_type_parent = :id", nativeQuery = true)
    List<RLThreeWRulesEntityType> getTypesWhereTypeIsParent(@Param("id") Integer typeId);
}
