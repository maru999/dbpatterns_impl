package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "clthreewr_entity_category_type_rollup")
@EntityListeners(AuditingEntityListener.class)
public class CLThreeWRollupsEntityCategoryTypeRollup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clthreewr_entity_category_type_rollup_id")
    private int clthreewrEntityCategoryTypeRollupId;

    @Column(name = "clthreewr_entity_category_type_rollup_from", nullable = false)
    private LocalDate clthreewrEntityCategoryTypeRollupFromDate;

    @Column(name = "clthreewr_entity_category_type_rollup_thru")
    private LocalDate clthreewrEntityCategoryTypeRollupThruDate;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_type_rollup_parent_entity_category_id")
    private CLThreeWRollupsEntityCategoryType clthreewrEntityCategoryTypeRollupParentCategory;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_type_rollup_child_entity_category_id")
    private CLThreeWRollupsEntityCategoryType clthreewrEntityCategoryTypeRollupChildCategory;

    @ManyToOne
    @JoinColumn(name = "clthreewr_entity_category_typer_entity_category_type_rtype_id")
    private CLThreeWRollupsEntityCategoryTypeRollupType clthreewrEntityCategoryTypeRollupType;

    public int getClthreewrEntityCategoryTypeRollupId() {
        return clthreewrEntityCategoryTypeRollupId;
    }

    public void setClthreewrEntityCategoryTypeRollupId(int clthreewrEntityCategoryTypeRollupId) {
        this.clthreewrEntityCategoryTypeRollupId = clthreewrEntityCategoryTypeRollupId;
    }

    public LocalDate getClthreewrEntityCategoryTypeRollupFromDate() {
        return clthreewrEntityCategoryTypeRollupFromDate;
    }

    public void setClthreewrEntityCategoryTypeRollupFromDate(LocalDate clthreewrEntityCategoryTypeRollupFromDate) {
        this.clthreewrEntityCategoryTypeRollupFromDate = clthreewrEntityCategoryTypeRollupFromDate;
    }

    public LocalDate getClthreewrEntityCategoryTypeRollupThruDate() {
        return clthreewrEntityCategoryTypeRollupThruDate;
    }

    public void setClthreewrEntityCategoryTypeRollupThruDate(LocalDate clthreewrEntityCategoryTypeRollupThruDate) {
        this.clthreewrEntityCategoryTypeRollupThruDate = clthreewrEntityCategoryTypeRollupThruDate;
    }

    public CLThreeWRollupsEntityCategoryType getClthreewrEntityCategoryTypeRollupParentCategory() {
        return clthreewrEntityCategoryTypeRollupParentCategory;
    }

    public void setClthreewrEntityCategoryTypeRollupParentCategory(CLThreeWRollupsEntityCategoryType clthreewrEntityCategoryTypeRollupParentCategory) {
        this.clthreewrEntityCategoryTypeRollupParentCategory = clthreewrEntityCategoryTypeRollupParentCategory;
    }

    public CLThreeWRollupsEntityCategoryType getClthreewrEntityCategoryTypeRollupChildCategory() {
        return clthreewrEntityCategoryTypeRollupChildCategory;
    }

    public void setClthreewrEntityCategoryTypeRollupChildCategory(CLThreeWRollupsEntityCategoryType clthreewrEntityCategoryTypeRollupChildCategory) {
        this.clthreewrEntityCategoryTypeRollupChildCategory = clthreewrEntityCategoryTypeRollupChildCategory;
    }

    public CLThreeWRollupsEntityCategoryTypeRollupType getClthreewrEntityCategoryTypeRollupType() {
        return clthreewrEntityCategoryTypeRollupType;
    }

    public void setClthreewrEntityCategoryTypeRollupType(CLThreeWRollupsEntityCategoryTypeRollupType clthreewrEntityCategoryTypeRollupType) {
        this.clthreewrEntityCategoryTypeRollupType = clthreewrEntityCategoryTypeRollupType;
    }
}
