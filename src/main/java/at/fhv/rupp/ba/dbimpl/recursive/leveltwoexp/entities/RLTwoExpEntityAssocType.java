package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rltwoxp_entity_assoc_type")
@EntityListeners(AuditingEntityListener.class)
public class RLTwoExpEntityAssocType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rltwoxp_entity_assoc_type_id")
    private int rlTwoExpEntityAssocTypeId;

    @Column(name = "rltwoxp_entity_assoc_type_name")
    private String rlTwoExpEntityAssocTypeName;

    public int getRlTwoExpEntityAssocTypeId() {
        return rlTwoExpEntityAssocTypeId;
    }

    public void setRlTwoExpEntityAssocTypeId(int rlTwoExpEntityAssocTypeId) {
        this.rlTwoExpEntityAssocTypeId = rlTwoExpEntityAssocTypeId;
    }

    public String getRlTwoExpEntityAssocTypeName() {
        return rlTwoExpEntityAssocTypeName;
    }

    public void setRlTwoExpEntityAssocTypeName(String rlTwoExpEntityAssocTypeName) {
        this.rlTwoExpEntityAssocTypeName = rlTwoExpEntityAssocTypeName;
    }
}
