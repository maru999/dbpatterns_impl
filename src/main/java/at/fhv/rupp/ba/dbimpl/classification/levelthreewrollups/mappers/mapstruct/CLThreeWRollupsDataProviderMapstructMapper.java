package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos.CLThreeWRollupsDataProviderDTO;
import at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.entities.CLThreeWRollupsDataProvider;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CLThreeWRollupsDataProviderMapstructMapper {

    @Mapping(source = "clthreewrPartyRoleId", target = "id")
    @Mapping(source = "clthreewrDataProviderName", target = "name")
    @Mapping(source = "roleName", target = "roleName")
    CLThreeWRollupsDataProviderDTO toDTO(CLThreeWRollupsDataProvider entity);

    @InheritInverseConfiguration(name = "toDTO")
    CLThreeWRollupsDataProvider toEntity(CLThreeWRollupsDataProviderDTO dto);

    @Mapping(source = "clthreewrPartyRoleId", target = "id")
    @Mapping(source = "clthreewrDataProviderName", target = "name")
    @Mapping(source = "roleName", target = "roleName")
    void updateEntityFromDTO(CLThreeWRollupsDataProvider entity, @MappingTarget CLThreeWRollupsDataProviderDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(CLThreeWRollupsDataProviderDTO dto, @MappingTarget CLThreeWRollupsDataProvider entity);

    List<CLThreeWRollupsDataProviderDTO> toDTOs(List<CLThreeWRollupsDataProvider> entities);

    List<CLThreeWRollupsDataProvider> toEntities(List<CLThreeWRollupsDataProviderDTO> dtos);
}
