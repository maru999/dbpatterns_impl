package at.fhv.rupp.ba.dbimpl.status.levelfour.entities;

import org.hibernate.annotations.Check;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

//TODO make sure the Check annotation actually does what it's supposed to do

@Entity
@Table(name = "slfour_status_appli")
@EntityListeners(AuditingEntityListener.class)
@Check(constraints = "num_nonnulls(slfour_status_appli_entity_one, slfour_status_appli_entity_two, slfour_status_appli_entity_three) = 1")
public class SLFourStatusApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slfour_status_appli_id")
    private int slFourStatusApplicationId;

    @Column(name = "slfour_status_appli_datetime")
    private LocalDateTime slFourStatusApplicationDateTime;

    @Column(name = "slfour_status_appli_status_from")
    private LocalDate slFourStatusApplicationStatusFrom;

    @Column(name = "slfour_status_appli_status_thru")
    private LocalDate slFourStatusApplicationStatusThru;

    @Column(name = "slfour_status_appli_from", nullable = false)
    private LocalDate slFourStatusApplicationFrom;

    @Column(name = "slfour_status_appli_thru")
    private LocalDate slFourStatusApplicationThru;

    @ManyToOne
    @JoinColumn(name = "slfour_status_appli_type")
    private SLFourStatusType slFourStatusApplicationType;

    @ManyToOne
    @JoinColumn(name = "slfour_status_appli_entity_one")
    private SLFourEntityOne slFourStatusApplicationEntityOne;

    @ManyToOne
    @JoinColumn(name = "slfour_status_appli_entity_two")
    private SLFourEntityTwo slFourStatusApplicationEntityTwo;

    @ManyToOne
    @JoinColumn(name = "slfour_status_appli_entity_three")
    private SLFourEntityThree slFourStatusApplicationEntityThree;

    public int getSlFourStatusApplicationId() {
        return slFourStatusApplicationId;
    }

    public void setSlFourStatusApplicationId(int slFourStatusApplicationId) {
        this.slFourStatusApplicationId = slFourStatusApplicationId;
    }

    public LocalDateTime getSlFourStatusApplicationDateTime() {
        return slFourStatusApplicationDateTime;
    }

    public void setSlFourStatusApplicationDateTime(LocalDateTime slFourStatusApplicationDateTime) {
        this.slFourStatusApplicationDateTime = slFourStatusApplicationDateTime;
    }

    public LocalDate getSlFourStatusApplicationStatusFrom() {
        return slFourStatusApplicationStatusFrom;
    }

    public void setSlFourStatusApplicationStatusFrom(LocalDate slFourStatusApplicationStatusFrom) {
        this.slFourStatusApplicationStatusFrom = slFourStatusApplicationStatusFrom;
    }

    public LocalDate getSlFourStatusApplicationStatusThru() {
        return slFourStatusApplicationStatusThru;
    }

    public void setSlFourStatusApplicationStatusThru(LocalDate slFourStatusApplicationStatusThru) {
        this.slFourStatusApplicationStatusThru = slFourStatusApplicationStatusThru;
    }

    public LocalDate getSlFourStatusApplicationFrom() {
        return slFourStatusApplicationFrom;
    }

    public void setSlFourStatusApplicationFrom(LocalDate slFourStatusApplicationFrom) {
        this.slFourStatusApplicationFrom = slFourStatusApplicationFrom;
    }

    public LocalDate getSlFourStatusApplicationThru() {
        return slFourStatusApplicationThru;
    }

    public void setSlFourStatusApplicationThru(LocalDate slFourStatusApplicationThru) {
        this.slFourStatusApplicationThru = slFourStatusApplicationThru;
    }

    public SLFourStatusType getSlFourStatusApplicationType() {
        return slFourStatusApplicationType;
    }

    public void setSlFourStatusApplicationType(SLFourStatusType slFourStatusApplicationType) {
        this.slFourStatusApplicationType = slFourStatusApplicationType;
    }

    public SLFourEntityOne getSlFourStatusApplicationEntityOne() {
        return slFourStatusApplicationEntityOne;
    }

    public void setSlFourStatusApplicationEntityOne(SLFourEntityOne slFourStatusApplicationEntityOne) {
        this.slFourStatusApplicationEntityOne = slFourStatusApplicationEntityOne;
    }

    public SLFourEntityTwo getSlFourStatusApplicationEntityTwo() {
        return slFourStatusApplicationEntityTwo;
    }

    public void setSlFourStatusApplicationEntityTwo(SLFourEntityTwo slFourStatusApplicationEntityTwo) {
        this.slFourStatusApplicationEntityTwo = slFourStatusApplicationEntityTwo;
    }

    public SLFourEntityThree getSlFourStatusApplicationEntityThree() {
        return slFourStatusApplicationEntityThree;
    }

    public void setSlFourStatusApplicationEntityThree(SLFourEntityThree slFourStatusApplicationEntityThree) {
        this.slFourStatusApplicationEntityThree = slFourStatusApplicationEntityThree;
    }
}
