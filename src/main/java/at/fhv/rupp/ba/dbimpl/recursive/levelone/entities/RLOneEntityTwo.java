package at.fhv.rupp.ba.dbimpl.recursive.levelone.entities;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "rlone_entity_two")
@EntityListeners(AuditingEntityListener.class)
public class RLOneEntityTwo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rlone_entity_two_id")
    private int rloneEntityTwoId;

    @Column(name = "rlone_entity_two_name")
    private String rloneEntityTwoName;

    @ManyToOne
    @JoinColumn(name = "rlone_entity_two_parent")
    private RLOneEntityOne rloneEntityTwoParent;

    public int getRloneEntityTwoId() {
        return rloneEntityTwoId;
    }

    public void setRloneEntityTwoId(int rloneEntityTwoId) {
        this.rloneEntityTwoId = rloneEntityTwoId;
    }

    public String getRloneEntityTwoName() {
        return rloneEntityTwoName;
    }

    public void setRloneEntityTwoName(String rloneEntityTwoName) {
        this.rloneEntityTwoName = rloneEntityTwoName;
    }

    public RLOneEntityOne getRloneEntityTwoParent() {
        return rloneEntityTwoParent;
    }

    public void setRloneEntityTwoParent(RLOneEntityOne rloneEntityTwoParent) {
        this.rloneEntityTwoParent = rloneEntityTwoParent;
    }
}
