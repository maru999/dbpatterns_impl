package at.fhv.rupp.ba.dbimpl.status.levelthree.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.levelthree.dtos.SLThreeEntityStatusDTO;
import at.fhv.rupp.ba.dbimpl.status.levelthree.entities.SLThreeEntityStatus;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {SLThreeEntityMapstructMapper.class, SLThreeStatusTypeMapstructMapper.class})
public interface SLThreeEntityStatusMapstructMapper {

    @Mapping(source = "slThreeEntityStatusId", target = "id")
    @Mapping(source = "slThreeEntityStatusFromDate", target = "entityStatusFrom")
    @Mapping(source = "slThreeEntityStatusThruDate", target = "entityStatusThru")
    @Mapping(source = "slThreeEntityStatusDateTime", target = "statusDateTime")
    @Mapping(source = "slThreeEntityStatusStatusFromDate", target = "statusFrom")
    @Mapping(source = "slThreeEntityStatusStatusThruDate", target = "statusThru")
    @Mapping(source = "slThreeEntityStatusEntity", target = "entity")
    @Mapping(source = "slThreeEntityStatusStatusType", target = "statusType")
    SLThreeEntityStatusDTO toDTO(SLThreeEntityStatus entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLThreeEntityStatus toEntity(SLThreeEntityStatusDTO dto);

    @Mapping(source = "slThreeEntityStatusId", target = "id")
    @Mapping(source = "slThreeEntityStatusFromDate", target = "entityStatusFrom")
    @Mapping(source = "slThreeEntityStatusThruDate", target = "entityStatusThru")
    @Mapping(source = "slThreeEntityStatusDateTime", target = "statusDateTime")
    @Mapping(source = "slThreeEntityStatusStatusFromDate", target = "statusFrom")
    @Mapping(source = "slThreeEntityStatusStatusThruDate", target = "statusThru")
    @Mapping(source = "slThreeEntityStatusEntity", target = "entity")
    @Mapping(source = "slThreeEntityStatusStatusType", target = "statusType")
    void updateEntityFromDTO(SLThreeEntityStatus entity, @MappingTarget SLThreeEntityStatusDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLThreeEntityStatusDTO dto, @MappingTarget SLThreeEntityStatus entity);

    List<SLThreeEntityStatusDTO> toDTOs(List<SLThreeEntityStatus> entities);

    List<SLThreeEntityStatus> toEntities(List<SLThreeEntityStatusDTO> dtos);
}
