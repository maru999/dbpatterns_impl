package at.fhv.rupp.ba.dbimpl.classification.leveltwo.repos;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntityNamedTypesIProjection;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CLTwoEntityRepo extends JpaRepository<CLTwoEntity, Integer> {

    @Query(value = "SELECT * FROM entTypes", nativeQuery = true)
    List<CLTwoEntityNamedTypesIProjection> getNamedTypes();

    @Query(value = "SELECT cltwo_entity_id,\n" +
            "       cltwo_entity_name,\n" +
            "       cltwo_entity_type_one_name,\n" +
            "       cltwo_entity_type_two_name,\n" +
            "       cltwo_entity_type_three_name\n" +
            "FROM cltwo_entity,\n" +
            "     cltwo_entity_type_three_class,\n" +
            "     cltwo_entity_type_three,\n" +
            "     cltwo_entity_type_one,\n" +
            "     cltwo_entity_type_two\n" +
            "WHERE cltwo_entity_id = cltwo_entity_type_three_class_entid\n" +
            "  AND cltwo_entity_type_three_id = cltwo_entity_type_three_class_threeid\n" +
            "  AND cltwo_entity_type_two_id = cltwo_entity_type_two_fk\n" +
            "  AND cltwo_entity_type_one_id = cltwo_entity_type_one_fk;", nativeQuery = true)
    List<CLTwoEntityNamedTypesIProjection> getNamedTypesWithoutView();

    @Query(value = "SELECT * FROM entTypes WHERE cltwo_entity_id = :id", nativeQuery = true)
    List<CLTwoEntityNamedTypesIProjection> getNamedTypeByEntityId(@Param("id") Integer id);

    @Query(value = "SELECT * FROM entTypes WHERE cltwo_entity_type_one_name = :name", nativeQuery = true)
    List<CLTwoEntityNamedTypesIProjection> getNamedTypesByTypeOneName(@Param("name") String typeOneName);

}
