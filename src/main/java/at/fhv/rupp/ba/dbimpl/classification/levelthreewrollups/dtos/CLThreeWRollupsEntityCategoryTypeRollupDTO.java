package at.fhv.rupp.ba.dbimpl.classification.levelthreewrollups.dtos;

import java.time.LocalDate;

public class CLThreeWRollupsEntityCategoryTypeRollupDTO {

    private Integer id;
    private LocalDate from;
    private LocalDate thru;
    private CLThreeWRollupsEntityCategoryTypeDTO parent;
    private CLThreeWRollupsEntityCategoryTypeDTO child;
    private CLThreeWRollupsEntityCategoryTypeRollupTypeDTO type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getThru() {
        return thru;
    }

    public void setThru(LocalDate thru) {
        this.thru = thru;
    }

    public CLThreeWRollupsEntityCategoryTypeDTO getParent() {
        return parent;
    }

    public void setParent(CLThreeWRollupsEntityCategoryTypeDTO parent) {
        this.parent = parent;
    }

    public CLThreeWRollupsEntityCategoryTypeDTO getChild() {
        return child;
    }

    public void setChild(CLThreeWRollupsEntityCategoryTypeDTO child) {
        this.child = child;
    }

    public CLThreeWRollupsEntityCategoryTypeRollupTypeDTO getType() {
        return type;
    }

    public void setType(CLThreeWRollupsEntityCategoryTypeRollupTypeDTO type) {
        this.type = type;
    }
}
