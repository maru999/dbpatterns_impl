package at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.dtos.RLTwoExpEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwoexp.entities.RLTwoExpEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = RLTwoExpEntityTypeMapstructMapper.class)
public interface RLTwoExpEntityMapstructMapper {
    @Mapping(source = "rlTwoExpEntityId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocToEntityOne", target = "assocToOne")
    @Mapping(source = "rlTwoExpEntityAssocToEntityTwo", target = "assocToTwo")
    @Mapping(source = "rlTwoExpEntityType", target = "type")
    RLTwoExpEntityDTO toDTO(RLTwoExpEntity entity);

    @InheritInverseConfiguration(name = "toDTO")
    RLTwoExpEntity toEntity(RLTwoExpEntityDTO dto);

    @Mapping(source = "rlTwoExpEntityId", target = "id")
    @Mapping(source = "rlTwoExpEntityAssocToEntityOne", target = "assocToOne")
    @Mapping(source = "rlTwoExpEntityAssocToEntityTwo", target = "assocToTwo")
    @Mapping(source = "rlTwoExpEntityType", target = "type")
    void updateEntityFromDTO(RLTwoExpEntity entity, @MappingTarget RLTwoExpEntityDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(RLTwoExpEntityDTO dto, @MappingTarget RLTwoExpEntity entity);

    List<RLTwoExpEntityDTO> toDTOs(List<RLTwoExpEntity> entities);

    List<RLTwoExpEntity> toEntities(List<RLTwoExpEntityDTO> dtos);
}
