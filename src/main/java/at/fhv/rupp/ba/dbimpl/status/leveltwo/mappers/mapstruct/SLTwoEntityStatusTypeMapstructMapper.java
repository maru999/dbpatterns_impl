package at.fhv.rupp.ba.dbimpl.status.leveltwo.mappers.mapstruct;

import at.fhv.rupp.ba.dbimpl.status.leveltwo.dtos.SLTwoEntityStatusTypeDTO;
import at.fhv.rupp.ba.dbimpl.status.leveltwo.entities.SLTwoEntityStatusType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SLTwoEntityStatusTypeMapstructMapper {

    @Mapping(source = "slTwoStatusTypeId", target = "id")
    @Mapping(source = "slTwoStatusTypeName", target = "name")
    @Mapping(source = "slTwoEntityStatusTypeSomeAttr", target = "someAttr")
    SLTwoEntityStatusTypeDTO toDTO(SLTwoEntityStatusType entity);

    @InheritInverseConfiguration(name = "toDTO")
    SLTwoEntityStatusType toEntity(SLTwoEntityStatusTypeDTO dto);

    @Mapping(source = "slTwoStatusTypeId", target = "id")
    @Mapping(source = "slTwoStatusTypeName", target = "name")
    @Mapping(source = "slTwoEntityStatusTypeSomeAttr", target = "someAttr")
    void updateEntityFromDTO(SLTwoEntityStatusType entity, @MappingTarget SLTwoEntityStatusTypeDTO dto);

    @InheritInverseConfiguration(name = "toDTO")
    void updateDTOFromEntity(SLTwoEntityStatusTypeDTO dto, @MappingTarget SLTwoEntityStatusType entity);

    List<SLTwoEntityStatusTypeDTO> toDTOs(List<SLTwoEntityStatusType> entities);

    List<SLTwoEntityStatusType> toEntities(List<SLTwoEntityStatusTypeDTO> dtos);
}
