package at.fhv.rupp.ba.dbimpl.recursive.leveltwo.controller;

import at.fhv.rupp.ba.dbimpl.DbimplApplication;
import at.fhv.rupp.ba.dbimpl.core.DataGenJavaFaker;
import at.fhv.rupp.ba.dbimpl.core.ScriptExecutor;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.datagen.RLTwoJavaFaker;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntitiesWTypesIProjection;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntityDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.dtos.RLTwoEntityTypeDTO;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntity;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.entities.RLTwoEntityType;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.exceptions.RLTwoEntityNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.exceptions.RLTwoEntityTypeNotFoundException;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.mappers.custom.RLTwoEntityMapper;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.mappers.custom.RLTwoEntityTypeMapper;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.repos.RLTwoEntityRepo;
import at.fhv.rupp.ba.dbimpl.recursive.leveltwo.repos.RLTwoEntityTypeRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ba/recursion/L2")
public class RLTwoController {
    private final RLTwoEntityTypeRepo entityTypeRepo;
    private final RLTwoEntityRepo entityRepo;
    private final DataGenJavaFaker faker;

    public RLTwoController(RLTwoEntityTypeRepo entityTypeRepo, RLTwoEntityRepo entityRepo) {
        this.entityTypeRepo = entityTypeRepo;
        this.entityRepo = entityRepo;
        this.faker = new RLTwoJavaFaker(entityRepo, entityTypeRepo);
    }

    @GetMapping("/entity/{id}")
    public ResponseEntity<RLTwoEntityDTO> getEntityById(@PathVariable(value = "id") Integer entityId) throws RLTwoEntityNotFoundException {
        RLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLTwoEntityNotFoundException(getExceptionString("RLTwoEntity", entityId)));
        return ResponseEntity.ok().body(RLTwoEntityMapper.toDTO(entity));
    }

    @GetMapping("/entities/all")
    public List<RLTwoEntityDTO> getAllEntities() {
        return RLTwoEntityMapper.toDTOs(entityRepo.findAll());
    }

    @PostMapping("/entities")
    public RLTwoEntityDTO createEntity(@Valid @RequestBody RLTwoEntityDTO entityDTO) {
        return RLTwoEntityMapper.toDTO(entityRepo.save(RLTwoEntityMapper.toEntity(entityDTO)));
    }

    @PutMapping("/entities/{id}")
    public ResponseEntity<RLTwoEntityDTO> updateEntity(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLTwoEntityDTO entityDetails) throws RLTwoEntityNotFoundException {
        RLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLTwoEntityNotFoundException(getExceptionString("RLTwoEntity", entityId)));
        RLTwoEntityDTO entityDTO = RLTwoEntityMapper.toDTO(entity);
        if (entityDetails.getType() != null) {
            entityDTO.setType(entityDetails.getType());
        }
        if (entityDetails.getParent() != null) {
            entityDTO.setParent(entityDetails.getParent());
        }
        final RLTwoEntityDTO updatedEntity = RLTwoEntityMapper.toDTO(entityRepo.save(RLTwoEntityMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/entities/{id}")
    public Map<String, Boolean> deleteEntity(@PathVariable(value = "id") Integer entityId) throws RLTwoEntityNotFoundException {
        RLTwoEntity entity = entityRepo.findById(entityId).orElseThrow(() -> new RLTwoEntityNotFoundException(getExceptionString("RLTwoEntity", entityId)));
        List<RLTwoEntity> childEntities = entityRepo.getAllEntitiesWhereEntityIsParent(entityId);
        for (RLTwoEntity childEntity : childEntities) {
            childEntity.setRlTwoEntityParent(null);
            entityRepo.save(childEntity);
        }
        entityRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/type/{id}")
    public ResponseEntity<RLTwoEntityTypeDTO> getEntityTypeById(@PathVariable(value = "id") Integer entityId) throws RLTwoEntityTypeNotFoundException {
        RLTwoEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLTwoEntityTypeNotFoundException(getExceptionString("RLTwoEntityType", entityId)));
        return ResponseEntity.ok().body(RLTwoEntityTypeMapper.toDTO(entity));
    }

    @GetMapping("/types/all")
    public List<RLTwoEntityTypeDTO> getAllEntityTypes() {
        return RLTwoEntityTypeMapper.toDTOs(entityTypeRepo.findAll());
    }

    @PostMapping("/types")
    public RLTwoEntityTypeDTO createEntityType(@Valid @RequestBody RLTwoEntityTypeDTO entityDTO) {
        return RLTwoEntityTypeMapper.toDTO(entityTypeRepo.save(RLTwoEntityTypeMapper.toEntity(entityDTO)));
    }

    @PutMapping("/types/{id}")
    public ResponseEntity<RLTwoEntityTypeDTO> updateEntityType(
            @PathVariable(value = "id") Integer entityId, @Valid @RequestBody RLTwoEntityTypeDTO entityDetails) throws RLTwoEntityTypeNotFoundException {
        RLTwoEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLTwoEntityTypeNotFoundException(getExceptionString("RLTwoEntityType", entityId)));
        RLTwoEntityTypeDTO entityDTO = RLTwoEntityTypeMapper.toDTO(entity);
        entityDTO.setName(entityDetails.getName());
        if (entityDetails.getParent() != null) {
            entityDTO.setParent(entityDetails.getParent());
        }
        final RLTwoEntityTypeDTO updatedEntity = RLTwoEntityTypeMapper.toDTO(entityTypeRepo.save(RLTwoEntityTypeMapper.toEntity(entityDTO)));
        return ResponseEntity.ok().body(updatedEntity);
    }

    @DeleteMapping("/types/{id}")
    public Map<String, Boolean> deleteEntityType(@PathVariable(value = "id") Integer entityId) throws RLTwoEntityTypeNotFoundException {
        RLTwoEntityType entity = entityTypeRepo.findById(entityId).orElseThrow(() -> new RLTwoEntityTypeNotFoundException(getExceptionString("RLTwoEntityType", entityId)));
        List<RLTwoEntityType> childTypes = entityTypeRepo.getAllTypesWhereTypeIsParent(entityId);
        for (RLTwoEntityType childType : childTypes) {
            childType.setRlTwoEntityTypeParent(null);
            entityTypeRepo.save(childType);
        }
        List<RLTwoEntity> referencingEntities = entityRepo.getAllEntitiesByTypeId(entityId);
        for (RLTwoEntity referencingEntity : referencingEntities) {
            referencingEntity.setRlTwoEntityType(null);
            entityRepo.save(referencingEntity);
        }
        entityTypeRepo.delete(entity);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/datagen")
    public Map<String, Boolean> fakeData() {
        faker.fillDB();
        Map<String, Boolean> response = new HashMap<>();
        response.put("Fake Data written to DB", Boolean.TRUE);
        return response;
    }

    @GetMapping("/RESET")
    public void resetTable() {
        ScriptExecutor.runScriptByName("sql/recursive/recursiveleveltwo.sql");
        DbimplApplication.restart();
    }

    @GetMapping("/entitiesWTypes")
    public List<RLTwoEntitiesWTypesIProjection> getAllEntitesByType() {
        return entityRepo.getAllEntitiesWithType();
    }

    @GetMapping("/entitiesWTypes/typeName/{name}")
    public List<RLTwoEntitiesWTypesIProjection> getEntitiesWTypesByTypeName(@PathVariable(value = "name") String typeName) {
        return entityRepo.getAllEntitiesWithTypeName(typeName);
    }

    @GetMapping("/entitiesWTypes/entityId/{id}")
    public List<RLTwoEntitiesWTypesIProjection> getEntitiesWTypesByEntityId(@PathVariable(value = "id") Integer entityId) {
        return entityRepo.getAllTypesByEntityId(entityId);
    }

    private String getExceptionString(String string, Integer id) {
        return string + " with " + id + " not found";
    }
}
