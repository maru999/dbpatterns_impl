package at.fhv.rupp.ba.dbimpl.classification.leveltwo.mappers.custom;

import at.fhv.rupp.ba.dbimpl.classification.leveltwo.dtos.CLTwoEntitySubtypeTwoDTO;
import at.fhv.rupp.ba.dbimpl.classification.leveltwo.entities.CLTwoEntitySubtypeTwo;

import java.util.LinkedList;
import java.util.List;

public class CLTwoEntitySubtypeTwoMapper {
    private CLTwoEntitySubtypeTwoMapper() {
        //hidden constructor
    }

    public static CLTwoEntitySubtypeTwoDTO toDTO(CLTwoEntitySubtypeTwo entity) {
        CLTwoEntitySubtypeTwoDTO dto = new CLTwoEntitySubtypeTwoDTO();
        dto.setId(entity.getClTwoEntityId());
        dto.setName(entity.getClTwoEntityName());
        if (entity.getClTwoEntityEntityTypeOne() != null) {
            dto.setTypeOne(CLTwoEntityTypeOneMapper.toDTO(entity.getClTwoEntityEntityTypeOne()));
        }
        if (entity.getClTwoEntityEntityTypeTwo() != null) {
            dto.setTypeTwo(CLTwoEntityTypeTwoMapper.toDTO(entity.getClTwoEntityEntityTypeTwo()));
        }
        if (entity.getClTwoEntitySubtypeTwoAttribute() != null) {
            dto.setAttr(entity.getClTwoEntitySubtypeTwoAttribute());
        }
        return dto;
    }

    public static CLTwoEntitySubtypeTwo toEntity(CLTwoEntitySubtypeTwoDTO dto) {
        CLTwoEntitySubtypeTwo entity = new CLTwoEntitySubtypeTwo();
        if (dto.getId() != null) {
            entity.setClTwoEntityId(dto.getId());
        }
        if (dto.getName() != null) {
            entity.setClTwoEntityName(dto.getName());
        }
        if (dto.getTypeOne() != null) {
            entity.setClTwoEntityEntityTypeOne(CLTwoEntityTypeOneMapper.toEntity(dto.getTypeOne()));
        }
        if (dto.getTypeTwo() != null) {
            entity.setClTwoEntityEntityTypeTwo(CLTwoEntityTypeTwoMapper.toEntity(dto.getTypeTwo()));
        }
        if (dto.getAttr() != null) {
            entity.setClTwoEntitySubtypeTwoAttribute(dto.getAttr());
        }
        return entity;
    }

    public static List<CLTwoEntitySubtypeTwoDTO> toDTOs(List<CLTwoEntitySubtypeTwo> entities) {
        LinkedList<CLTwoEntitySubtypeTwoDTO> result = new LinkedList<>();
        for (CLTwoEntitySubtypeTwo entity : entities) {
            result.add(toDTO(entity));
        }
        return result;
    }

    public static List<CLTwoEntitySubtypeTwo> toEntities(List<CLTwoEntitySubtypeTwoDTO> dtos) {
        LinkedList<CLTwoEntitySubtypeTwo> result = new LinkedList<>();
        for (CLTwoEntitySubtypeTwoDTO dto : dtos) {
            result.add(toEntity(dto));
        }
        return result;
    }
}
