package at.fhv.rupp.ba.dbimpl.classification.levelthree.repos;

import at.fhv.rupp.ba.dbimpl.classification.levelthree.entities.CLThreeEntityCategoryClassification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CLThreeEntityCategoryClassificationRepo extends JpaRepository<CLThreeEntityCategoryClassification, Integer> {

    @Query(value = "SELECT * FROM clthree_entity_category_classification WHERE clthree_entity_category_classification_entity = :id", nativeQuery = true)
    List<CLThreeEntityCategoryClassification> getCategoryClassificationsByEntityId(@Param("id") Integer entityId);
}
